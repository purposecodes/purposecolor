//
//  SelectYourFeel.m
//  PurposeColor
//
//  Created by Purpose Code on 16/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kTagForTextField                1
#define kCellMinimumCount               1
#define kHeightForHeader                60
#define kSectionCount                   1
#define kPadding                        60
#define kHeightForFooter                0.1
#define kSuccessCode                    200

#import "SelectYourGoalsAndDreams.h"
#import "Constants.h"
#import "CreateActionInfoViewController.h"
#import "GoalsCustomCell.h"
#import "QuickGoalViewController.h"
#import <AFNetworking/AFNetworking.h>

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"
#import "PrefixHeader.pch"
#import "SelectYourGoalsAndDreams.h"

@interface SelectYourGoalsAndDreams () <UITextFieldDelegate>{
    
    IBOutlet UIButton *btnSkip;
    IBOutlet UITableView *tableView;
    NSMutableArray *arrEmotions;
    BOOL isDataAvailable;
    NSInteger selectedIndex;
    QuickGoalViewController *quickGoalView;
    IBOutlet UIButton *btnAddGoal;
    NSString *strErrorMsg;
    IBOutlet UIView *vwBG;
    IBOutlet UIView *vwLoadingScreen;
}

@end

@implementation SelectYourGoalsAndDreams

-(void)showSelectionPopUp{
    
    [self getAllEmotions];
    [self setUp];
    
}

-(void)setUp{
    
    vwBG.layer.cornerRadius = 5;
    vwBG.layer.borderWidth = 1.f;
    vwBG.layer.borderColor = [UIColor clearColor].CGColor;
    
    isDataAvailable = false;
    btnAddGoal.layer.cornerRadius = 5;
    btnAddGoal.layer.borderWidth = 1.f;
    btnAddGoal.layer.borderColor = [UIColor clearColor].CGColor;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopUp)];
    [tapGesture setNumberOfTapsRequired:1];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
    arrEmotions = [NSMutableArray new];
    selectedIndex = -1;
    btnSkip.layer.cornerRadius = 5;
    btnSkip.layer.borderWidth = 1.f;

}

-(void)getAllEmotions{
    
    [self showLoadingScreen];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    NSString *userToken = [sharedDefaults objectForKey:@"TOKEN"];
     [self getAllEmotionalAwarenessGEMSListsWithToken:userToken gemType:@"goal" goalID:0 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self showAllEmotionsWith:responseObject];
        [self hideLoadingScreen]; 
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isDataAvailable = false;
        [self hideLoadingScreen];
        
    }];
    

}

- (void)getAllEmotionalAwarenessGEMSListsWithToken:(NSString*)token gemType:(NSString*)gemType goalID:(NSInteger)goalID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getallgems",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:gemType forKey:@"gem_type"];
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        strErrorMsg = error.localizedDescription;
        isDataAvailable = false;
        [tableView reloadData];
        failure (operation,error);
    }];
    
}


-(void)showAllEmotionsWith:(NSDictionary*)dictEmotions{
    
    if (NULL_TO_NIL([dictEmotions objectForKey:@"resultarray"]))
        arrEmotions = [NSMutableArray arrayWithArray:[dictEmotions objectForKey:@"resultarray"]];
    if (arrEmotions.count) isDataAvailable = true;
        
    [tableView reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable)return kCellMinimumCount;
    return arrEmotions.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GoalsCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BDCustomCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GoalsCustomCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnQuickView.tag = indexPath.row;
   // cell.imgRadio.image = [UIImage imageNamed:@"Goal_Radio_Inactive.png"];
    cell.btnQuickView.layer.borderColor = [UIColor getThemeColor].CGColor;
    cell.btnQuickView.layer.borderWidth = 1.f;
    cell.btnQuickView.layer.cornerRadius = 5.f;
    cell.lblTitle.textColor =  [UIColor colorWithRed:0.30 green:0.33 blue:0.38 alpha:1.0];;
    if (indexPath.row == selectedIndex) {
        //cell.imgRadio.image = [UIImage imageNamed:@"Goal_Radio_Active.png"];
   }
    if (!isDataAvailable) {
        cell =  (GoalsCustomCell*)[Utility getNoDataCustomCellWith:aTableView withTitle:strErrorMsg];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.userInteractionEnabled = false;
        return cell;
    }
    
    if (indexPath.row < arrEmotions.count) {
        cell.btnQuickView.tag = indexPath.row;
        NSDictionary *details = arrEmotions[indexPath.row];
        if (NULL_TO_NIL([details objectForKey:@"title"])) {
             cell.lblTitle.text = [details objectForKey:@"title"];
        }
        cell.imgRadio.image = [UIImage imageNamed:@"NoImage_Goals_Dreams"];
        if ([[details objectForKey:@"type"] isEqualToString:@"purposecolor"]) {
            cell.imgRadio.image = [UIImage imageNamed:@"Purposecolor_Image"];
           // cell.lblTitle.text = [NSString stringWithFormat:@"eg : %@",[details objectForKey:@"title"]];
        }else{
            if (NULL_TO_NIL([details objectForKey:@"display_image"])) {
                [cell.imgRadio sd_setImageWithURL:[NSURL URLWithString:[details objectForKey:@"display_image"]]
                                 placeholderImage:[UIImage imageNamed:@"Purposecolor_Image"]
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            
                                        }];
            }
            

        }
        
    }
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 60;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (arrEmotions.count <= 0) return;
    selectedIndex = indexPath.row;
    if (arrEmotions.count <= 0) return;
    if (selectedIndex < arrEmotions.count ) {
        
        GoalsCustomCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0]];
        [UIView animateWithDuration:0.3/1.5 animations:^{
            cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.4, 1.4);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
                
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    cell.transform = CGAffineTransformIdentity;
                } completion:^(BOOL finished) {
                    if (finished) {
                        NSDictionary *details = arrEmotions[selectedIndex];
                        NSString *title = [details objectForKey:@"title"];
                        NSInteger emotionID = [[details objectForKey:@"id"] integerValue];
                        _selectedGoalID = [[details objectForKey:@"id"] integerValue];
                        if ([self.delegate respondsToSelector:@selector(goalsAndDreamsSelectedWithTitle:goalId:)])
                            [self.delegate goalsAndDreamsSelectedWithTitle:title goalId:emotionID];
                        [self closePopUp];
                    }
                }];
            }];
        }];
        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return  kHeightForFooter;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return nil;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:tableView])
        return NO;
    return YES;
}
-(IBAction)goalSelected:(UIButton*)sender{
    
    if (arrEmotions.count <= 0) return;
    selectedIndex = sender.tag;
    if (selectedIndex < arrEmotions.count ) {
        
        GoalsCustomCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0]];
        [UIView animateWithDuration:0.3/1.5 animations:^{
            cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.4, 1.4);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
                
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    cell.transform = CGAffineTransformIdentity;
                } completion:^(BOOL finished) {
                    if (finished) {
                        NSDictionary *details = arrEmotions[selectedIndex];
                        NSString *title = [details objectForKey:@"title"];
                        NSInteger emotionID = [[details objectForKey:@"id"] integerValue];
                        _selectedGoalID = [[details objectForKey:@"id"] integerValue];
                        if ([self.delegate respondsToSelector:@selector(goalsAndDreamsSelectedWithTitle:goalId:)])
                            [self.delegate goalsAndDreamsSelectedWithTitle:title goalId:emotionID];
                        [self closePopUp];
                    }
                }];
            }];
        }];
        
    }
}



/*! Event Creation & Delegates !*/
-(IBAction)skip:(id)sender{
    
    [self closePopUp];
    if ([self.delegate respondsToSelector:@selector(skipButtonApplied)])
        [self.delegate skipButtonApplied];
}



-(void)goalsAndDreamsCreatedWithGoalTitle:(NSString*)goalTitle goalID:(NSInteger)goalID{
    
    if ([self.delegate respondsToSelector:@selector(goalsAndDreamsSelectedWithTitle:goalId:)])
        [self.delegate goalsAndDreamsSelectedWithTitle:goalTitle goalId:goalID];
    [self closePopUp];
}

-(IBAction)closePopUp{
    
    if ([self.delegate respondsToSelector:@selector(selectYourGoalsAndDreamsPopUpCloseAppplied)]) {
        [self.delegate selectYourGoalsAndDreamsPopUpCloseAppplied];
    }
}


-(void)showLoadingScreen{
    
    [UIView animateWithDuration:0.4 animations:^(void) {
        vwLoadingScreen.alpha = 1;
    }];
    
    
}
-(void)hideLoadingScreen{
    [UIView animateWithDuration:0.4 animations:^(void) {
        vwLoadingScreen.alpha = 0;
    }];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
