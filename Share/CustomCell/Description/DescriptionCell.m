//
//  ActionMediaComposeCell.m
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "DescriptionCell.h"
#import "PrefixHeader.pch"

@implementation DescriptionCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [_vwBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwBg.layer setBorderWidth:2.f];
    _vwBg.layer.cornerRadius = 3.f;
    
    // drop shadow
    [_vwBg.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwBg.layer setShadowOpacity:0.3];
    [_vwBg.layer setShadowRadius:2.0];
    [_vwBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
