//
//  ActionMediaComposeCell.h
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIView *vwDate;
@property (nonatomic,weak) IBOutlet UIView *vwStatus;

@property (nonatomic,weak) IBOutlet UILabel *lblDate;
@property (nonatomic,weak) IBOutlet UILabel *lblStatus;

@property (nonatomic,weak) IBOutlet UIImageView *imagee;
@property (nonatomic,weak) IBOutlet UITextField *txtCategory;

@end
