//
//  ActionMediaComposeCell.m
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "DateCell.h"
#import "PrefixHeader.pch"

@implementation DateCell

- (void)awakeFromNib {
    // Initialization code
    [self setUp];
}


-(void)setUp{
    
    
    
    [_vwDate.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwDate.layer setBorderWidth:2.f];
    _vwDate.layer.cornerRadius = 3.f;
    
    // drop shadow
    [_vwDate.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwDate.layer setShadowOpacity:0.3];
    [_vwDate.layer setShadowRadius:2.0];
    [_vwDate.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    [_vwStatus.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwStatus.layer setBorderWidth:2.f];
    _vwStatus.layer.cornerRadius = 3.f;
    
    // drop shadow
    [_vwStatus.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwStatus.layer setShadowOpacity:0.3];
    [_vwStatus.layer setShadowRadius:2.0];
    [_vwStatus.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
