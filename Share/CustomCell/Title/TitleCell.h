//
//  ActionMediaComposeCell.h
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UITextField *txtTitle;
@property (nonatomic,weak) IBOutlet UIView *vwBg;

@end
