//
//  CreateActionInfoViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kUnAuthorized           403
#define kMaintenanceMode        503
#define kUpdateMode             505

typedef enum{
    
    eActionTypeGoalsAndDreams = 0,
    eActionTypeCommunity = 1,
    
}ActionType;


typedef enum{
    
    eTitle = 0,
    eDescription = 1,
    eGoalDescription = 2,
    
} eCellType;

typedef enum{
    
    eSectionOne = 0,
    eSectionTwo = 1,
    
} eSectionType;


#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <Photos/Photos.h>

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "APIMapper.h"
#import "Constants.h"
#import "PrefixHeader.pch"
#import "FTPopOverMenu.h"
#import "CustomeImagePicker.h"
#import "MTDURLPreview.h"
#import "SDAVAssetExportSession.h"
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
#import "ContactsPickerViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "SelectYourGoalsAndDreams.h"

#import "ShareViewController.h"
#import "TitleCell.h"
#import "DateCell.h"
#import "DescriptionCell.h"
#import "ActionMediaListCell.h"
#import "PhotoBrowser.h"
#import "AudioManagerView.h"
#import "CustomAudioPlayerView.h"
#import "GoalCategoryViewController.h"

#define kSectionCount                   2
#define kSuccessCode                    200
#define kMinimumCellCount               2
#define kMinimumCellCountForGoal        3
#define kHeaderHeight                   40
#define kCellHeightForDescription       205
#define kCellHeightForTtile             50
#define kCellHeightForDate              60
#define kEmptyHeaderAndFooter           0
#define kDefaultCellHeight              250
#define kMaxDescriptionLength           500
#define kHeightForFooter                0.1

static NSString *const AppGroupId = @"group.com.purposecodes.purposecolor";

@import GoogleMaps;
@import GooglePlacePicker;

@interface ShareViewController () <GMSAutocompleteViewControllerDelegate,ActionInfoCellDelegate,ContactPickerDelegate,PhotoBrowserDelegate,CustomeImagePickerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CustomAudioPlayerDelegate,UIGestureRecognizerDelegate,SelectYourGoalsAndDreamsDelegate,GoalCategoryDelegte>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnSubmit;
    
    IBOutlet UILabel *lblMenuTitle;
    IBOutlet UIView *vwMenuBG;
    
    IBOutlet UIView *vwPickAGoalBG;
    IBOutlet NSLayoutConstraint *heightForPickAGoal;
    IBOutlet NSLayoutConstraint *topForPickAGoal;
    
    UIView *inputAccView;
    
    NSString *strTitle;
    NSMutableString *strDescription;
    NSString *strPreviewURL;
    
    NSString *strAchievementDate;
    NSString *strStatus;
    NSMutableArray *arrDataSource;

    IBOutlet UIView *vwPickerOverLay;
    IBOutlet UIDatePicker *datePicker;
    
    CLLocationCoordinate2D locationCordinates;
    NSString *strLocationAddress;
    NSString *strLocationName;
    NSMutableString *strContactName;
    
    NSUserDefaults *sharedUserDefaults;
    
    IBOutlet UISegmentedControl *segmentControl;

    NSDataDetector *detector;
    BOOL showPreview;
    NSURL *alreadyPreviewdURL;
    UITextView *txtViewDescription;
    PhotoBrowser *photoBrowser;
    AudioManagerView *vwRecordPopOver;
    CustomAudioPlayerView *vwAudioPlayer;
    SelectYourGoalsAndDreams *vwGoalsSelection;
    NSMutableArray *arrDeletedIDs;
    NSString *txtToBeShared;
    
    NSString *strGemID; // When edit a Goal from list
    
    ActionType _actionType;
    CGPoint viewStartLocation;
    GoalCategory *goalCategory;
    
    //Preview
    
    IBOutlet UIView *vwURLPreview;
    IBOutlet UILabel *lblPreviewTitle;
    IBOutlet UILabel *lblPreviewDescription;
    IBOutlet  UILabel *lblPreviewDomain;
    IBOutlet UIImageView *imgPreview;
    IBOutlet UIActivityIndicatorView *previewIndicator;
    IBOutlet UIButton *btnShowPreviewURL;
    IBOutlet NSLayoutConstraint *bottomForPreview;
    IBOutlet UIView *vwLoadingScreen;
}


@property (nonatomic,strong) NSIndexPath *draggingCellIndexPath;
@property (nonatomic,strong) UIView *cellSnapshotView;
    
@end



@implementation ShareViewController

    


- (void)viewDidLoad {
    
    [self showLoadingScreen];
    [self removeAllContentsInMediaFolder];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:CommonFont size:13], NSFontAttributeName,
                                [UIColor getThemeColor], NSForegroundColorAttributeName, nil];
    [segmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                  [UIFont fontWithName:CommonFont size:13], NSFontAttributeName,
                  [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [segmentControl setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
    strContactName = [NSMutableString new];
   // strLocationName = [NSMutableString new];
    strDescription = [NSMutableString new];
    showPreview = true;
    
    btnSubmit.layer.cornerRadius = 5.f;
    btnSubmit.layer.borderWidth = 1.f;
    btnSubmit.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [vwMenuBG.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwMenuBG.layer setBorderWidth:2.f];
    vwMenuBG.layer.cornerRadius = 3.f;
    [vwMenuBG.layer setShadowColor:[UIColor blackColor].CGColor];
    [vwMenuBG.layer setShadowOpacity:0.3];
    [vwMenuBG.layer setShadowRadius:2.0];
    [vwMenuBG.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    [vwPickAGoalBG.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwPickAGoalBG.layer setBorderWidth:2.f];
    vwPickAGoalBG.layer.cornerRadius = 3.f;
    [vwPickAGoalBG.layer setShadowColor:[UIColor blackColor].CGColor];
    [vwPickAGoalBG.layer setShadowOpacity:0.3];
    [vwPickAGoalBG.layer setShadowRadius:2.0];
    [vwPickAGoalBG.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    float top = 5;
    float height = 0;
    topForPickAGoal.constant = top;
    heightForPickAGoal.constant = height;
    vwPickAGoalBG.hidden = true;
    
    
    arrDataSource = [NSMutableArray new];
    arrDeletedIDs = [NSMutableArray new];
    [self setUpGoogleMapConfiguration];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    
    vwPickerOverLay.hidden = true;
    _actionType = eActionTypeCommunity;
    
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(dragCell:)];
    [longPressGestureRecognizer setDelegate:self];
    [tableView addGestureRecognizer:longPressGestureRecognizer];
    
    sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupId];
    [self didSelectPost];
    
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    BaseURLString = [sharedDefaults objectForKey:@"BaseURL"];
    AppVersion = [sharedDefaults objectForKey:@"AppVersion"];
    
    if ([sharedDefaults objectForKey:@"TOKEN"]) {
        
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"LOGIN"
                                                                       message:@"Please login to the App to continue"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  [self goBack:nil];
                                                              }];
        
        [alert addAction:firstAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }


    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didSelectPost
{
    
    NSExtensionItem *inputItem = self.extensionContext.inputItems.firstObject;
    //NSLog(@"info %@",inputItem.userInfo);
    NSArray *arrAttachments = [inputItem.userInfo valueForKey:NSExtensionItemAttachmentsKey];
    
    for ( NSItemProvider *itemProvider in arrAttachments) {
        
        if([itemProvider hasItemConformingToTypeIdentifier:@"public.image"]) {
            
            [itemProvider loadItemForTypeIdentifier:@"public.image" options:nil completionHandler: ^(id<NSSecureCoding> item, NSError *error) {
                
                NSData *imgData;
                if([(NSObject*)item isKindOfClass:[NSURL class]]) {
                    imgData = [NSData dataWithContentsOfURL:(NSURL*)item];
                }
                if([(NSObject*)item isKindOfClass:[UIImage class]]) {
                    imgData = UIImagePNGRepresentation((UIImage*)item);
                }
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                    UIImage *im = [UIImage imageWithData: imgData];
                    [self saveSelectedImageFileToFolderWithImage:im shouldReload:YES compression:0.5f];
                    
                });
               
            }];
        }
        
        else if ([itemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypePlainText]) {
            
            [itemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypePlainText options:nil completionHandler:^(NSString *plain, NSError *error) {
                
                if (![self checkIfURLisPresentOn:plain]) {
                     txtToBeShared = plain;
                    [strDescription appendString:plain];
                }else{
                    strPreviewURL = plain;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tableView reloadData];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self hideLoadingScreen];
         
                    });
                    
                });
            }];
        }
        
        else if ([itemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypeURL]){
            
            [itemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypeURL options:nil completionHandler:^(NSURL *url, NSError *error)
             {
                 //[arrDataSource addObject:url];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     if (url) {
                         [self writeMediaToFolderWithFromPath:url];
                     }else{
                         [self hideLoadingScreen];
                     }
      
                 });
             }];
            
        }else if ([itemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypeImage]){
            
            [itemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypeImage
                                            options:nil
                                  completionHandler:^(NSURL *url, NSError *error) {
                                      if (url) {
                                          [self writeMediaToFolderWithFromPath:url];
                                      }else{
                                          [self hideLoadingScreen];
                                      }
                                     
                                      
                                 
                                  }];
        }
        else if ([itemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypeVideo]){
            
            [itemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypeVideo
                                            options:nil
                                  completionHandler:^(NSURL *url, NSError *error) {
                                      
                                      if (url) {
                                         [self writeMediaToFolderWithFromPath:url];
                                      }else{
                                          [self hideLoadingScreen];
                                      }
                                      
                                      
                                      
                                  }];
        }
        else if ([itemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypeQuickTimeMovie]){
            
            [itemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypeQuickTimeMovie
                                            options:nil
                                  completionHandler:^(NSURL *url, NSError *error) {
                                      
                                      if (url) {
                                           [self writeMediaToFolderWithFromPath:url];
                                      }else{
                                          [self hideLoadingScreen];
                                      }
       
                                  }];
        }
       
    }
    
}


-(void)setUpGoogleMapConfiguration{
    
    [GMSServices provideAPIKey:GoogleMapAPIKey];
    [GMSPlacesClient provideAPIKey:GoogleMapAPIKey];
}



-(IBAction)changeMenu:(id)sender{
    
    UIAlertController * alert=  [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* networkPost = [UIAlertAction actionWithTitle:@"Post to self-help networking" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
      
        [self updateGoalMenuOptionShouldShow:NO topValue:5];
        [self configureValuesIsCreateGoal:NO];
        lblMenuTitle.text = @"Post to self-help networking";
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction* create = [UIAlertAction actionWithTitle:@"Create new goal" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        [self updateGoalMenuOptionShouldShow:NO topValue:0];
        [self configureValuesIsCreateGoal:YES];
        lblMenuTitle.text = @"Create new goal";
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction* addToExsiting = [UIAlertAction actionWithTitle:@"Add to existing goal" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        [self updateGoalMenuOptionShouldShow:YES topValue:10];
          [self configureValuesIsCreateGoal:YES];
        lblMenuTitle.text = @"Add to existing goal";
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
   
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                             {
                             }];
    
    [alert addAction:networkPost];
    [alert addAction:create];
    [alert addAction:addToExsiting];
    
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)configureValuesIsCreateGoal:(BOOL)isCreateGoal{
    
    [self clearSelections];
    _actionType = eActionTypeCommunity;
    [btnSubmit setTitle:@"POST" forState:UIControlStateNormal];
    if (isCreateGoal) {
        _actionType = eActionTypeGoalsAndDreams;
        [btnSubmit setTitle:@"SAVE" forState:UIControlStateNormal];
    }
    [tableView reloadData];
}

-(void)updateGoalMenuOptionShouldShow:(BOOL)shouldShow topValue:(float)topValue{
    
    if (shouldShow) {
        vwPickAGoalBG.hidden = false;
    }
    float top = topValue;
    float height = 0;
    if (shouldShow) {
         height = 45;
    }
    [self.view layoutIfNeeded];
    topForPickAGoal.constant = top;
    heightForPickAGoal.constant = height;
    [UIView animateWithDuration:.5
                     animations:^{
                         [self.view layoutIfNeeded];
                         topForPickAGoal.constant = top;
                         heightForPickAGoal.constant = height;
                         // Called on parent view
                     }completion:^(BOOL finished) {
                         if (!shouldShow) {
                             vwPickAGoalBG.hidden = true;
                         }
                     }];
}



-(void)clearSelections{
    
    strGemID = @"";
    [arrDeletedIDs removeAllObjects];
    [strDescription setString:@""];
    if (txtToBeShared) {
         [strDescription setString:txtToBeShared];
    }
    strTitle = @"";
    strStatus = @"Active";
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"yyyy-MM-dd"];
    strAchievementDate = [dateformater stringFromDate:[NSDate date]];
    
    strLocationAddress = @"";
    strLocationName = @"";
    [strContactName setString:@""];
    if (arrDataSource.count) {
        NSMutableArray *discardedItems = [NSMutableArray array];
        for (id object in arrDataSource) {
            if ([object isKindOfClass:[NSDictionary class]] ) {
                // Editing medias
                [discardedItems addObject:object];
            }
            
        }
        if (discardedItems.count) {
            [arrDataSource removeObjectsInArray:discardedItems];
        }
        
    }

    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (arrDataSource.count) {
        return 3;
    }
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        if ( _actionType == eActionTypeCommunity) {
            return 0;
        }
        return 3;
    }else if (section == 1) {
        return 1;
    }
    else if (section == 2) {
        return arrDataSource.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            TitleCell *titleCell = [self configureTitleCellForIndexPath:indexPath];
            titleCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return titleCell;
        }
        if (indexPath.row == 1) {
            DateCell *titleCell = [self configureDateInfoCellForIndexPath:indexPath];
            titleCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return titleCell;
        }
        if (indexPath.row == 2) {
            static NSString *CellIdentifier = @"CustomCellForCategory";
            DateCell *cell = (DateCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell.txtCategory.text = goalCategory.strCatName;
            cell.txtCategory.placeholder = @"Pick a category";
            return cell;
        }
        
    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            DescriptionCell *titleCell = [self configureDesriptionCellForIndexPath:indexPath];
           // [titleCell.previewIndicator stopAnimating];
            titleCell.vwURLPreview.hidden = false;
            titleCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return titleCell;
        }
    }
    else if (indexPath.section == 2){
        ActionMediaListCell *titleCell = [self configureMediaCelltAtPath:indexPath];
        titleCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return titleCell;
    }
  
    return cell;
}

-(CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 60;
        }
        if (indexPath.row == 1) {
            return 50;
        }
        if (indexPath.row == 2) {
            return 60;
        }
    }else if (indexPath.section == 1){
        float height = 200;
        if (strPreviewURL.length) {
            if ( [self checkIfURLisPresentOn:strPreviewURL]) {
                height += 205;
            }
        }else if(strDescription.length){
            if ( [self checkIfURLisPresentOn:strDescription]) {
                 height += 100;
            }
        }
        return height;
    }
    
    CGFloat cellHeight = kDefaultCellHeight;
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray *images = [NSMutableArray new];
    NSString *mediaType ;
    if (indexPath.section == 2){
        if (indexPath.row < arrDataSource.count) {
            
            id object = arrDataSource[indexPath.row];
            if ([object isKindOfClass:[NSDictionary class]]) {
                NSDictionary *mediaInfo = arrDataSource[indexPath.row];
                mediaType = [mediaInfo objectForKey:@"media_type"];
                if ([mediaType isEqualToString:@"image"]) {
                    NSURL *url =  [NSURL URLWithString:[mediaInfo objectForKey:@"gem_media"]];
                    [images addObject:url];
                }
            }else{
                NSURL *fileURL = [NSURL fileURLWithPath:arrDataSource[indexPath.row]];
                NSString *fileExtension = [fileURL pathExtension];
                NSString *UTI = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)fileExtension, NULL);
                NSString *contentType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)UTI, kUTTagClassMIMEType);
                if ([contentType hasPrefix:@"image"]){
                    [images addObject:fileURL];
                }
            }
   
        }
        if (images.count) {
            for (id details in arrDataSource) {
                if ([details isKindOfClass:[NSDictionary class]]) {
                    mediaType = [details objectForKey:@"media_type"];
                    if ([mediaType isEqualToString:@"image"]) {
                        NSURL *url =  [NSURL URLWithString:[details objectForKey:@"gem_media"]];
                        if (![images containsObject:url]) {
                            [images addObject:url];
                        }
                    }
                }else{
                    NSString *filePath = (NSString*)details;
                    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
                    NSString *fileExtension = [fileURL pathExtension];
                    NSString *UTI = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)fileExtension, NULL);
                    NSString *contentType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)UTI, kUTTagClassMIMEType);
                    if ([contentType hasPrefix:@"image"]){
                        if (![images containsObject:fileURL]) {
                            [images addObject:fileURL];
                        }
                    }
                }
                
            }
        }
    }
  
    if (images.count) {
        [self presentGalleryWithImages:images];
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kEmptyHeaderAndFooter;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return nil;
}

-(TitleCell*)configureTitleCellForIndexPath:(NSIndexPath *)indexPath{
    
    TitleCell *cell = (TitleCell*)[tableView dequeueReusableCellWithIdentifier:@"TitleCell"];
    cell.txtTitle.placeholder = @"Title";
    if (_actionType == eActionTypeGoalsAndDreams) cell.txtTitle.placeholder = @"Goal Name";
    cell.txtTitle.tag = 0;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    cell.txtTitle.leftView = paddingView;
    cell.txtTitle.leftViewMode = UITextFieldViewModeAlways;
    if (strTitle) {
        cell.txtTitle.text = strTitle;
    }
    return cell;
}

-(DateCell*)configureDateInfoCellForIndexPath:(NSIndexPath *)indexPath{
    
    DateCell *cell = (DateCell*)[tableView dequeueReusableCellWithIdentifier:@"DateCell"];
    cell.lblDate.text = @"Achievement Date";
    if (strAchievementDate) {
        cell.lblDate.text = strAchievementDate;
    }
    cell.lblStatus.text = @"Active";
    if (strStatus) {
        cell.lblStatus.text = strStatus;
    }
    
    return cell;
}

-(DescriptionCell*)configureDesriptionCellForIndexPath:(NSIndexPath *)indexPath{
    
    DescriptionCell *cell = (DescriptionCell*)[tableView dequeueReusableCellWithIdentifier:@"DescriptionCell"];
    [self setUpFooterMenuWithView:cell.vwFooter];
    cell.txtDecsription.tag = 1;
    cell.txtDecsription.text = strDescription;
    cell.heightForPreview.constant = 0;
    cell.vwURLPreview.hidden = true;
    if (strPreviewURL.length) {
        [self managePreviewWithText:strPreviewURL withCell:cell];
        cell.heightForPreview.constant = 205;
        cell.vwURLPreview.hidden = false;
    }
    cell.txtDecsription.text = strDescription;
    txtViewDescription = cell.txtDecsription;
    cell.lblLocInfo.text = @"";
    cell.lblContactInfo.text = @"";
    if (strLocationName.length > 0){
        
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"- @ : %@",strLocationName]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor] range:NSMakeRange(0,6)];
        cell.lblLocInfo.attributedText = string;
    }
    
    if (strContactName.length > 0){
        
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"- With : %@",strContactName]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor] range:NSMakeRange(0,9)];
        cell.lblContactInfo.attributedText = string;
    }
    
    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resetContactInfo)];
    gesture1.numberOfTapsRequired = 1;
    cell.lblContactInfo.userInteractionEnabled = true;
    [cell.lblContactInfo addGestureRecognizer:gesture1];
    
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resetLocationInfo)];
    gesture2.numberOfTapsRequired = 1;
    cell.lblLocInfo.userInteractionEnabled = true;
    [cell.lblLocInfo addGestureRecognizer:gesture2];
    

    return cell;
}

-(ActionMediaListCell*)configureMediaCelltAtPath:(NSIndexPath*)indexPath{
    
    static NSString *CellIdentifier = @"MediaList";
    ActionMediaListCell *cell = (ActionMediaListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [[cell btnVideoPlay]setHidden:true];
    [[cell btnAudioPlay]setHidden:true];
    cell.delegate = self;
    [cell.indicator stopAnimating];
    [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];
    
    id object = arrDataSource[indexPath.row];
    NSDictionary *mediaInfo;
    if ([object isKindOfClass:[NSString class]]) {
        
        if (indexPath.row < arrDataSource.count) {
            NSURL *fileURL = [NSURL fileURLWithPath:arrDataSource[indexPath.row]];
            
            if ([[fileURL pathExtension] isEqualToString:@"jpeg"]) {
                cell.lblTitle.text = [fileURL lastPathComponent];
                [cell.indicator startAnimating];
                [cell.imgMediaThumbnail sd_setImageWithURL:fileURL
                                          placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                     [cell.indicator stopAnimating];
                                                 }];
                
            }
            else if ([[fileURL pathExtension] isEqualToString:@"mp4"]) {
                [[cell btnVideoPlay]setHidden:false];
                cell.lblTitle.text = [fileURL lastPathComponent];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                    UIImage *thumbnail = [self getThumbNailFromVideoURL:fileURL];
                    dispatch_sync(dispatch_get_main_queue(), ^(void) {
                        cell.imgMediaThumbnail.image = thumbnail;
                    });
                    
                });
                
                
                
            }
            else {
                [[cell btnAudioPlay]setHidden:false];
                cell.lblTitle.text = [fileURL lastPathComponent];
                cell.imgMediaThumbnail.image = [UIImage imageNamed:@"NoImage_Goals_Dreams"];
                
            }
            
        }
    }else{
        
        mediaInfo = (NSDictionary*)arrDataSource[indexPath.row];
        cell.imgMediaThumbnail.backgroundColor = [UIColor clearColor];
        NSString *fileName;
        NSString *mediaType;
        if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
            fileName = [mediaInfo objectForKey:@"gem_media"];
            cell.lblTitle.text = [fileName lastPathComponent];
        }
        if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
            mediaType = [mediaInfo objectForKey:@"media_type"];
        }
        if ([mediaType isEqualToString:@"video"]) {
            [[cell btnVideoPlay]setHidden:false];
            if (NULL_TO_NIL([mediaInfo objectForKey:@"video_thumb"])) {
                [cell.indicator startAnimating];
                NSString *thumb = [mediaInfo objectForKey:@"video_thumb"];
                [cell.imgMediaThumbnail sd_setImageWithURL:[NSURL URLWithString:thumb]
                                          placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                     [cell.indicator stopAnimating];
                                                 }];
            }
            //This is Image File with .png Extension , Photos.
        }
        else if ([mediaType isEqualToString:@"audio"]) {
            [cell.indicator startAnimating];
            [[cell btnAudioPlay]setHidden:false];
            cell.imgMediaThumbnail.image = [UIImage imageNamed:@"NoImage_Goals_Dreams"];
            [cell.indicator stopAnimating];
            
        }
        else if ([mediaType isEqualToString:@"image"]){
            
            if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                [cell.indicator startAnimating];
                NSString *imageURL = [mediaInfo objectForKey:@"gem_media"];
                [cell.imgMediaThumbnail sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                          placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                     [cell.indicator stopAnimating];
                                                 }];
            }
            
        }
        

    }
   
    cell.btnDelete.tag = indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(deleteSelectedMedia:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}



-(void)setUpFooterMenuWithView:(UIView*)vwFooter{
    
    if (vwFooter) {
        vwFooter.backgroundColor = [UIColor clearColor];

        UIButton *btnGallery = [UIButton buttonWithType:UIButtonTypeCustom];
        btnGallery.translatesAutoresizingMaskIntoConstraints = NO;
        [btnGallery setImage:[UIImage imageNamed:@"Media_Gallery.png"] forState:UIControlStateNormal];
        [btnGallery addTarget:self action:@selector(showGallery:) forControlEvents:UIControlEventTouchUpInside];
        [vwFooter addSubview:btnGallery];
        btnGallery.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        UIButton *btnLoc = [UIButton buttonWithType:UIButtonTypeCustom];
        btnLoc.translatesAutoresizingMaskIntoConstraints = NO;
        [btnLoc setImage:[UIImage imageNamed:@"Media_Location.png"] forState:UIControlStateNormal];
        [btnLoc addTarget:self action:@selector(showLocation:) forControlEvents:UIControlEventTouchUpInside];
        [vwFooter addSubview:btnLoc];
        btnLoc.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        
        UIButton *btnContact = [UIButton buttonWithType:UIButtonTypeCustom];
        btnContact.translatesAutoresizingMaskIntoConstraints = NO;
        [btnContact setImage:[UIImage imageNamed:@"Media_Contact.png"] forState:UIControlStateNormal];
        [btnContact addTarget:self action:@selector(showContacts:) forControlEvents:UIControlEventTouchUpInside];
        [vwFooter addSubview:btnContact];
        btnContact.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
       
        
        [btnGallery addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                              multiplier:1.0
                                                                constant:40]];
        
     
        
        [btnLoc addConstraint:[NSLayoutConstraint constraintWithItem:btnLoc
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:40]];
        
        [btnContact addConstraint:[NSLayoutConstraint constraintWithItem:btnContact
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                              multiplier:1.0
                                                                constant:40]];
        
        [vwFooter addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:vwFooter attribute:NSLayoutAttributeBottom multiplier:1 constant:-5]];
        NSDictionary *views = NSDictionaryOfVariableBindings(btnGallery,btnLoc, btnContact);
        [vwFooter addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[btnGallery][btnLoc(==btnGallery)][btnContact(==btnGallery)]|" options:NSLayoutFormatAlignAllBottom metrics:nil views:views]];

        
        
    }
    
    
}

#pragma mark - UITextfield delegate methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        TitleCell *cell = (TitleCell*)textField.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        [self getTextFromField:cell.txtTitle.tag data:textField.text];
    }
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    
    [tableView setContentOffset:contentOffset animated:YES];
    NSIndexPath *indexPath;
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    }
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    //[self createInputAccessoryView];
    //[textField setInputAccessoryView:inputAccView];
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    [tableView setContentOffset:contentOffset animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return true;
}



#pragma mark - UITextView delegate methods


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    
    [self createInputAccessoryView];
    [textView setInputAccessoryView:inputAccView];
    CGPoint pointInTable = [textView.superview.superview convertPoint:textView.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    NSIndexPath *indexPath;
    if ([textView.superview.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
        indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    }
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint pointInTable = [textView.superview.superview convertPoint:textView.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    if ([textView.superview.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
    if ([textView.superview.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        DescriptionCell *cell = (DescriptionCell*)textView.superview.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        [self getTextFromField:cell.txtDecsription.tag data:textView.text];
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (strPreviewURL.length <= 0 && textView.text.length > 0) {
        [self managePreviewForUserTypedText:textView.text];
        
    }
    
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
    }
    else if([[textView text] length] >= kMaxDescriptionLength)
    {
        return NO;
    }
    return YES;
}

-(void)textViewDidChange:(UITextView*)txtView{
    
    [self decorateTags:txtView];
}

-(void)managePreviewForUserTypedText:(NSString*)strText{
    
    if (!detector) {
        NSError *error = nil;
        detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                   error:&error];
    }
    if (strText) {
        
        NSArray *matches = [detector matchesInString:strText
                                             options:0
                                               range:NSMakeRange(0, [strText length])];
        if (matches.count > 0) {
            NSTextCheckingResult *match = [matches firstObject];
            if (![self isEquivalentURLOne:alreadyPreviewdURL URLTwo:[match URL]])showPreview = true;
            // else showPreview = false;
            if (showPreview) {
                [previewIndicator startAnimating];
                [MTDURLPreview loadPreviewWithURL:[match URL] completion:^(MTDURLPreview *preview, NSError *error) {
                    if (error) {
                        vwURLPreview.hidden = true;
                        [previewIndicator stopAnimating];
                        lblPreviewTitle.text = @"";
                        lblPreviewDescription.text = @"";
                        lblPreviewTitle.text = @"";
                        lblPreviewDomain.text = @"";
                        imgPreview.image = nil;
                        
                    }else{
                        
                        alreadyPreviewdURL = [match URL];
                        vwURLPreview.hidden = false;
                        [previewIndicator stopAnimating];
                        lblPreviewTitle.text = preview.title;
                        lblPreviewDescription.text = preview.content;
                        lblPreviewTitle.text = preview.title;
                        lblPreviewDomain.text = preview.domain;
                        [imgPreview sd_setImageWithURL:preview.imageURL
                                           placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  }];
                    }
                    
                    
                }];
                
            }
            
        }else{
            
            vwURLPreview.hidden = true;
            [previewIndicator stopAnimating];
            lblPreviewTitle.text = @"";
            lblPreviewDescription.text = @"";
            lblPreviewDomain.text = @"";
            imgPreview.image = nil;
        }
        
    }

}

-(void)decorateTags:(UITextView *)txtView{
    
    NSString *stringWithTags = txtView.text;
    
    NSError *error = nil;
    
    //For "Vijay #Apple Dev"
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:stringWithTags options:0 range:NSMakeRange(0, stringWithTags.length)];
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:stringWithTags];
    UIFont *font=[UIFont fontWithName:CommonFont_New size:14.0f];
    [attString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, stringWithTags.length)];
    
    for (NSTextCheckingResult *match in matches) {
        
        NSRange wordRange = [match rangeAtIndex:0];
        
        //Set Background Color
        UIColor *backgroundColor = [UIColor colorWithRed:0.90 green:0.93 blue:1.00 alpha:1.0];;
        [attString addAttribute:NSBackgroundColorAttributeName value:backgroundColor range:wordRange];
        
        //Set Foreground Color
        UIColor *foregroundColor = [UIColor blackColor];
        [attString addAttribute:NSForegroundColorAttributeName value:foregroundColor range:wordRange];
        
    }
    
    txtView.attributedText = attString;
}
-(void)getTextFromField:(NSInteger)type data:(NSString*)string{
    
    switch (type) {
        case 0:
            strTitle = string;
            break;
        case 1:
            [strDescription setString:string];
            break;
        default:
            break;
    }
    
    
}

-(void)createInputAccessoryView{
    
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 45.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 1];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 85, 5.0f, 80.0f, 35.0f)];
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor getThemeColor]];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    btnDone.layer.cornerRadius = 5.f;
    btnDone.layer.borderWidth = 1.f;
    btnDone.layer.borderColor = [UIColor whiteColor].CGColor;
    
    btnDone.titleLabel.font = [UIFont fontWithName:CommonFont_New size:14];
    [inputAccView addSubview:btnDone];
}


#pragma mark - IBActions



-(IBAction)showDatePicker:(id)sender{
    
    [self.view endEditing:YES];
    vwPickerOverLay.hidden = false;
    datePicker.date = [NSDate date];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"yyyy-MM-dd"];
    strAchievementDate = [dateformater stringFromDate:datePicker.date];
}

-(IBAction)getSelectedDate{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"yyyy-MM-dd"];
    strAchievementDate = [dateformater stringFromDate:datePicker.date];
}

-(IBAction)hidePicker:(id)sender{
    
    vwPickerOverLay.hidden = true;
    NSRange range = NSMakeRange(0, 1);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"yyyy-MM-dd"];
    strAchievementDate = [dateformater stringFromDate:datePicker.date];
}



-(IBAction)showStatusPicker:(id)sender{
    
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Goal Status" message:@"Change Goal status" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Active" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        strStatus = @"Active";
        NSRange range = NSMakeRange(0, 1);
        NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
        [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
        [actionSheet dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Completed" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
         strStatus = @"Completed";
        NSRange range = NSMakeRange(0, 1);
        NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
        [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
        [actionSheet dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
 
}

#pragma mark - MENU actions


-(IBAction)resetContactInfo{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete"
                                                                   message:@"Delete Contact Info?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"DELETE"
                                                          style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                              [strContactName setString:@""];
                                                              NSRange range = NSMakeRange(1, 1);
                                                              NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
                                                              [tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
                                                          }];
    UIAlertAction *second = [UIAlertAction actionWithTitle:@"CANCEL"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                     }];
    
    [alert addAction:firstAction];
    [alert addAction:second];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(IBAction)resetLocationInfo{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete"
                                                                   message:@"Delete Location Info?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"DELETE"
                                                          style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                              strLocationName = @"";
                                                              NSRange range = NSMakeRange(1, 1);
                                                              NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
                                                              [tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
                                                          }];
    
    UIAlertAction *second = [UIAlertAction actionWithTitle:@"CANCEL"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         
                                                     }];
    
    [alert addAction:firstAction];
    [alert addAction:second];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


-(IBAction)showGallery:(id)sender{
    
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UPLOAD" message:@"Upload Media" preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Image" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       
        [self showPhotoGallery:YES];
        [actionSheet dismissViewControllerAnimated:YES completion:^{
            
            
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       
        [self showPhotoGallery:NO];
        [actionSheet dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [actionSheet dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
   
}



#pragma mark - Record And Play Audio actions

-(IBAction)recordAudio:(UILongPressGestureRecognizer*)gesture{
    if (gesture && gesture.view)
        [self recordMediaByView:gesture];
}

-(void)recordMediaByView:(UILongPressGestureRecognizer*)gesture{
    
    if (gesture.view) {
        
        if (gesture.state == UIGestureRecognizerStateBegan) {
            
            if (!vwRecordPopOver) {
                
                float xPadding = 10;
                float yPadding = 70;
                
                CGPoint point = [gesture.view.superview convertPoint:gesture.view.center toView:self.view];
                CGRect rect = CGRectMake(point.x - xPadding, point.y - yPadding, 100, 40);
                vwRecordPopOver = [AudioManagerView new];
                vwRecordPopOver.isExtension = true;
                vwRecordPopOver.frame = rect;
                [self.view addSubview:vwRecordPopOver];
                [vwRecordPopOver setUp];
                [vwRecordPopOver startRecording];
                
            }
        }
        
        if ((gesture.state == UIGestureRecognizerStateCancelled) || (gesture.state == UIGestureRecognizerStateFailed) || (gesture.state ==UIGestureRecognizerStateEnded)) {
            
            [vwRecordPopOver stopRecording];
            [vwRecordPopOver removeFromSuperview];
            vwRecordPopOver = nil;
            [self loadAllMedias];
            
        }
        
    }
    
}


-(void)showToast{
    
    [self.view endEditing:YES];
    [ALToastView toastInView:self.view withText:@"Hold to record , Release to save."];
}

-(void)showAudioPlayerWithURL:(NSURL*)url{
    
    if (!vwAudioPlayer) {
        vwAudioPlayer = [[[NSBundle mainBundle] loadNibNamed:@"CustomAudioPlayerView" owner:self options:nil] objectAtIndex:0];
        vwAudioPlayer.delegate = self;
    }
    
    UIView *vwPopUP = vwAudioPlayer;
    [self.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer setupAVPlayerForURL:url];
    }];
    
}

-(void)closeAudioPlayerView{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwAudioPlayer.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer removeFromSuperview];
        vwAudioPlayer = nil;
        NSError* error;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        [[AVAudioSession sharedInstance] setActive:NO error:&error];
    }];
    
    
    
}



-(IBAction)showCamera:(id)sender{
    
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"CAPTURE" message:@"Record Media" preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Image" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera ]) {
            
            UIImagePickerController *picker= [[UIImagePickerController alloc] init];
            [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
            picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
            [picker setDelegate:self];
            [self presentViewController:picker animated:YES completion:nil];
        }
        
        [actionSheet dismissViewControllerAnimated:YES completion:^{
            
            
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker= [[UIImagePickerController alloc] init];
        [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [picker setDelegate:self];
        picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie,nil];
        [actionSheet dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [actionSheet dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
        NSString *uniqueFileName = [NSString stringWithFormat:@"%@", guid];
        NSString *outputFile = [NSString stringWithFormat:@"%@/%@.mp4",[self getMediaSaveFolderPath],uniqueFileName];
        [self compressVideoWithURL:[info objectForKey:UIImagePickerControllerMediaURL] outputURL:[NSURL fileURLWithPath:outputFile] onComplete:^(bool completed) {
            if (completed) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self loadAllMedias];
                });
        }
            
        }];
        
    }else{
        UIImage *image =[Utility fixrotation:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
        [self saveSelectedImageFileToFolderWithImage:image shouldReload:YES compression:0.5f];
        
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}


#pragma mark - Nearby Location Catch

-(IBAction)showLocation:(id)sender{
    
    [self getNearBYLocations];
    [self.view endEditing:YES];
}

-(void)getNearBYLocations{
    
    [self.view endEditing:YES];
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    acController.autocompleteFilter.type = kGMSPlacesAutocompleteTypeFilterCity;
    [self presentViewController:acController animated:YES completion:nil];
    
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    strLocationName = place.name;
    locationCordinates.latitude = place.coordinate.latitude;
    locationCordinates.longitude = place.coordinate.longitude;
    [tableView reloadData];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
}



-(IBAction)showContacts:(id)sender{
    
    [self getAllContacts];
    [self.view endEditing:YES];
}

#pragma mark - Get All Contact Details

- (void)getAllContacts {
    
    ContactsPickerViewController *picker = [[UIStoryboard storyboardWithName:@"MainInterface" bundle:nil] instantiateViewControllerWithIdentifier:StoryBoardIdentifierForContactsPickerVC];
    picker.isFromShare = true;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person{
    
    NSString *firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty) ;
    
    [strContactName appendString:[NSString stringWithFormat:@"%@,",firstName]] ;
    [peoplePicker dismissViewControllerAnimated:YES completion:^{
        [tableView reloadData];
    }];
}

-(void)pickedContactsList:(NSMutableArray*)lists{
    NSMutableArray *names = [NSMutableArray new];
    for (NSDictionary *dict in lists) {
        if ([[dict objectForKey:@"isSelected"] boolValue]) {
            if ([dict objectForKey:@"name"]) {
                [names addObject:[dict objectForKey:@"name"]];
            }
        }
    }
    if (names.count) strContactName = [NSMutableString stringWithString:[names componentsJoinedByString:@","]];
    [tableView reloadData];
}

#pragma mark - Photo Browser & Deleagtes

- (void)presentGalleryWithImages:(NSArray*)images
{
    [self.view endEditing:YES];
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    UIView *vwPopUP = photoBrowser;
    [self.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}

#pragma mark - Photo Gallery

-(IBAction)showPhotoGallery:(BOOL)isPhoto
{
    CustomeImagePicker *cip =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:StoryboardForLogin Identifier:StoryBoardIdentifierForImagePicker];
    cip.delegate = self;
    cip.isPhotos = isPhoto;
    [cip setHideSkipButton:NO];
    [cip setHideNextButton:NO];
    [cip setMaxPhotos:MAX_ALLOWED_PICK];
    
    [self presentViewController:cip animated:YES completion:^{
    }];
}

-(void)imageSelected:(NSArray*)arrayOfGallery isPhoto:(BOOL)isPhoto;
{
    [self showLoadingScreen];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self saveMediaFrom:arrayOfGallery isPhoto:isPhoto];
    });
}
-(void) imageSelectionCancelled
{
    
}
-(void)saveMediaFrom:(NSArray*)arrayOfGallery isPhoto:(BOOL)isPhoto{
    float __block width = 300;
    width = self.view.frame.size.width;
    PHImageRequestOptions *requestOptions  = [[PHImageRequestOptions alloc] init];
    requestOptions.synchronous = YES;
    requestOptions.resizeMode = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    requestOptions.networkAccessAllowed = YES;
    
    __block NSInteger imageCount = 0;
    __block NSInteger videoCount = 0;
    
    for (PHAsset *asset in arrayOfGallery) {
        
        if (isPhoto) {
            
            PHImageManager *manager = [PHImageManager defaultManager];
            [manager requestImageForAsset:asset
                               targetSize:CGSizeMake(600, 600)
                              contentMode:PHImageContentModeDefault
                                  options:requestOptions
                            resultHandler:^void(UIImage *image, NSDictionary *info) {
                                [self saveSelectedImageFileToFolderWithImage:image shouldReload:NO compression:0.5f];
                                imageCount ++;
                                if (imageCount == arrayOfGallery.count) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self performSelector:@selector(loadAllMedias) withObject:self afterDelay:1];
                                    });
                                }
                                
                            }];
            
        }else{
            PHVideoRequestOptions *options = [PHVideoRequestOptions new];
            options.networkAccessAllowed = YES;
            options.deliveryMode = PHVideoRequestOptionsDeliveryModeMediumQualityFormat;
            
            [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset *_asset, AVAudioMix *audioMix, NSDictionary *info) {
                if ([_asset isKindOfClass:[AVURLAsset class]]) {
                    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                    NSString *uniqueFileName = [NSString stringWithFormat:@"%@", guid];
                    NSString *outputFile = [NSString stringWithFormat:@"%@/%@.mp4",[self getMediaSaveFolderPath],uniqueFileName];
                    NSURL *URL = [(AVURLAsset *)_asset URL];
                    [self compressVideoWithURL:URL outputURL:[NSURL fileURLWithPath:outputFile] onComplete:^(bool completed) {
                        if (completed) {
                            
                            videoCount ++;
                            
                            if (imageCount + videoCount == arrayOfGallery.count) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self performSelector:@selector(loadAllMedias) withObject:self afterDelay:1];
                                });
                            }
                        }else{
                            [self hideLoadingScreen];
                        }
                        
                    }];
                }
            }];
        }
    }
}


#pragma mark - Gallery Write and Fetch

-(void)writeMediaToFolderWithFromPath:(NSURL*)urlPath{
    
    /*
     <key>NSExtensionActivationSupportsImageWithMaxCount</key>
     <integer>10</integer>
     <key>NSExtensionActivationSupportsMovieWithMaxCount</key>
     <integer>1</integer>
     <key>NSExtensionActivationSupportsWebURLWithMaxCount</key>
     <integer>1</integer>
     <key>NSExtensionActivationSupportsText</key>
     <true/>*/
    
    NSString *fileExtension = [urlPath pathExtension];
    NSString *UTI = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)fileExtension, NULL);
    NSString *contentType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)UTI, kUTTagClassMIMEType);
    NSLog(@"tyope %@",contentType);

    if ([contentType hasPrefix:@"image"]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
            UIImage *im = [UIImage imageWithData: [NSData dataWithContentsOfURL:urlPath]];
            [self saveSelectedImageFileToFolderWithImage:im shouldReload:YES compression:0.5f];
            
            
        });
    }
    else if ([contentType hasPrefix:@"video"]) {
        NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
        NSString *uniqueFileName = [NSString stringWithFormat:@"%@", guid];
        NSString *outputFile = [NSString stringWithFormat:@"%@/%@.mp4",[self getMediaSaveFolderPath],uniqueFileName];
        
       [self compressVideoWithURL:urlPath outputURL:[NSURL fileURLWithPath:outputFile] onComplete:^(bool completed) {
           
           dispatch_async(dispatch_get_main_queue(), ^{
               [self loadAllMedias];
               
           });           
       }];
    }
    else if ([contentType hasPrefix:@"audio"]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
          [self writeAudioFileToGalleryFromURL:urlPath];
            
        });
        
    }else{

        if ([urlPath isKindOfClass:[NSURL class]]) {
            strPreviewURL = [urlPath absoluteString];
            dispatch_async(dispatch_get_main_queue(), ^{
                [tableView reloadData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hideLoadingScreen];
                    
                });
                
            });
        }
        [self hideLoadingScreen];
    }
   
}


-(void)saveSelectedImageFileToFolderWithImage:(UIImage*)_image shouldReload:(BOOL)shouldReload compression:(float)compression{
    
    UIImage *image = [Utility fixrotation:_image];
    UIImage *neImage = [self imageWithImage:image scaledToWidth:self.view.frame.size.width];
    NSData *pngData = UIImageJPEGRepresentation(neImage, compression);
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
    NSString *uniqueFileName = [NSString stringWithFormat:@"%@", guid];
    NSString *outputFile = [NSString stringWithFormat:@"%@/%@.jpeg",[self getMediaSaveFolderPath],uniqueFileName];
    [pngData writeToFile:outputFile atomically:YES];
    if (shouldReload) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadAllMedias];
        });
    }
    
}

-(UIImage*)imageWithImage:(UIImage*)sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(NSString*)getMediaSaveFolderPath{
    
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Share"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    return dataPath;
    
}

-(void)writeAudioFileToGalleryFromURL:(NSURL*)sourceURL{
    
    NSData *audio = [NSData dataWithContentsOfURL:sourceURL];
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
    NSString *outputFile = [NSString stringWithFormat:@"%@/%@.%@",[self getMediaSaveFolderPath],guid,[sourceURL pathExtension]];
    if ([audio writeToFile:outputFile atomically:YES]) {
        // yeah - file written
    } else {
        // oops - file not written
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadAllMedias];
    });

}

-(void)loadAllMedias{
    
    NSMutableArray *mediaForEdit = [NSMutableArray new];
    for (id object in arrDataSource) {
        if ([object isKindOfClass:[NSDictionary class]]) {
            [mediaForEdit addObject:object];
        }
        
    }
    [arrDataSource removeAllObjects];
    [arrDataSource addObjectsFromArray:mediaForEdit];
    
    NSString *dataPath = [self getMediaSaveFolderPath];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    NSError* error = nil;
    // sort by creation date
    NSMutableArray* filesAndProperties = [NSMutableArray arrayWithCapacity:[directoryContent count]];
    for(NSString* file in directoryContent) {
        
        if (![file isEqualToString:@".DS_Store"]) {
            NSString* filePath = [dataPath stringByAppendingPathComponent:file];
            NSDictionary* properties = [[NSFileManager defaultManager]
                                        attributesOfItemAtPath:filePath
                                        error:&error];
            NSDate* modDate = [properties objectForKey:NSFileCreationDate];
            
            [filesAndProperties addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                           file, @"path",
                                           modDate, @"lastModDate",
                                           nil]];
            
        }
    }
    // sort using a block
    // order inverted as we want latest date first
    NSArray* sortedFiles = [filesAndProperties sortedArrayUsingComparator:
                            ^(id path1, id path2)
                            {
                                // compare
                                NSComparisonResult comp = [[path1 objectForKey:@"lastModDate"] compare:
                                                           [path2 objectForKey:@"lastModDate"]];
                                // invert ordering
                                if (comp == NSOrderedDescending) {
                                    comp = NSOrderedAscending;
                                }
                                else if(comp == NSOrderedAscending){
                                    comp = NSOrderedDescending;
                                }
                                return comp;
                            }];
    
    for (NSInteger i = sortedFiles.count - 1; i >= 0; i --) {
        NSDictionary *dict = sortedFiles[i];
        NSString *outputFile = [NSString stringWithFormat:@"%@/%@",[self getMediaSaveFolderPath],[dict objectForKey:@"path"]];
        [arrDataSource insertObject:outputFile atIndex:0];
    }
    [tableView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideLoadingScreen];
        
    });
}

-(void)removeAllContentsInMediaFolder{
    
    NSString *dataPath = [self getMediaSaveFolderPath];
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager removeItemAtPath:dataPath error:&error];
    if (success){
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
}

#pragma mark - Audio and Video player

-(void)playSelectedMediaWithIndex:(NSInteger)tag{
    /*
    if (tag < arrDataSource.count) {
        
        NSString *fileName = arrDataSource[tag];
        if ([[fileName pathExtension] isEqualToString:@"jpeg"]) {
            //This is Image File with .png Extension , Photos.
            //NSString *filePath = [Utility getMediaSaveFolderPath];
            
        }
        else if ([[fileName pathExtension] isEqualToString:@"mp4"]) {
            //This is Image File with .mp4 Extension , Video Files
            NSError* error;
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
            [[AVAudioSession sharedInstance] setActive:NO error:&error];
            NSURL  *videourl = [NSURL fileURLWithPath:fileName];
            AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
            playerViewController.player = [AVPlayer playerWithURL:videourl];
            [playerViewController.player play];
            [self presentViewController:playerViewController animated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(videoDidFinish:)
                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:[playerViewController.player currentItem]];
            
        }
        else {
            
          //  [self showAudioPlayerWithURL:[NSURL fileURLWithPath:fileName]];
            // Recorded Audio
            
        }
    }*/
}

- (void)videoDidFinish:(id)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //fade out / remove subview
}

#pragma mark - Goal Category selction

-(IBAction)showGoalCategories{
    
    GoalCategoryViewController *goalCategory =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:@"MainInterface" Identifier:StoryBoardIdentifierForGoalCategory];
    goalCategory.delegate = self;
    [self presentViewController:goalCategory animated:YES completion:NULL];
}


-(void)goalCategorySelectedCategoryID:(GoalCategory*)category{
    
    goalCategory = category;
    [tableView reloadData];
    
}

#pragma mark - GOAL LIST VIEW and EDIT View

-(IBAction)showGoalsAndDreamsSelection{
    
    vwGoalsSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourGoalsAndDreams" owner:self options:nil] objectAtIndex:0];
    vwGoalsSelection.translatesAutoresizingMaskIntoConstraints = NO;
    vwGoalsSelection.delegate = self;
    [self.view addSubview:vwGoalsSelection];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwGoalsSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwGoalsSelection)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwGoalsSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwGoalsSelection)]];
    [vwGoalsSelection showSelectionPopUp];
    
    UIView *vwPopUP = vwGoalsSelection;
    vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, .01, .01);
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(void)selectYourGoalsAndDreamsPopUpCloseAppplied{
    
    UIView *vwPopUP = vwGoalsSelection;
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, 0.01, 0.01);
    } completion:^(BOOL finished) {
        [vwGoalsSelection removeFromSuperview];
        vwGoalsSelection.delegate = nil;
        vwGoalsSelection = nil;
        
    }];
    
}

-(void)goalsAndDreamsSelectedWithTitle:(NSString*)title goalId:(NSInteger)goalsId{
    
    strGemID = [NSString stringWithFormat:@"%ld",(long)goalsId];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    NSString *userToken = [sharedDefaults objectForKey:@"TOKEN"];
    [self showLoadingScreen];
    
    [self getGemDetailsForEditWithGEMID:[NSString stringWithFormat:@"%ld",(long)goalsId] gemType:@"goal" token:userToken success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        [self parseResponds:responseObject];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self manageApiErrorCaseWithOperation:task responds:task.responseObject];
        [self hideLoadingScreen];
    }];

    
}

-(void)parseResponds:(NSDictionary*)responds{
    
    
    if (responds) {
        
        [strContactName setString:@""];
        if (NULL_TO_NIL([responds objectForKey:@"contact_name"])) {
            [strContactName appendString:[NSString stringWithFormat:@"%@,",[responds objectForKey:@"contact_name"]]];
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"gem_details"])) {
            [strDescription appendString:[NSString stringWithFormat:@"%@",[responds objectForKey:@"gem_details"]]] ;
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"goal_enddate"])) {
            double date = [[responds objectForKey:@"goal_enddate"] doubleValue];
            if (date > 0) {
                strAchievementDate = [Utility getDateStringFromSecondsWith:[[responds objectForKey:@"goal_enddate"] doubleValue] withFormat:@"yyyy-MM-dd"] ;
            }
            
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"goal_status"])) {
            strStatus = @"Active";
            if ([[responds objectForKey:@"goal_status"] integerValue] == 1) {
                strStatus = @"Completed";
            }
        }
        
        strLocationAddress = @"";
        strLocationName  = @"";
        if (NULL_TO_NIL([responds objectForKey:@"location_address"])) {
            strLocationAddress = [responds objectForKey:@"location_address"];
        }
        if (NULL_TO_NIL([responds objectForKey:@"location_name"])) {
            strLocationName = [responds objectForKey:@"location_name"];
        }
        
        float latitude = 0;
        float longitude = 0;
        if (NULL_TO_NIL([responds objectForKey:@"location_latitude"])) {
            latitude = [[responds objectForKey:@"location_latitude"] floatValue];
        }
        if (NULL_TO_NIL([responds objectForKey:@"location_longitude"])) {
            longitude = [[responds objectForKey:@"location_longitude"] floatValue];
        }
        locationCordinates = CLLocationCoordinate2DMake(latitude, longitude);
        
        if (NULL_TO_NIL([responds objectForKey:@"gem_title"])) {
            strTitle = [responds objectForKey:@"gem_title"];
        }
        
        
        if (arrDataSource.count) {
            NSMutableArray *discardedItems = [NSMutableArray array];
            for (id object in arrDataSource) {
                if ([object isKindOfClass:[NSDictionary class]] ) {
                    // Editing medias
                    [discardedItems addObject:object];
                }
                
            }
            if (discardedItems.count) {
                [arrDataSource removeObjectsInArray:discardedItems];
            }
            
        }
        if (NULL_TO_NIL([responds objectForKey:@"gem_media"])) {
            NSArray *medias = [responds objectForKey:@"gem_media"];
            for (NSDictionary *dict in medias) {
                [arrDataSource addObject:dict];
            }
        }
        
    }
    
    [tableView reloadData];
    
    
}

/*

-(void)parseResponds:(NSDictionary*)responds{
    
    if (responds) {
        
        if (NULL_TO_NIL([responds objectForKey:@"contact_name"])) {
            [strContactName appendString:[NSString stringWithFormat:@"%@,",[responds objectForKey:@"contact_name"]]];
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"gem_details"])) {
            strDescription = [responds objectForKey:@"gem_details"];
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"goal_enddate"])) {
            double date = [[responds objectForKey:@"goal_enddate"] doubleValue];
            if (date > 0) {
                strAchievementDate = [Utility getDateStringFromSecondsWith:[[responds objectForKey:@"goal_enddate"] doubleValue] withFormat:@"yyyy-MM-dd"] ;
            }
            
            
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"goal_status"])) {
            strStatus = @"Active";
            if ([[responds objectForKey:@"goal_status"] integerValue] == 1) {
                strStatus = @"Completed";
            }
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"location_address"])) {
            strLocationAddress = [responds objectForKey:@"location_address"];
        }
        if (NULL_TO_NIL([responds objectForKey:@"location_name"])) {
            strLocationName = [responds objectForKey:@"location_name"];
        }
        
        float latitude = 0;
        float longitude = 0;
        if (NULL_TO_NIL([responds objectForKey:@"location_latitude"])) {
            latitude = [[responds objectForKey:@"location_latitude"] floatValue];
        }
        if (NULL_TO_NIL([responds objectForKey:@"location_longitude"])) {
            longitude = [[responds objectForKey:@"location_longitude"] floatValue];
        }
        locationCordinates = CLLocationCoordinate2DMake(latitude, longitude);
        
        if (NULL_TO_NIL([responds objectForKey:@"gem_title"])) {
            strTitle = [responds objectForKey:@"gem_title"];
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"preview_url"])) {
            strURLPreview = [responds objectForKey:@"preview_url"];
        }
        
        if (NULL_TO_NIL([responds objectForKey:@"gem_media"])) {
            NSArray *medias = [responds objectForKey:@"gem_media"];
            for (NSDictionary *dict in medias) {
                [arrDataSource addObject:dict];
            }
        }
        
    }
    
    [tableView reloadData];
    
    
}
*/


-(void)getGemDetailsForEditWithGEMID:(NSString*)gemID gemType:(NSString*)gemType token:(NSString*)token success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=editgem",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"gem_id": gemID,
                             @"gem_type": gemType
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       
        failure (operation,error);
    }];
    
    
}



#pragma mark - SUBMIT GOALS AND POSTS

    -(IBAction)submitDetails{
        
        [self.view endEditing:YES];
        [self checkAllFieldsAreValid:^{
            
            [self showLoadingScreen];
            
            if (_actionType == eActionTypeCommunity) {
                
                [self uploadMediasForType:eActionTypeCommunity];
                
                
            }else{
                
                [self uploadMediasForType:eActionTypeGoalsAndDreams];
                
            }
            
        } failure:^{
            
            NSString *message = @"Please fill all the required fields";
            if (_actionType == eActionTypeCommunity){
                message = @"Please enter description or attach any media";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"SHARE"
                                                                           message:message
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      
                                                                      
                                                                  }];
            
            [alert addAction:firstAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }];
        
    }
    

    

    
-(void)checkAllFieldsAreValid:(void (^)())success failure:(void (^)())failure{
    
    BOOL isValid = false;
    NSString *errorMsg;
    if (_actionType == eActionTypeCommunity) {
        
        if (([strDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length) > 0) {
            isValid = true;
        }
        else if(([strPreviewURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length) > 0){
             isValid = true;
        }
        else{
            isValid = false;
            if (arrDataSource.count > 0) isValid = true;
        }
    }else{
        
        if ([strTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length){
            
            if (([strDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length) || ([strPreviewURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length)){
                
                isValid = true;
                if (_actionType == eActionTypeGoalsAndDreams) {
                    isValid = false;
                    if (([strAchievementDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length) && ([strStatus stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length)){
                        isValid = true;
                    }
                }
                
            }
            
        }else{
            
            isValid = false;
        }
    }
    
    if (isValid)success();
    else failure(errorMsg);
    
}

-(void)uploadMediasForType:(NSInteger)type{
    
    [self showLoadingScreen];
    NSMutableArray *arrThumb = [NSMutableArray new];
    NSMutableArray *arrOrder = [NSMutableArray new];
    
    if (arrDataSource.count) {
        
        typedef void (^UploadMediaBlock)(NSUInteger);
        
        __block __weak UploadMediaBlock uploadMedia = nil;
        
        UploadMediaBlock addMedia = ^(NSUInteger index) {
            
            if (index < arrDataSource.count)
            {
                UploadMediaBlock strongUploadMedia = uploadMedia;
                
                id object = arrDataSource[index];
                if ([object isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *_media  = (NSDictionary*)object;
                    if ([_media objectForKey:@"gem_media"]) {
                        
                        NSURL *yourURL = [NSURL URLWithString:[_media objectForKey:@"gem_media"]];
                        NSString *fileName = [yourURL lastPathComponent];
                        [arrOrder addObject:fileName];
                        if ([[_media objectForKey:@"media_type"] isEqualToString:@"video"]) {
                            NSURL *yourURL = [NSURL URLWithString:[_media objectForKey:@"video_thumb"]];
                            NSString *fileName = [yourURL lastPathComponent];
                            [arrThumb addObject:fileName];
                        }else{
                            [arrThumb addObject:[NSNull null]];
                        }
                    }
                    
                    strongUploadMedia(index+1);
                    
                }else{
                    
                    [self uploadMediasToServerWithPath:arrDataSource[index] OnSuccess:^(id responseObject) {
                        
                        if ([responseObject objectForKey:@"media_name"]) {
                            [arrOrder addObject:[responseObject objectForKey:@"media_name"]];
                        }
                        if ([responseObject objectForKey:@"media_thumb"]) {
                            [arrThumb addObject:[responseObject objectForKey:@"media_thumb"]];
                            
                        }else{
                            [arrThumb addObject:[NSNull null]];
                        }
                        
                        strongUploadMedia(index+1);
                        
                    } failure:^(NSError *error) {
                        
                        
                        [self hideLoadingScreen];
                        return ;
                    }];
                    
                }
                
                
            }else{
                
                [self resumeEditOeUploadGoalParametersWithImageNames:arrOrder thumbNames:arrThumb type:type];
            }
            
        };
        uploadMedia = addMedia;
        addMedia(0);
        
    }else{
        
        [self resumeEditOeUploadGoalParametersWithImageNames:arrOrder thumbNames:arrThumb type:type];
    }
}

-(void)uploadMediasToServerWithPath:(NSString*)filePath OnSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    
    [self uploadMediasAtPath:filePath type:@"gem" Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
         [self manageApiErrorCaseWithOperation:task responds:task.responseObject];
        failure(error);
    }];
    
    
}

- (void)uploadMediasAtPath:(NSString*)objectPath type:(NSString*)type Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=imageupload",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    NSString *userToken = [sharedDefaults objectForKey:@"TOKEN"];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if (userToken) {
        [manager.requestSerializer setValue:userToken forHTTPHeaderField:@"Authorization"];
    }
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (type.length) [params setObject:type forKey:@"type"];
    else [params setObject:@"gem" forKey:@"type"];
    
    AFHTTPRequestOperation *operation = [manager POST:urlString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if([[NSFileManager defaultManager] fileExistsAtPath:objectPath])
        {
            NSData *data = [[NSFileManager defaultManager] contentsAtPath:objectPath];
            
            if ([[objectPath pathExtension] isEqualToString:@"jpeg"]){
                [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"media_name"] fileName:@"Media" mimeType:@"image/jpeg"];
            }
            else if ([[objectPath pathExtension] isEqualToString:@"mp4"]){
                [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"media_name"] fileName:@"Media" mimeType:@"video/mp4"];
                UIImage *thumbnail = [Utility getThumbNailFromVideoURL:objectPath];
                NSData *imageData = UIImageJPEGRepresentation(thumbnail,1);
                if (imageData.length)
                    [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"media_thumb"] fileName:@"Media" mimeType:@"image/jpeg"];
            }
            else if ([[objectPath pathExtension] isEqualToString:@"aac"]){
                [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"media_name"] fileName:@"Media" mimeType:@"audio/aac"];
            }
        }

        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
        
    }];
    
}

-(void)resumeEditOeUploadGoalParametersWithImageNames:(NSMutableArray*)imageNames thumbNames:(NSMutableArray*)thumbNames type:(NSInteger)type {
    
    if (_actionType == eActionTypeCommunity) {
        
        [self createOrEditAGemWith:arrDataSource eventTitle:@"" description:strDescription latitude:locationCordinates.latitude longitude:locationCordinates.longitude locName:strLocationName address:strLocationAddress contactName:strContactName gemID:nil goalID:nil deletedIDs:arrDeletedIDs gemType:@"community" previewURL:strPreviewURL imageNames:imageNames thumbNames:thumbNames  OnSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self hideLoadingScreen];
            [self goBack:nil];
            
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            [self hideLoadingScreen];
             [self manageApiErrorCaseWithOperation:task responds:task.responseObject];
            
            
        }];
        
    }else{
        
        NSString *dateString = strAchievementDate;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        dateFromString = [dateFormatter dateFromString:dateString];
        double achievementDate = [dateFromString timeIntervalSince1970];
        
        [self createOrEditAGoalWith:arrDataSource eventTitle:strTitle description:strDescription latitude:locationCordinates.latitude longitude:locationCordinates.longitude locName:strLocationName address:strLocationAddress contactName:strContactName gemID:strGemID goalID:nil deletedIDs:arrDeletedIDs gemType:@"goal" achievementDate:achievementDate status:strStatus isPurposeColor:NO previewURL:strPreviewURL imageNames:imageNames thumbNames:thumbNames catID:goalCategory.strCatID  OnSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self hideLoadingScreen];
            [self goBack:nil];
            
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            [self hideLoadingScreen];
            [self manageApiErrorCaseWithOperation:task responds:task.responseObject];
            
            
        } ];
    }
    
}
-(void)createOrEditAGemWith:(NSArray*)dataSource eventTitle:(NSString*)title description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)locName address:(NSString*)locAddress contactName:(NSString*)conatctName gemID:(NSString*)gemID goalID:(NSString*)goalID deletedIDs:(NSMutableArray*)deletedIDs gemType:(NSString*)gemType previewURL:(NSString*)previewURL imageNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=gemaction",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    NSString *userToken = [sharedDefaults objectForKey:@"TOKEN"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if (userToken) {
        [manager.requestSerializer setValue:userToken forHTTPHeaderField:@"Authorization"];
    }
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (title.length > 0) [params setObject:title forKey:@"title"];
    [params setObject:descritption forKey:@"details"];
    [params setObject:gemType forKey:@"gem_type"];
    if (previewURL.length) [params setObject:previewURL forKey:@"preview_url"];
    
    if (locName.length){
        NSMutableDictionary *location = [NSMutableDictionary new];
        [location setObject:locName forKey:@"location_name"];
        if (locAddress.length)[location setObject:locAddress forKey:@"location_address"];
        [location setObject:[NSNumber numberWithDouble:latitude] forKey:@"location_latitude"];
        [location setObject:[NSNumber numberWithDouble:longitude] forKey:@"location_longitude"];
        [params setObject:location forKey:@"location"];
        
    }
    if (conatctName.length) [params setObject:conatctName forKey:@"contact_name"];
    if (gemID.length) [params setObject:gemID forKey:@"gem_id"];
    if (goalID.length) [params setObject:goalID forKey:@"goal_id"];
    NSInteger mediacount = dataSource.count;
    if (gemID.length){
        mediacount = 0;
        for (id obj in dataSource) {
            if (![obj isKindOfClass:[NSDictionary class]])
                mediacount ++;
        }
    }
    [params setObject:[NSNumber numberWithInteger:mediacount] forKey:@"media_count"];
    [params setObject:deletedIDs forKey:@"media_id"];
    [params setObject:mediaNames forKey:@"media_name"];
    [params setObject:thumbNames forKey:@"media_thumb"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *jsonEvent = [NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"json_event", nil];
    
    [manager POST:urlString parameters:jsonEvent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure (operation,error);
    }];
    
}

-(void)createOrEditAGoalWith:(NSArray*)dataSource eventTitle:(NSString*)title description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)locName address:(NSString*)locAddress contactName:(NSString*)conatctName gemID:(NSString*)gemID goalID:(NSString*)goalID deletedIDs:(NSMutableArray*)deletedIDs gemType:(NSString*)gemType achievementDate:(double)achievementDate status:(NSString*)status isPurposeColor:(BOOL)isPurposeColor previewURL:(NSString*)previewURL imageNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames catID:(NSString*)catID OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=gemaction",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    NSString *userToken = [sharedDefaults objectForKey:@"TOKEN"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if (userToken) {
        [manager.requestSerializer setValue:userToken forHTTPHeaderField:@"Authorization"];
    }
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    if (title.length)  [params setObject:title forKey:@"title"];
    if (descritption.length)  [params setObject:descritption forKey:@"details"];
    if (previewURL.length) [params setObject:previewURL forKey:@"preview_url"];
    if (catID.length > 0) [params setObject:catID forKey:@"category_id"];
    [params setObject:gemType forKey:@"gem_type"];
    if (locName.length){
        NSMutableDictionary *location = [NSMutableDictionary new];
        [location setObject:locName forKey:@"location_name"];
        if (locAddress.length)[location setObject:locAddress forKey:@"location_address"];
        [location setObject:[NSNumber numberWithDouble:latitude] forKey:@"location_latitude"];
        [location setObject:[NSNumber numberWithDouble:longitude] forKey:@"location_longitude"];
        [params setObject:location forKey:@"location"];
        
    }
    [params setObject:[NSNumber numberWithDouble:achievementDate] forKey:@"goal_enddate"];
    NSInteger _status = 0;
    if ([status isEqualToString:@"Completed"]) {
        _status = 1;
    }
    [params setObject:[NSNumber numberWithInteger:_status] forKey:@"goal_status"];
    if (gemID.length){
        if (isPurposeColor) [params setObject:gemID forKey:@"purposegem_id"];
        else [params setObject:gemID forKey:@"gem_id"];
    }
    if (conatctName.length) [params setObject:conatctName forKey:@"contact_name"];
    if (goalID.length) [params setObject:goalID forKey:@"goal_id"];
    NSInteger mediacount = dataSource.count;
    if (gemID.length){
        mediacount = 0;
        for (id obj in dataSource) {
            if (![obj isKindOfClass:[NSDictionary class]])
                mediacount ++;
        }
    }
    [params setObject:[NSNumber numberWithInteger:mediacount] forKey:@"media_count"];
    [params setObject:deletedIDs forKey:@"media_id"];
    [params setObject:mediaNames forKey:@"media_name"];
    [params setObject:thumbNames forKey:@"media_thumb"];
    
   
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *jsonEvent = [NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"json_event", nil];
    
    
    [manager POST:urlString parameters:jsonEvent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    
}

#pragma mark - API error manager

-(void)manageApiErrorCaseWithOperation:(AFHTTPRequestOperation*)operation responds:(NSDictionary*)notification{
    
    if ([operation.response statusCode] == kUpdateMode) {
        [self showMandatoryVersionUpdate:notification];
    } else if ([operation.response statusCode] == kMaintenanceMode) {
        [self showMaintainanceModeMessage];
    }
    else if ([operation.response statusCode] == kUnAuthorized) {
        [self logoutSinceUnAuthorized:notification];
    }
    
}

-(void)logoutSinceUnAuthorized:(NSDictionary*)notification{
    NSString *text = @"Invalid authentication token!";
    if (notification) {
        NSDictionary *dict  = notification;
        text = [dict objectForKey:@"text"];
    }
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Logout"
                                          message:text
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self goBack:nil];
                                   
                               }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

-(void)showMandatoryVersionUpdate:(NSDictionary*)responseObject{
    
    if (responseObject && [responseObject objectForKey:@"message"] && [responseObject objectForKey:@"title"]) {
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:[responseObject objectForKey:@"title"]
                                              message:[responseObject objectForKey:@"message"]
                                              preferredStyle:UIAlertControllerStyleAlert];
       
        UIAlertAction *cancel = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     [self goBack:nil];
                                 }];
        
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

-(void)showMaintainanceModeMessage{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Maintenance"
                                          message:@"We are currently upgrading our system. Thank you for your patience & we'll be back in a couple of hours"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self goBack:nil];
                                   
                               }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark -General Methods



-(BOOL)checkIfURLisPresentOn:(NSString*)strText{
    
    if (!detector) {
        NSError *error = nil;
        detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                   error:&error];
    }
    if (strText) {
        
        NSArray *matches = [detector matchesInString:strText
                                             options:0
                                               range:NSMakeRange(0, [strText length])];
        if (matches.count > 0) {
            return YES;
        }
    }
 return NO;
}

-(void)dragCell:(UILongPressGestureRecognizer *)panner{
    
        if(panner.state == UIGestureRecognizerStateBegan)
        {
            viewStartLocation = [panner locationInView:tableView];
            tableView.scrollEnabled = NO;
            
            //if needed do some initial setup or init of views here
        }
        else if(panner.state == UIGestureRecognizerStateChanged)
        {
            //move your views here.
            if (! self.cellSnapshotView) {
                CGPoint loc = [panner locationInView:tableView];
                self.draggingCellIndexPath = [tableView indexPathForRowAtPoint:loc];
                if (self.draggingCellIndexPath.section < 2) return;
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:self.draggingCellIndexPath];
                if (cell){
                    
                    UIGraphicsBeginImageContextWithOptions(cell.bounds.size, NO, 0);
                    [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
                    UIImage *cellImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                    // create and image view that we will drag around the screen
                    self.cellSnapshotView = [[UIImageView alloc] initWithImage:cellImage];
                    
                    //self.cellSnapshotView = [cell snapshotViewAfterScreenUpdates:YES];
                    self.cellSnapshotView.alpha = 0.8;
                    self.cellSnapshotView.layer.borderColor = [UIColor redColor].CGColor;
                    self.cellSnapshotView.layer.borderWidth = 1;
                    self.cellSnapshotView.frame =  cell.frame;
                    [tableView addSubview:self.cellSnapshotView];
                    
                    //[tableView reloadRowsAtIndexPaths:@[self.draggingCellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
                // replace the cell with a blank one until the drag is over
            }
            
            CGPoint location = [panner locationInView:tableView];
            CGPoint translation;
            translation.x = location.x - viewStartLocation.x;
            translation.y = location.y - viewStartLocation.y;
            CGPoint cvCenter = self.cellSnapshotView.center;
            cvCenter.x = location.x;
            cvCenter.y = location.y;
            self.cellSnapshotView.center = cvCenter;
            
            
            
        }
        else if(panner.state == UIGestureRecognizerStateEnded || (panner.state == UIGestureRecognizerStateCancelled) || (panner.state == UIGestureRecognizerStateFailed))
        {
            tableView.scrollEnabled = YES;
            UITableViewCell *droppedOnCell;
            CGRect largestRect = CGRectZero;
            for (UITableViewCell *cell in tableView.visibleCells) {
                CGRect intersection = CGRectIntersection(cell.frame, self.cellSnapshotView.frame);
                if (intersection.size.width * intersection.size.height >= largestRect.size.width * largestRect.size.height) {
                    largestRect = intersection;
                    droppedOnCell =  cell;
                }
            }
            
            NSIndexPath *droppedOnCellIndexPath = [tableView indexPathForCell:droppedOnCell];
            if (droppedOnCellIndexPath.section < 2 ) {
                [self.cellSnapshotView removeFromSuperview];
                self.cellSnapshotView = nil;
                return;
            }
            [UIView animateWithDuration:.2 animations:^{
                self.cellSnapshotView.center = droppedOnCell.center;
            } completion:^(BOOL finished) {
                [self.cellSnapshotView removeFromSuperview];
                self.cellSnapshotView = nil;
                NSIndexPath *savedDraggingCellIndexPath = self.draggingCellIndexPath;
                
                if (droppedOnCellIndexPath.section < 2 ) return;
                if (savedDraggingCellIndexPath.section < 2 ) return;
                
                if (![self.draggingCellIndexPath isEqual:droppedOnCellIndexPath]) {
                    self.draggingCellIndexPath = [NSIndexPath indexPathForRow:-1 inSection:0];
                    [arrDataSource exchangeObjectAtIndex:savedDraggingCellIndexPath.row withObjectAtIndex:droppedOnCellIndexPath.row];
                    [tableView reloadRowsAtIndexPaths:@[savedDraggingCellIndexPath, droppedOnCellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                }else{
                    self.draggingCellIndexPath = [NSIndexPath indexPathForRow:-1 inSection:0];
                    [tableView reloadRowsAtIndexPaths:@[savedDraggingCellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            }];
            
            tableView.scrollEnabled = YES;
            
        }
}
    
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
        return YES;
}
    
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
    
-(void)deleteSelectedMedia:(UIButton*)btnDelete{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete"
                                  message:@"Delete the selected media ?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             if (btnDelete.tag < arrDataSource.count) {
                                 id object = arrDataSource[btnDelete.tag];
                                 if ([object isKindOfClass:[NSString class]]) {
                                     // Created medias
                                     NSString *fileName = arrDataSource[btnDelete.tag];
                                     [self removeAMediaFileWithName:fileName];
                                 }else{
                                     //Editing medias
                                     NSDictionary *media = [arrDataSource objectAtIndex:btnDelete.tag];
                                     if ([media objectForKey:@"media_id"])
                                         [arrDeletedIDs addObject:[media objectForKey:@"media_id"]];
                                     [arrDataSource removeObjectAtIndex:btnDelete.tag];
                                      [self loadAllMedias];
                                     
                                 }
                                 
                             }
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)removeAMediaFileWithName:(NSString *)filePath{
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success){}
    else NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    [self loadAllMedias];
}
-(void)compressVideoWithURL:(NSURL*)videoURL outputURL:(NSURL*)outputURL onComplete:(void (^)(bool completed))completed{
    
    SDAVAssetExportSession *encoder = [SDAVAssetExportSession.alloc initWithAsset:[AVAsset assetWithURL:videoURL]];
    NSURL *url = outputURL;
    encoder.outputURL=url;
    encoder.outputFileType = AVFileTypeMPEG4;
    encoder.shouldOptimizeForNetworkUse = YES;
    encoder.videoSettings = @
    {
    AVVideoCodecKey: AVVideoCodecH264,
    AVVideoWidthKey:[NSNumber numberWithInteger:360], // required
    AVVideoHeightKey:[NSNumber numberWithInteger:480], // required
    AVVideoCompressionPropertiesKey: @
        {
        AVVideoAverageBitRateKey: @500000, // Lower bit rate here
        AVVideoProfileLevelKey: AVVideoProfileLevelH264High40,
        },
    };
    
    encoder.audioSettings = @
    {
    AVFormatIDKey: @(kAudioFormatMPEG4AAC),
    AVNumberOfChannelsKey: @2,
    AVSampleRateKey: @44100,
    AVEncoderBitRateKey: @128000,
    };
    
    [encoder exportAsynchronouslyWithCompletionHandler:^
     {
         int status = encoder.status;
         if (status == AVAssetExportSessionStatusCompleted)
         {
             
             
         }
         else if (status == AVAssetExportSessionStatusCancelled)
         {
             NSLog(@"Video export cancelled");
         }
         else
         {
         }
         
         completed(YES);
     }];
}


-(void)managePreviewWithText:(NSString*)strText withCell:(DescriptionCell*)cell{
    
    if (!detector) {
        NSError *error = nil;
        detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                   error:&error];
    }
    if (strText) {
        
        NSArray *matches = [detector matchesInString:strText
                                             options:0
                                               range:NSMakeRange(0, [strText length])];
        if (matches.count > 0) {
            NSTextCheckingResult *match = [matches firstObject];
            if (![self isEquivalentURLOne:alreadyPreviewdURL URLTwo:[match URL]])showPreview = true;
            // else showPreview = false;
            if (showPreview) {
                [cell.previewIndicator startAnimating];
                [MTDURLPreview loadPreviewWithURL:[match URL] completion:^(MTDURLPreview *preview, NSError *error) {
                    if (error) {
                        cell.vwURLPreview.hidden = true;
                        [cell.previewIndicator stopAnimating];
                        cell.lblPreviewTitle.text = @"";
                        cell.lblPreviewDescription.text = @"";
                        cell.lblPreviewTitle.text = @"";
                        cell.lblPreviewDomain.text = @"";
                        cell.imgPreview.image = nil;
                        cell.heightForPreview.constant = 0;
                        
                        
                        
                    }else{
                        
                        
                        alreadyPreviewdURL = [match URL];
                        cell.vwURLPreview.hidden = false;
                         cell.heightForPreview.constant = 205;
                        [cell.previewIndicator stopAnimating];
                        cell.lblPreviewTitle.text = preview.title;
                        cell.lblPreviewDescription.text = preview.content;
                        cell.lblPreviewTitle.text = preview.title;
                        cell.lblPreviewDomain.text = preview.domain;
                        [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                      placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             }];
                    }
                    
                    
                }];
                
            }
            
        }else{
            
            cell.vwURLPreview.hidden = true;
            [cell.previewIndicator stopAnimating];
            cell.lblPreviewTitle.text = @"";
            cell.lblPreviewDescription.text = @"";
            cell.lblPreviewDomain.text = @"";
            cell.imgPreview.image = nil;
        }
        
    }
    
}


-(void)doneTyping{
    
    [self.view endEditing:YES];
}

- (BOOL)isEquivalentURLOne:(NSURL *)urlOne URLTwo:(NSURL *)URLTwo {
    
    if (urlOne && URLTwo) {
        
        if ([urlOne isEqual:URLTwo]) return YES;
        if ([[urlOne scheme] caseInsensitiveCompare:[URLTwo scheme]] != NSOrderedSame) return NO;
        if ([[urlOne host] caseInsensitiveCompare:[URLTwo host]] != NSOrderedSame) return NO;
        
        // NSURL path is smart about trimming trailing slashes
        // note case-sensitivty here
        if ([[urlOne path] compare:[URLTwo path]] != NSOrderedSame) return NO;
        
        // at this point, we've established that the urls are equivalent according to the rfc
        // insofar as scheme, host, and paths match
        
        // according to rfc2616, port's can weakly match if one is missing and the
        // other is default for the scheme, but for now, let's insist on an explicit match
        if ([urlOne port] || [URLTwo port]) {
            if (![[urlOne port] isEqual:[URLTwo port]]) return NO;
            if (![[urlOne query] isEqual:[URLTwo query]]) return NO;
        }
        return YES;
    }
    return NO;
    
    // for things like user/pw, fragment, etc., seems sensible to be
    // permissive about these.
    
}
-(UIImage*)getThumbNailFromVideoURL:(NSURL*)videoURL{
    
    NSURL *url = videoURL;
    AVAsset *asset = [AVAsset assetWithURL:url];
    //  Get thumbnail at the very start of the video
    CMTime thumbnailTime = [asset duration];
    thumbnailTime.value = 0;
    //  Get image from the video at the given time
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = true;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbnailTime actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return thumbnail;
}

- (void)keyBoardShown:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    bottomForPreview.constant = -(keyboardFrameBeginRect.size.height);
}

- (void)keyboardDidHide:(NSNotification*)notification
{
    
    
    bottomForPreview.constant = -100;
    vwURLPreview.hidden = true;
    [previewIndicator stopAnimating];
    lblPreviewTitle.text = @"";
    lblPreviewDescription.text = @"";
    lblPreviewTitle.text = @"";
    lblPreviewDomain.text = @"";
    imgPreview.image = nil;
}

-(IBAction)hidePreviewPopUp:(id)sender{
    
    showPreview = false;
    vwURLPreview.hidden = true;
    [previewIndicator stopAnimating];
    lblPreviewTitle.text = @"";
    lblPreviewDescription.text = @"";
    lblPreviewTitle.text = @"";
    lblPreviewDomain.text = @"";
    imgPreview.image = nil;
}

-(IBAction)showPreviewDetailPage:(id)sender{
    
    if (lblPreviewDomain.text) {
        NSString *myURLString = lblPreviewDomain.text;
        NSURL *myURL;
        if ([myURLString.lowercaseString hasPrefix:@"http://"]) {
            myURL = [NSURL URLWithString:myURLString];
        } else {
            myURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",myURLString]];
        }
        
       // [[UIApplication sharedApplication] openURL:myURL];
    }
    
}


-(IBAction)goBack:(id)sender{
    
    [self removeAllContentsInMediaFolder];
    [self.extensionContext completeRequestReturningItems:@[]  completionHandler:nil];
}
    
-(void)showLoadingScreen{
    
    [UIView animateWithDuration:0.4 animations:^(void) {
        vwLoadingScreen.alpha = 1;
    }];
    
    
}
-(void)hideLoadingScreen{
    [UIView animateWithDuration:0.4 animations:^(void) {
        vwLoadingScreen.alpha = 0;
    }];
    
}
    
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
