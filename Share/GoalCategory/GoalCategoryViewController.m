//
//  GoalCategoryViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 03/12/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "GoalCategoryViewController.h"
#import "GoalCategoryCollectionViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import "Constants.h"
#import "PrefixHeader.pch"

@interface GoalCategoryViewController (){
    
    IBOutlet UICollectionView *collectionView;
    IBOutlet UIView *vwLoadingScreen;
    IBOutlet UIButton *nextButton;
    NSMutableArray *dataSource;
    GoalCategory *selectedCategory;
}

@end

@implementation GoalCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self getAllCategories];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    [nextButton setEnabled:false];
    nextButton.alpha = 0.3;
    dataSource = [NSMutableArray new];
    collectionView.alwaysBounceVertical = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    nextButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    nextButton.layer.borderWidth = 1.0f;
    nextButton.layer.cornerRadius = 5.0f;
}

-(void)getAllCategories{
    
    [self showLoadingScreen];
    
    [self getAllGoalCategoriesOnSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self parseResponds:responseObject];
            [self hideLoadingScreen];
        });
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
          [self hideLoadingScreen];
    }];
    
}

- (void)getAllGoalCategoriesOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    NSString *userToken = [sharedDefaults objectForKey:@"TOKEN"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getallcategory",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:userToken forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       
        failure (operation,error);
    }];
}

-(void)parseResponds:(NSArray*)responds{
    
    if ([responds isKindOfClass:[NSArray class]]) {
        
        for (NSDictionary *details in responds) {
            GoalCategory *cat = [[GoalCategory alloc] initWithName:[details objectForKey:@"category_name"] catID:[details objectForKey:@"category_id"] image:[details objectForKey:@"category_img"]];
            [dataSource addObject:cat];
        }
        [collectionView reloadData];
    }
    
    
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section{
    
    NSInteger count = dataSource.count;
    return count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GoalCategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (indexPath.row < dataSource.count) {
        GoalCategory *category = dataSource[indexPath.row];
        cell.lblTitle.text = category.strCatName;
        if (category.strCatImg) {
            [cell.indicator startAnimating];
            [cell.imgVw sd_setImageWithURL:[NSURL URLWithString:category.strCatImg]
                          placeholderImage:[UIImage imageNamed:@""]
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      [cell.indicator stopAnimating];
                                 }];
        }
        cell.vwBg.backgroundColor = [UIColor lightGrayColor];
        if (category.isSelected) {
            cell.vwBg.backgroundColor = [UIColor getThemeColor];
        }
       
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = _collectionView.frame.size.width / 2.0;
    float height = _collectionView.frame.size.width / 2.0 ;
    return CGSizeMake(width, height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < dataSource.count) {
        [nextButton setEnabled:true];
         nextButton.alpha = 1;
        [self clearAllSelections];
        GoalCategory *category = dataSource[indexPath.row];
        category.isSelected = !category.isSelected;
        if (category.isSelected) {
            selectedCategory = category;
        }
        [collectionView reloadData];
    }
   
}


-(IBAction)passSelection:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(goalCategorySelectedCategoryID:)]){
        [self.delegate goalCategorySelectedCategoryID:selectedCategory];
        [self goBack:nil];
    }
}

-(void)clearAllSelections{
    
    for (GoalCategory *category in dataSource) {
        category.isSelected = false;
    }
}

-(void)showLoadingScreen{
    
    [UIView animateWithDuration:0.4 animations:^(void) {
        vwLoadingScreen.alpha = 1;
    }];
    
    
}
-(void)hideLoadingScreen{
    [UIView animateWithDuration:0.4 animations:^(void) {
        vwLoadingScreen.alpha = 0;
    }];
    
}

-(IBAction)goBack:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
