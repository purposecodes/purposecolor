//
//  Community+CoreDataClass.h
//  
//
//  Created by Purpose Code on 20/04/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Community : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Community+CoreDataProperties.h"
