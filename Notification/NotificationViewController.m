//
//  NotificationViewController.m
//  Notification
//
//  Created by Purpose Code on 05/04/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "NotificationViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>

@interface NotificationViewController () <UNNotificationContentExtension>{
    
    IBOutlet UILabel *label;
    IBOutlet UIImageView *imgView;
    UNNotification *_notification;
}

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    float width = self.view.frame.size.width;
    float height = (width * 80)/100;;
    self.preferredContentSize = CGSizeMake(width,height + 30);
}

- (void)didReceiveNotificationResponse:(UNNotificationResponse *)response completionHandler:(void (^)(UNNotificationContentExtensionResponseOption option))completion{
    
    //completion(UNNotificationContentExtensionResponseOptionDismiss);
}

- (void)didReceiveNotification:(UNNotification *)notification {
    _notification = notification;
    label.text = notification.request.content.body;
    NSArray *attachments = notification.request.content.attachments;
    if (attachments.count) {
        UNNotificationAttachment *attachmnt = attachments[0];
        NSURL *url = attachmnt.URL;
        if ([attachmnt.URL startAccessingSecurityScopedResource]) {
            UIImage *image = [UIImage imageWithContentsOfFile:url.path];
            imgView.image = image;
        }

    }
}

-(void)dealloc{
    
    NSArray *attachments = _notification.request.content.attachments;
    if (attachments.count) {
        UNNotificationAttachment *attachmnt = attachments[0];
        [attachmnt.URL stopAccessingSecurityScopedResource];
        
    }
}

@end
