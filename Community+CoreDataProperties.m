//
//  Community+CoreDataProperties.m
//  
//
//  Created by Purpose Code on 20/04/18.
//
//

#import "Community+CoreDataProperties.h"

@implementation Community (CoreDataProperties)

+ (NSFetchRequest<Community *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Community"];
}

@dynamic pageno;
@dynamic responds;

@end
