//
//  Community+CoreDataProperties.h
//  
//
//  Created by Purpose Code on 20/04/18.
//
//

#import "Community+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Community (CoreDataProperties)

+ (NSFetchRequest<Community *> *)fetchRequest;

@property (nonatomic) int16_t pageno;
@property (nullable, nonatomic, copy) NSString *responds;

@end

NS_ASSUME_NONNULL_END
