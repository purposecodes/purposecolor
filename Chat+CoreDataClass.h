//
//  Chat+CoreDataClass.h
//  
//
//  Created by Purpose Code on 31/05/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Chat : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Chat+CoreDataProperties.h"
