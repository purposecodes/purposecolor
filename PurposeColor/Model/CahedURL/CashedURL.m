//
//  User.m
//  SignSpot
//
//  Created by Purpose Code on 10/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CashedURL.h"

@implementation CashedURL

+(CashedURL*)sharedManager {
    
    static CashedURL *currentUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentUser = [[self alloc] init];
    });
    return currentUser;
    
}

- (CashedURL*)init {
    if ( (self = [super init]) ) {
        // your custom initialization
        _dictCashedURLS = [NSMutableDictionary new];
    }
    return self;
}


@end
