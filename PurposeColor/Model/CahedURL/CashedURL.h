//
//  User.h
//  SignSpot
//
//  Created by Purpose Code on 10/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CashedURL : NSObject

@property (nonatomic,strong) NSMutableDictionary * dictCashedURLS;


+ (CashedURL*)sharedManager;

@end
