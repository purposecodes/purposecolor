//
//  APIMapper.h
//  SignSpot
//
//  Created by Purpose Code on 11/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface APIMapper : NSObject

+ (void)registerUserWithName:(NSString*)userName userEmail:(NSString*)email phoneNumber:(NSString*)phone countryID:(NSString*)country userPassword:(NSString*)password success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                     failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)loginUserWithUserName:(NSString*)userName userPassword:(NSString*)password success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                     failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)forgotPasswordWithEmail:(NSString*)email success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllCommunityGemsByUserID:(NSString*)userID pageNo:(NSInteger)pageNo type:(NSString*)type purposeGemShow:(BOOL)shouldShow shouldDisableCach:(BOOL)shouldDisableCach success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)likeAGEMWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllCommentsForAGemWithGemID:(NSString*)gemID gemType:(NSString*)gemType shareComment:(NSInteger)shareType pageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)postCommentWithUserID:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType comment:(NSString*)comment shareComment:(NSInteger)shareType commentID:(NSString*)commentID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)removeCommentWithCommentID:(NSString*)commentID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)sendFollowRequestWithUserID:(NSString*)userID followerID:(NSString*)followerID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getMyGemsByUserID:(NSString*)userID pageNo:(NSInteger)pageNo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)deleteAGEMWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)hideAGEMWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)shareAGEMToCommunityWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType shareMsg:(NSString*)shareMsg success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)loadAllMyGoalsAndDreamsWith:(NSString*)userID pageNo:(NSInteger)pageNo status:(NSInteger)status success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getGoalActionDetailsByGoalActionID:(NSString*)goalActionID actionID:(NSString*)actionID goalID:(NSString*)goalID andUserID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateGoalActionStatusWithActionID:(NSString*)actionID goalID:(NSString*)goalID goalActionID:(NSString*)goalActionID andUserID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllMyRemindersWith:(NSString*)userID pageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getPieChartViewWithUserID:(NSString*)userID startDate:(double)startDate endDate:(double)endDate session:(NSInteger)session isFirstTime:(BOOL)isFirstTime success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)deleteEmotionFromSupportingEmotionsWithEmotionID:(NSString*)emotionID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllGoalsWith:(NSString*)userID pageNo:(NSInteger)pageNo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllGEMEmotionsByUserID:(NSString*)userID pageNo:(NSInteger)pageNo isFirstTime:(BOOL)isFirstTime success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllEventsByUserID:(NSString*)userID groupID:(NSString*)groupID pageNo:(NSInteger)pageNo sortType:(NSInteger)sortType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)setPushNotificationTokenWithUserID:(NSString*)userID token:(NSString*)token success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateFollowRequestByUserID:(NSString*)userID followRequestID:(NSInteger)followRequestID status:(NSString*)status success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;
                                   
+ (void)getAllMyNotificationsWith:(NSString*)userID pageNumber:(NSInteger)pageNumber shouldDisableCach:(BOOL)shouldDisableCach success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllChatUsersWithUserID:(NSString*)userID pageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)postChatMessageWithUserID:(NSString*)userID toUserID:(NSString*)toUser message:(NSString*)message indexOfDB:(NSInteger)indexOfDB success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)loadChatHistoryWithFrom:(NSString*)userID toUserID:(NSString*)toUser page:(NSInteger)pageNo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)deleteChatWithIDs:(NSArray*)ids success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllEmotionsWithUserID:(NSString*)userID emotionValue:(NSInteger)emotionID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)addNewEmotionWithUserID:(NSString*)userID feelValue:(NSInteger)feelValue emotionTitle:(NSString*)title operationType:(NSInteger)type emotionID:(NSInteger)emotionID  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)addNewEventWithUserID:(NSString*)userID eventTitle:(NSString*)title operationType:(NSInteger)type eventID:(NSInteger)eventID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllEmotionalAwarenessGEMSListsWithUserID:(NSString*)userID gemType:(NSString*)gemType goalID:(NSInteger)goalID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+(void)createOrEditAGemWith:(NSArray*)dataSource eventTitle:(NSString*)title description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)adress address:(NSString*)locAddress contactName:(NSString*)conatctName gemID:(NSString*)gemID goalID:(NSString*)goalID deletedIDs:(NSMutableArray*)deletedIDs gemType:(NSString*)gemType hashTags:(NSMutableArray*)hashTags previewURL:(NSString*)previewurl imageNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+(void)shareGEMToPurposeColorWith:(NSArray*)dataSource eventTitle:(NSString*)title description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)adress address:(NSString*)locAddress contactName:(NSString*)conatctName gemID:(NSString*)gemID deletedIDs:(NSMutableArray*)deletedIDs gemType:(NSString*)gemType previewURL:(NSString*)previewurl hashTags:(NSMutableArray*)hashTags imageNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)createNewReminderForActionWithActionID:(NSInteger)goalActionID title:(NSString*)title descrption:(NSString*)description startDate:(double)startDate endDate:(double)endDate  startTime:(NSString*)startTime endTime:(NSString*)endTime actionAlert:(NSInteger)alertTime userID:(NSString*)userID repeatValue:(NSString*)repeatValue success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)addGemToFavouritesWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllFavouritesByUserID:(NSString*)userID pageNo:(NSInteger)pageNo type:(BOOL)isInspired success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)removeFromFavouritesWithUserID:(NSString*)userID favouriteID:(NSString*)favouriteID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getGemDetailsForEditWithGEMID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getGoalDetailsWithGoalID:(NSString*)goalID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllTodaysMemoryWithUserID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getUserProfileWithUserID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateUserProfileWithUserID:(NSString*)userID firstName:(NSString*)fname statusMsg:(NSString*)statusMsg uploadedImage:(NSString*)imageName canFollow:(BOOL)canFollow birthDay:(NSString*)Bday success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getUserListingsForATypeWithID:(NSString*)selectedID type:(NSString*)listType userID:(NSString*)userID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)addEmotionToSupportingEmotionsWithEmotionID:(NSString*)emotionID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)copyAGEMToNewGEMWithGEMID:(NSString*)gemID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)logoutFromAccount:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateBadgeCountWithType:(NSString*)type Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateUserSettingsWithUserID:(NSString*)userID canFollow:(BOOL)canFollow dailyNotification:(BOOL)dailyNotification adminListSuggestions:(NSMutableDictionary*)dictList confirmfollowRequest:(BOOL)confirmFollow success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)changePasswordWithCurrentPwd:(NSString*)currentPWD newPWD:(NSString*)newPWD confirmPWD:(NSString*)confirmPWD success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+(void)createOrEditAGoalWith:(NSArray*)dataSource eventTitle:(NSString*)title description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)locName address:(NSString*)locAddress contactName:(NSString*)conatctName gemID:(NSString*)gemID goalID:(NSString*)goalID deletedIDs:(NSMutableArray*)deletedIDs gemType:(NSString*)gemType achievementDate:(double)achievementDate status:(NSString*)status isPurposeColor:(BOOL)isPurposeColor hashTags:(NSMutableArray*)hashTags previewURL:(NSString*)previewurl imageNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames catID:(NSString*)catID OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+(void)createAJournelWith:(NSArray*)dataSource description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)locName address:(NSString*)locAddress contactName:(NSString*)conatctName hashTags:(NSMutableArray*)hashTags emotionAwarenssValues:(NSDictionary*)awarenessValues mediaNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllAbuseReasonsOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)postReportAbuseWithUserID:(NSString*)userID gemUserID:(NSString*)gemUserID gemID:(NSString*)gemID commentID:(NSString*)commentID replyID:(NSString*)replyID type:(NSString*)type commenttext:(NSString*)commenttext replytext:(NSString*)replytext option:(NSInteger)option otherInfo:(NSString*)otherInfo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getWebContentWithType:(NSString*)type success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)checkUserHasEntryInPurposeColorOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getUserJournalWithUserID:(NSString*)userID page:(NSInteger)pageNo region:(NSString*)region Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAnyGemDetailsWithGEMID:(NSString*)gemID gemType:(NSString*)gemType userID:(NSString*)userId success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)deleteJournalWithJournalID:(NSString*)journalID userID:(NSString*)userId success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getVersionStatusOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getUserInfoOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllCommentsForAJournalWithJournalID:(NSString*)journalID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)postJournalCommentWithUserID:(NSString*)userID journalID:(NSString*)journalID editID:(NSString*)editID comment:(NSString*)comment  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)removeJournalCommentWithCommentID:(NSString*)commentID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)socialMediaRegistrationnWithFirstName:(NSString*)firstName profileImage:(NSString*)profileImg fbID:(NSString*)fbID googleID:(NSString*)googleID email:(NSString*)email success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                     failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)deleteOrHideMyAccountWithType:(NSString*)type success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)enableUserAccountOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllTaggedGemsByPageNo:(NSInteger)pageNo keyWord:(NSString*)keyWord isPublic:(BOOL)isPublic success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllRepliesForACommentWith:(NSString*)gemID commentID:(NSString*)commentID pageNo:(NSInteger)pageNo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)postReplyForACommentWithCommentID:(NSString*)commentID gemID:(NSString*)gemID reply:(NSString*)mesage replyID:(NSString*)replyID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)removeReplyWithCommentID:(NSString*)commentID replyID:(NSString*)replyID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)likeReplyOrCommentWithType:(NSString*)type gemID:(NSString*)gemID selectedID:(NSString*)selectedID emojiCode:(NSInteger)emojiCode gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getNotificationCountOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)postFeedBackWithName:(NSString*)name email:(NSString*)email description:(NSString*)description success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)hideAGoalWithGoalID:(NSString*)goalID goalActionID:(NSString*)goalActionID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)blockUserWith:(NSString*)userID type:(NSString*)block Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)loadBlockedUserListOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)inviteToChatWithChatUserID:(NSString*)chatUserID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateChatRequestByUserID:(NSString*)userID chatReqID:(NSInteger)chatReqID status:(NSString*)status success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)blockChatByUserID:(NSString*)userID status:(NSString*)block success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)clearChatBadgeWithUserID:(NSString*)chatUserID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getFollowingUserList:(NSInteger)type forTheUser:(NSString*)userID pageNo:(NSInteger)pageNo Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)refershJournalDetailsWithJournalID:(NSString*)journalID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)uploadMediasAtPath:(NSString*)path orAsObject:(UIImage*)image type:(NSString*)type Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)verifyUserProfileWithName:(NSString*)name email:(NSString*)email address:(NSString*)address phone:(NSString*)phone uploadedImage:(NSString*)imageName success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getBaseURLOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAdminMessagesIfAnyOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateBackendAdminMessageReadOnSucces:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getBdayCountOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getBirthDayUserListOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)postBdayWishesToUserID:(NSString*)userID OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getWishedUserListForTheUserID:(NSString*)userID OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getUserInfoMessageOnCommunityPageOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)searchUserNameWithText:(NSString*)strText onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getSearchedUserPostsLisitngWithPageNo:(NSInteger)pageNo toUserID:(NSString*)userID shouldDisableCach:(BOOL)shouldDisableCach onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updatePostDetailPageWithGemID:(NSString*)gemID gemType:(NSString*)gemType onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateGoalStatusWithGoalID:(NSString*)goalID status:(NSInteger)status onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllGoalCategoriesOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)saveInterestsWithIDs:(NSMutableArray*)interests onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getSurveyDetailsOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

@end
