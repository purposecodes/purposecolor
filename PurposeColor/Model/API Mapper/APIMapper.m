//
//  APIMapper.m
//  SignSpot
//
//  Created by Purpose Code on 11/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kUnAuthorized           403
#define kMaintenanceMode        503

#import "APIMapper.h"
#import "Constants.h"

@implementation APIMapper

+ (void)registerUserWithName:(NSString*)userName userEmail:(NSString*)email phoneNumber:(NSString*)phone countryID:(NSString*)country userPassword:(NSString*)password success:(void (^)(AFHTTPRequestOperation *task, id responseObject))success
                     failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=register",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"firstname": userName,
                             @"email": email,
                             @"password":password,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)loginUserWithUserName:(NSString*)userName userPassword:(NSString*)password success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                      failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=login",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"email": userName,
                             @"password": password,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}


+ (void)forgotPasswordWithEmail:(NSString*)email success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=forgotpassword",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"email": email,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)getAllCommunityGemsByUserID:(NSString*)userID pageNo:(NSInteger)pageNo type:(NSString*)type purposeGemShow:(BOOL)shouldShow shouldDisableCach:(BOOL)shouldDisableCach success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getallcommunitygems",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:type forKey:@"type"];
    [params setObject:[NSNumber numberWithInteger:pageNo] forKey:@"page_no"];
    if (shouldDisableCach) [params setObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] forKey:@"cache_disable"];    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)likeAGEMWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=like",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"user_id": userID,
                             @"gem_id": gemID,
                             @"gem_type": gemType
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)addGemToFavouritesWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=addtofavorite",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"gem_id": gemID,
                             @"gem_type": gemType
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}



+ (void)getAllCommentsForAGemWithGemID:(NSString*)gemID gemType:(NSString*)gemType shareComment:(NSInteger)shareType pageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getcomments",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"gem_id": gemID,
                             @"gem_type": gemType,
                             @"user_id": [User sharedManager].userId,
                             @"share_comment": [NSNumber numberWithInteger:shareType],
                             @"count": [NSNumber numberWithInteger:50],
                             @"page_no": [NSNumber numberWithInteger:pageNumber]
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}


+ (void)postCommentWithUserID:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType comment:(NSString*)comment shareComment:(NSInteger)shareType commentID:(NSString*)commentID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=addcomments",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:gemID forKey:@"gem_id"];
    [params setObject:gemType forKey:@"gem_type"];
    [params setObject:[NSNumber numberWithInteger:shareType] forKey:@"share_comment"];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:comment forKey:@"comment_txt"];
    if (commentID.length) [params setObject:commentID forKey:@"comment_id"];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)removeCommentWithCommentID:(NSString*)commentID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=removecomment",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"comment_id": commentID,
                             @"user_id": [User sharedManager].userId,
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getAllRepliesForACommentWith:(NSString*)gemID commentID:(NSString*)commentID pageNo:(NSInteger)pageNo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getreply",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"gem_id": gemID,
                             @"comment_id": commentID,
                             @"page_no": [NSNumber numberWithInteger:pageNo],
                             @"count": [NSNumber numberWithInteger:50]
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)postReplyForACommentWithCommentID:(NSString*)commentID gemID:(NSString*)gemID reply:(NSString*)mesage replyID:(NSString*)replyID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=addreply",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:gemID forKey:@"gem_id"];
    [params setObject:mesage forKey:@"reply_txt"];
    [params setObject:commentID forKey:@"comment_id"];
    [params setObject:gemType forKey:@"gem_type"];
    if (replyID.length) [params setObject:replyID forKey:@"reply_id"];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}


+ (void)likeReplyOrCommentWithType:(NSString*)type gemID:(NSString*)gemID selectedID:(NSString*)selectedID emojiCode:(NSInteger)emojiCode gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=commentlike",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"gem_id": gemID,
                             @"type": type,
                             @"select_id": selectedID,
                             @"emoji_code": [NSNumber numberWithInteger:emojiCode],
                             @"gem_type": gemType
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}



+ (void)removeReplyWithCommentID:(NSString*)commentID replyID:(NSString*)replyID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=removereply",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"comment_id": commentID,
                             @"reply_id": replyID,
                             @"user_id": [User sharedManager].userId,
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)sendFollowRequestWithUserID:(NSString*)userID followerID:(NSString*)followerID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=follow",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"followid": followerID
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getMyGemsByUserID:(NSString*)userID pageNo:(NSInteger)pageNo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=mygems",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"page_no": [NSNumber numberWithInteger:pageNo]
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)deleteAGEMWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=deletegem",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"gem_id": gemID,
                             @"gem_type": gemType
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)hideAGEMWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=hidegem",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"gem_id": gemID,
                             @"gem_type": gemType
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)shareAGEMToCommunityWith:(NSString*)userID gemID:(NSString*)gemID gemType:(NSString*)gemType shareMsg:(NSString*)shareMsg success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=sharetocommunity",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:gemType forKey:@"gem_type"];
    [params setObject:gemID forKey:@"gem_id"];
    if (shareMsg.length > 0) [params setObject:shareMsg forKey:@"share_msg"];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)loadAllMyGoalsAndDreamsWith:(NSString*)userID pageNo:(NSInteger)pageNo status:(NSInteger)status success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=goalsanddreams",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"page_no": [NSNumber numberWithInteger:pageNo],
                             @"status": [NSNumber numberWithInteger:status],
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)getGoalActionDetailsByGoalActionID:(NSString*)goalActionID actionID:(NSString*)actionID goalID:(NSString*)goalID andUserID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getactiondetails",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"goalaction_id": goalActionID,
                             @"user_id": userID,
                             @"goal_id": goalID
                             //@"action_id": actionID
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)updateGoalActionStatusWithActionID:(NSString*)actionID goalID:(NSString*)goalID goalActionID:(NSString*)goalActionID  andUserID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=updategoalaction",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"goal_id": goalID,
                             //@"action_id": actionID,
                             @"goalaction_id": goalActionID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getPieChartViewWithUserID:(NSString*)userID startDate:(double)startDate endDate:(double)endDate session:(NSInteger)session isFirstTime:(BOOL)isFirstTime success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=emotional_intelligence",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"user_id"];
    if (startDate > 0) [params setObject:[NSNumber numberWithDouble:startDate] forKey:@"start_date"];
    if (endDate > 0) [params setObject:[NSNumber numberWithDouble:endDate] forKey:@"end_date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"ZZZZZ"];
    [params setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"timezone"];
    [params setObject:[NSNumber numberWithInteger:session] forKey:@"time_at"];
    [params setObject:[NSNumber numberWithBool:isFirstTime] forKey:@"first_time"];
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)setPushNotificationTokenWithUserID:(NSString*)userID token:(NSString*)token success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=setusertoken",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if (userID.length > 0) {
        NSDictionary *params = @{@"user_id": userID,
                                 @"registration_id": token,
                                 @"device_os" : @"ios"
                                 };
        
        [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            success(operation,responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
            failure (operation,error);
        }];
    }
    
    
}


+ (void)updateFollowRequestByUserID:(NSString*)userID followRequestID:(NSInteger)followRequestID status:(NSString*)status success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=updatefollowstatus",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"follow_status": status,
                             @"followrequest_id" : [NSNumber numberWithInteger:followRequestID]
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getAllMyNotificationsWith:(NSString*)userID pageNumber:(NSInteger)pageNumber shouldDisableCach:(BOOL)shouldDisableCach success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getallnotifications",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:[NSNumber numberWithInteger:pageNumber] forKey:@"page_no"];
    if (shouldDisableCach) [params setObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] forKey:@"cache_disable"];
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getAllMyRemindersWith:(NSString*)userID pageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getallreminder",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"page_no": [NSNumber numberWithInteger:pageNumber]
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}


+ (void)getAllChatUsersWithUserID:(NSString*)userID pageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=chatuserlist",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)postChatMessageWithUserID:(NSString*)userID toUserID:(NSString*)toUser message:(NSString*)message indexOfDB:(NSInteger)indexOfDB success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=chatmessage",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"from_id"];
    [params setObject:toUser forKey:@"to_id"];
    [params setObject:message forKey:@"msg"];
    if (indexOfDB >= 0)[params setObject:[NSNumber numberWithInteger:indexOfDB] forKey:@"index"];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)loadChatHistoryWithFrom:(NSString*)userID toUserID:(NSString*)toUser page:(NSInteger)pageNo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=chathistory",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"from_id": userID,
                             @"to_id": toUser,
                             @"page_no": [NSNumber numberWithInteger:pageNo]
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)deleteChatWithIDs:(NSArray*)ids success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=deletechat",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"chat_ids": ids,
                             @"user_id": [User sharedManager].userId,
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getAllEmotionsWithUserID:(NSString*)userID emotionValue:(NSInteger)emotionID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=emotions",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"emotion_value" : [NSNumber numberWithInteger:emotionID]
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)addNewEmotionWithUserID:(NSString*)userID feelValue:(NSInteger)feelValue emotionTitle:(NSString*)title operationType:(NSInteger)type emotionID:(NSInteger)emotionID  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=manageemotion",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"emotion_value" : [NSNumber numberWithInteger:feelValue],
                             @"emotion_title": title,
                             @"type" : [NSNumber numberWithInteger:type],
                             @"emotion_id" : [NSNumber numberWithInteger:emotionID],
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getAllEmotionalAwarenessGEMSListsWithUserID:(NSString*)userID gemType:(NSString*)gemType goalID:(NSInteger)goalID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getallgems",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    [params setObject:gemType forKey:@"gem_type"];
    if (goalID > 0) [params setObject:[NSNumber numberWithInteger:goalID] forKey:@"goal_id"];
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}



+(void)createOrEditAGemWith:(NSArray*)dataSource eventTitle:(NSString*)title description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)locName address:(NSString*)locAddress contactName:(NSString*)conatctName gemID:(NSString*)gemID goalID:(NSString*)goalID deletedIDs:(NSMutableArray*)deletedIDs gemType:(NSString*)gemType hashTags:(NSMutableArray*)hashTags previewURL:(NSString*)previewurl imageNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=gemaction",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    if (title.length > 0) [params setObject:title forKey:@"title"];
    if (descritption.length > 0)  [params setObject:descritption forKey:@"details"];
    if (previewurl.length > 0)  [params setObject:previewurl forKey:@"preview_url"];
    [params setObject:gemType forKey:@"gem_type"];
    if (hashTags.count)[params setObject:hashTags forKey:@"hash_tags"];
    if (locName.length){
        NSMutableDictionary *location = [NSMutableDictionary new];
        [location setObject:locName forKey:@"location_name"];
        if (locAddress.length)[location setObject:locAddress forKey:@"location_address"];
        [location setObject:[NSNumber numberWithDouble:latitude] forKey:@"location_latitude"];
        [location setObject:[NSNumber numberWithDouble:longitude] forKey:@"location_longitude"];
        [params setObject:location forKey:@"location"];
        
    }
    if (conatctName.length) [params setObject:conatctName forKey:@"contact_name"];
    if (gemID.length) [params setObject:gemID forKey:@"gem_id"];
    if (goalID.length) [params setObject:goalID forKey:@"goal_id"];
    NSInteger mediacount = dataSource.count;
    if (gemID.length){
        mediacount = 0;
        for (id obj in dataSource) {
            if (![obj isKindOfClass:[NSDictionary class]])
            mediacount ++;
        }
    }
    [params setObject:[NSNumber numberWithInteger:mediacount] forKey:@"media_count"];
    [params setObject:deletedIDs forKey:@"media_id"];
    [params setObject:mediaNames forKey:@"media_name"];
    [params setObject:thumbNames forKey:@"media_thumb"];
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *jsonEvent = [NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"json_event", nil];
    
    [manager POST:urlString parameters:jsonEvent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+(void)createOrEditAGoalWith:(NSArray*)dataSource eventTitle:(NSString*)title description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)locName address:(NSString*)locAddress contactName:(NSString*)conatctName gemID:(NSString*)gemID goalID:(NSString*)goalID deletedIDs:(NSMutableArray*)deletedIDs gemType:(NSString*)gemType achievementDate:(double)achievementDate status:(NSString*)status isPurposeColor:(BOOL)isPurposeColor hashTags:(NSMutableArray*)hashTags previewURL:(NSString*)previewurl imageNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames catID:(NSString*)catID OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=gemaction",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    if (title.length > 0) [params setObject:title forKey:@"title"];
    if (descritption.length > 0)  [params setObject:descritption forKey:@"details"];
    if (previewurl.length > 0)  [params setObject:previewurl forKey:@"preview_url"];
    if (catID.length > 0) [params setObject:catID forKey:@"category_id"];
    [params setObject:gemType forKey:@"gem_type"];
    if (hashTags.count)[params setObject:hashTags forKey:@"hash_tags"];
    
    if (locName.length){
        NSMutableDictionary *location = [NSMutableDictionary new];
        [location setObject:locName forKey:@"location_name"];
        if (locAddress.length)[location setObject:locAddress forKey:@"location_address"];
        [location setObject:[NSNumber numberWithDouble:latitude] forKey:@"location_latitude"];
        [location setObject:[NSNumber numberWithDouble:longitude] forKey:@"location_longitude"];
        [params setObject:location forKey:@"location"];
        
    }
    [params setObject:[NSNumber numberWithDouble:achievementDate] forKey:@"goal_enddate"];
    NSInteger _status = 0;
    if ([status isEqualToString:@"Completed"]) {
        _status = 1;
    }
    [params setObject:[NSNumber numberWithInteger:_status] forKey:@"goal_status"];
    if (gemID.length){
        if (isPurposeColor) [params setObject:gemID forKey:@"purposegem_id"];
        else [params setObject:gemID forKey:@"gem_id"];
    }
    if (conatctName.length) [params setObject:conatctName forKey:@"contact_name"];
    if (goalID.length) [params setObject:goalID forKey:@"goal_id"];
    NSInteger mediacount = dataSource.count;
    if (gemID.length){
        mediacount = 0;
        for (id obj in dataSource) {
            if (![obj isKindOfClass:[NSDictionary class]])
            mediacount ++;
        }
    }
    [params setObject:[NSNumber numberWithInteger:mediacount] forKey:@"media_count"];
    [params setObject:deletedIDs forKey:@"media_id"];
    [params setObject:mediaNames forKey:@"media_name"];
    [params setObject:thumbNames forKey:@"media_thumb"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *jsonEvent = [NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"json_event", nil];
    [manager POST:urlString parameters:jsonEvent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+(void)shareGEMToPurposeColorWith:(NSArray*)dataSource eventTitle:(NSString*)title description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)locName address:(NSString*)locAddress contactName:(NSString*)conatctName gemID:(NSString*)gemID deletedIDs:(NSMutableArray*)deletedIDs gemType:(NSString*)gemType previewURL:(NSString*)previewurl hashTags:(NSMutableArray*)hashTags imageNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=savepost",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    if (title.length > 0) [params setObject:title forKey:@"title"];
    if (descritption.length > 0)  [params setObject:descritption forKey:@"details"];
    if (previewurl.length > 0)  [params setObject:previewurl forKey:@"preview_url"];
    if (hashTags.count)[params setObject:hashTags forKey:@"hash_tags"];
    [params setObject:gemType forKey:@"gem_type"];
    if (locName.length){
        NSMutableDictionary *location = [NSMutableDictionary new];
        [location setObject:locName forKey:@"location_name"];
        if (locAddress.length)[location setObject:locAddress forKey:@"location_address"];
        [location setObject:[NSNumber numberWithDouble:latitude] forKey:@"location_latitude"];
        [location setObject:[NSNumber numberWithDouble:longitude] forKey:@"location_longitude"];
        [params setObject:location forKey:@"location"];
        
    }
    if (conatctName.length) [params setObject:conatctName forKey:@"contact_name"];
    if (gemID.length) [params setObject:gemID forKey:@"gem_id"];
    NSInteger mediacount = dataSource.count;
    if (gemID.length){
        mediacount = 0;
        for (id obj in dataSource) {
            if (![obj isKindOfClass:[NSDictionary class]])
            mediacount ++;
        }
    }
    [params setObject:[NSNumber numberWithInteger:mediacount] forKey:@"media_count"];
    [params setObject:deletedIDs forKey:@"media_id"];
    
    [params setObject:mediaNames forKey:@"media_name"];
    [params setObject:thumbNames forKey:@"media_thumb"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *jsonEvent = [NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"json_event", nil];
    
    [manager POST:urlString parameters:jsonEvent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+(void)createAJournelWith:(NSArray*)dataSource description:(NSString*)descritption latitude:(double)latitude longitude:(double)longitude locName:(NSString*)locName address:(NSString*)locAddress contactName:(NSString*)conatctName hashTags:(NSMutableArray*)hashTags  emotionAwarenssValues:(NSDictionary*)awarenessValues mediaNames:(NSMutableArray*)mediaNames thumbNames:(NSMutableArray*)thumbNames OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=savejournal",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    if (descritption.length)[params setObject:descritption forKey:@"journal_desc"];
    if (hashTags.count)[params setObject:hashTags forKey:@"hash_tags"];
    if (locName.length){
        //NSMutableDictionary *location = [NSMutableDictionary new];
        [params setObject:locName forKey:@"location_name"];
        if (locAddress.length)[params setObject:locAddress forKey:@"location_address"];
        [params setObject:[NSNumber numberWithDouble:latitude] forKey:@"location_latitude"];
        [params setObject:[NSNumber numberWithDouble:longitude] forKey:@"location_longitude"];
        //[params setObject:location forKey:@"location"];
        
    }
    
    [params setObject:mediaNames forKey:@"media_name"];
    [params setObject:thumbNames forKey:@"media_thumb"];
    
    if (conatctName.length) [params setObject:conatctName forKey:@"contact_name"];
    NSInteger mediacount = dataSource.count;
    [params setObject:[NSNumber numberWithInteger:mediacount] forKey:@"media_count"];
    
    NSMutableDictionary *jounalValues = [NSMutableDictionary new];
    if ([params count]) {
        [jounalValues setObject:params forKey:@"json_journal"];
    }
    
    if ([awarenessValues count]) {
        [jounalValues setObject:awarenessValues forKey:@"json_awareness"];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jounalValues options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSDictionary *json = @{@"json": jsonString};
    
    [manager POST:urlString parameters:json success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}


+ (void)createNewReminderForActionWithActionID:(NSInteger)goalActionID title:(NSString*)title descrption:(NSString*)description startDate:(double)startDate endDate:(double)endDate  startTime:(NSString*)startTime endTime:(NSString*)endTime actionAlert:(NSInteger)alertTime userID:(NSString*)userID repeatValue:(NSString*)repeatValue success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=newreminder",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"ZZZZZ"];
    
    NSDictionary *params = @{@"user_id": userID,
                             @"goalaction_id": [NSNumber numberWithInteger:goalActionID],
                             @"reminder_startdate": [NSNumber numberWithLong:startDate],
                             @"reminder_starttime": startTime,
                             @"reminder_enddate": [NSNumber numberWithLong:endDate],
                             @"reminder_endtime": endTime,
                             @"reminder_title": title,
                             @"reminder_desc": description,
                             @"reminder_alert": [NSNumber numberWithInteger:alertTime],
                             @"reminder_repeat": repeatValue,
                             @"timezone": [dateFormatter stringFromDate:[NSDate date]],
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}


+ (void)getAllFavouritesByUserID:(NSString*)userID pageNo:(NSInteger)pageNo type:(BOOL)isInspired success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    NSString *urlString = [NSString stringWithFormat:@"%@action=getallsavedgems",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"page_no": [NSNumber numberWithInteger:pageNo],
                             };
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)removeFromFavouritesWithUserID:(NSString*)userID favouriteID:(NSString*)favouriteID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=removesavedgem",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"gem_id": favouriteID,
                             @"gem_type": gemType
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getGemDetailsForEditWithGEMID:(NSString*)gemID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=editgem",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"gem_id": gemID,
                             @"gem_type": gemType
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)getAnyGemDetailsWithGEMID:(NSString*)gemID gemType:(NSString*)gemType userID:(NSString*)userId success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=gemdetails",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"gem_id": gemID,
                             @"gem_type": gemType,
                             @"user_id" : userId
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}
+ (void)getGoalDetailsWithGoalID:(NSString*)goalID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getgoaldetails",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"goal_id": goalID,
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)getAllTodaysMemoryWithUserID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    NSString *urlString = [NSString stringWithFormat:@"%@action=memories",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)getUserProfileWithUserID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getuserprofile",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)updateUserProfileWithUserID:(NSString*)userID firstName:(NSString*)fname statusMsg:(NSString*)statusMsg uploadedImage:(NSString*)imageName canFollow:(BOOL)canFollow birthDay:(NSString*)Bday success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=editprofile",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:fname forKey:@"firstname"];
    if (imageName.length) [params setObject:imageName forKey:@"profile_image"];
    if (statusMsg.length)  [params setObject:statusMsg forKey:@"status"];
    if (Bday.length)  [params setObject:Bday forKey:@"dob"];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)updateUserSettingsWithUserID:(NSString*)userID canFollow:(BOOL)canFollow dailyNotification:(BOOL)dailyNotification adminListSuggestions:(NSMutableDictionary*)dictList confirmfollowRequest:(BOOL)confirmFollow success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=editsettings",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:[NSNumber numberWithBool:canFollow] forKey:@"can_follow"];
    [params setObject:[NSNumber numberWithBool:dailyNotification] forKey:@"daily_notify"];
    [params setObject:[NSNumber numberWithBool:confirmFollow] forKey:@"follow_confirm"];
    
    if ([dictList objectForKey:@"Show_Admin_Goals"]) {
        [params setObject:[dictList objectForKey:@"Show_Admin_Goals"] forKey:@"admin_goal"];
    }
    if ([dictList objectForKey:@"Show_Admin_Emotions"]) {
        [params setObject:[dictList objectForKey:@"Show_Admin_Emotions"] forKey:@"admin_emotion"];
    }
    if ([dictList objectForKey:@"Show_Admin_Journal"]) {
        [params setObject:[dictList objectForKey:@"Show_Admin_Journal"] forKey:@"admin_journal"];
    }
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getUserListingsForATypeWithID:(NSString*)selectedID type:(NSString*)listType userID:(NSString*)userID gemType:(NSString*)gemType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=userlist",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (selectedID.length)  [params setObject:selectedID forKey:@"gem_id"];
    if (listType.length)  [params setObject:listType forKey:@"type"];
    if (userID.length)  [params setObject:userID forKey:@"user_id"];
    if (gemType.length)  [params setObject:gemType forKey:@"gem_type"];
    
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)addEmotionToSupportingEmotionsWithEmotionID:(NSString*)emotionID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=addtosupportemotion",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"emotion_id": emotionID,
                             @"user_id": userID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)deleteEmotionFromSupportingEmotionsWithEmotionID:(NSString*)emotionID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=removesupportemotion",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"emotion_id": emotionID,
                             @"user_id": userID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)logoutFromAccount:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=logout",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)changePasswordWithCurrentPwd:(NSString*)currentPWD newPWD:(NSString*)newPWD confirmPWD:(NSString*)confirmPWD success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=changepassword",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": [User sharedManager].userId,
                             @"oldpass": currentPWD,
                             @"newpass": newPWD,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)updateBadgeCountWithType:(NSString*)type Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=updatebadge",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": [User sharedManager].userId,
                             @"type":type
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}
+ (void)getAllAbuseReasonsOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=abusereasons",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)postReportAbuseWithUserID:(NSString*)userID gemUserID:(NSString*)gemUserID gemID:(NSString*)gemID commentID:(NSString*)commentID replyID:(NSString*)replyID type:(NSString*)type commenttext:(NSString*)commenttext replytext:(NSString*)replytext option:(NSInteger)option otherInfo:(NSString*)otherInfo success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=sendreportabuse",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (otherInfo.length)[params setObject:otherInfo forKey:@"other_info"];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:gemUserID forKey:@"touser_id"];
    [params setObject:gemID forKey:@"gem_id"];
    
    if (commentID.length) [params setObject:commentID forKey:@"comment_id"];
    if (replyID.length) [params setObject:replyID forKey:@"reply_id"];
    if (type.length) [params setObject:type forKey:@"type"];
    if (commenttext.length) [params setObject:commenttext forKey:@"comment_txt"];
    if (replytext.length) [params setObject:replytext forKey:@"reply_txt"];
    
    if (option >= 0)[params setObject:[NSNumber numberWithInteger:option] forKey:@"abuse_id"];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)getWebContentWithType:(NSString*)type success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=webcontent",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"type": type,
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)checkUserHasEntryInPurposeColorOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=listusersettings",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": [User sharedManager].userId,
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}
+ (void)getUserJournalWithUserID:(NSString*)userID page:(NSInteger)pageNo region:(NSString*)region Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=listjournal",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (region.length)[params setObject:region forKey:@"region"];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:[NSNumber numberWithInteger:pageNo] forKey:@"page_no"];
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
    
}
+ (void)deleteJournalWithJournalID:(NSString*)journalID userID:(NSString*)userId success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=hidejournal",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userId,
                             @"journal_id": journalID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getVersionStatusOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=version_update",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"device": @"ios",
                             };
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getUserInfoOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=alert_message",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)getAllCommentsForAJournalWithJournalID:(NSString*)journalID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=listjournalcomment",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"journal_id": journalID,
                             @"user_id": [User sharedManager].userId,
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)postJournalCommentWithUserID:(NSString*)userID journalID:(NSString*)journalID editID:(NSString*)editID comment:(NSString*)comment success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=addjournalcomment",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (editID.length)[params setObject:editID forKey:@"journalcomment_id"];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:journalID forKey:@"journal_id"];
    [params setObject:comment forKey:@"comment_txt"];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)removeJournalCommentWithCommentID:(NSString*)commentID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=removejournalcomment",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"journalcomment_id": commentID,
                             @"user_id": [User sharedManager].userId,
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)socialMediaRegistrationnWithFirstName:(NSString*)firstName profileImage:(NSString*)profileImg fbID:(NSString*)fbID googleID:(NSString*)googleID email:(NSString*)email success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                      failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=socialmedialogin",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (email.length)[params setObject:email forKey:@"email"];
    if (profileImg.length)[params setObject:profileImg forKey:@"profileimg"];
    if (firstName.length)[params setObject:firstName forKey:@"firstname"];
    if (fbID.length)[params setObject:fbID forKey:@"fbid"];
    if (googleID.length)[params setObject:googleID forKey:@"googleid"];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)deleteOrHideMyAccountWithType:(NSString*)type success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=deleteaccount",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:type forKey:@"type"];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)enableUserAccountOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=enableaccount",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    [manager POST:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
    
}

+ (void)getAllTaggedGemsByPageNo:(NSInteger)pageNo keyWord:(NSString*)keyWord isPublic:(BOOL)isPublic success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=gettagedgems",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"page_no": [NSNumber numberWithInteger:pageNo],
                             @"tag_keyword": keyWord,
                             @"isPublic": [NSNumber numberWithBool:isPublic]
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}


+ (void) getNotificationCountOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getnotificationcount",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)postFeedBackWithName:(NSString*)name email:(NSString*)email description:(NSString*)description success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    NSString *urlString = [NSString stringWithFormat:@"%@action=feedback",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"name": name,
                             @"email": email,
                             @"msg": description
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)hideAGoalWithGoalID:(NSString*)goalID goalActionID:(NSString*)goalActionID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=deletegoal",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:goalID forKey:@"goal_id"];
    if (goalActionID.length) {
        [params setObject:goalActionID forKey:@"goalaction_id"];
    }
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)blockUserWith:(NSString*)userID type:(NSString*)block Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=block",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"touserid": userID,
                             @"type": block
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)loadBlockedUserListOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=blockeduserlist",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)inviteToChatWithChatUserID:(NSString*)chatUserID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=chatrequest",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"touserid": chatUserID,
                             };
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}


+ (void)updateChatRequestByUserID:(NSString*)userID chatReqID:(NSInteger)chatReqID status:(NSString*)status success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=updatechatstatus",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"user_id": userID,
                             @"chat_status": status,
                             @"chatrequest_id" : [NSNumber numberWithInteger:chatReqID]
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)blockChatByUserID:(NSString*)userID status:(NSString*)block success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=chatblock",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    NSDictionary *params = @{@"touserid": userID,
                             @"type": block
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}
+ (void)clearChatBadgeWithUserID:(NSString*)chatUserID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=updatechatbadge",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"to_userid": chatUserID,
                             };
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getFollowingUserList:(NSInteger)type forTheUser:(NSString*)userID pageNo:(NSInteger)pageNo Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getfollowuserlist",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"type": [NSNumber numberWithInteger:type],
                             @"user_id": userID,
                              @"page_no": [NSNumber numberWithInteger:pageNo],
                             };
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)uploadMediasAtPath:(NSString*)objectPath orAsObject:(UIImage*)image type:(NSString*)type Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=imageupload",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (type.length) [params setObject:type forKey:@"type"];
    else [params setObject:@"gem" forKey:@"type"];
    
    AFHTTPRequestOperation *operation = [manager POST:urlString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if ([type isEqualToString:@"profile"] || [type isEqualToString:@"doc"]) {
            if (image) {
                NSData *data;
                if (image) data =  UIImageJPEGRepresentation(image, 0.4);
                [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"media_name"] fileName:@"Media" mimeType:@"image/jpeg"];
            }
        }
        else{
            
            if([[NSFileManager defaultManager] fileExistsAtPath:objectPath])
            {
                NSData *data = [[NSFileManager defaultManager] contentsAtPath:objectPath];
                
                if ([[objectPath pathExtension] isEqualToString:@"jpeg"]){
                    [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"media_name"] fileName:@"Media" mimeType:@"image/jpeg"];
                }
                else if ([[objectPath pathExtension] isEqualToString:@"mp4"]){
                    [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"media_name"] fileName:@"Media" mimeType:@"video/mp4"];
                    UIImage *thumbnail = [Utility getThumbNailFromVideoURL:objectPath];
                    NSData *imageData = UIImageJPEGRepresentation(thumbnail,1);
                    if (imageData.length)
                    [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"media_thumb"] fileName:@"Media" mimeType:@"image/jpeg"];
                }
                else if ([[objectPath pathExtension] isEqualToString:@"aac"]){
                    [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"media_name"] fileName:@"Media" mimeType:@"audio/aac"];
                }
            }
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
        
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        
        
    }];
    
}
+ (void)verifyUserProfileWithName:(NSString*)name email:(NSString*)email address:(NSString*)address phone:(NSString*)phone uploadedImage:(NSString*)imageName success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=verify_request",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"email": email,
                             @"name": name,
                             @"address": address,
                             @"phone": phone,
                             @"attach_file": imageName
                             };
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getBaseURLOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getbaseurl",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getAdminMessagesIfAnyOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getpushmessages",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)updateBackendAdminMessageReadOnSucces:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=update_push",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager POST:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getBdayCountOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=show_birthday",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getBirthDayUserListOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getbirthday_users",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}


+ (void)postBdayWishesToUserID:(NSString*)userID OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=wish_birthday",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"to_userid": userID,
                             };
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getWishedUserListForTheUserID:(NSString*)userID OnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=wish_userlist",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"to_userid": userID,
                             };
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getUserInfoMessageOnCommunityPageOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=messageinfo",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)searchUserNameWithText:(NSString*)strText onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=search_user",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    NSDictionary *params = @{@"keyword": strText,
                             };
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getSearchedUserPostsLisitngWithPageNo:(NSInteger)pageNo toUserID:(NSString*)userID shouldDisableCach:(BOOL)shouldDisableCach onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=mygems",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    
    
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSNumber numberWithInteger:pageNo] forKey:@"page_no"];
    [params setObject: userID forKey:@"to_userid"];
    if (shouldDisableCach) [params setObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] forKey:@"cache_disable"];    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
    
}

+ (void)updatePostDetailPageWithGemID:(NSString*)gemID gemType:(NSString*)gemType onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=update_viewcount",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"gem_id": gemID,
                             @"gem_type": gemType
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)updateGoalStatusWithGoalID:(NSString*)goalID status:(NSInteger)status onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=update_goalstatus",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"goal_id": goalID,
                             @"status": [NSNumber numberWithInteger:status]
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getAllGoalCategoriesOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=getallcategory",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
  
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)saveInterestsWithIDs:(NSMutableArray*)interests onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@action=save_interests",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
    NSDictionary *params = @{@"interest": interests,
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

+ (void)getSurveyDetailsOnSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    NSString *urlString = [NSString stringWithFormat:@"%@action=show_popup",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];;
    if ( [User sharedManager].token) {
        [manager.requestSerializer setValue:[User sharedManager].token forHTTPHeaderField:@"Authorization"];
    }
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Device-Type"];
   
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app manageApiErrorCaseWithOperation:operation responds:operation.responseObject];
        failure (operation,error);
    }];
}

@end


