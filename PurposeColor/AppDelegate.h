//
//  AppDelegate.h
//  SignSpot
//
//  Created by Purpose Code on 09/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

 enum{
    
    eMenu_EmotionalAwareness = 0,
    eMenu_Emotional_Intelligence = 1,
    eMenu_Goals_Dreams = 2,
    eMenu_Community_GEMS = 3,
     
    eMenu_MyPosts = 4,
    eMenu_Journal = 5,
    eMenu_SavedGEMs = 6,
    eMenu_Notifications = 7,
    eMenu_Reminders = 8,
    eMenu_BlockedList = 9,
     
    eMenu_Privacy = 10,
    eMenu_Terms = 11,
    eMenu_Help = 12,
    eMenu_Invite_Friends = 13,
    eMenu_Share = 14,
    eMenu_Feedback = 15,
    eMenu_Logout = 16,
    
    eMenu_Memories = 17,
    eMenu_Favourites = 18,
    eMenu_Profile = 19,
    eMenu_Settings = 20,
    
    
     
 };typedef NSInteger EMenuActions ;


#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "User.h"
#import "LaunchPageViewController.h"
#import <AFNetworking/AFNetworking.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (nonatomic,strong) User *currentUser;
@property (strong, nonatomic)  UINavigationController *navGeneral;
@property (strong, nonatomic) LaunchPageViewController *launchPage;

-(void)goToHomeAfterLogin;
-(void)showLauchPage;
-(void)checkUserStatus;
- (void)saveContext;
-(void)enablePushNotification;
-(void)changeHomePageDynamicallyWithType:(EMenuActions)menu_type;
-(void)clearUserSessions;
-(void)showEmotionalAwarenessPage;
-(void)showJournalListView;
-(void)showCommubityGEMS;
-(void)showGoalsAndDreams;
-(void)logoutSinceUnAuthorized:(NSDictionary*)notification;
-(void)showIntelligenceJournalView;
-(void)showMaintainanceModeMessage;
-(void)manageApiErrorCaseWithOperation:(AFHTTPRequestOperation*)operation responds:(NSDictionary*)notification;

@end

