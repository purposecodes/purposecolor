

#import "Constants.h"


NSString * const StoryboardForLogin = @"Login&RegisterStoryBoard";
NSString * const HomeDetailsStoryBoard = @"HomeDetails";
NSString * const GEMDetailsStoryBoard = @"GEMDetails";
NSString * const ChatDetailsStoryBoard = @"ChatDetails";
NSString * const PROJECT_NAME = @"PurposeColor";
NSString * const NETWORK_ERROR_MESSAGE = @"Something went wrong!!";
NSString * const ExternalWebPageURL = @"";
NSString * const PlaceHolderImageGoal = @"default_goal.jpg";
NSString * const PlaceHolderImageAction = @"default_action.jpg";
NSString * const PlaceHolderImageEmotion = @"default_emotion.jpg";

NSString * const CommonFont = @"DroidSans";
NSString * const CommonFontBold = @"DroidSans-Bold";

NSString * const CommonFont_New = @"DroidSans";
NSString * const CommonFontBold_New = @"DroidSans-Bold";
NSString *AppVersion = @"1.4.8";


NSString *  BaseURLString = @"https://purposecolor.app/app/v1/api.php?";
//NSString *  BaseURLString = @"http://purposecodes.com/july/v1/api.php?";

NSString * const AppStoreURL = @"https://itunes.apple.com/us/app/purpose-color-life-success/id1186639523?ls=1&mt=8";

NSString * const GoogleMapAPIKey = @"AIzaSyDwcah6ptF0g4YVJObxiKzTNUPfbl1nUYE";

NSString * const StoryBoardIdentifierForLoginPage = @"LoginPage";
NSString * const StoryBoardIdentifierForMenuPage = @"MenuPage";
NSString * const StoryBoardIdentifierForHomePage = @"HomePage";
NSString * const StoryBoardIdentifierForRegistrationPage = @"RegistrationPage";
NSString * const StoryBoardIdentifierForCommunityGEMListings = @"CommunityGEMListings";
NSString * const StoryBoardIdentifierForCommentCompose = @"CommentComposeView";
NSString * const StoryBoardIdentifierForGEMDetailPage = @"ComminityGEMDetailPage";
NSString * const StoryBoardIdentifierForMyGEMS = @"MyGEMListings";
NSString * const StoryBoardIdentifierForGoalsDreams = @"Goals&Dreams";
NSString * const StoryBoardIdentifierForGoalsDreamsDetailPage = @"GoalsAndDreamsDetailPage";
NSString * const StoryBoardIdentifierForCreateActionMedias = @"CreateActionMedias";
NSString * const StoryBoardIdentifierForEventManager = @"EventManager";
NSString * const StoryBoardIdentifierForEmotionalIntelligence = @"EmotionalIntelligence";
NSString * const StoryBoardIdentifierForGEMListings = @"GEMListings";
NSString * const StoryBoardIdentifierForGEMSEventListings = @"EventListingPage";
NSString * const StoryBoardIdentifierForNotificationsListing = @"NotificationsListing";
NSString * const StoryBoardIdentifierForChatUserListings = @"ChatUserListings";
NSString * const StoryBoardIdentifierForChatComposer = @"ChatComposer";
NSString * const StoryBoardIdentifierForImotionalAwareness = @"ImotionalAwareness";
NSString * const StoryBoardIdentifierForImotionalAwarenessMedia = @"AwarenessMedia";
NSString * const StoryBoardIdentifierForReplyComment = @"ReplyComment";
NSString * const StoryBoardIdentifierForGemsUnderNotification = @"GemsUnderNotification";


NSString * const StoryBoardIdentifierForMyFavourites = @"MyFavourites";
NSString * const StoryBoardIdentifierForMyMemmories = @"MyMemmories";
NSString * const StoryBoardIdentifierForGoalDetails = @"GoalDetails";
NSString * const StoryBoardIdentifierForProfilePage = @"ProfilePage";
NSString * const StoryBoardIdentifierForLikedAndCommentedUsers = @"Liked&CommentedUserListings";
NSString * const StoryBoardIdentifierForWebBrowser = @"Browser";
NSString * const StoryBoardIdentifierForJournal = @"Journal";
NSString * const StoryBoardIdentifierForQuickGoalView = @"QuickGoalView";
NSString * const StoryBoardIdentifierForSettings = @"Settings";
NSString * const StoryBoardIdentifierForGEMWithHeaderListings = @"GEMWithHeaderListings";
NSString * const StoryBoardIdentifierForReminderListings = @"ReminderListings";
NSString * const StoryBoardIdentifierForLaunchPage = @"LaunchPage";
NSString * const StoryBoardIdentifierForReportAbuse = @"ReportAbuse";
NSString * const StoryBoardIdentifierForJournalListVC = @"JournalListVC";
NSString * const StoryBoardIdentifierForJournalDetailPage = @"JournalDetailPage";
NSString * const StoryBoardIdentifierForJournalGallery = @"JournalGallery";
NSString * const StoryBoardIdentifierForContactsPickerVC = @"ContactsPickerVC";
NSString * const StoryBoardIdentifierForJournalCommentView = @"Journal_Comment_View";
NSString * const StoryBoardIdentifierForIntroScreen = @"IntroScreen";
NSString * const StoryBoardIdentifierForHashTagListings = @"HashTagListings";
NSString * const StoryBoardIdentifierForHelpScreen = @"HelpScreen";
NSString * const StoryBoardIdentifierForImagePicker = @"CustomImagePicker";
NSString * const StoryBoardIdentifierForIntelligenceJournalView = @"IntelligenceJournalView";
NSString * const StoryBoardIdentifierForBlockedList = @"BlockedList";
NSString * const StoryBoardIdentifierForPushedImageDetialView = @"PushImageDetail";
NSString * const StoryBoardIdentifierForFollowList = @"FollowList";
NSString * const StoryBoardIdentifierForHashTagForSavedPosts = @"HashTagForSavedPosts";
NSString * const StoryBoardIdentifierForSplashScreen = @"SplashScreen";
NSString * const StoryBoardIdentifierForBdayVC = @"BdayVC";
NSString * const StoryBoardIdentifierForMaintainanceView = @"MaintainanceMode";

NSString * const StoryBoardIdentifierForSearchPosts = @"SearchPosts";
NSString * const StoryBoardIdentifierForSearchedUserPosts = @"SearchedUserPosts";
NSString * const StoryBoardIdentifierForGoalCategory = @"GoalCategory";

