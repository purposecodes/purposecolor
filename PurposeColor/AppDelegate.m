//
//  AppDelegate.m
//  SignSpot
//
//  Created by Purpose Code on 09/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define FACEBOOK_SCHEME         @"fb976918209110946"
#define SHARED_POST_SCHEME    @"purposecolorfbfeed"

#define kUnAuthorized           403
#define kMaintenanceMode        503
#define kUpdateMode             505


#import <UserNotifications/UserNotifications.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "AppDelegate.h"
#import "MenuViewController.h"
#import "Constants.h"
#import "ChatComposeViewController.h"
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterIOSStyle.h"
#import "ChatUserListingViewController.h"
#import "MyMemmoriesViewController.h"
#import "Reachability.h"
#import "CommunityGEMListingViewController.h"
#import "EmotionalAwarenessViewController.h"
#import "GEMSListingsViewController.h"
#import "GoalsAndDreamsListingViewController.h"
#import "EmotionalIntelligenceViewController.h"
#import "MyFavouritesListingViewController.h"
#import "NotificationsListingViewController.h"
#import "ProfilePageViewController.h"
#import "WebBrowserViewController.h"
#import "SettingsViewController.h"
#import "HomePageViewController.h"
#import "EventCreateViewController.h"
#import "ReminderListingViewController.h"
#import "LaunchPageViewController.h"
#import "JournalListViewController.h"
#import "IntroScreenViewController.h"
#import "LikedAndCommentedUserListings.h"
#import "BdayWishingViewController.h"

#import  <Bolts/Bolts.h>
#import "GemsUnderNotificationDetail.h"
#import "ReviewPopUp.h"
#import "MyGEMListingViewController.h"
#import "Reachability.h"
#import "AdminMsgPopUp.h"
#import "DummyLaunchPageViewController.h"
#import "Chat+CoreDataClass.h"
#import "MaintainanceModeDisplay.h"
#import "GoalCategoryViewController.h"


@import Firebase;
@import GoogleSignIn;

#define NOTIFICATION_TYPE_FOLLOW                    @"follow"
#define NOTIFICATION_TYPE_CHAT                      @"chat"
#define NOTIFICATION_TYPE_MEMMORY                   @"memory"
#define NOTIFICATION_TYPE_SAVED_POST                @"save"
#define NOTIFICATION_TYPE_CHAT_REQUEST              @"chatrequest"
#define NOTIFICATION_TYPE_CHAT_RESPONDS             @"chatresponse"
#define NOTIFICATION_TYPE_CHAT_FOLLOWING            @"following"
#define NOTIFICATION_TYPE_GOAL_REMINDER             @"goalreminder"
#define NOTIFICATION_TYPE_ADMIN_MESSAGE             @"image_push"
#define NOTIFICATION_TYPE_DO_LOGIN                  @"do_login"
#define NOTIFICATION_TYPE_INDIVIDUAL_MSG            @"user_message"
#define NOTIFICATION_TYPE_SHARED_GOAL               @"share"
#define NOTIFICATION_TYPE_BDAY_WISH                 @"birthday_wish"
#define NOTIFICATION_TYPE_BDAY_REMINDER             @"birthday_reminder"
#define NOTIFICATION_TYPE_INTEREST_REMINDER         @"interest_reminder"
#define NOTIFICATION_TYPE_SURVEY                    @"survey"


@interface AppDelegate ()<UIAlertViewDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate,GIDSignInDelegate>{
    
    Reachability *internetReachability;
    SWRevealViewController *revealController;
    NSDictionary *pushNotificationUserInfo;
    BOOL isAlertInProgress;
    BOOL isInBg;
    BOOL isBaseURLReady;
    BOOL isUserMsgGetting;
    BOOL isInMaitenanceMode;
    BOOL isLaunching;
    NSMutableDictionary *sharedPostParams;
}

@end


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Override point for customization after application launch.
    
    [self setUp];
    [self showSplashScreen];
    [self setUpFCM];
    [self configureGoogleLoginIn];
    [Fabric with:@[[Crashlytics class]]];
    [Utility setUpGoogleMapConfiguration];
    [self reachability];
    [self getBaseURLAtLaunch];
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    [self enablePushNotification];
    
    return YES;
}

-(void)setUp{
    
    AppVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    [sharedDefaults setObject:AppVersion forKey:@"AppVersion"];
    isUserMsgGetting = false;
    isLaunching = true;
}



-(void)showNotificationsIfAny{
    
    // Show notificarions as popup if any.
    
    if (sharedPostParams.count){
        [self openSharedPostDetailFromSharedLinkWithID:[sharedPostParams objectForKey:@"id"] andType:[sharedPostParams objectForKey:@"type"]];
    }
    else
        [self handleNotificationWhenBackGroundWith:pushNotificationUserInfo shouldShowInternalAlert:NO];
}



#pragma mark - Login with FB and Google configurations


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([[url scheme] isEqualToString:FACEBOOK_SCHEME]){
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }else if ([[url scheme] isEqualToString:SHARED_POST_SCHEME]){
        
        //Link clicked which is shared from purposecolor
        
        if ([[url query] hasPrefix:@"id="]) {
            sharedPostParams = [NSMutableDictionary new];
            for (NSString *param in [[url query] componentsSeparatedByString:@"&"]) {
                NSArray *elts = [param componentsSeparatedByString:@"="];
                if([elts count] < 2) continue;
                [sharedPostParams setObject:[elts lastObject] forKey:[elts firstObject]];
            }
            if (isBaseURLReady) {
                 if (sharedPostParams.count) [self openSharedPostDetailFromSharedLinkWithID:[sharedPostParams objectForKey:@"id"] andType:[sharedPostParams objectForKey:@"type"]];
            }
            return YES;
            
        }else{
            
            return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation];
        }
    }
    else
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
    
    
    
    
    
}

-(void)configureGoogleLoginIn{
    
    [GIDSignIn sharedInstance].clientID = [FIRApp defaultApp].options.clientID;
    [GIDSignIn sharedInstance].delegate = self;
    
}

-(void)getUserInfo{
    
    // Get user aletr from admin, if any.
    
    [APIMapper getUserInfoOnsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[responseObject objectForKey:@"title"]
                                                  message:[responseObject objectForKey:@"message"]
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           if ([[responseObject objectForKey:@"notify_type"] integerValue] == 1) {
                                               exit(0);
                                           }
                                       }];
            
            [alertController addAction:okAction];
            [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        if (error.localizedDescription) {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"ERROR"
                                                  message:error.localizedDescription
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           
                                       }];
            
            [alertController addAction:okAction];
            [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        }
        
    }];
    
}


-(void)getVersionStatus{
    
    /// Get version status, show alert message if any.
    
    [APIMapper getVersionStatusOnsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self showVersionUpdateMessage:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        NSString* message = @"You are entering to offline mode.Please connect your application to working internet connection for best user experience.";
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"OFFLINE"
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction *action)
                                 {
                                 }];
        
        [alertController addAction:cancel];
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        
    }];
    
}

#pragma mark - API Error Case

-(void)manageApiErrorCaseWithOperation:(AFHTTPRequestOperation*)operation responds:(NSDictionary*)notification{
    
    // Manage All API errror status
    
    if ([operation.response statusCode] == kUpdateMode) {
        [self showMandatoryVersionUpdate:notification];
    } else if ([operation.response statusCode] == kMaintenanceMode) {
        if (isInMaitenanceMode) return;
        isInMaitenanceMode = true;
        [self showMaintainanceModeMessage];
    }
    else if ([operation.response statusCode] == kUnAuthorized) {
        [self logoutSinceUnAuthorized:notification];
    }
    
}

-(void)showMaintainanceModeMessage{
    
    // Maintenance mode
    
    MaintainanceModeDisplay *maintainaceView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForMaintainanceView];
    maintainaceView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.window.rootViewController presentViewController:maintainaceView animated:YES completion:^{
        
    }];
}


-(void)logoutSinceUnAuthorized:(NSDictionary*)notification{
    if(isAlertInProgress) return;
    BOOL userExists = [self loadUserObjectWithKey:@"USER"];
    if (!userExists) return;
    isAlertInProgress = true;
    NSString *text = @"Invalid authentication token!";
    if (notification) {
        NSDictionary *dict  = notification;
        text = [dict objectForKey:@"text"];
    }
    [self clearUserSessions];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Logout"
                                          message:text
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   isAlertInProgress = false;
                                   
                               }];
    
    [alertController addAction:okAction];
    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    
    
}

-(void)showMandatoryVersionUpdate:(NSDictionary*)responseObject{
    
    if (responseObject && [responseObject objectForKey:@"message"] && [responseObject objectForKey:@"title"]) {
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:[responseObject objectForKey:@"title"]
                                              message:[responseObject objectForKey:@"message"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"UPDATE"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AppStoreURL] options:@{}
                                                                    completionHandler:^(BOOL success) {
                                                                        exit(0);
                                                                    }];
                                       }
                                       
                                   }];
        UIAlertAction *cancel = [UIAlertAction
                                 actionWithTitle:@"CANCEL"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction *action)
                                 {
                                     exit(0);
                                 }];
        
        [alertController addAction:okAction];
        [alertController addAction:cancel];
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
    
}

-(void)showVersionUpdateMessage:(NSDictionary*)responseObject{
    
    if ([[responseObject objectForKey:@"code"] integerValue] == 200) {
        
        if ([responseObject objectForKey:@"resultarray"]) {
            
            NSDictionary *result = [responseObject objectForKey:@"resultarray"];
            NSString *actualVersion = AppVersion;
            NSString* requiredVersion = [result objectForKey:@"app_version"];
            
            if ([requiredVersion compare:actualVersion options:NSNumericSearch] != NSOrderedSame) {
                // actualVersion not same as requiredVersion
                if ([[result objectForKey:@"notify_type"] boolValue]) {
                    
                    // Major
                    
                    UIAlertController *alertController = [UIAlertController
                                                          alertControllerWithTitle:[result objectForKey:@"notify_title"]
                                                          message:[result objectForKey:@"notify_msg"]
                                                          preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction
                                               actionWithTitle:@"UPDATE"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action)
                                               {
                                                   if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AppStoreURL] options:@{}
                                                                                completionHandler:^(BOOL success) {
                                                                                    exit(0);
                                                                                }];
                                                   }
                                                   
                                               }];
                    UIAlertAction *cancel = [UIAlertAction
                                             actionWithTitle:@"CANCEL"
                                             style:UIAlertActionStyleCancel
                                             handler:^(UIAlertAction *action)
                                             {
                                                 exit(0);
                                             }];
                    
                    [alertController addAction:okAction];
                    [alertController addAction:cancel];
                    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
                    
                    
                    
                }else{
                    // Minor
                    
                    
                    UIAlertController *alertController = [UIAlertController
                                                          alertControllerWithTitle:[result objectForKey:@"notify_title"]
                                                          message:[result objectForKey:@"notify_msg"]
                                                          preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction
                                               actionWithTitle:@"UPDATE"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action)
                                               {
                                                   if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                                                       
                                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AppStoreURL] options:@{}
                                                                                completionHandler:^(BOOL success) {
                                                                                    exit(0);
                                                                                }];
                                                   }
                                                   
                                               }];
                    UIAlertAction *cancel = [UIAlertAction
                                             actionWithTitle:@"LATER"
                                             style:UIAlertActionStyleCancel
                                             handler:^(UIAlertAction *action)
                                             {
                                                 [self getUserInfo];
                                                 
                                             }];
                    
                    [alertController addAction:okAction];
                    [alertController addAction:cancel];
                    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
                    
                    
                }
                
            }
            
        }
        
    }else{
        [self getUserInfo];
    }
}

#pragma mark - FCM Cloud Messaging

-(void)setUpFCM{
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    FIRFirestore *defaultFirestore = [FIRFirestore firestore];

    
}
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
     [self registerFopTopic];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken{
    [self registerFopTopic];
   
}
// [END refresh_token]

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.
- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"Received data message: %@", remoteMessage.appData);
}


-(void)registerFopTopic{
    
    //NSLog(@"toke %@",[FIRMessaging messaging].FCMToken);
    BOOL userExists = [self loadUserObjectWithKey:@"USER"];
    if (!userExists) {
        [[FIRMessaging messaging] unsubscribeFromTopic:@"message"];
         [[FIRMessaging messaging] subscribeToTopic:@"doLogin"];
    }else{
         [[FIRMessaging messaging] unsubscribeFromTopic:@"doLogin"];
         [[FIRMessaging messaging] subscribeToTopic:@"message"];
    }
}

#pragma mark - Reachability

-(void)reachability{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    internetReachability = [Reachability reachabilityForInternetConnection];
    [internetReachability startNotifier];
}

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    NetworkStatus netStatus = [internetReachability currentReachabilityStatus];
    switch (netStatus)
    {
            case NotReachable:        {
                [iSnackBar snackBarWithMessage:@"No connection" font:[UIFont fontWithName:CommonFont size:11] backgroundColor:[UIColor blackColor] textColor:[UIColor whiteColor] duration:2.0f];
                break;
            }
            
            case ReachableViaWWAN:        {
                [self getBaseURLAtLaunch];
                [self uploadOfflineChatIfAny];
                [iSnackBar snackBarWithMessage:@"Back online" font:[UIFont fontWithName:CommonFont size:11] backgroundColor:[UIColor colorWithRed:0.00 green:0.66 blue:0.41 alpha:1.0] textColor:[UIColor whiteColor] duration:2.0f];
                break;
            }
            case ReachableViaWiFi:        {
                [self getBaseURLAtLaunch];
                [self uploadOfflineChatIfAny];
                [iSnackBar snackBarWithMessage:@"Back online" font:[UIFont fontWithName:CommonFont size:11] backgroundColor:[UIColor colorWithRed:0.00 green:0.66 blue:0.41 alpha:1.0] textColor:[UIColor whiteColor] duration:2.0f];
                break;
            }
    }
    
}
-(void)getBaseURLAtLaunch{
    
    // get BaseURL from FCM
    
    if (!isBaseURLReady) {
        FIRFirestore *defaultFirestore = [FIRFirestore firestore];
        [[[defaultFirestore collectionWithPath:@"BaseURL"] documentWithPath:@"BaseURL"]
         addSnapshotListener:^(FIRDocumentSnapshot *snapshot, NSError *error) {
             if (snapshot == nil) {
                 [self conituneAppOnSuccess:NO withURL:nil isAtLaunch:isLaunching];
                 return;
             }
             [self continueAppWithBaseURL:snapshot.data isAtLaunch:isLaunching];
         }];
    }
    
}

-(void)continueAppWithBaseURL:(NSDictionary*)urlInfo isAtLaunch:(BOOL)isAtLaunch{
    
    if (urlInfo && [urlInfo objectForKey:@"testurl"]) {
        isBaseURLReady = true;
        if ([urlInfo objectForKey:@"testurl"]) [self conituneAppOnSuccess:YES withURL:[urlInfo objectForKey:@"testurl"] isAtLaunch:isAtLaunch];
        else  [self conituneAppOnSuccess:NO withURL:nil isAtLaunch:isAtLaunch];
    }else{
        [self conituneAppOnSuccess:NO withURL:nil isAtLaunch:isAtLaunch];
    }
}

-(void)conituneAppOnSuccess:(BOOL)isSuccess withURL:(NSString*)strURL isAtLaunch:(BOOL)isAtLaunch{
    
    isLaunching = false;
    if (isSuccess){
        BaseURLString = [NSString stringWithFormat:@"%@api.php?",strURL];
        NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
        [sharedDefaults setObject:BaseURLString forKey:@"BaseURL"];
    }
   
    [self resetBadgeCount];
    [self uploadOfflineChatIfAny];
    if (isAtLaunch) {
        [self getVersionStatus];
        [self checkUserStatus];
    }
}

-(void)resetBadgeCount{
    
    BOOL userExists = [self loadUserObjectWithKey:@"USER"];
    if (!userExists) return;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

-(void)getNotificationCount{
    
    if (_navGeneral && [[[_navGeneral viewControllers] firstObject] isKindOfClass:[LaunchPageViewController class]]) {
        LaunchPageViewController *launchPage = [[_navGeneral viewControllers] firstObject];
        [launchPage getNotificationCount];
    }
}

-(void)refreshCommunityAndHomePage{
    
    if (_navGeneral && [[[_navGeneral viewControllers] firstObject] isKindOfClass:[LaunchPageViewController class]]) {
        LaunchPageViewController *launchPage = [[_navGeneral viewControllers] firstObject];
        [launchPage refreshCommunityPage];
    }
    
}

-(void)refreshBdayBadgeInHomePage{
    
    if (_navGeneral && [[[_navGeneral viewControllers] firstObject] isKindOfClass:[LaunchPageViewController class]]) {
        LaunchPageViewController *launchPage = [[_navGeneral viewControllers] firstObject];
        [launchPage refreshBdayBadge];
    }
    
}


#pragma mark - Push Notifications

-(void)enablePushNotification{
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        
        if(!error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            });
            
        }
    }];
}

/*
 - (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
 
 NSLog(@"CALLED %@",response.actionIdentifier);
 }
 */


- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [self updateTokenToWebServerWithToken:token];
    [self registerFopTopic];
}



-(void)updateTokenToWebServerWithToken:(NSString*)token{
    
    BOOL userExists = [self loadUserObjectWithKey:@"USER"];
    if (!userExists) return;
    if ((token.length) && [User sharedManager].userId.length)
    [APIMapper setPushNotificationTokenWithUserID:[User sharedManager].userId token:token success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
    }];
    
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    [self resetBadgeCount];
    if(application.applicationState == UIApplicationStateInactive) {
        [self handleNotificationWhenBackGroundWith:userInfo shouldShowInternalAlert:NO];
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        [self handleNotificationWhenForeGroundWith:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);
        
    }
}


-(void)handleNotificationWhenBackGroundWith:(NSDictionary*)userInfo shouldShowInternalAlert:(BOOL)shouldShowInternalAlert{
   
    if (!isBaseURLReady){
        pushNotificationUserInfo = userInfo;
        return;
        // Should return, otherwise the popup will be removed by RootVC
    }
   
    if (_navGeneral && [[[_navGeneral viewControllers] firstObject] isKindOfClass:[LaunchPageViewController class]]) {
        LaunchPageViewController *launchPage = [[_navGeneral viewControllers] firstObject];
        [launchPage getNotificationCount];
    }
    if (NULL_TO_NIL([userInfo objectForKey:@"aps"])) {
        if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"])) {
            NSString *notification_type =[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"];
            if ([notification_type isEqualToString:NOTIFICATION_TYPE_CHAT]){
                
                UINavigationController *nav = _navGeneral;
                if ([[nav.viewControllers lastObject] isKindOfClass:[ChatComposeViewController class]]) {
                    ChatComposeViewController *chatView = (ChatComposeViewController*)[nav.viewControllers lastObject];
                    NSString *fromUserID = [[userInfo objectForKey:@"aps"] objectForKey:@"from_id"];
                    if ([chatView.chatUserInfo objectForKey:@"chatuser_id"]) {
                        NSString *toUserID =[chatView.chatUserInfo objectForKey:@"chatuser_id"];
                        if ([toUserID isEqualToString:fromUserID]) {
                            [chatView newChatHasReceivedWithDetails:userInfo];
                        }else{
                            
                            NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: nav.viewControllers];
                            [navigationArray removeLastObject];  // You can pass your index here
                            nav.viewControllers = navigationArray;
                            ChatComposeViewController *chatCompose =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatComposer];
                            NSString *fname = [[userInfo objectForKey:@"aps"] objectForKey:@"from_user"];
                            NSString *image = [[userInfo objectForKey:@"aps"] objectForKey:@"profileimg"];
                            NSString *userID = [[userInfo objectForKey:@"aps"] objectForKey:@"from_id"];
                            double dateTime = [[[userInfo objectForKey:@"aps"] objectForKey:@"chat_datetime"] doubleValue];
                            NSDictionary *chatInfo = [[NSDictionary alloc] initWithObjectsAndKeys:fname,@"firstname",image,@"profileimage",userID,@"chatuser_id",[NSNumber numberWithDouble:dateTime],@"chat_datetime", nil];
                            chatCompose.chatUserInfo = chatInfo;
                            [nav pushViewController:chatCompose animated:YES];
                            
                        }
                    }
                    
                }else{
                    
                    ChatComposeViewController *chatCompose =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatComposer];
                    NSString *fname = [[userInfo objectForKey:@"aps"] objectForKey:@"from_user"];
                    NSString *image = [[userInfo objectForKey:@"aps"] objectForKey:@"profileimg"];
                    NSString *userID = [[userInfo objectForKey:@"aps"] objectForKey:@"from_id"];
                    double dateTime = [[[userInfo objectForKey:@"aps"] objectForKey:@"chat_datetime"] doubleValue];
                    NSDictionary *chatInfo = [[NSDictionary alloc] initWithObjectsAndKeys:fname,@"firstname",image,@"profileimage",userID,@"chatuser_id",[NSNumber numberWithDouble:dateTime],@"chat_datetime", nil];
                    chatCompose.chatUserInfo = chatInfo;
                    [nav pushViewController:chatCompose animated:YES];
                    
                }
                
            }else if ([notification_type isEqualToString:NOTIFICATION_TYPE_MEMMORY]){
                [self configureMemmoryUserInfo:userInfo isFromBackGround:YES];
            }else if ([notification_type isEqualToString:NOTIFICATION_TYPE_FOLLOW] || [notification_type isEqualToString:NOTIFICATION_TYPE_CHAT_REQUEST] ){
                [self configureFollowRequestWithUserInfo:userInfo isFromForeGround:NO];
            }else if ([notification_type isEqualToString:NOTIFICATION_TYPE_CHAT_FOLLOWING]){
                [self configureListingWithFollowStatus:userInfo isFromBackGround:YES];
            }else if ([notification_type isEqualToString:NOTIFICATION_TYPE_CHAT_RESPONDS]){
                [self configureChatListWithChatResponds:userInfo isFromBackGround:YES];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_GOAL_REMINDER]){
                [self showNotificationDetailForAdminMessage:userInfo isFromBackGround:YES shouldShowInternalAlert:shouldShowInternalAlert];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_INDIVIDUAL_MSG]){
                [self showIndividualMsgsWithDetails:userInfo isFromBackGround:YES];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_BDAY_WISH]){
                [self configureBdayWishWithDetails:userInfo isFromForeGround:NO];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_BDAY_REMINDER]){
                [self configureBdayReminderWithDetails:userInfo isFromForeGround:NO];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_INTEREST_REMINDER]){
                [self configureInterestReminderWithDetails:userInfo isFromForeGround:NO];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_SURVEY]){
                [self configureSurveyDetails:userInfo isFromForeGround:NO];
            }
            
            else{
                [self handleOtherNotificationTypes:userInfo isFromForeGround:NO];
            }
            
        }
        if ([[userInfo objectForKey:@"notification_type"] isEqualToString:NOTIFICATION_TYPE_ADMIN_MESSAGE] || [[userInfo objectForKey:@"notification_type"] isEqualToString:NOTIFICATION_TYPE_DO_LOGIN] ){
            NSMutableDictionary *details = [NSMutableDictionary new];
            NSMutableDictionary *items = [NSMutableDictionary new];
            [items setObject:[[userInfo objectForKey:@"aps"]objectForKey:@"alert"] forKey:@"alert"];
            [items setObject:[userInfo objectForKey:@"alert_img"] forKey:@"alert_img"];
            [items setObject:[userInfo objectForKey:@"height"] forKey:@"height"];
            [items setObject:[userInfo objectForKey:@"width"] forKey:@"width"];
            [details setObject:items forKey:@"aps"];
            [self showNotificationDetailForAdminMessage:details isFromBackGround:YES shouldShowInternalAlert:shouldShowInternalAlert];
        }else if ([[userInfo objectForKey:@"notification_type"] isEqualToString:NOTIFICATION_TYPE_SHARED_GOAL]){
            NSMutableDictionary *details = [NSMutableDictionary new];
            NSMutableDictionary *items = [NSMutableDictionary new];
            [items setObject:[[userInfo objectForKey:@"aps"]objectForKey:@"alert"] forKey:@"alert"];
            [items setObject:[userInfo objectForKey:@"gem_id"] forKey:@"gem_id"];
            [items setObject:[userInfo objectForKey:@"gem_type"] forKey:@"gem_type"];
             [items setObject:@"share" forKey:@"notification_type"];
            [details setObject:items forKey:@"aps"];
            [self handleOtherNotificationTypes:details isFromForeGround:NO];
            
        }
    }
    
}
-(void)handleNotificationWhenForeGroundWith:(NSDictionary*)userInfo{
    
    //handle notification from Foreground
    
    if (_navGeneral && [[[_navGeneral viewControllers] firstObject] isKindOfClass:[LaunchPageViewController class]]) {
        LaunchPageViewController *launchPage = [[_navGeneral viewControllers] firstObject];
        [launchPage getNotificationCount];
    }
    if (NULL_TO_NIL([userInfo objectForKey:@"aps"])) {
        if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"])) {
            NSString *notification_type =[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"];
            if ([notification_type isEqualToString:NOTIFICATION_TYPE_CHAT]){
                __block UINavigationController *nav = _navGeneral;
                if ([[nav.viewControllers lastObject] isKindOfClass:[ChatComposeViewController class]]) {
                    
                    /*! If the user standing in the  chat page !*/
                    
                    ChatComposeViewController *chatView = (ChatComposeViewController*)[nav.viewControllers lastObject];
                    NSString *fromUserID = [[userInfo objectForKey:@"aps"] objectForKey:@"from_id"];
                    if ([chatView.chatUserInfo objectForKey:@"chatuser_id"]) {
                        NSString *toUserID =[chatView.chatUserInfo objectForKey:@"chatuser_id"];
                        if ([toUserID isEqualToString:fromUserID]) {
                            
                            /*! If chat notification comes with a same user !*/
                            [chatView newChatHasReceivedWithDetails:userInfo];
                            
                        }else{
                            
                            /*! If chat notification comes with a defefrent user !*/
                            
                            NSString *message;
                            NSString *appName = PROJECT_NAME;
                            if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
                            message = [NSString stringWithFormat:@"%@\n%@",[[userInfo objectForKey:@"aps"] objectForKey:@"from_user"],[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
                            [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
                            [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
                                
                                NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: nav.viewControllers];
                                [navigationArray removeLastObject];  // You can pass your index here
                                nav.viewControllers = navigationArray;
                                ChatComposeViewController *chatCompose =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatComposer];
                                NSString *fname = [[userInfo objectForKey:@"aps"] objectForKey:@"from_user"];
                                NSString *image = [[userInfo objectForKey:@"aps"] objectForKey:@"profileimg"];
                                NSString *userID = [[userInfo objectForKey:@"aps"] objectForKey:@"from_id"];
                                double dateTime = [[[userInfo objectForKey:@"aps"] objectForKey:@"chat_datetime"] doubleValue];
                                NSDictionary *chatInfo = [[NSDictionary alloc] initWithObjectsAndKeys:fname,@"firstname",image,@"profileimage",userID,@"chatuser_id",[NSNumber numberWithDouble:dateTime],@"chat_datetime", nil];
                                chatCompose.chatUserInfo = chatInfo;
                                [nav pushViewController:chatCompose animated:YES];
                                
                            }];
                            
                        }
                    }
                    
                }
                else if([[nav.viewControllers lastObject] isKindOfClass:[ChatUserListingViewController class]]){
                    
                    /*! If the user standing in the  Chat User List page !*/
                    
                    ChatUserListingViewController *chatList = (ChatUserListingViewController*)[nav.viewControllers lastObject];
                    [chatList loadAllChatUsers];
                    NSString *message;
                    NSString *appName = PROJECT_NAME;
                    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
                    message = [NSString stringWithFormat:@"%@\n%@",[[userInfo objectForKey:@"aps"] objectForKey:@"from_user"],[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
                    [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
                    [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
                        ChatComposeViewController *chatCompose =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatComposer];
                        NSString *fname = [[userInfo objectForKey:@"aps"] objectForKey:@"from_user"];
                        NSString *image = [[userInfo objectForKey:@"aps"] objectForKey:@"profileimg"];
                        NSString *userID = [[userInfo objectForKey:@"aps"] objectForKey:@"from_id"];
                        double dateTime = [[[userInfo objectForKey:@"aps"] objectForKey:@"chat_datetime"] doubleValue];
                        NSDictionary *chatInfo = [[NSDictionary alloc] initWithObjectsAndKeys:fname,@"firstname",image,@"profileimage",userID,@"chatuser_id",[NSNumber numberWithDouble:dateTime],@"chat_datetime", nil];
                        chatCompose.chatUserInfo = chatInfo;
                        [nav pushViewController:chatCompose animated:YES];
                        
                    }];
                    
                }
                else{
                    
                    /*! All other pages !*/
                    
                    NSString *message;
                    NSString *appName = PROJECT_NAME;
                    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
                    message = [NSString stringWithFormat:@"%@\n%@",[[userInfo objectForKey:@"aps"] objectForKey:@"from_user"],[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
                    
                    [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
                    [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
                        
                        ChatComposeViewController *chatCompose =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatComposer];
                        NSString *fname = [[userInfo objectForKey:@"aps"] objectForKey:@"from_user"];
                        NSString *image = [[userInfo objectForKey:@"aps"] objectForKey:@"profileimg"];
                        NSString *userID = [[userInfo objectForKey:@"aps"] objectForKey:@"from_id"];
                        double dateTime = [[[userInfo objectForKey:@"aps"] objectForKey:@"chat_datetime"] doubleValue];
                        NSDictionary *chatInfo = [[NSDictionary alloc] initWithObjectsAndKeys:fname,@"firstname",image,@"profileimage",userID,@"chatuser_id",[NSNumber numberWithDouble:dateTime],@"chat_datetime", nil];
                        chatCompose.chatUserInfo = chatInfo;
                        [nav pushViewController:chatCompose animated:YES];
                        
                    }];
                    
                }
                
            }else if ([notification_type isEqualToString:NOTIFICATION_TYPE_MEMMORY]){
                [self configureMemmoryUserInfo:userInfo isFromBackGround:NO];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_FOLLOW] || [notification_type isEqualToString:NOTIFICATION_TYPE_CHAT_REQUEST]){
                [self configureFollowRequestWithUserInfo:userInfo isFromForeGround:YES];
            }else if ([notification_type isEqualToString:NOTIFICATION_TYPE_CHAT_FOLLOWING]){
                [self configureListingWithFollowStatus:userInfo isFromBackGround:NO];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_CHAT_RESPONDS]){
                [self configureChatListWithChatResponds:userInfo isFromBackGround:NO];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_GOAL_REMINDER]){
                [self showNotificationDetailForAdminMessage:userInfo isFromBackGround:NO shouldShowInternalAlert:YES];
                
            } else if ([notification_type isEqualToString:NOTIFICATION_TYPE_INDIVIDUAL_MSG]){
                [self showIndividualMsgsWithDetails:userInfo isFromBackGround:NO];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_BDAY_WISH]){
                [self configureBdayWishWithDetails:userInfo isFromForeGround:YES];
            } else if ([notification_type isEqualToString:NOTIFICATION_TYPE_BDAY_REMINDER]){
                [self configureBdayReminderWithDetails:userInfo isFromForeGround:YES];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_INTEREST_REMINDER]){
                [self configureInterestReminderWithDetails:userInfo isFromForeGround:YES];
            }
            else if ([notification_type isEqualToString:NOTIFICATION_TYPE_SURVEY]){
                [self configureSurveyDetails:userInfo isFromForeGround:YES];
            }
            
            else{
                [self handleOtherNotificationTypes:userInfo isFromForeGround:YES];
            }
        }
        else{
            if ([[userInfo objectForKey:@"notification_type"] isEqualToString:NOTIFICATION_TYPE_ADMIN_MESSAGE] || [[userInfo objectForKey:@"notification_type"] isEqualToString:NOTIFICATION_TYPE_DO_LOGIN] ){
                NSMutableDictionary *details = [NSMutableDictionary new];
                NSMutableDictionary *items = [NSMutableDictionary new];
                [items setObject:[[userInfo objectForKey:@"aps"]objectForKey:@"alert"] forKey:@"alert"];
                [items setObject:[userInfo objectForKey:@"alert_img"] forKey:@"alert_img"];
                [items setObject:[userInfo objectForKey:@"height"] forKey:@"height"];
                [items setObject:[userInfo objectForKey:@"width"] forKey:@"width"];
                [details setObject:items forKey:@"aps"];
                [self showNotificationDetailForAdminMessage:details isFromBackGround:NO shouldShowInternalAlert:YES];
                
            }else if ([[userInfo objectForKey:@"notification_type"] isEqualToString:NOTIFICATION_TYPE_SHARED_GOAL]){
                NSMutableDictionary *details = [NSMutableDictionary new];
                NSMutableDictionary *items = [NSMutableDictionary new];
                [items setObject:[[userInfo objectForKey:@"aps"]objectForKey:@"alert"] forKey:@"alert"];
                [items setObject:[userInfo objectForKey:@"gem_id"] forKey:@"gem_id"];
                [items setObject:[userInfo objectForKey:@"gem_type"] forKey:@"gem_type"];
                [items setObject:@"share" forKey:@"notification_type"];
                [details setObject:items forKey:@"aps"];
               [self handleOtherNotificationTypes:details isFromForeGround:YES];
                
            }
        }
        
    }
    
}

-(void)showIndividualMsgsWithDetails:(NSDictionary*)userInfo isFromBackGround:(BOOL)isBackground{
    
    if (!isBackground) {
        NSString *message;
        NSString *appName = PROJECT_NAME;
        if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
            message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            
            [self getAdminMessagesIfAny];
        }];
    }else{
        [self getAdminMessagesIfAny];
    }
    

}


-(void)configureListingWithFollowStatus:(NSDictionary*)userInfo isFromBackGround:(BOOL)isBackground{
    
    if ([userInfo objectForKey:@"aps"]) {
        
        if (!isBackground) {
            NSString *message;
            NSString *appName = PROJECT_NAME;
            if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
            message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
            
            [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
            [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
                
            }];
        }
        if ([[userInfo objectForKey:@"aps"] objectForKey:@"follow_userid"] && [[userInfo objectForKey:@"aps"] objectForKey:@"can_follow"] && [[userInfo objectForKey:@"aps"] objectForKey:@"follow_status"]) {
            NSString *userID = [[userInfo objectForKey:@"aps"] objectForKey:@"follow_userid"];
            NSNumber *status = [[userInfo objectForKey:@"aps"] objectForKey:@"follow_status"];
            NSDictionary* userInfo = @{@"userID":userID ,@"status":status};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"FollowReqFromProfile" object:self userInfo:userInfo];
            
        }
        if (_navGeneral && [[[_navGeneral viewControllers] lastObject] isKindOfClass:[ProfilePageViewController class]]) {
            ProfilePageViewController *profile = [[_navGeneral viewControllers] lastObject];
            [profile refreshData];
        }
    }
    
}

-(void)configureBdayReminderWithDetails:(NSDictionary*)userInfo isFromForeGround:(BOOL)isForeground{
    
    __block UINavigationController *nav = _navGeneral;
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
        message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (isForeground) {
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            BdayWishingViewController *bdayScreen =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForBdayVC];
            [nav pushViewController:bdayScreen animated:YES];
        }];
        
    }else{
        BdayWishingViewController *bdayScreen =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForBdayVC];
        [nav pushViewController:bdayScreen animated:YES];
    }
    
}


-(void)configureInterestReminderWithDetails:(NSDictionary*)userInfo isFromForeGround:(BOOL)isForeground{
    
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
        message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (isForeground) {
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            if (![self checkProfilePageIsOnTop]) {
                 [self showUserProfilePageEnableInterst:YES];
            }
           
        }];
        
    }else{
        if (![self checkProfilePageIsOnTop]) {
            [self showUserProfilePageEnableInterst:YES];
        }
    }
    
}

-(void)configureSurveyDetails:(NSDictionary*)userInfo isFromForeGround:(BOOL)isForeground{
    
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
        message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (isForeground) {
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            if (_navGeneral && [[[_navGeneral viewControllers] firstObject] isKindOfClass:[LaunchPageViewController class]]) {
                LaunchPageViewController *launchPage = [[_navGeneral viewControllers] firstObject];
                [launchPage showSurveyPopUpWithInfo:[userInfo objectForKey:@"aps"]];
            }
            
        }];
        
    }else{
        
        if (_navGeneral && [[[_navGeneral viewControllers] firstObject] isKindOfClass:[LaunchPageViewController class]]) {
            LaunchPageViewController *launchPage = [[_navGeneral viewControllers] firstObject];
             [launchPage showSurveyPopUpWithInfo:[userInfo objectForKey:@"aps"]];
        }
        
    }
    
}



-(BOOL)checkProfilePageIsOnTop{
    
    UINavigationController *nav = _navGeneral;
    if ([[nav.viewControllers lastObject] isKindOfClass:[ProfilePageViewController class]]) {
        ProfilePageViewController *profile = (ProfilePageViewController*)[nav.viewControllers lastObject];
        [profile showGoalCategories];
        return YES;
    }
    return false;
}


-(void)configureBdayWishWithDetails:(NSDictionary*)userInfo isFromForeGround:(BOOL)isForeground{
    
    __block UINavigationController *nav = _navGeneral;
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
        message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (isForeground) {
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            LikedAndCommentedUserListings *userListings =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForLikedAndCommentedUsers];
            [userListings loadUserListingsForBirthdayForTheUser:[User sharedManager].userId];
            [nav pushViewController:userListings animated:YES];
        }];
        
    }else{
        LikedAndCommentedUserListings *userListings =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForLikedAndCommentedUsers];
        [userListings loadUserListingsForBirthdayForTheUser:[User sharedManager].userId];
        [nav pushViewController:userListings animated:YES];
    }
    
}



-(void)configureFollowRequestWithUserInfo:(NSDictionary*)userInfo isFromForeGround:(BOOL)isForeground{
    
    __block UINavigationController *nav = _navGeneral;
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
    message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (isForeground) {
        
        if ([[nav.viewControllers lastObject] isKindOfClass:[NotificationsListingViewController class]]) {
            NotificationsListingViewController *notifications = (NotificationsListingViewController*)[nav.viewControllers lastObject];
            [notifications refreshData];
            
        }else{
            
            [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
            [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
                NotificationsListingViewController *notifications =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForNotificationsListing];
                [nav pushViewController:notifications animated:YES];
            }];
        }
        
    }else{
        if ([[nav.viewControllers lastObject] isKindOfClass:[NotificationsListingViewController class]]) {
            NotificationsListingViewController *notifications = (NotificationsListingViewController*)[nav.viewControllers lastObject];
            [notifications refreshData];
            
        }else{
            NotificationsListingViewController *notifications =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForNotificationsListing];
            [nav pushViewController:notifications animated:YES];
        }
    }
    
}

-(void)showNotificationDetailForAdminMessage:(NSDictionary*)userInfo isFromBackGround:(BOOL)isBackground shouldShowInternalAlert:(BOOL)shouldShowInternalAlert{
    
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
        message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (shouldShowInternalAlert) {
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            
            [self showPopUpWithGoalInfo:userInfo andAdminMsg:nil];
            
        }];
    }else{
          [self showPopUpWithGoalInfo:userInfo andAdminMsg:nil];
    }
    
    
}


-(void)configureChatListWithChatResponds:(NSDictionary*)userInfo isFromBackGround:(BOOL)isFromBackGround{
    
    __block UINavigationController *nav = _navGeneral;
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
    message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if ([[nav.viewControllers lastObject] isKindOfClass:[ChatUserListingViewController class]]) {
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            
        }];
        ChatUserListingViewController *notifications = (ChatUserListingViewController*)[nav.viewControllers lastObject];
        [notifications loadAllChatUsers];
        
    }else{
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            
            ChatUserListingViewController *chatUser =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatUserListings];
            [self.navGeneral pushViewController:chatUser animated:YES];
            
        }];
    }
    
}

-(void)configureMemmoryUserInfo:(NSDictionary*)memoryDetails isFromBackGround:(BOOL)isFromBackGround{
    
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[memoryDetails objectForKey:@"aps"] objectForKey:@"alert"]))
    message = [[memoryDetails objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (isFromBackGround) {
        __block UINavigationController *nav = _navGeneral;
        
        if ([[nav.viewControllers lastObject] isKindOfClass:[MyMemmoriesViewController class]]) {
            /*! If the user standing in the  Memmory page !*/
        }else{
            
            /*! All other pages !*/
            
            MyMemmoriesViewController *myMemmories =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForMyMemmories];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app.navGeneral pushViewController:myMemmories animated:YES];
        }
        
    }else{
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            
            __block UINavigationController *nav = _navGeneral;
            
            if ([[nav.viewControllers lastObject] isKindOfClass:[MyMemmoriesViewController class]]) {
                /*! If the user standing in the  Memmory page !*/
            }else{
                
                /*! All other pages !*/
                
                MyMemmoriesViewController *myMemmories =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForMyMemmories];
                AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [app.navGeneral pushViewController:myMemmories animated:YES];
            }
            
        }];
    }
    
    
    
}

-(void)handleOtherNotificationTypes:(NSDictionary*)userInfo isFromForeGround:(BOOL)isForeGround{
    
    NSString *title = @"PurposeColor";
    NSString *message;
    NSString *appName = PROJECT_NAME;
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"title"]))
    title = [[userInfo objectForKey:@"aps"] objectForKey:@"title"];
    
    if (NULL_TO_NIL([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]))
    message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (isForeGround) {
        
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
        [JCNotificationCenter enqueueNotificationWithTitle:appName message:message tapHandler:^{
            
            [self showNotifivationDetailPageWithDetails:userInfo];
            
        }];
        
    }else{
        
        [self showNotifivationDetailPageWithDetails:userInfo];
    }
    
}

-(void)showNotifivationDetailPageWithDetails:(NSDictionary*)userInfo{
    
    __block UINavigationController *nav = _navGeneral;
    
    if ([[nav.viewControllers lastObject] isKindOfClass:[GemsUnderNotificationDetail class]]) {
        /*! If the user standing in the  Memmory page !*/
    }else{
        
        /*! All other pages !*/
        
        if ([[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"] isEqualToString:@"comment"] || [[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"] isEqualToString:@"reply"]||[[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"] isEqualToString:@"like"]||[[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"] isEqualToString:@"save"]||[[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"] isEqualToString:@"share"]||[[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"] isEqualToString:@"commentlike"] || [[[userInfo objectForKey:@"aps"] objectForKey:@"notification_type"] isEqualToString:@"replylike"]) {
            
            GemsUnderNotificationDetail *notificationDetails =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGemsUnderNotification];
            [notificationDetails getGemDetailsWithGemID:[[userInfo objectForKey:@"aps"] objectForKey:@"gem_id"] gemType:[[userInfo objectForKey:@"aps"] objectForKey:@"gem_type"]];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app.navGeneral pushViewController:notificationDetails animated:YES];
            
        }
        
    }
}

-(void)openSharedPostDetailFromSharedLinkWithID:(NSString*)strID andType:(NSString*)gemType{
    
    GemsUnderNotificationDetail *notificationDetails =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGemsUnderNotification];
    [notificationDetails getGemDetailsWithGemID:strID gemType:gemType];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:notificationDetails animated:YES];
}

-(void)getAdminMessagesIfAny{
    
    if (isUserMsgGetting) return;
    isUserMsgGetting = true;
    [APIMapper getAdminMessagesIfAnyOnsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject objectForKey:@"messages"]) {
            [self showPopUpWithGoalInfo:nil andAdminMsg:responseObject];
            [self updateBackendSinceUserHasSeenMessages];
        }else{
            isUserMsgGetting = false;
        }
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isUserMsgGetting = false;
    }];
    
}

-(void)updateBackendSinceUserHasSeenMessages{
    
    [APIMapper updateBackendAdminMessageReadOnSucces:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        isUserMsgGetting = false;
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isUserMsgGetting = false;
    }];
}

-(void)showPopUpWithGoalInfo:(NSDictionary*)details andAdminMsg:(NSDictionary*)adminMsgDetails{
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"AdminMsgPopUp"
                                                          owner:nil
                                                        options:nil];
    
    AdminMsgPopUp *vwPopUP = [arrayOfViews objectAtIndex:0];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.4 delay:.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
    if (details) {
        [vwPopUP setUpGoalReminderWithDetails:[details objectForKey:@"aps"]];
    }else{
        [vwPopUP showAdminMessages:adminMsgDetails];
    }
    
}

#pragma mark - Login Actions

-(void)checkUserStatus{
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"SHOULD_SHOW_HELP"])
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:1] forKey:@"SHOULD_SHOW_HELP"];
    if ([User sharedManager]) self.currentUser = (User*)[User sharedManager];
    BOOL userExists = [self loadUserObjectWithKey:@"USER"];
    if (userExists) [self showLauchPage];
    else            [self showIntroLoginPage];
    [self enablePushNotification];
    
    
}

/*!.........Check Availability of User !...........*/

- (BOOL )loadUserObjectWithKey:(NSString *)key {
    BOOL isUserExists = false;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    if (encodedObject) {
        User *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        if (object)isUserExists = true;
        [self createUserObject:object];
        NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
        [sharedDefaults setObject: [User sharedManager].token forKey:@"TOKEN"];
        [sharedDefaults synchronize];
        
    }
    return isUserExists;
}

-(void)createUserObject:(User*)user{
    
    // Creating singleton user object with Decoded data from NSUserDefaults
    
    [User sharedManager].isLoggedIn = user.isLoggedIn;
    [User sharedManager].userId = user.userId;
    [User sharedManager].userTypeId = user.userTypeId;
    [User sharedManager].name = user.name;
    [User sharedManager].email = user.email;
    [User sharedManager].regDate = user.regDate;
    [User sharedManager].loggedStatus = user.loggedStatus;
    [User sharedManager].verifiedStatus = user.verifiedStatus;
    [User sharedManager].profileurl = user.profileurl;
    [User sharedManager].cartCount = user.cartCount;
    [User sharedManager].notificationCount = user.notificationCount;
    [User sharedManager].companyID = user.companyID;
    [User sharedManager].statusMsg = user.statusMsg;
    [User sharedManager].follow_status = user.follow_status;
    [User sharedManager].daily_notify  = user.daily_notify;
    [User sharedManager].token = user.token;
    
    
}


-(void)showIntroLoginPage{
    
    IntroScreenViewController *introScreen =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForIntroScreen];
    UINavigationController *logginVC = [[UINavigationController alloc] initWithRootViewController:introScreen];
    logginVC.navigationBarHidden = true;
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = logginVC;
    [self.window makeKeyAndVisible];
    [self showNotificationsIfAny];
}
-(void)showSplashScreen{
    
    DummyLaunchPageViewController *splashScreen =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:StoryboardForLogin Identifier:StoryBoardIdentifierForSplashScreen];
    UINavigationController *logginVC = [[UINavigationController alloc] initWithRootViewController:splashScreen];
    logginVC.navigationBarHidden = true;
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = logginVC;
    [self.window makeKeyAndVisible];
}

// Success call back from login after successful attempt

-(void)goToHomeAfterLogin{
    
    [self enablePushNotification];
    _launchPage = nil;
    revealController = nil;
    [self showLauchPage];
    
}

- (void)showLauchPage {
    
    // Launching page with Nav bar. This page launches initially
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if (!_launchPage){
        _launchPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForLaunchPage];
        // GEMSWithHeaderListingsViewController *imotionalAwareness =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGEMWithHeaderListings];
        UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:_launchPage];
        MenuViewController *menuVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForMenuPage];
        UINavigationController *navMenu = [[UINavigationController alloc] initWithRootViewController:menuVC];
        navMenu.navigationBarHidden = true;
        revealController = [[SWRevealViewController alloc] initWithRearViewController:navMenu frontViewController:navHome];
        revealController.rightViewController = navMenu;
        navHome.navigationBarHidden = true;
        self.navGeneral = navHome;
    }
    
    [UIView transitionWithView:app.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{ app.window.rootViewController = revealController; }
                    completion:^(BOOL finished) {
                        if (finished) {
                             [self showNotificationsIfAny];
                             [self getAdminMessagesIfAny];
                        }
                    }];
    
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"PurposeColor"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

#pragma mark - Dynamic Menu Selection

- (void)changeHomePageDynamicallyWithType:(EMenuActions)menu_type {
    
    switch (menu_type) {
            
            case eMenu_Community_GEMS:
            [self showCommubityGEMS];
            break;
            
            case eMenu_EmotionalAwareness:
            [self showEmotionalAwarenessPage];
            break;
            
            
            case eMenu_Goals_Dreams:
            [self showGoalsAndDreams];
            break;
            
            case eMenu_Emotional_Intelligence:
            [self showIntelligenceJournalView];
            break;
            
            case eMenu_Favourites:
            [self showAllMyFavouriteGEMs];
            break;
            
            case eMenu_Notifications:
            [self showNotificationListings];
            break;
            
            case eMenu_Help:
            [self showHelpPage];
            break;
            
            case eMenu_SavedGEMs:
            [self showAllInspiredGEMs];
            break;
            
            case eMenu_Memories:
            [self showMyMemmories];
            break;
            
            case eMenu_Profile:
            [self showUserProfilePageEnableInterst:NO];
            break;
            
            case eMenu_Logout:
            [self logOutUser];
            break;
            
            case eMenu_Settings:
            [self showSettings];
            break;
            
            case eMenu_Reminders:
            [self showReminders];
            break;
            
            case eMenu_BlockedList:
            [self showBlockedList];
            break;
            
            case eMenu_Terms:
            [self showTermsOfSerivice];
            break;
            
            case eMenu_Privacy:
            [self showPrivacyPolicy];
            break;
            
            case eMenu_Share:
            [self shareApp];
            break;
            
            case eMenu_Feedback:
            [self showFeedBackPopUp];
            break;
            
            case eMenu_Journal:
            [self showJournalListView];
            break;
            
            case eMenu_MyPosts:
            [self showMyPosts];
            break;
            
            case eMenu_Invite_Friends:
            [self inviteFriends];
            break;
            
            
            
        default:
            break;
    }
    
    
}

-(void)showCommubityGEMS{
    
    [_launchPage showCommunityGems:nil];
}

-(void)showGoalsAndDreams{
    
    [_launchPage showGoalsAndDreams:nil];
}


-(void)showIntelligenceJournalView{
    
    [_launchPage showIntelligenceJournalView:nil];
}

-(void)showAllMyFavouriteGEMs{
    
    MyFavouritesListingViewController *myFavourites =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForMyFavourites];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:myFavourites animated:YES];
}


-(void)showAllInspiredGEMs{
    
    MyFavouritesListingViewController *myFavourites =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForMyFavourites];
    myFavourites.isInspiredGEM = true;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:myFavourites animated:YES];
}


-(void)showNotificationListings{
    
    NotificationsListingViewController *notifications =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForNotificationsListing];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:notifications animated:YES];
    
}


-(void)showMyMemmories{
    
    MyMemmoriesViewController *myMemmories =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForMyMemmories];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:myMemmories animated:YES];
}

-(void)showUserProfilePageEnableInterst:(BOOL)showInterstPage{
    
    ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:profilePage animated:YES];
    profilePage.canEdit = true;
    if (showInterstPage) profilePage.shouldOpenInterestPage = YES;
    [profilePage loadUserProfileWithUserID:[User sharedManager].userId showBackButton:YES];
    
}

-(void)showHelpPage{
    
    WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:browser animated:YES];
    browser.strTitle = @"HELP";
}

-(void)showPrivacyPolicy{
    
    WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:browser animated:YES];
    browser.strTitle = @"PRIVACY POLICY";
    
    
}

-(void)shareApp{
    
    NSString *sharetxt = [NSString stringWithFormat:@"Hey,\nPurposeColor helps you to reach your goals & build network with inspiring people.\nDownload the free app to get started.\n"];
    NSMutableArray *sharingItems = [NSMutableArray new];
    [sharingItems addObject:sharetxt];
    [sharingItems addObject:[NSURL URLWithString:@"http://purposecolor.com/download/"]];
    UIActivityViewController* activityVC = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypePostToWeibo,UIActivityTypePrint,UIActivityTypeSaveToCameraRoll,UIActivityTypeAssignToContact,UIActivityTypeAirDrop];
    activityVC.excludedActivityTypes = excludeActivities;
    AppDelegate *deleagte = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [deleagte.window.rootViewController presentViewController:activityVC animated:TRUE completion:nil];
    
    
}


-(void)showBlockedList{
    
    WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForBlockedList];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:browser animated:YES];
    
}

-(void)showTermsOfSerivice{
    
    WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:browser animated:YES];
    browser.strTitle = @"TERMS OF SERVICE";
    
}

-(void)showSettings{
    
    SettingsViewController *settings =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForSettings];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:settings animated:YES];
}

-(void)showReminders{
    
    ReminderListingViewController *reminder =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForReminderListings];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:reminder animated:YES];
}
-(void)showJournalListView{
    
    JournalListViewController *journalList =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForJournalListVC];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:journalList animated:YES];
}

-(void)showMyPosts{
    
    MyGEMListingViewController *gemListingVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForMyGEMS];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:gemListingVC animated:YES];
}

-(void)inviteFriends{
    
    NSString *sharetxt = [NSString stringWithFormat:@"Hey,\nPurposeColor helps you to reach your goals & build network with inspiring people.\nDownload the free app to get started.\n"];
    NSMutableArray *sharingItems = [NSMutableArray new];
    [sharingItems addObject:sharetxt];
    [sharingItems addObject:[NSURL URLWithString:@"http://purposecolor.com/download/"]];
    UIActivityViewController* activityVC = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypePostToWeibo,UIActivityTypePrint,UIActivityTypeSaveToCameraRoll,UIActivityTypeAssignToContact,UIActivityTypeAirDrop];
    activityVC.excludedActivityTypes = excludeActivities;
    AppDelegate *deleagte = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [deleagte.window.rootViewController presentViewController:activityVC animated:TRUE completion:nil];
}


-(void)showEmotionalAwarenessPage{
    
    [_launchPage showEmoitonalAwareness:nil];
    
}

-(IBAction)showFeedBackPopUp{
    
    // User feedback popup
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ReviewPopUp"
                                                          owner:nil
                                                        options:nil];
    ReviewPopUp *vwPopUP = [arrayOfViews objectAtIndex:0];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
    
}

-(void)uploadOfflineChatIfAny{
    
    // Uplaod all pending oflline chat
    
    NSError *error = nil;
    NSManagedObjectContext* context = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"Chat" inManagedObjectContext:context]];
    NSArray *results = [context executeFetchRequest:fetch error:&error];
    NSError * err;
    for (Chat *chat in results) {
        NSString *myString = chat.responds;
        NSData *data = [myString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * responds;
        if(data!=nil){
            responds = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            NSString *touserID = chat.toUserID;
            NSString *message = [responds objectForKey:@"msg"];
            if (touserID && message && [User sharedManager].userId) {
                [APIMapper postChatMessageWithUserID:[User sharedManager].userId toUserID:touserID message:message indexOfDB:chat.index success:^(AFHTTPRequestOperation *operation, id responseObject){
                    
                    [self deleteChatRecords:responseObject];
                    
                } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                    
                    
                }];
            }
            
        }
    }
    
}

-(void)deleteChatRecords:(NSDictionary*)chatInfo{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([chatInfo objectForKey:@"chat"]) {
            NSString *toUserID = [[chatInfo objectForKey:@"chat"] objectForKey:@"to_id"];
            NSInteger index = [[[chatInfo objectForKey:@"chat"] objectForKey:@"index"] integerValue];
            NSManagedObjectContext* context = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
            NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
            [fetch setEntity:[NSEntityDescription entityForName:@"Chat" inManagedObjectContext:context]];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"toUserID == %@ AND index == %d", toUserID,index];
            [fetch setPredicate:predicate];
            NSArray * result = [context executeFetchRequest:fetch error:nil];
            for (id basket in result)
            [context deleteObject:basket];
        }
        
    });
    
}

-(void)logOutUser{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Confirm Logout"
                                  message:@"Logout from the app ?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Logout"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             [self clearUserSessions];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

-(void)clearUserSessions{
    
    // Clear session when logout
    
    BOOL userExists = [self loadUserObjectWithKey:@"USER"];
    if (!userExists) return;
    
    [self showLoadingScreen];
    [APIMapper logoutFromAccount:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
        [sharedDefaults setObject: nil forKey:@"TOKEN"];
        [sharedDefaults synchronize];
        
        [[GIDSignIn sharedInstance] signOut];
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (defaults && [defaults objectForKey:@"USER"])
        [defaults removeObjectForKey:@"USER"];
        [defaults removeObjectForKey:@"SURVEY_TIME"];
        [delegate checkUserStatus];
        [self hideLoadingScreen];
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Logout"
                                      message:[error localizedDescription]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alert addAction:ok];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
        [self hideLoadingScreen];
    }];
    
    
}
-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.window animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.window animated:YES];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    isInBg = true;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [self getNotificationCount];
    [self resetBadgeCount];
    if (isInBg) [self refreshCommunityAndHomePage];
    isInBg = false;
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    if ([User sharedManager].userId.length) [self enablePushNotification];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    [self saveContext];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}


@end
