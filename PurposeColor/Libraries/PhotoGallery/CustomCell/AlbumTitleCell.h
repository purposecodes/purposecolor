//
//  AlbumTitleCell.h
//  PurposeColor
//
//  Created by Purpose Code on 08/02/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumTitleCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *imgView;
@property (nonatomic,weak) IBOutlet UILabel *lblTitle;

@end
