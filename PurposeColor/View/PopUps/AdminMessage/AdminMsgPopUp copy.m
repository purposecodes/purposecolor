//
//  ReviewPopUp.m
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "AdminMsgPopUp.h"
#import "Constants.h"

@interface AdminMsgPopUp (){
    
    IBOutlet NSLayoutConstraint *constraintImageHeight;
    IBOutlet NSLayoutConstraint *topConstraintForTitle;
    IBOutlet NSLayoutConstraint *bottomConstraintForTitle;
    IBOutlet UIImageView *imgMedia;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UILabel *lblTitle;
    IBOutlet UIView *vwTitleBG;
    IBOutlet UIView *vwBg;
    NSDictionary *dictDetails;
}

@end

@implementation AdminMsgPopUp


-(void)setUp{
    
    [self setUpDesign];
    [self setUpGesture];
    [self getAdminMsg];
}

-(void)setUpGesture{
    
    UISwipeGestureRecognizer *leftRecognizer= [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft)];
    [leftRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight)];
    [rightRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [vwBg addGestureRecognizer:leftRecognizer];
    [vwBg addGestureRecognizer:rightRecognizer];
}

-(void)getAdminMsg{
    
}

-(void)setUpDesign{
    
    constraintImageHeight.constant = 0;
    [indicator stopAnimating];
    
    if (dictDetails) {
        float imageHeight = 0;
        if ([[dictDetails objectForKey:@"aps"] objectForKey:@"alert_img"]) {
            NSString *url = [[dictDetails objectForKey:@"aps"] objectForKey:@"alert_img"];
            float width = [[[dictDetails objectForKey:@"aps"] objectForKey:@"width"] floatValue];
            float height =[[[dictDetails objectForKey:@"aps"] objectForKey:@"height"] floatValue];
            float padding = 0;
            if ((width && height) > 0) {
                float ratio = width / height;
                AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                CGFloat deviceWidth = delegate.window.frame.size.width;
                imageHeight = (deviceWidth - padding) / ratio;
                constraintImageHeight.constant = imageHeight;
            }
            if (url.length) {
                [indicator startAnimating];
                [imgMedia sd_setImageWithURL:[NSURL URLWithString:url]
                            placeholderImage:[UIImage imageNamed:@""]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                       [indicator stopAnimating];
                                   }];
            }
        }if ([[dictDetails objectForKey:@"aps"] objectForKey:@"alert"]) {
            lblTitle.text = [[dictDetails objectForKey:@"aps"] objectForKey:@"alert"];
            CGSize textSize = { self.frame.size.width - 20, CGFLOAT_MAX };
            CGRect frame = [lblTitle.text boundingRectWithSize:textSize
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{ NSFontAttributeName:[UIFont fontWithName:CommonFont size:15] }
                                                       context:nil];
            CGSize size = CGSizeMake(frame.size.width, frame.size.height + 20);
            CAGradientLayer *gradient = [CAGradientLayer layer];
            gradient.frame =  CGRectMake(0, vwTitleBG.bounds.origin.y, vwTitleBG.bounds.size.width, size.height);
            UIColor *colr1 = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.0];
            UIColor *colr2 = [UIColor blackColor];
            gradient.colors = [NSArray arrayWithObjects:(id)[colr1 CGColor],(id)[colr2 CGColor], nil];
            [vwTitleBG.layer insertSublayer:gradient atIndex:0];
            
        }
        
    }
    
}

-(void)handleSwipeRight{
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:@"cube"];
    [animation setSubtype:kCATransitionFromRight];
    [[vwBg layer] addAnimation:animation forKey:@"cube"];
}
-(void)handleSwipeLeft{
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:@"cube"];
    [animation setSubtype:kCATransitionFromRight];
    [[vwBg layer] addAnimation:animation forKey:@"cube"];
}

-(void)loadImageWithDetails:(NSDictionary*)details{
    dictDetails = details;
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    [MBProgressHUD hideHUDForView:self animated:YES];
}

-(IBAction)goBack:(id)sender{
    
    
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
   
   
}


@end
