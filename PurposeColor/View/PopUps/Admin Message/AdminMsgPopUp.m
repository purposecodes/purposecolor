//
//  ReviewPopUp.m
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "AdminMsgPopUp.h"
#import "Constants.h"

@interface AdminMsgPopUp (){
    
    IBOutlet NSLayoutConstraint *constraintImageHeight;
    IBOutlet UIImageView *imgMedia;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UILabel *lblTitle;
    IBOutlet UIView *vwBg;
    IBOutlet UILabel *lblCount;
    NSDictionary *dictDetails;
    NSArray *arrAdminMessages;
    NSInteger indexOfObj;
}

@end

@implementation AdminMsgPopUp


-(void)setUpGoalReminderWithDetails:(NSDictionary*)details{
    
    if (details) {
        dictDetails = details;
    }
    [self setUpDesign];
   
}

-(void)showAdminMessages:(NSDictionary*)details{
    
    arrAdminMessages = [details objectForKey:@"messages"];
    if (arrAdminMessages.count) {
        dictDetails = [arrAdminMessages firstObject];
        lblCount.text = [NSString stringWithFormat:@"%ld/%lu",indexOfObj+1,(unsigned long)arrAdminMessages.count];
    }
     [self setUpGesture];
     [self setUpDesign];
}


-(void)setUpGesture{
    
    UISwipeGestureRecognizer *leftRecognizer= [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft)];
    [leftRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight)];
    [rightRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [vwBg addGestureRecognizer:leftRecognizer];
    [vwBg addGestureRecognizer:rightRecognizer];
    
  
}

-(void)getAdminMsg{
    
}

-(void)setUpDesign{
    
    constraintImageHeight.constant = 0;
    [indicator stopAnimating];
    
    if (dictDetails) {
        float imageHeight = 0;
        if ([dictDetails objectForKey:@"alert_img"]) {
            NSString *url = [dictDetails objectForKey:@"alert_img"];
            float width = [[dictDetails objectForKey:@"width"] floatValue];
            float height = [[dictDetails objectForKey:@"height"] floatValue];
            float padding = 40;
            if ((width && height) > 0) {
                float ratio = width / height;
                AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                CGFloat deviceWidth = delegate.window.frame.size.width;
                imageHeight = (deviceWidth - padding) / ratio;
                constraintImageHeight.constant = imageHeight;
            }
            if (url.length) {
                [indicator startAnimating];
                [imgMedia sd_setImageWithURL:[NSURL URLWithString:url]
                            placeholderImage:[UIImage imageNamed:@""]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                       [indicator stopAnimating];
                                   }];
            }
        }if ([dictDetails objectForKey:@"alert"]) {
            NSString *alert = [dictDetails objectForKey:@"alert"];
            NSMutableAttributedString* attrString = [[NSMutableAttributedString  alloc] initWithString:alert];
            NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
            [style setLineSpacing:5];
            [attrString addAttribute:NSParagraphStyleAttributeName
                               value:style
                               range:NSMakeRange(0, alert.length)];
            lblTitle.attributedText = attrString;
            lblTitle.textAlignment = NSTextAlignmentJustified;
            
            /*
            CGSize size = CGSizeMake(frame.size.width, frame.size.height + 20);
            CAGradientLayer *gradient = [CAGradientLayer layer];
            gradient.frame =  CGRectMake(0, vwTitleBG.bounds.origin.y, vwTitleBG.bounds.size.width, size.height);
            UIColor *colr1 = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.0];
            UIColor *colr2 = [UIColor blackColor];
            gradient.colors = [NSArray arrayWithObjects:(id)[colr1 CGColor],(id)[colr2 CGColor], nil];
            [vwTitleBG.layer insertSublayer:gradient atIndex:0];*/
            
            
        }
        
    }
    
}

-(void)handleSwipeLeft{
    
    if (indexOfObj < arrAdminMessages.count - 1) {
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.5];
        [animation setType:@"cube"];
        [animation setSubtype:kCATransitionFromRight];
        [[vwBg layer] addAnimation:animation forKey:@"cube"];
        indexOfObj ++;
        [self showNextObjIfAny];
    }   
}
-(void)handleSwipeRight{
    
    if (indexOfObj > 0) {
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.5];
        [animation setType:@"cube"];
        [animation setSubtype:kCATransitionFromLeft];
        [[vwBg layer] addAnimation:animation forKey:@"cube"];
        indexOfObj --;
        [self showNextObjIfAny];
    }
   
}

-(void)showNextObjIfAny{
    
    if (indexOfObj < arrAdminMessages.count) {
        dictDetails = arrAdminMessages[indexOfObj];
        [self setUpDesign];
        lblCount.text = [NSString stringWithFormat:@"%ld/%lu",indexOfObj+1,(unsigned long)arrAdminMessages.count];
    }
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    [MBProgressHUD hideHUDForView:self animated:YES];
}

-(IBAction)goBack:(id)sender{
    
    [self endEditing:YES];
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        [self removeFromSuperview];
        // if you want to do something once the animation finishes, put it here
    }];
    
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
   
    [vwBg.layer setBorderColor:[UIColor getSeperatorColor].CGColor];
    [vwBg.layer setBorderWidth:1.f];
    [vwBg.layer setCornerRadius:5];
    
}


@end
