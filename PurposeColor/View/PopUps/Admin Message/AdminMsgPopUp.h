//
//  ReviewPopUp.h
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AdminMsgPopUp : UIView

@property (nonatomic,assign) NSInteger objIndex;
-(void)setUpGoalReminderWithDetails:(NSDictionary*)details;
-(void)showAdminMessages:(NSDictionary*)details;

@end
