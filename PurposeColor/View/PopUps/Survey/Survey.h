//
//  ReviewPopUp.h
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SurveyDelegate <NSObject>


@optional


/*!
 *This method is invoked when user Clicks "CLOSE" Button
 */
//-(void)sharePostTextPopUpCloseApppliedAtIndex:(NSInteger)index;

/*!
 *This method is invoked when user Clicks "POST COMMENT" Button
 */
//-(void)sharePostSubmitAppliedWithText:(NSString*)shareTxt AtIndex:(NSInteger)index;


@end



@interface Survey : UIView

@property (nonatomic,weak)  id<SurveyDelegate>delegate;
-(void)showPopUpWithDetails:(NSDictionary*)details;

@end
