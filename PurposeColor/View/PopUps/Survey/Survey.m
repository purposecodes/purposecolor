//
//  ReviewPopUp.m
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "Survey.h"
#import "Constants.h"

@interface Survey(){
    
    IBOutlet UIView *vwBg;
    IBOutlet UIButton *btnSurevey;
    IBOutlet UIButton *btnCancel;
    IBOutlet NSLayoutConstraint *heightImage;
    IBOutlet NSLayoutConstraint *imageTop;
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblDescripiton;
    IBOutlet UIImageView *imgView;
    IBOutlet UIActivityIndicatorView *indicator;
    
    NSDictionary *details;
}

@end

@implementation Survey


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    // Drawing code
    [vwBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwBg.layer setBorderWidth:1.f];
    [vwBg.layer setCornerRadius:5];
    vwBg.clipsToBounds = YES;
    imgView.hidden = true;
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = btnSurevey.layer.bounds;
    gradientLayer.colors = [NSArray arrayWithObjects:
                            (id)[UIColor getThemeColor].CGColor,
                            (id)[UIColor colorWithRed:0.01 green:0.89 blue:1.00 alpha:1.0].CGColor,
                            nil];
    
    gradientLayer.locations = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:0.7f],
                               nil];
    
    [btnSurevey.layer addSublayer:gradientLayer];
    [btnSurevey.layer insertSublayer:gradientLayer atIndex:0];
    btnSurevey.titleLabel.font = [UIFont fontWithName:CommonFontBold size:14];
    [btnSurevey setTitle:@"TAKE SURVEY" forState:UIControlStateNormal];
    [btnSurevey setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}

-(void)showPopUpWithDetails:(NSDictionary*)_details{
    
    details = _details;
    lblTitle.text = [details objectForKey:@"title"];
    lblDescripiton.text = [details objectForKey:@"description"];
    NSString *url = [details objectForKey:@"image_url"];
    if (url.length) {
        imgView.hidden = false;
        [indicator startAnimating];
        [imgView sd_setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@""]
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                               [indicator stopAnimating];
                           }];
    }else{
        
        [indicator stopAnimating];
        imageTop.constant = 0;
        heightImage.constant = 0;
    }
    
    
}

-(IBAction)closePopUp{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [self removeFromSuperview];
    }];
}
-(IBAction)takeSurvey{
    
    NSString *url = [details objectForKey:@"url"];
    if (url.length) {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{}
                                     completionHandler:^(BOOL success) {
                                         [self closePopUp];
                                     }];
        }
    }
}


@end
