//
//  ReviewPopUp.h
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SharePopUpTextDelegate <NSObject>


@optional


/*!
 *This method is invoked when user Clicks "CLOSE" Button
 */
-(void)sharePostTextPopUpCloseApppliedAtIndex:(NSInteger)index;

/*!
 *This method is invoked when user Clicks "POST COMMENT" Button
 */
-(void)sharePostSubmitAppliedWithText:(NSString*)shareTxt AtIndex:(NSInteger)index;


@end



@interface SharePost : UIView

@property (nonatomic,weak)  id<SharePopUpTextDelegate>delegate;
@property (nonatomic,assign) NSInteger objIndex;

@end
