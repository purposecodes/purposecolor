//
//  ReviewPopUp.m
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "SharePost.h"
#import "Constants.h"

@interface SharePost(){
    
    IBOutlet UIView *vwBg;
    IBOutlet UITextView *txtView;
    IBOutlet NSLayoutConstraint *topConstraint;
    IBOutlet NSLayoutConstraint *centerConstraint;
    UIView *inputAccView;
    NSString *strDescription;
    float topValue;
}

@end

@implementation SharePost




#pragma mark - UITextView delegate methods


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    [self createInputAccessoryView];
    [textView setInputAccessoryView:inputAccView];
    [self adjustTopValue];
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
  
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
    [self getTextFromField:textView.text];
    return YES;
}


#pragma mark - Submit Values


-(void)getTextFromField:(NSString*)txt{
    
     strDescription = txt;
    
}

-(void)createInputAccessoryView{
    
    if (!inputAccView) {
        
        inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, 45.0)];
        [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
        [inputAccView setAlpha: 1];
        
        UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 85, 5.0f, 80.0f, 35.0f)];
        [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
        [btnDone setBackgroundColor:[UIColor getThemeColor]];
        btnDone.layer.cornerRadius = 5.f;
        btnDone.layer.borderWidth = 1.f;
        btnDone.layer.borderColor = [UIColor whiteColor].CGColor;
        
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
        btnDone.titleLabel.font = [UIFont fontWithName:CommonFont_New size:14];
        [inputAccView addSubview:btnDone];
    }
    
}

-(void)doneTyping{
    
    topConstraint.priority = 998;
    centerConstraint.priority = 999;
    [self endEditing:YES];
}

-(void)adjustTopValue{
    
    topConstraint.priority = 999;
    centerConstraint.priority = 998;

}

-(IBAction)shareApplied:(id)sender{
    
     [self closePopUp];
    if ([self.delegate respondsToSelector:@selector(sharePostSubmitAppliedWithText:AtIndex:)]) {
        [self.delegate sharePostSubmitAppliedWithText:strDescription AtIndex:(NSInteger)_objIndex];
    }
}

-(IBAction)closePopUp:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(sharePostTextPopUpCloseApppliedAtIndex:)]) {
        [self.delegate sharePostTextPopUpCloseApppliedAtIndex:_objIndex];
    }
    [self closePopUp];
   
}

-(void)closePopUp{
    
    [self endEditing:YES];
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.transform = CGAffineTransformMakeScale(0.0001, 0.0001);
    } completion:^(BOOL finished){
        [self removeFromSuperview];
        // if you want to do something once the animation finishes, put it here
    }];
    
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
   
    [txtView.layer setBorderColor:[UIColor getSeperatorColor].CGColor];
    [txtView.layer setBorderWidth:1.f];
    [txtView.layer setCornerRadius:5];
    txtView.clipsToBounds = YES;
    
    // Drawing code
    [vwBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwBg.layer setBorderWidth:1.f];
    [vwBg.layer setCornerRadius:5];
    vwBg.clipsToBounds = YES;
}


@end
