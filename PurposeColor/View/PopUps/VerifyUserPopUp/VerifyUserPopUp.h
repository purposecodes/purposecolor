//
//  ReviewPopUp.h
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

@protocol VerifyUserProfileDelegate <NSObject>


@optional


/*!
 *This method is invoked when user Clicks "CLOSE" Button
 */

/*!
 *This method is invoked when user Clicks "POST COMMENT" Button
 */


@end

#import <UIKit/UIKit.h>

@interface VerifyUserPopUp : UIView

@property (nonatomic,weak)  id<VerifyUserProfileDelegate>delegate;


@end
