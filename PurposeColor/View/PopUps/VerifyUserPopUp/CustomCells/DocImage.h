//
//  FeedbackDescription.h
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocImage : UITableViewCell

@property (nonatomic,weak) IBOutlet NSLayoutConstraint *imageHeight;
@property (nonatomic,weak) IBOutlet UIView *vwBg;
@property (nonatomic,weak) IBOutlet UIImageView *imgView;


@end
