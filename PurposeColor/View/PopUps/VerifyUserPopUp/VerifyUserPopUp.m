//
//  ReviewPopUp.m
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "VerifyUserPopUp.h"
#import "Constants.h"
#import "VerifyUserNormalCell.h"
#import "UserDescriptionCell.h"
#import "DocImage.h"

@interface VerifyUserPopUp()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
    IBOutlet UIView *vwBg;
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnSubmit;
    UIView *inputAccView;
    
    NSString *strName;
    NSString *strEmail;
    NSString *strAddress;
    NSString *strPhone;
    UIImage *imgDoc;
    float imageHeight;

}

@end

@implementation VerifyUserPopUp



#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rows = 4;
    if (imgDoc) {
        rows += 1;
    }
    return rows;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row == 0) {
        NSString *MyIdentifier = @"VerifyUserNormalCell";
        VerifyUserNormalCell *_cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VerifyUserNormalCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            _cell = [topLevelObjects objectAtIndex:0];
            _cell.txtField.placeholder = @"Name";
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            _cell.txtField.leftView = paddingView;
            _cell.txtField.leftViewMode = UITextFieldViewModeAlways;
           
        }
        _cell.txtField.keyboardType = UIKeyboardTypeDefault;
        _cell.txtField.tag = 0;
        _cell.txtField.text = strName;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
         return _cell;
        
    }
    else if (indexPath.row == 1) {
        NSString *MyIdentifier = @"VerifyUserNormalCell";
        VerifyUserNormalCell *_cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VerifyUserNormalCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            _cell = [topLevelObjects objectAtIndex:0];
            _cell.txtField.placeholder = @"Email";
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            _cell.txtField.leftView = paddingView;
            _cell.txtField.leftViewMode = UITextFieldViewModeAlways;
        }
        _cell.txtField.keyboardType = UIKeyboardTypeDefault;
         _cell.txtField.tag = 1;
        _cell.txtField.text = strEmail;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
         return _cell;
    }
    else if (indexPath.row == 2) {
        NSString *MyIdentifier = @"VerifyUserNormalCell";
        VerifyUserNormalCell *_cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VerifyUserNormalCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            _cell = [topLevelObjects objectAtIndex:0];
        }
        _cell.txtField.keyboardType = UIKeyboardTypePhonePad;
         _cell.txtField.placeholder = @"Phone";
         _cell.txtField.tag = 2;
        _cell.txtField.text = strPhone;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
         return _cell;
    }
    else if (indexPath.row == 3) {
        NSString *MyIdentifier = @"UserDescriptionCell";
        UserDescriptionCell *_cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"UserDescriptionCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            _cell = [topLevelObjects objectAtIndex:0];
        }
        _cell.txtView.keyboardType = UIKeyboardTypeDefault;
        _cell.txtView.tag = 3;
        _cell.txtView.text = strAddress;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return _cell;
    }
    else if (indexPath.row == 4) {
        NSString *MyIdentifier = @"DocImage";
        DocImage *_cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DocImage" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            _cell = [topLevelObjects objectAtIndex:0];
        }
        if (imgDoc) {
            _cell.imgView.image = imgDoc;
            _cell.imageHeight.constant = imageHeight;
        }
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return _cell;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 3) {
        return 150;
    }if (indexPath.row == 4) {
        return imageHeight + 20;
    }
    return 50;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.01;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *vwFooter = [UIView new];
    vwFooter.backgroundColor = [UIColor clearColor];
    
    UIButton *btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    [vwFooter addSubview:btnDelete];
    btnDelete.translatesAutoresizingMaskIntoConstraints = NO;
    [btnDelete addTarget:self action:@selector(showCamera:)
        forControlEvents:UIControlEventTouchUpInside];
    btnDelete.layer.borderColor = [UIColor clearColor].CGColor;
    btnDelete.titleLabel.font = [UIFont fontWithName:CommonFont_New size:15];
    [btnDelete setTitle:@"Upload Address Proof" forState:UIControlStateNormal];
    [btnDelete setBackgroundColor:[UIColor getThemeColor]];
    [btnDelete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDelete.layer setBorderColor:[UIColor clearColor].CGColor];
    [btnDelete.layer setBorderWidth:1.f];
    [btnDelete.layer setCornerRadius:5];
    
    [vwFooter addConstraint:[NSLayoutConstraint constraintWithItem:btnDelete
                                                         attribute:NSLayoutAttributeLeft
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwFooter
                                                         attribute:NSLayoutAttributeLeft
                                                        multiplier:1.0
                                                          constant:10]];
    
    [vwFooter addConstraint:[NSLayoutConstraint constraintWithItem:btnDelete
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwFooter
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1.0
                                                          constant:10]];
    
    [btnDelete addConstraint:[NSLayoutConstraint constraintWithItem:btnDelete
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:1.0
                                                          constant:170]];
    
    [btnDelete addConstraint:[NSLayoutConstraint constraintWithItem:btnDelete
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0
                                                           constant:40]];
    return vwFooter;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 60.0;
}

#pragma mark - Gallery methods


-(IBAction)showCamera:(id)sender{
    
    UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Upload Address Proof" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        [self openCamera];
        
    }];
    [alert addAction:camera];
    UIAlertAction* gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        [self showGallery];
        
    }];
    
    [alert addAction:gallery];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                             {
                                 
                             }];
    
    [alert addAction:cancel];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
    
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = [Utility fixrotation:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    
    float width = image.size.width;
    float height = image.size.height;
    float ratio = width / height;
    height = (self.frame.size.width - 60) / ratio;
    if (image) {
        UIImage *newImage = [self imageWithImage:image convertToSize:CGSizeMake(self.frame.size.width - 60, height)];
        if (!newImage) imgDoc = image;
        else imgDoc = newImage;
        imageHeight = newImage.size.height;
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        
        
    }];
    
    [tableView reloadData];
}

-(void)showGallery{
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.delegate = self;
    [app.window.rootViewController presentViewController:picker animated:YES completion:nil];
}
-(void)openCamera{
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;

    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    [app.window.rootViewController presentViewController:picker animated:YES completion:nil];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


#pragma mark - UITextfield delegate methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    [self getTextFromField:textField.text andTag:textField.tag];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.autocorrectionType = UITextAutocorrectionTypeYes;
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    NSIndexPath *indexPath;
    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
    indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    [tableView setContentOffset:contentOffset animated:YES];
    
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    
}

#pragma mark - UITextView delegate methods


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    [self createInputAccessoryView];
    [textView setInputAccessoryView:inputAccView];
    CGPoint pointInTable = [textView.superview.superview convertPoint:textView.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    NSIndexPath *indexPath;
    CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
    indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint pointInTable = [textView.superview.superview convertPoint:textView.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
    CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    [self getTextFromField:textView.text andTag:textView.tag];
    return YES;
}


#pragma mark - Submit Values

-(void)uploadMediasToServerOnSuccess:(void (^)(id responseObject))success failure:(void (^)( NSError *error))failure{
    
    
    [APIMapper uploadMediasAtPath:nil orAsObject:imgDoc type:@"doc" Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        failure(error);
    }];
    
}

-(void)getTextFromField:(NSString*)txt andTag:(NSInteger)tag{
    
    NSString *string = txt;
    switch (tag) {
        case 0:
            strName = string;
            break;
        case 1:
            strEmail = string;
            break;
        case 2:
            strPhone = string;
            break;
        case 3:
            strAddress = string;
            break;
            
        default:
            break;
    }
    
}

-(void)createInputAccessoryView{
    
    if (!inputAccView) {
        inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, 45.0)];
        [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
        [inputAccView setAlpha: 1];
        
        UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 90, 5.0f, 85.0f, 35.0f)];
        [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
        [btnDone setBackgroundColor:[UIColor getThemeColor]];
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDone.layer.cornerRadius = 5.f;
        btnDone.layer.borderWidth = 1.f;
        btnDone.layer.borderColor = [UIColor whiteColor].CGColor;
        btnDone.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
        [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
        [inputAccView addSubview:btnDone];
    }
    
}

-(void)doneTyping{
    
    [self endEditing:YES];
}

-(IBAction)submitClicked:(id)sender{
    
    [self endEditing:YES];
    [self checkAllFieldsAreValid:^{
        
        [self showLoadingScreen];
        
        [self uploadMediasToServerOnSuccess:^(id responseObject) {
            if ([responseObject objectForKey:@"media_name"]) {
                [self resumeVerifyUserUploadedImageName:[responseObject objectForKey:@"media_name"]];
            }else{
                [self hideLoadingScreen];
                if ([responseObject objectForKey:@"text"]) {
                    [ALToastView toastInView:self withText:[responseObject objectForKey:@"text"]];
                }
            }
            
        } failure:^(NSError *error) {
            
            [self hideLoadingScreen];
            [ALToastView toastInView:self withText:error.localizedDescription];
            
        }];
        
        
    }failure:^(NSString *errorMsg) {
        
        [self hideLoadingScreen];
        [self showAlertWithMessage:errorMsg title:@"Verify User"];
        
    }];
    
}

-(void)checkAllFieldsAreValid:(void (^)())success failure:(void (^)(NSString *errorMsg))failure{
    
    BOOL isValid = false;
    NSString *errorMsg;
    if ((strName.length) && (strEmail.length) && (strPhone.length) > 0 && (strAddress.length) && imgDoc) {
        isValid = true;
        if (![strEmail isValidEmail]) {
            
            errorMsg = @"Invalid Email Address.";
            isValid = false;
        }
    }else{
        
        errorMsg = @"Please fill all fields";
    }
    if (isValid)success();
    else failure(errorMsg);
    
}

-(void)showAlertWithMessage:(NSString*)alertMessage title:(NSString*)alertTitle{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:alertTitle
                                          message:alertMessage
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                               }];
    
    [alertController addAction:okAction];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    
    
}

-(void)resumeVerifyUserUploadedImageName:(NSString*)imageName{
    
    [APIMapper verifyUserProfileWithName:strName email:strEmail address:strAddress phone:strPhone uploadedImage:imageName success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject objectForKey:@"text"])
            [[[UIAlertView alloc] initWithTitle:@"Verify User" message:[responseObject objectForKey:@"text"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        if ([[responseObject objectForKey:@"code"]integerValue] == 200) {
            [self closePopUp:nil];
        }
         [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
         [self hideLoadingScreen];
        [[[UIAlertView alloc] initWithTitle:@"Verify User" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }];
    

}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self animated:YES];
    
}
-(IBAction)closePopUp:(id)sender{
    
    [self endEditing:YES];
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        [self removeFromSuperview];
        // if you want to do something once the animation finishes, put it here
    }];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    [btnSubmit.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btnSubmit.layer setBorderWidth:1.f];
    [btnSubmit.layer setCornerRadius:5];
    
    // Drawing code
    [vwBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwBg.layer setBorderWidth:1.f];
    [vwBg.layer setCornerRadius:5];
    vwBg.clipsToBounds = YES;
}


@end
