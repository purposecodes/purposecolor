//
//  FeedbackCellTitleCell.h
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackCellTitleCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UITextField *txtField;
@property (nonatomic,weak) IBOutlet UIView *vwBg;

@end
