//
//  FeedbackCellTitleCell.m
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "FeedbackCellTitleCell.h"

@implementation FeedbackCellTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_vwBg.layer setBorderColor:[UIColor getSeperatorColor].CGColor];
    [_vwBg.layer setBorderWidth:1.f];
    [_vwBg.layer setCornerRadius:3.f];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
