//
//  ReviewPopUp.m
//  PurposeColor
//
//  Created by Purpose Code on 29/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "ReviewPopUp.h"
#import "Constants.h"
#import "FeedbackCellTitleCell.h"
#import "FeedbackDescription.h"

@interface ReviewPopUp(){
    
    IBOutlet UIView *vwBg;
    IBOutlet UITableView *tableView;
    IBOutlet NSLayoutConstraint *topConstraint;
    IBOutlet NSLayoutConstraint *centerConstraint;
    UIView *inputAccView;
    
    NSString *strName;
    NSString *strEmail;
    NSString *strDescription;
    float topValue;
}

@end

@implementation ReviewPopUp



#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row == 0) {
        NSString *MyIdentifier = @"FeedbackCellTitleCell";
        FeedbackCellTitleCell *_cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FeedbackCellTitleCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            _cell = [topLevelObjects objectAtIndex:0];
            _cell.txtField.placeholder = @"Name";
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            _cell.txtField.leftView = paddingView;
            _cell.txtField.leftViewMode = UITextFieldViewModeAlways;
           
        }
        _cell.txtField.tag = 0;
        _cell.txtField.text = strName;
         return _cell;
        
    }
    else if (indexPath.row == 1) {
        NSString *MyIdentifier = @"FeedbackCellTitleCell";
        FeedbackCellTitleCell *_cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FeedbackCellTitleCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            _cell = [topLevelObjects objectAtIndex:0];
            _cell.txtField.placeholder = @"Email";
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            _cell.txtField.leftView = paddingView;
            _cell.txtField.leftViewMode = UITextFieldViewModeAlways;
        }
         _cell.txtField.tag = 1;
        _cell.txtField.text = strEmail;
         return _cell;
    }
    else if (indexPath.row == 2) {
        NSString *MyIdentifier = @"FeedbackDescription";
        FeedbackDescription *_cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FeedbackDescription" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            _cell = [topLevelObjects objectAtIndex:0];
        }
         _cell.txtView.tag = 2;
        _cell.txtView.text = strDescription;
         return _cell;
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 2) {
        return 250;
    }
    return 50;
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor whiteColor];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
   
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    lblTitle.text = @"Feedback";
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.font = [UIFont fontWithName:CommonFontBold size:20];
    lblTitle.textColor = [UIColor blackColor];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1.0
                                                          constant:20]];
    
    
    UIButton *btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    [vwHeader addSubview:btnDelete];
    btnDelete.translatesAutoresizingMaskIntoConstraints = NO;
    [btnDelete addTarget:self action:@selector(closePopUp:)
        forControlEvents:UIControlEventTouchUpInside];
    [btnDelete setImage:[UIImage imageNamed:@"Close_Gray"] forState:UIControlStateNormal];
    [btnDelete addConstraint:[NSLayoutConstraint constraintWithItem:btnDelete
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0
                                                      constant:40]];
    
    [btnDelete addConstraint:[NSLayoutConstraint constraintWithItem:btnDelete
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0
                                                      constant:40]];
    
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnDelete
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:vwHeader
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.0
                                                           constant:-10]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnDelete
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1.0
                                                          constant:10]];

   
    
    return vwHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 60;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *vwFooter = [UIView new];
    vwFooter.backgroundColor = [UIColor getThemeColor];
    
    UIButton *btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    [vwFooter addSubview:btnDelete];
    btnDelete.translatesAutoresizingMaskIntoConstraints = NO;
    [btnDelete addTarget:self action:@selector(submitClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    btnDelete.layer.borderColor = [UIColor clearColor].CGColor;
    btnDelete.titleLabel.font = [UIFont fontWithName:CommonFontBold size:17];
    [btnDelete setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [btnDelete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [vwFooter addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnDelete]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnDelete)]];
    [vwFooter addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[btnDelete]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnDelete)]];
    
    return vwFooter;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 50;
}


#pragma mark - UITextfield delegate methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    [self getTextFromField:textField.text andTag:textField.tag];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.autocorrectionType = UITextAutocorrectionTypeYes;
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    NSIndexPath *indexPath;
    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
    indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    [tableView setContentOffset:contentOffset animated:YES];
    [self adjustTopValue];
    
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    
}

#pragma mark - UITextView delegate methods


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    [self createInputAccessoryView];
    [textView setInputAccessoryView:inputAccView];
    CGPoint pointInTable = [textView.superview.superview convertPoint:textView.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    NSIndexPath *indexPath;
    CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
    indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    [self adjustTopValue];
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint pointInTable = [textView.superview.superview convertPoint:textView.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
    CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    [self getTextFromField:textView.text andTag:textView.tag];
    return YES;
}


#pragma mark - Submit Values


-(void)getTextFromField:(NSString*)txt andTag:(NSInteger)tag{
    
    NSString *string = txt;
    switch (tag) {
        case 0:
            strName = string;
            break;
        case 1:
            strEmail = string;
            break;
        case 2:
            strDescription = string;
            break;
            
        default:
            break;
    }
    
}

-(void)createInputAccessoryView{
    
    if (!inputAccView) {
        inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, 45.0)];
        [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
        [inputAccView setAlpha: 1];
        
        UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 90, 5.0f, 85.0f, 35.0f)];
        [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
        [btnDone setBackgroundColor:[UIColor getThemeColor]];
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDone.layer.cornerRadius = 5.f;
        btnDone.layer.borderWidth = 1.f;
        btnDone.layer.borderColor = [UIColor whiteColor].CGColor;
        btnDone.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
        [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
        [inputAccView addSubview:btnDone];
    }
    
}

-(void)doneTyping{
    
    topConstraint.priority = 998;
    centerConstraint.priority = 999;
    [self endEditing:YES];
}

-(IBAction)submitClicked:(id)sender{
    
    [self endEditing:YES];
    [self checkAllFieldsAreValid:^{
        
        [self showLoadingScreen];
        [APIMapper postFeedBackWithName:strName email:strEmail description:strDescription success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self showAlertWithMessage:[responseObject objectForKey:@"text"] title:@"Feedback"];
            [self hideLoadingScreen];
            [self closePopUp:nil];
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            [self showAlertWithMessage:[error localizedDescription] title:@"Feedback"];
            [self hideLoadingScreen];
        }];
        
        
    }failure:^(NSString *errorMsg) {
        
        [self showAlertWithMessage:errorMsg title:@"Feedback"];
        
    }];
    
}

-(void)checkAllFieldsAreValid:(void (^)())success failure:(void (^)(NSString *errorMsg))failure{
    
    BOOL isValid = false;
    NSString *errorMsg;
    if ((strName.length) && (strEmail.length) && (strDescription.length) > 0) {
        isValid = true;
        if (![strEmail isValidEmail]) {
            
            errorMsg = @"Invalid Email Address.";
            isValid = false;
        }
    }else{
        
        errorMsg = @"Please fill all fields.";
    }
    if (isValid)success();
    else failure(errorMsg);
    
}

-(void)showAlertWithMessage:(NSString*)alertMessage title:(NSString*)alertTitle{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:alertTitle
                                          message:alertMessage
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                               }];
    
    [alertController addAction:okAction];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    
    
}
-(void)adjustTopValue{
    
    topConstraint.priority = 999;
    centerConstraint.priority = 998;

}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self animated:YES];
    
}
-(IBAction)closePopUp:(id)sender{
    
    [self endEditing:YES];
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        [self removeFromSuperview];
        // if you want to do something once the animation finishes, put it here
    }];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    strName = [User sharedManager].name;
    strEmail = [User sharedManager].email;
    [tableView reloadData];
    
    // Drawing code
    [vwBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwBg.layer setBorderWidth:1.f];
    [vwBg.layer setCornerRadius:5];
    vwBg.clipsToBounds = YES;
}


@end
