//
//  MyFavouritesListingViewController.h
//  PurposeColor
//
//  Created by Purpose Code on 23/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//



#import <UIKit/UIKit.h>

@interface SearchedUserPostListing : UIViewController

@property (nonatomic,strong) NSString *strToUserID;
@property (nonatomic,strong) NSString *strUserName;


@end
