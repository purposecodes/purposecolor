//
//  GoalsAndDreamsListingViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 21/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eCompleted = 1,
    ePending = 0,
    
} eStatus;


#define kSectionCount               1
#define kSuccessCode                200
#define kMinimumCellCount           0
#define kHeaderHeight               0.1
#define kCellHeight                 395
#define kEmptyHeaderAndFooter       0
#define kUnAuthorized               403

#import "GoalsAndDreamsListingViewController.h"
#import "GoalsAndDreamsCustomCell.h"
#import "CreateActionInfoViewController.h"
#import "GoalDetailViewController.h"
#import "MenuViewController.h"
#import "MTDURLPreview.h"
#import "HashTagGemListingViewController.h"
#import "WebBrowserViewController.h"
#import "GoalsAndDreams+CoreDataClass.h"
#import "ActionDetailPageViewController.h"
#import <CoreText/CoreText.h>

@interface GoalsAndDreamsListingViewController ()<GoalsAndDreamsCustomCellDelegate,GoalDetailViewDelegate,SWRevealViewControllerDelegate,CreateMediaInfoDelegate,ActionDetailsDelegate>{
    
    IBOutlet UIButton *btnScrollTop;
    IBOutlet UITableView *tableView;
    IBOutlet UIView *vwPaginationPopUp;
    IBOutlet NSLayoutConstraint *paginationBottomConstraint;
    IBOutlet NSLayoutConstraint *rightForGoals;
    IBOutlet UIButton *btnSlideMenu;
    IBOutlet UIView *vwOverLay;
    IBOutlet UIView *vwSegmentSelection;
    IBOutlet UIButton *btnPending;
    IBOutlet UIButton *btnCompleted;
    IBOutlet UIButton *btnCreate;
    
    UIRefreshControl *refreshControl;
    BOOL isDataAvailable;
    BOOL isPageRefresing;
    NSInteger totalPages_pending;
    NSInteger totalPages_completed;
    
    NSInteger currentPage_completed;
    NSInteger currentPage_pending;
    NSString *strNoDataText;
    
    NSMutableArray *arrCompleted;
    NSMutableArray *arrPending;
    NSMutableArray *arrDataSource;
    
    eStatus EStatus;
   
    BOOL isCachedData;

}

@end

@implementation GoalsAndDreamsListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self loadAllGoalsAndDreamsByPagination:NO withPageNumber:currentPage_pending shouldScrollUp:NO];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)setUp{

    btnScrollTop.hidden = true;
    arrPending         = [NSMutableArray new];
    arrCompleted       = [NSMutableArray new];
    arrDataSource      = [NSMutableArray new];
    
    currentPage_pending = 1;
    currentPage_completed = 1;
    
    totalPages_pending = 0;
    totalPages_completed = 0;
    
    EStatus = ePending;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    isDataAvailable = false;
    tableView.hidden = true;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 300;
    [tableView setContentInset:UIEdgeInsetsMake(0,0,100,0)];
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,tableView.bounds.size.width, 0.01f)];


    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    btnCreate.layer.cornerRadius = 5.f;
    btnCreate.layer.borderColor = [UIColor clearColor].CGColor;
    btnCreate.layer.borderWidth = 1.f;
    

}

-(IBAction)segmentChanged:(UIButton*)sender{
    
    [arrDataSource removeAllObjects];
    
    if (sender.tag == 1) {
        EStatus = ePending;
        if (arrPending.count <= 0) {
            currentPage_pending = 1;
            totalPages_pending = 0;
            [self loadAllGoalsAndDreamsByPagination:NO withPageNumber:currentPage_pending shouldScrollUp:NO];
           
        }
        
         [self changeAnimatedSelectionToCompleted:NO];
    }else{
        
        EStatus = eCompleted;
        if (arrCompleted.count <= 0) {
            currentPage_completed = 1;
            totalPages_completed = 0;
            [self loadAllGoalsAndDreamsByPagination:NO withPageNumber:currentPage_completed shouldScrollUp:NO];
           
        }
         [self changeAnimatedSelectionToCompleted:YES];
    }
    
   [self configureDataSource:NO];
    
}

-(void)changeAnimatedSelectionToCompleted:(BOOL)toCompeleted{
    
    if (toCompeleted) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = vwSegmentSelection.frame;
            frame.origin.x = self.view.frame.size.width / 2;
            vwSegmentSelection.frame = frame;
        }completion:^(BOOL finished) {
            [btnPending setAlpha:0.7];
            [btnCompleted setAlpha:1];
            
        }];
    }else{
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = vwSegmentSelection.frame;
            frame.origin.x = 0;
            vwSegmentSelection.frame = frame;
        }completion:^(BOOL finished) {
            [btnCompleted setAlpha:0.7];
            [btnPending setAlpha:1];
        }];
    }
    
}


#pragma mark - API Integration

-(void)loadAllGoalsAndDreamsByPagination:(BOOL)isPagination withPageNumber:(NSInteger)pageNo shouldScrollUp:(BOOL)shouldScroll {
    
    if (!isPagination) {
        [self showLoadingScreen];
        tableView.hidden = true;
    }
    
    [APIMapper loadAllMyGoalsAndDreamsWith:[User sharedManager].userId pageNo:pageNo status:EStatus success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        tableView.hidden = false;
        isPageRefresing = NO;
        [refreshControl endRefreshing];
        [self getGoalsAndDreamsFrom:responseObject shouldScrollUp:shouldScroll];
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        [self saveRecordsToDB:responseObject pageNumber:pageNo];
        dispatch_async(dispatch_get_main_queue(),^{
            if (shouldScroll && arrDataSource.count) {
                NSIndexPath *scrollToPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [tableView scrollToRowAtIndexPath:scrollToPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
        });
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        if (error && error.localizedDescription){
            [ALToastView toastInView:self.view withText:NETWORK_ERROR_MESSAGE];
            strNoDataText = error.localizedDescription;
            
        }
        isPageRefresing = NO;
        [refreshControl endRefreshing];
        tableView.hidden = false;
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        [tableView reloadData];
        if (pageNo == 1) [self loadAllCashedRecords];
            
        
    }];
}

-(void)refreshData{
    
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    [arrCompleted removeAllObjects];
    [arrPending removeAllObjects];
    [arrDataSource removeAllObjects];
    [self showLoadingScreen];
    currentPage_completed = 1;
    currentPage_pending = 1;
    isPageRefresing = YES;
    NSInteger currentPage = 1;
    isCachedData = false;
    [self loadAllGoalsAndDreamsByPagination:YES withPageNumber:currentPage shouldScrollUp:YES];
    
    
}

-(void)getGoalsAndDreamsFrom:(NSDictionary*)responds shouldScrollUp:(BOOL)shouldScrollUp{
    
    NSArray *goals;
    if (EStatus == ePending) {
        EStatus = ePending;
        if (NULL_TO_NIL([responds objectForKey:@"goalsanddreams"])) {
            goals = [responds objectForKey:@"goalsanddreams"];
            if (goals.count) [arrPending addObjectsFromArray:goals];
            if (NULL_TO_NIL([[responds objectForKey:@"header"] objectForKey:@"currentPage"]))
                currentPage_pending = [[[responds objectForKey:@"header"] objectForKey:@"currentPage"] integerValue];
            if (NULL_TO_NIL([[responds objectForKey:@"header"] objectForKey:@"pageCount"]))
                totalPages_pending = [[[responds objectForKey:@"header"] objectForKey:@"pageCount"] integerValue];
            if (arrPending.count > 0) {
                arrDataSource = [NSMutableArray arrayWithArray:arrPending];
            }
        }
    }else{
        EStatus = eCompleted;
        if (NULL_TO_NIL([responds objectForKey:@"goalsanddreams"])) {
            goals = [responds objectForKey:@"goalsanddreams"];
            if (goals.count) [arrCompleted addObjectsFromArray:goals];
            if (NULL_TO_NIL([[responds objectForKey:@"header"] objectForKey:@"currentPage"]))
                currentPage_completed = [[[responds objectForKey:@"header"] objectForKey:@"currentPage"] integerValue];
            if (NULL_TO_NIL([[responds objectForKey:@"header"] objectForKey:@"pageCount"]))
                totalPages_completed = [[[responds objectForKey:@"header"] objectForKey:@"pageCount"] integerValue];
            if (arrCompleted.count > 0) {
                arrDataSource = [NSMutableArray arrayWithArray:arrCompleted];
            }

        }
    }
    if ([[[responds objectForKey:@"header"] objectForKey:@"code"] integerValue] != kSuccessCode) {
        strNoDataText = [[responds objectForKey:@"header"] objectForKey:@"text"];
    }
    [self configureDataSource:shouldScrollUp];
}

-(void)configureDataSource:(BOOL)shouldScrollUp{
    
    if (EStatus == ePending) {
        EStatus = ePending;
        if (arrPending.count > 0) {
            arrDataSource = [NSMutableArray arrayWithArray:arrPending];
        }
    }else{
        
        EStatus = eCompleted;
        if (arrCompleted.count > 0) {
            arrDataSource = [NSMutableArray arrayWithArray:arrCompleted];
        }
    }
    isDataAvailable = false;
    if (arrDataSource.count) isDataAvailable = true;
    [tableView reloadData];
    if (arrDataSource.count && shouldScrollUp) {
        NSIndexPath *scrollToPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [tableView scrollToRowAtIndexPath:scrollToPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }

}


#pragma mark - DB Handling

-(void)saveRecordsToDB:(NSDictionary*)responseObject pageNumber:(NSInteger)pageNumber{
    
    if (isDataAvailable && arrDataSource.count) {
        if (pageNumber == 1) [self clearTable]; // First call, clear all entries.
        if (responseObject) {
            NSError * err;
            NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:responseObject options:0 error:&err];
            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            if (myString) {
                NSManagedObjectContext* wContext = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
                GoalsAndDreams * _goalsAndDreams = [[GoalsAndDreams alloc] initWithContext: wContext ];
                _goalsAndDreams.responds = myString;
                _goalsAndDreams.pagenumber = pageNumber;
                _goalsAndDreams.type = EStatus;
                if (![wContext save:nil]) {
                }
            }
        }
    }
    
}

-(void)clearTable{
    
    NSManagedObjectContext* context = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"GoalsAndDreams" inManagedObjectContext:context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type == %d", EStatus];
    [fetch setPredicate:predicate];
    
    NSArray * result = [context executeFetchRequest:fetch error:nil];
    for (id basket in result)
        [context deleteObject:basket];
}

-(void)loadAllCashedRecords{
    
    isCachedData = true;
    NSError *error = nil;
    NSManagedObjectContext* context = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"GoalsAndDreams" inManagedObjectContext:context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type == %d", EStatus];
    [fetch setPredicate:predicate];
    [arrDataSource removeAllObjects];
    NSArray *results = [context executeFetchRequest:fetch error:&error];
    NSError * err;
    for (GoalsAndDreams *_community in results) {
        
        NSString *myString = _community.responds;
        NSData *data =[myString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * responds;
        if(data!=nil){
            responds = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            NSArray *goals;
            if (EStatus == ePending) {
                EStatus = ePending;
                if (NULL_TO_NIL([responds objectForKey:@"goalsanddreams"])) {
                    goals = [responds objectForKey:@"goalsanddreams"];
                    if (goals.count) [arrPending addObjectsFromArray:goals];
                    if (NULL_TO_NIL([[responds objectForKey:@"header"] objectForKey:@"currentPage"]))
                        currentPage_pending = [[[responds objectForKey:@"header"] objectForKey:@"currentPage"] integerValue];
                    if (NULL_TO_NIL([[responds objectForKey:@"header"] objectForKey:@"pageCount"]))
                        totalPages_pending = [[[responds objectForKey:@"header"] objectForKey:@"pageCount"] integerValue];
                    if (arrPending.count > 0) {
                        arrDataSource = [NSMutableArray arrayWithArray:arrPending];
                    }
                }
            }else{
                EStatus = eCompleted;
                if (NULL_TO_NIL([responds objectForKey:@"goalsanddreams"])) {
                    goals = [responds objectForKey:@"goalsanddreams"];
                    if (goals.count) [arrCompleted addObjectsFromArray:goals];
                    if (NULL_TO_NIL([[responds objectForKey:@"header"] objectForKey:@"currentPage"]))
                        currentPage_completed = [[[responds objectForKey:@"header"] objectForKey:@"currentPage"] integerValue];
                    if (NULL_TO_NIL([[responds objectForKey:@"header"] objectForKey:@"pageCount"]))
                        totalPages_completed = [[[responds objectForKey:@"header"] objectForKey:@"pageCount"] integerValue];
                    if (arrCompleted.count > 0) {
                        arrDataSource = [NSMutableArray arrayWithArray:arrCompleted];
                    }
                    
                }
            }
        
            [self configureDataSource:NO];
        }
    }
   
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    return arrDataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:strNoDataText];
        cell.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.96 alpha:1.0];
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.96 alpha:1.0];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}


- (void)addReadMoreStringToUILabel:(UILabel*)label
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSString *readMoreText = @" ...Continue Reading";
        NSMutableAttributedString *answerAttributed;
        NSInteger lengthForVisibleString =  [self fitString:label.text intoLabel:label];
        if (lengthForVisibleString > 0) {
            NSMutableString *mutableString = [[NSMutableString alloc] initWithString:label.text];
            NSString *trimmedString = [mutableString stringByReplacingCharactersInRange:NSMakeRange(lengthForVisibleString, (label.text.length - lengthForVisibleString)) withString:@""];
            NSInteger readMoreLength = readMoreText.length;
            NSString *trimmedForReadMore = [trimmedString stringByReplacingCharactersInRange:NSMakeRange((trimmedString.length - readMoreLength), readMoreLength) withString:@""];
             answerAttributed = [[NSMutableAttributedString alloc] initWithString:trimmedForReadMore attributes:@{
                                                                                                                                            NSFontAttributeName : label.font
                                                                                                                                            }];
            
            NSMutableAttributedString *readMoreAttributed = [[NSMutableAttributedString alloc] initWithString:readMoreText attributes:@{
                                                                                                                                        NSFontAttributeName : [UIFont fontWithName:CommonFont size:14],
                                                                                                                                        NSForegroundColorAttributeName : [UIColor lightGrayColor]
                                                                                                                                        }];
            
            [answerAttributed appendAttributedString:readMoreAttributed];
           
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
             label.attributedText = answerAttributed;
        });
    });
    
    
   
}

- (NSUInteger)fitString:(NSString *)string intoLabel:(UILabel *)label
{
    UIFont *font           = label.font;
    NSLineBreakMode mode   = label.lineBreakMode;
    
    CGFloat labelWidth     = self.view.frame.size.width - 25;
    CGFloat labelHeight    = 112;
    CGSize  sizeConstraint = CGSizeMake(labelWidth, CGFLOAT_MAX);
    
    NSDictionary *attributes = @{ NSFontAttributeName : font };
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    CGRect boundingRect = [attributedText boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    {
        if (boundingRect.size.height > labelHeight)
        {
            NSUInteger index = 0;
            NSUInteger prev;
            NSMutableCharacterSet* testCharSet = [[NSMutableCharacterSet alloc] init];
            [testCharSet formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
            [testCharSet formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
            [testCharSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
            [testCharSet formUnionWithCharacterSet:[NSCharacterSet newlineCharacterSet]];
            
            do
            {
                prev = index;
                if (mode == NSLineBreakByCharWrapping)
                    index++;
                else
                    index = [string rangeOfCharacterFromSet:testCharSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
            }
            
            while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height <= labelHeight);
            
            return prev;
        }
    }
    
    
    return [string length];
}


-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    static NSString *CellIdentifier = @"CustomCell";
    GoalsAndDreamsCustomCell *cell = (GoalsAndDreamsCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    if (indexPath.row < arrDataSource.count){
        
        NSDictionary *goalsDetails = arrDataSource[indexPath.row];
       
        [self setupActionVariables:cell tag:indexPath.row details:goalsDetails];
        [self setupPreviewVariables:cell tag:indexPath.row];
        
        [self getURLGromGemDetails:indexPath cell:cell details:goalsDetails];
        cell.row = indexPath.row;
        cell.switchGoal.tag = indexPath.row;
        [cell.switchGoal setOn:false];
       
        cell.btnShareStatus.hidden = [[goalsDetails objectForKey:@"share_status"] boolValue] ? false : true;
        cell.titleRightContraint.constant = 75;
        if (cell.btnShareStatus.hidden) {
            cell.titleRightContraint.constant = 5;
        }
        cell.imgTransparentVideo.hidden = true;
       // NSString *status = EStatus == eCompleted ? @"Completed" : @"Active";
        if (EStatus == eCompleted) [cell.switchGoal setOn:true];
        if (NULL_TO_NIL([goalsDetails objectForKey:@"action_count"])) {
            NSInteger count = [[goalsDetails objectForKey:@"action_count"] integerValue];
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%lu ACTION(S)",(long)count]];
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor] range:NSMakeRange(0, str.length - 10)];
            cell.lblActionCount.attributedText = str;
        }
        cell.lblProgress.text = @"Completeness : 0%";
        if ([goalsDetails objectForKey:@"goal_completeness"])cell.lblProgress.text = [NSString stringWithFormat:@"Completeness : %ld%%",[[goalsDetails objectForKey:@"goal_completeness"] integerValue]];
        NSInteger width = self.view.frame.size.width - 90;
        NSInteger perecentage = [[goalsDetails objectForKey:@"goal_completeness"] integerValue];
        NSInteger value = (width * perecentage) / 100;
        cell.progressWidth.constant = value;
        
        if ([[goalsDetails objectForKey:@"display_type"] isEqualToString:@"video"])cell.imgTransparentVideo.hidden = false;
        
        if (NULL_TO_NIL([goalsDetails objectForKey:@"goal_datetime"]))
            cell.lblGoalDate.text = [Utility getDaysBetweenTwoDatesWith:[[goalsDetails objectForKey:@"goal_datetime"] doubleValue]];
        if (NULL_TO_NIL([goalsDetails objectForKey:@"goal_title"]))
            cell.lblTitle.text = [goalsDetails objectForKey:@"goal_title"];
        if (NULL_TO_NIL([goalsDetails objectForKey:@"goal_details"])){
            cell.lblDescription.text = [goalsDetails objectForKey:@"goal_details"];
            float lblHeight = [Utility getSizeOfLabelWithText:[goalsDetails objectForKey:@"goal_details"] width:self.view.frame.size.width - 140 font:[UIFont fontWithName:CommonFont size:14]];
            
            int charSize = lroundf([UIFont fontWithName:CommonFont size:14].lineHeight);
            int rHeight = lroundf(lblHeight);
            int lineCount = rHeight/charSize;
            
            if (lineCount > 3) {
                
                UIFont *uiFont = [UIFont fontWithName:CommonFont size:14]; // whichever font you're using
                CTFontRef ctFont = CTFontCreateWithName((CFStringRef)uiFont.fontName, uiFont.pointSize, NULL);
                NSDictionary *attr = [NSDictionary dictionaryWithObject:(__bridge id)ctFont forKey:(id)kCTFontAttributeName];
                NSAttributedString *attrString  = [[NSAttributedString alloc] initWithString:[goalsDetails objectForKey:@"goal_details"] attributes:attr];
                CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attrString);
                CFRange fitRange;
                CTFramesetterSuggestFrameSizeWithConstraints(
                                                             frameSetter,
                                                             CFRangeMake(0, 0),
                                                             NULL,
                                                             CGSizeMake(self.view.frame.size.width - 40, 80),
                                                             &fitRange);
                CFRelease(frameSetter);
                CFIndex numberOfCharactersThatFit = fitRange.length - 5;
                
                NSString *ellipse = @" ...Continue Reading";
                //NSLog(@"chgat ** %ld ******** %d",numberOfCharactersThatFit,string.length);
                NSString *details = [goalsDetails objectForKey:@"goal_details"];
                if ((numberOfCharactersThatFit - ellipse.length) < details.length){
                    NSString *newString = [[goalsDetails objectForKey:@"goal_details"] substringToIndex:numberOfCharactersThatFit - ellipse.length];
                    newString = [NSString stringWithFormat:@"%@%@",newString,ellipse];
                    NSMutableAttributedString *sttr = [[NSMutableAttributedString alloc] initWithString:newString];
                    [sttr addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(newString.length - ellipse.length,ellipse.length)];
                    [sttr addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:CommonFont size:14.0]
                                 range:NSMakeRange(0, newString.length)];
                    cell.lblDescription.attributedText = sttr;
                }else{
                     cell.lblDescription.text = details;
                }
                
            }
         
            cell.lblDescription.hashtagLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
                // Open URLs
                [self hashTagClickedWithTag:string];
                
            };
        }
        
        if (NULL_TO_NIL([goalsDetails objectForKey:@"goal_details"])){
            cell.lblDate.text = [NSString stringWithFormat:@"%@ - %@",[Utility getDateStringFromSecondsWith:[[goalsDetails objectForKey:@"goal_startdate"] doubleValue] withFormat:@"d MMM yyyy"],[Utility getDateStringFromSecondsWith:[[goalsDetails objectForKey:@"goal_enddate"] doubleValue] withFormat:@"d MMM yyyy"]]  ;
        }
        [cell.activityIndicator stopAnimating];
        if (NULL_TO_NIL([goalsDetails objectForKey:@"goal_media"])){
            NSString *url = [goalsDetails objectForKey:@"goal_media"];
            if (url.length) {
                [cell.activityIndicator startAnimating];
                                [cell.imgGemMedia sd_setImageWithURL:[NSURL URLWithString:url]
                                    placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                               [cell.activityIndicator stopAnimating];
                                           }];
            }
        }else{
            [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
        }
        cell.lblLocation.text = @"";
        cell.lblContacts.text = @"";
        cell.topForLocation.constant = 0;
        cell.topForContacts.constant = 0;
        
        if (NULL_TO_NIL([goalsDetails objectForKey:@"location_name"])) {
            cell.topForLocation.constant = 8;
            NSMutableAttributedString *myString = [NSMutableAttributedString new];
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            UIImage *icon = [UIImage imageNamed:@"Loc_Small"];
            attachment.image = icon;
            attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblLocation.font.descender + 2), icon.size.width, icon.size.height);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            [myString appendAttributedString:attachmentString];
            NSAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",[goalsDetails objectForKey:@"location_name"]]];
            [myString appendAttributedString:myText];
            cell.lblLocation.attributedText = myString;
        }
        
        if (NULL_TO_NIL([goalsDetails objectForKey:@"contact_name"])) {
            cell.topForContacts.constant = 8;
            NSMutableAttributedString *myString = [NSMutableAttributedString new];
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            UIImage *icon = [UIImage imageNamed:@"contact_icon"];
            attachment.image = icon;
            attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblLocation.font.descender + 2), icon.size.width, icon.size.height);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            [myString appendAttributedString:attachmentString];
            NSAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",[goalsDetails objectForKey:@"contact_name"]]];
            [myString appendAttributedString:myText];
            cell.lblContacts.attributedText = myString;
        }
        
   }
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeaderHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < arrDataSource.count) {
        [self goalDetailsClickedWith:indexPath.row];
    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    if (isCachedData) return;
    
    NSInteger endScrolling = scrollView.contentOffset.y+ scrollView.frame.size.height;
    NSInteger height = scrollView.contentSize.height;
    if (endScrolling >= height){
        
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            NSInteger nextPage = currentPage_completed;
            NSInteger totalPages = totalPages_completed;
            if (EStatus == ePending) {
                nextPage = currentPage_pending ;
                totalPages = totalPages_pending;
            }
            nextPage += 1;
            if (nextPage  <= totalPages) {
                isPageRefresing = YES;
                [self showPaginationPopUp];
                [self loadAllGoalsAndDreamsByPagination:YES withPageNumber:nextPage shouldScrollUp:NO];
            }
            
        }
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (!isDataAvailable) return;
    if (scrollView.contentOffset.y <= 200) {
        btnScrollTop.hidden = true;
        
        [self.view layoutIfNeeded];
        rightForGoals.constant = 20;
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
        }];
        
    }else if (scrollView.contentOffset.y > 200){
        btnScrollTop.hidden = false;
        
        [self.view layoutIfNeeded];
        rightForGoals.constant = 70;
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
        }];
        
    }
    
}

#pragma mark - URL Preview And Actions SetUp

-(void)setupPreviewVariables:(GoalsAndDreamsCustomCell*)cell tag:(NSInteger)tag{
    
    cell.heightForPreview.constant = 0;
    cell.topForPreiew.constant = 0;
    [cell.previewIndicator stopAnimating];
    cell.lblPreviewDescription.text = @"";
    cell.lblPreviewTitle.text = @"";
    cell.lblPreviewDomain.text = @"";
    cell.vwURLPreview.hidden = true;
    cell.btnShowPreviewURL.tag = tag;
    [cell.btnShowPreviewURL addTarget:self action:@selector(previewClickedWithGesture:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setupActionVariables:(GoalsAndDreamsCustomCell*)cell tag:(NSInteger)tag details:(NSDictionary*)details{
    
    cell.topForActionView.constant = 0;
    cell.heightForActionView.constant = 0;
    if ([details objectForKey:@"action"]) {
        NSArray *actions = [details objectForKey:@"action"];
        [cell setUpActionsWithDataSource:actions];
        float heading = 27;
        float cellHeight = 40;
        cell.heightForActionView.constant = (actions.count * cellHeight) + heading;
        cell.topForActionView.constant = 10;
    }
   
}



-(void)getURLGromGemDetails:(NSIndexPath*)indexPath cell:(GoalsAndDreamsCustomCell*)cell details:(NSDictionary*)details{
    
    
    NSString *string;
    BOOL isPreview = false;
    if (NULL_TO_NIL([details objectForKey:@"preview_url"])){
        string = [details objectForKey:@"preview_url"];
        isPreview = true;
        
    }else if (NULL_TO_NIL([details objectForKey:@"goal_details"])){
        string = [details objectForKey:@"goal_details"];
    }
    
    if (string.length) {
        NSError *error = nil;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                   error:&error];
        NSArray *matches = [detector matchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
        if (matches.count > 0) {
            cell.vwURLPreview.hidden = false;
            cell.topForPreiew.constant = 0;
            cell.topForActionView.constant = 10;
            if (!isPreview) {
                cell.heightForPreview.constant = 105;
                cell.previewImageHeight.constant = 0;
                cell.previewImageTop.constant = 0;
            }else{
                cell.heightForPreview.constant = 305;
                cell.previewImageHeight.constant = 200;
                cell.previewImageTop.constant = 5;
            }
            if ([[CashedURL sharedManager].dictCashedURLS objectForKey:string]) {
                MTDURLPreview *preview = [[CashedURL sharedManager].dictCashedURLS objectForKey:string];
                [cell.previewIndicator stopAnimating];
                cell.lblPreviewTitle.text = preview.title;
                cell.lblPreviewDescription.text = preview.content;
                cell.lblPreviewTitle.text = preview.title;
                cell.lblPreviewDomain.text = preview.domain;
                [cell.imgPreview setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
                if (preview.imageURL) {
                    [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                       placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  
                                                  
                                                  
                                              }];
                }
                return;
            }
            
            NSTextCheckingResult *match = [matches firstObject];
            [cell.previewIndicator startAnimating];
            [cell.imgPreview setImage:[UIImage imageNamed:@""]];
            [MTDURLPreview loadPreviewWithURL:[match URL] completion:^(MTDURLPreview *preview, NSError *error) {
                if (!error) {
                    if (preview) [[CashedURL sharedManager].dictCashedURLS setObject:preview forKey:string];
                }
                [cell.previewIndicator stopAnimating];
                cell.lblPreviewTitle.text = preview.title;
                cell.lblPreviewDescription.text = preview.content;
                cell.lblPreviewTitle.text = preview.title;
                cell.lblPreviewDomain.text = preview.domain;
                [cell.imgPreview setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
                if (preview.imageURL) {
                    [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                       placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  
                                                  
                                                  
                                              }];
                }
                
            }];
        }
    }

}

-(void)previewClickedWithGesture:(UIButton*)btn{
    
    if (btn.tag < arrDataSource.count) {
        
        NSDictionary *details = arrDataSource[btn.tag];
        
        NSString *string;
        if (NULL_TO_NIL([details objectForKey:@"preview_url"])){
            string = [details objectForKey:@"preview_url"];
            
        }else if (NULL_TO_NIL([details objectForKey:@"goal_details"])){
            string = [details objectForKey:@"goal_details"];
        }
        if (string.length){
            NSError *error = nil;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                       error:&error];
            NSArray *matches = [detector matchesInString:string
                                                 options:0
                                                   range:NSMakeRange(0, [string length])];
            if (matches.count > 0) {
                NSTextCheckingResult *match = [matches firstObject];
                if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                    [[UIApplication sharedApplication] openURL:[match URL] options:@{}
                                             completionHandler:^(BOOL success) {
                                             }];
                }
                
            }

        }
        
    }
}



#pragma mark - CustomCell Delegte Methods

-(void)showActionDetailByGoalIndex:(NSInteger)index actionIndex:(NSInteger)actionIndex{

    if (index < arrDataSource.count) {
        NSDictionary *goalsDetails = arrDataSource[index];
        if ([goalsDetails objectForKey:@"action"]) {
            NSArray *actions = [goalsDetails objectForKey:@"action"];
            if (actionIndex < actions.count) {
                NSDictionary *_actionDetails = actions[actionIndex];
                ActionDetailPageViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGoalsDreamsDetailPage];
                detailPage.delegate = self;
                NSString *actionID = [_actionDetails objectForKey:@"action_id"];
                NSString *goalActionID = [_actionDetails objectForKey:@"goalaction_id"];
                AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [app.navGeneral pushViewController:detailPage animated:YES];
                [detailPage getActionDetailsByGoalActionID:goalActionID actionID:actionID goalID:[goalsDetails objectForKey:@"goal_id"]];
            }
        }
    }
    
}


-(void)refershGoalsAndDreamsAfterUpdate{
    
    [self performSelector:@selector(refreshData) withObject:self afterDelay:0.7];
    //[self refreshData];
}

-(void)updatePendingStatusByIndex:(NSInteger)index actionIndex:(NSInteger)actionIndex{
    
    if (index < arrDataSource.count) {
         NSMutableDictionary *goalsDetails = [NSMutableDictionary dictionaryWithDictionary:arrDataSource[index]];
        if ([goalsDetails objectForKey:@"action"]) {
            NSMutableArray *actions = [NSMutableArray arrayWithArray:[goalsDetails objectForKey:@"action"]];
            if (actionIndex < actions.count) {
                NSString *strMessage = @"Do you want to change this action to Complete ?";
                NSMutableDictionary *_actionDetails = [NSMutableDictionary dictionaryWithDictionary:actions[actionIndex]];  ;
                if ([[_actionDetails objectForKey:@"action_status"] integerValue] == 1) strMessage = @"Do you want to change this action to Pending ?";
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Action"
                                                                               message:strMessage
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"YES"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          if (_actionDetails) {
                                                                              if ([[_actionDetails objectForKey:@"action_status"] integerValue] == 0)
                                                                                  [_actionDetails setObject:[NSNumber numberWithInt:1] forKey:@"action_status"];
                                                                              else
                                                                                  [_actionDetails setObject:[NSNumber numberWithInt:0] forKey:@"action_status"];
                                                                              
                                                                              [actions replaceObjectAtIndex:actionIndex withObject:_actionDetails];
                                                                              [goalsDetails setObject:actions forKey:@"action"];
                                                                              [arrDataSource replaceObjectAtIndex:index withObject:goalsDetails];
                                                                              
                                                                              [tableView beginUpdates];
                                                                              [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:index inSection:0], nil] withRowAnimation:UITableViewRowAnimationFade];
                                                                              [tableView endUpdates];
                                                                              
                                                                          }
                                                                          
                                                                          NSString *actionID = [_actionDetails objectForKey:@"action_id"];
                                                                          NSString *goalActionID = [_actionDetails objectForKey:@"goalaction_id"];
                                                                          [APIMapper updateGoalActionStatusWithActionID:actionID goalID:[goalsDetails objectForKey:@"goal_id"] goalActionID:goalActionID andUserID:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                              
                                                                              if ([responseObject objectForKey:@"text"]) [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
                                                                              
                                                                          } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                                                              
                                                                              if (error) [ALToastView toastInView:self.view withText:error.localizedDescription];
                                                                              if (_actionDetails) {
                                                                                  if ([[_actionDetails objectForKey:@"action_status"] integerValue] == 0)
                                                                                      [_actionDetails setObject:[NSNumber numberWithInt:1] forKey:@"action_status"];
                                                                                  else
                                                                                      [_actionDetails setObject:[NSNumber numberWithInt:0] forKey:@"action_status"];
                                                                                  
                                                                                  [actions replaceObjectAtIndex:actionIndex withObject:_actionDetails];
                                                                                  [goalsDetails setObject:actions forKey:@"action"];
                                                                                  [arrDataSource replaceObjectAtIndex:index withObject:goalsDetails];
                                                                                  
                                                                                  [tableView beginUpdates];
                                                                                  [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:index inSection:0], nil] withRowAnimation:UITableViewRowAnimationFade];
                                                                                  [tableView endUpdates];
                                                                                  
                                                                              }
                                                                          }];
                                                                          
                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                      }];
                UIAlertAction *second = [UIAlertAction actionWithTitle:@"NO"
                                                                 style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                     
                                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                                                 }];
                [alert addAction:firstAction];
                [alert addAction:second];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }
    }
    
}

-(void)goalsAndDreamsCreatedWithGoalTitle:(NSString*)goalTitle goalID:(NSInteger)goalID{
    // Delegate call back from Goal created
    //[self refreshData];
    [self performSelector:@selector(refreshData) withObject:self afterDelay:0.7];
}

-(void)refershGoalsAndDreamsListingAfterUpdateShouldScrollToTop:(BOOL)scrollTop{
    
    // Delegate call back from Goal details when something has been updated
    //  [self refreshData];
    [self performSelector:@selector(refreshData) withObject:self afterDelay:0.7];
}

-(void)refershGoalsAndDreamsListingAfterUpdate{
    
     [self performSelector:@selector(refreshData) withObject:self afterDelay:0.7];
}

-(void)goalDetailsClickedWith:(NSInteger)index {
    
    if (index < arrDataSource.count) {
        NSDictionary *gemDetails = arrDataSource[index];
        GoalDetailViewController *goalDetailsVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForGoalDetails];
        goalDetailsVC.delegate = self;
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.navGeneral pushViewController:goalDetailsVC animated:YES];
        [goalDetailsVC getGoaDetailsByGoalID:[gemDetails objectForKey:@"goal_id"]];
        
    }
    
}



#pragma mark - Generic Methods

-(IBAction)changeGoalStatus:(UISwitch*)sender{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Change goal status"
                                          message:@"Do you really want to change the goal status?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"YES"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   if (sender.tag < arrDataSource.count) {
                                       NSDictionary *goalsDetails = arrDataSource[sender.tag];
                                       NSInteger status = 0;
                                       if ([sender isOn]) {
                                           status = 1;
                                       }
                                       [self showLoadingScreen];
                                       [APIMapper updateGoalStatusWithGoalID:[goalsDetails objectForKey:@"goal_id"] status:status onSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           
                                           [self hideLoadingScreen];
                                           [self refreshData];
                                           [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
                                           
                                       } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                           
                                           [self hideLoadingScreen];
                                       }];
                                       
                                       
                                   }
                                  
                               }];
    UIAlertAction *cancel = [UIAlertAction
                               actionWithTitle:@"CANCEL"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction *action)
                               {
                                   if (EStatus == ePending) {
                                       [sender setOn:FALSE];
                                   }else{
                                       [sender setOn:TRUE];
                                   }
                                   
                               }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(IBAction)tableScrollToTop:(id)sender{
    
     [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
   // [tableView setContentOffset:CGPointZero animated:YES];
    [self.view layoutIfNeeded];
    rightForGoals.constant = 20;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    
}


-(IBAction)hashTagClickedWithTag:(NSString*)tag{
    
    HashTagGemListingViewController *hashListing =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForHashTagListings];
    hashListing.strTagName = tag;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:hashListing animated:YES];
    
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)showPaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)hidePaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = -40;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

// "+" Button Action

-(IBAction)composeNewActionAndMedias:(id)sender{
    
    CreateActionInfoViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCreateActionMedias];
    detailPage.strTitle = @"CREATE GOAL";
    detailPage.delegate = self;
    detailPage.actionType = eActionTypeGoalsAndDreams;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:detailPage animated:YES];

}


-(IBAction)goBack:(id)sender{
    
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
