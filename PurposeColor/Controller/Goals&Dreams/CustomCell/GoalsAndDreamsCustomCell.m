//
//  GemDetailsCustomTableViewCell.m
//  PurposeColor
//
//  Created by Purpose Code on 18/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


#define kSectionCount               1
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kHeaderHeight               0
#define kCellHeight                 45
#define kEmptyHeaderAndFooter       0
#define kCellHeightForCompleted     190


#import "GoalsAndDreamsCustomCell.h"
#import "GoalsAndDreamsActionCell.h"

@interface GoalsAndDreamsCustomCell (){
    
    BOOL isDataAvailable;
    NSArray *arrDataSource;
}

@end

@implementation GoalsAndDreamsCustomCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    // Initialization code

    
    [_vwBg.layer setBorderColor:[UIColor getSeperatorColor].CGColor];
    [_vwBg.layer setBorderWidth:1.f];
    // drop shadow
    [_vwBg.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwBg.layer setShadowOpacity:0.3];
    [_vwBg.layer setShadowRadius:2.0];
    [_vwBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    _vwBg.layer.cornerRadius = 5.f;
    _vwBg.clipsToBounds = YES;
    
    _btnShareStatus.layer.borderWidth = 1.f;
    _btnShareStatus.layer.borderColor = [UIColor colorWithRed:0.02 green:0.53 blue:0.87 alpha:0.3].CGColor;
    _btnShareStatus.layer.cornerRadius = 3.f;
    _lblDescription.systemURLStyle = YES;
    
    // Attach block for handling taps on usenames
    _lblDescription.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        // Open URLs
        [self attemptOpenURL:[NSURL URLWithString:string]];
    };
    
    _lblDescription.allOtherClicks = ^(KILabel *label, NSString *string, NSRange range) {
        // Open URLs
        if ([self.delegate respondsToSelector:@selector(goalDetailsClickedWith:)]) {
            [self.delegate goalDetailsClickedWith:self.row];
        }
        
    };
   // _tableView.layer.borderWidth = 1.f;
   // _tableView.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    

    
    
 }

-(IBAction)updateStatus:(UIButton*)btnStatus{
    
    if ([self.delegate respondsToSelector:@selector(updatePendingStatusByIndex:actionIndex:)]) {
        [self.delegate updatePendingStatusByIndex:self.row actionIndex:btnStatus.tag];
    }
    
}

-(IBAction)goalDetailButtonClicked:(UIButton*)btnStatus{
    
    if ([self.delegate respondsToSelector:@selector(goalDetailsClickedWith:)]) {
        [self.delegate goalDetailsClickedWith:self.row];
    }
    
}

- (void)attemptOpenURL:(NSURL *)url
{
    
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (!safariCompatible) {
        
        NSString *urlString = url.absoluteString;
        urlString = [NSString stringWithFormat:@"http://%@",url.absoluteString];
        url = [NSURL URLWithString:urlString];
        
    }
    safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:url options:@{}
                                     completionHandler:^(BOOL success) {
                                     }];
        }
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem"
                                                                       message:@"The selected link cannot be opened."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dismiss"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:cancel];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
    }
}


-(void)setUpActionsWithDataSource:(NSArray*)actions{
    
    arrDataSource = actions;
    [_tableView reloadData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
       return arrDataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40;
}





-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    static NSString *CellIdentifier = @"ActionCell";
    GoalsAndDreamsActionCell *cell = (GoalsAndDreamsActionCell*)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.row < arrDataSource.count) {
        NSDictionary *details = arrDataSource[indexPath.row];
        cell.lblTitle.text = [details objectForKey:@"action_title"];
        cell.btnStatus.tag = indexPath.row;
        [cell.btnStatus setImage:[UIImage imageNamed:@"CheckBox_Goals"] forState:UIControlStateNormal];
        if ([[details objectForKey:@"action_status"] boolValue])
            [cell.btnStatus setImage:[UIImage imageNamed:@"CheckBox_Goals_Active"] forState:UIControlStateNormal];
        
        
    }
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.delegate respondsToSelector:@selector(showActionDetailByGoalIndex:actionIndex:)]) {
        [self.delegate showActionDetailByGoalIndex:self.row actionIndex:indexPath.row];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


