//
//  DummyLaunchPageViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 29/05/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "MaintainanceModeDisplay.h"

@interface MaintainanceModeDisplay (){
    
    IBOutlet UIScrollView *vwBg;
}

@end

@implementation MaintainanceModeDisplay

- (void)viewDidLoad {
    [super viewDidLoad];
    
    vwBg.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    vwBg.layer.borderWidth = 1.f;
    [vwBg.layer setShadowColor:[UIColor blackColor].CGColor];
    [vwBg.layer setShadowOpacity:0.3];
    [vwBg.layer setShadowRadius:2.0];
    [vwBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goBack:(id)sender{
    
    exit(0);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
