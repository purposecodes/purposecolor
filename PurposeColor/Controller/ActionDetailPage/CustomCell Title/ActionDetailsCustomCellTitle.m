//
//  GemDetailsCustomTableViewCell.m
//  PurposeColor
//
//  Created by Purpose Code on 18/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "ActionDetailsCustomCellTitle.h"


@interface ActionDetailsCustomCellTitle (){
    
}

@end

@implementation ActionDetailsCustomCellTitle

- (void)awakeFromNib {
    // Initialization code
    
   
}

-(IBAction)moreButtonClicked:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(moreButtonClicked)]) {
        [self.delegate moreButtonClicked];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


