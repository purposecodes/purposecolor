//
//  GoalsAndDreamsDetailViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 21/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eSectionInfo = 0,
    eSectionMedia = 1,
    
} eSectionDetails;


#define kSectionCount               2
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kHeaderHeight               0
#define kDefaultCellHeight          300
#define kEmptyHeaderAndFooter       0
#define kPadding                    130;
#define kHeightPercentage           80;

#import "ActionDetailPageViewController.h"
#import "Constants.h"
#import "GemDetailsCustomTableViewCell.h"
#import "ActionDetailsCustomCell.h"
#import <AVKit/AVKit.h>
#import "CustomAudioPlayerView.h"
#import "CreateActionInfoViewController.h"
#import "CommentComposeViewController.h"
#import "PhotoBrowser.h"
#import "MTDURLPreview.h"
#import "ActionDetailsCustomCellTitle.h"
#import "GemDetailsCustomCellLocation.h"
#import "GemDetailsCustomCellContact.h"
#import "GemDetailsCustomCellDescription.h"
#import "GemDetailsCustomCellPreview.h"
#import "HashTagGemListingViewController.h"
#import "SharePost.h"

@interface ActionDetailPageViewController ()<GemDetailPageCellDelegate,ActionDetailsCustomCellDelegate,CustomAudioPlayerDelegate,CommentActionDelegate,PhotoBrowserDelegate,CreateMediaInfoDelegate,ActionDetailsDelegateMoreOption,SharePopUpTextDelegate>{
    
    IBOutlet  UITableView *tableView;
    IBOutlet  UIImageView *imgFavourite;
    IBOutlet  UIImageView *imgShared;
    IBOutlet  UILabel *lblShare;
    IBOutlet UIView *vwFooter;
    IBOutlet UIButton *btnShare;
    
    
    BOOL isDataAvailable;
    NSMutableArray *arrDataSource;
    
    NSInteger playingIndex;
    BOOL isScrolling;
    BOOL isPlaying;
    
    NSString *strTitle;
    NSString *strDescription;
    
    NSString *goalID;
    NSString *goalActionID;
    NSString *strErrorMsg;
    
    
    NSMutableDictionary *actionDetails;
    CustomAudioPlayerView *vwAudioPlayer;
    PhotoBrowser *photoBrowser;
    
}


@end

@implementation ActionDetailPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];

    
    // Do any additional setup after loading the view.
}

-(void)setUp{
    
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 50;
    
    arrDataSource = [NSMutableArray new];
    tableView.hidden = true;
    
    isDataAvailable = false;
    playingIndex = -1;
    
    strTitle = [NSString new];
    strDescription = [NSString new];
    
    vwFooter.hidden = true;
    [vwFooter.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwFooter.layer setBorderWidth:1.f];
    // drop shadow
    [vwFooter.layer setShadowColor:[UIColor blackColor].CGColor];
    [vwFooter.layer setShadowOpacity:0.3];
    [vwFooter.layer setShadowRadius:2.0];
    [vwFooter.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)getActionDetailsByGoalActionID:(NSString*)_goalActionID actionID:(NSString*)_actionID goalID:(NSString*)_goalID{
    
    goalID = _goalID;
    goalActionID = _goalActionID;
    
    if (_goalActionID &&_goalID) {
        [self showLoadingScreen];
        [APIMapper getGoalActionDetailsByGoalActionID:_goalActionID actionID:nil goalID:_goalID andUserID:[User sharedManager].userId  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([[responseObject objectForKey:@"code"] integerValue] == kSuccessCode)[self showActionDetailsWith:responseObject];
            else{
                vwFooter.hidden = true;
                isDataAvailable = false;
                tableView.hidden = false;
                if ([responseObject objectForKey:@"text"]) {
                    strErrorMsg = [responseObject objectForKey:@"text"];
                    [tableView reloadData];
                }
            }
            
            
            [self hideLoadingScreen];
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            strErrorMsg = error.localizedDescription;
            vwFooter.hidden = true;
            tableView.hidden = false;
            isDataAvailable = false;
            [tableView reloadData];
            [self hideLoadingScreen];
            
        }];
        
    }
}

-(void)showActionDetailsWith:(NSDictionary*)details{
    
    vwFooter.hidden = false;
    isDataAvailable = true;
    [arrDataSource removeAllObjects];
    if (NULL_TO_NIL([details objectForKey:@"gem_media"]))
        arrDataSource = [NSMutableArray arrayWithArray:[details objectForKey:@"gem_media"]];
    actionDetails = [NSMutableDictionary dictionaryWithDictionary:details];
    [tableView reloadData];
    tableView.hidden = false;
    
    [btnShare setImage:[UIImage imageNamed:@"Share.png"] forState:UIControlStateNormal];
    [btnShare setTitle:@"Share" forState:UIControlStateNormal];
    [btnShare setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if ([[actionDetails objectForKey:@"share_status"] boolValue]) {
        [btnShare setImage:[UIImage imageNamed:@"Shared.png"] forState:UIControlStateNormal];
        [btnShare setTitle:@"Shared" forState:UIControlStateNormal];
        [btnShare setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
    }

}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (!isDataAvailable) return kMinimumCellCount;
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    if (section == eSectionInfo){
        NSInteger rows = 5;
        
        NSString *string;
        if (NULL_TO_NIL([actionDetails objectForKey:@"preview_url"])){
            string = [actionDetails objectForKey:@"preview_url"];
            
        }else if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"])){
            string = [actionDetails objectForKey:@"gem_details"];
        }
        if (string.length) {
            NSError *error = nil;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                       error:&error];
            NSArray *matches = [detector matchesInString:string
                                                 options:0
                                                   range:NSMakeRange(0, [string length])];
            if (matches.count > 0) {
                rows = 6;
            }

        }
        return rows;
    }
    else
        return arrDataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        UITableViewCell *cell = [Utility getNoDataCustomCellWith:aTableView withTitle:strErrorMsg];
        return cell;
    }
    
    if (indexPath.section == eSectionInfo){
        
        if (indexPath.row == 0) {
            
            ActionDetailsCustomCellTitle *cell = (ActionDetailsCustomCellTitle *)[tableView dequeueReusableCellWithIdentifier:@"ActionDetailsCustomCellTitle"];
            cell.delegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.lbltTitle.text = [actionDetails objectForKey:@"gem_title"];
            cell.lblDate.text = [Utility getDaysBetweenTwoDatesWith:[[actionDetails objectForKey:@"gem_datetime"] doubleValue]];
            if (_isFromJournal)cell.btnMore.hidden = true;
            
            return cell;
            
        }
        
        if (indexPath.row == 1) {
            GemDetailsCustomCellLocation *cell = (GemDetailsCustomCellLocation *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellLocation"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (NULL_TO_NIL([actionDetails objectForKey:@"location_name"])){
                cell.lblLocation.text = [actionDetails objectForKey:@"location_name"];
            }
            return cell;
        }
        
        if (indexPath.row == 2) {
            GemDetailsCustomCellContact *cell = (GemDetailsCustomCellContact *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellContact"];
            if (NULL_TO_NIL([actionDetails objectForKey:@"contact_name"])){
                cell.lblContact.text = [actionDetails objectForKey:@"contact_name"];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
        if (indexPath.row == 3) {
            GemDetailsCustomCellDescription *cell = (GemDetailsCustomCellDescription *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellDescription"];
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:CommonFont_New size:14],
                                         };
            NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[actionDetails objectForKey:@"gem_details"] attributes:attributes];
            cell.lbltDescription.attributedText = attributedText;
            cell.lbltDescription.systemURLStyle = YES;
            cell.lbltDescription.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
                // Open URLs
                [self attemptOpenURL:[NSURL URLWithString:string]];
            };
            cell.lbltDescription.hashtagLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
                // Open URLs
                [self hashTagClickedWithTag:string];
            };
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row == 4) {
            cell = [self configureActionDetailsInfoCell:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row == 5) {
            GemDetailsCustomCellPreview *cell = (GemDetailsCustomCellPreview *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellPreview"];
             BOOL isPreview = false;
            NSString *string;
            if (NULL_TO_NIL([actionDetails objectForKey:@"preview_url"])){
                string = [actionDetails objectForKey:@"preview_url"];
                isPreview = true;
                
            }else if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"])){
                string = [actionDetails objectForKey:@"gem_details"];
            }
            if (string.length) {
                NSError *error = nil;
                
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                           error:&error];
                NSArray *matches = [detector matchesInString:string
                                                     options:0
                                                       range:NSMakeRange(0, [string length])];
                if (matches.count > 0) {
                    
                    if (!isPreview) {
                        cell.heightForPreview.constant = 105;
                        cell.previewImageHeight.constant = 0;
                        cell.previewImageTop.constant = 0;
                    }else{
                        cell.heightForPreview.constant = 305;
                        cell.previewImageHeight.constant = 200;
                        cell.previewImageTop.constant = 5;
                    }
                    
                    if ([[CashedURL sharedManager].dictCashedURLS objectForKey:string]) {
                        MTDURLPreview *preview = [[CashedURL sharedManager].dictCashedURLS objectForKey:string];
                        [cell.indicator stopAnimating];
                        cell.lblTitle.text = preview.title;
                        cell.lblDescription.text = preview.content;
                        cell.lblDomain.text = preview.domain;
                        [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                           placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  }];
                    }else{
                        
                        NSTextCheckingResult *match = [matches firstObject];
                        [MTDURLPreview loadPreviewWithURL:[match URL] completion:^(MTDURLPreview *preview, NSError *error) {
                            if (!error) {
                                if (preview) [[CashedURL sharedManager].dictCashedURLS setObject:preview forKey:string];
                            }
                            [cell.indicator stopAnimating];
                            cell.lblTitle.text = preview.title;
                            cell.lblDescription.text = preview.content;
                            cell.lblDomain.text = preview.domain;
                            [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                               placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                      }];
                            
                            
                        }];
                    }
                   
                }
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
    }
    
    else{
        static NSString *CellIdentifier = @"mediaListingCell";
        GemDetailsCustomTableViewCell *cell = (GemDetailsCustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [self resetCellVariables:cell];
        cell.delegate = self;
        [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];
        if (indexPath.row < arrDataSource.count)
            [self showMediaDetailsWithCell:cell andDetails:arrDataSource[indexPath.row] index:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
      
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) {
         return  UITableViewAutomaticDimension;
    }
    if (indexPath.section == eSectionInfo) {
        
        if (indexPath.row == 1) {
            if (NULL_TO_NIL([actionDetails objectForKey:@"location_name"]))
                return UITableViewAutomaticDimension;
            else
                return 0;
        }
        else if (indexPath.row == 2) {
            if (NULL_TO_NIL([actionDetails objectForKey:@"contact_name"]))
                return UITableViewAutomaticDimension;
            else
                return 0;
        }
        else if (indexPath.row == 3) {
            if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"]))
                return UITableViewAutomaticDimension;
            else
                return 0;
        }
        
        return UITableViewAutomaticDimension;
    }
    else{
         return UITableViewAutomaticDimension;
    }
    return UITableViewAutomaticDimension;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeaderHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 && indexPath.row == 5) {
        [self openPreviewURL];
        
    }else{
        NSString *mediaType ;
        NSMutableArray *images = [NSMutableArray new];
        if (indexPath.section == eSectionMedia){
            if (indexPath.row < arrDataSource.count) {
                NSDictionary *mediaInfo = arrDataSource[indexPath.row];
                
                if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
                    mediaType = [mediaInfo objectForKey:@"media_type"];
                }
                if (mediaType) {
                    if ([mediaType isEqualToString:@"image"]) {
                        NSURL *url =  [NSURL URLWithString:[mediaInfo objectForKey:@"gem_media"]];
                        [images addObject:url];
                        
                        for (NSDictionary *details in arrDataSource) {
                            if (NULL_TO_NIL([details objectForKey:@"media_type"])) {
                                mediaType = [details objectForKey:@"media_type"];
                            }
                            if (mediaType) {
                                if ([mediaType isEqualToString:@"image"]) {
                                    NSURL *url =  [NSURL URLWithString:[details objectForKey:@"gem_media"]];
                                    if (![images containsObject:url]) {
                                        [images addObject:url];
                                    }
                                    
                                }
                                
                            }
                        }
                        if (images.count) {
                            [self presentGalleryWithImages:images];
                        }
                    }
                    
                }
            }
        }
    }
    
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    playingIndex = -1;
    isScrolling = NO;
    isPlaying = FALSE;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)aScrollView
{
    isScrolling = YES;
    isPlaying = FALSE;
}

#pragma mark - Custom Cell Customization Methods

-(ActionDetailsCustomCell*)configureActionDetailsInfoCell:(NSIndexPath*)indexPath{
    
    static NSString *CellIdentifier = @"infoCustomCell";
    ActionDetailsCustomCell *cell = (ActionDetailsCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.bottomForStatus.constant = 20;
    cell.vwURLPreview.hidden = true;
    if (NULL_TO_NIL([actionDetails objectForKey:@"gem_title"])){
        cell.lblTitle.text = [actionDetails objectForKey:@"gem_title"];
        strTitle = [actionDetails objectForKey:@"gem_title"];
   }
   if (NULL_TO_NIL([actionDetails objectForKey:@"gem_datetime"]))
        cell.lblDate.text = [Utility getDaysBetweenTwoDatesWith:[[actionDetails objectForKey:@"gem_datetime"] doubleValue]];
    
    cell.vwLocInfo.hidden = true;
    if (NULL_TO_NIL([actionDetails objectForKey:@"location_name"])){
        cell.vwLocInfo.hidden = false;
        cell.lblLocDetails.text = [actionDetails objectForKey:@"location_name"];
    }
    cell.vwContactInfo.hidden = true;
    if (NULL_TO_NIL([actionDetails objectForKey:@"contact_name"])){
        cell.lblContactDetails.text =[actionDetails objectForKey:@"contact_name"];
        cell.vwContactInfo.hidden = false;
    }
   
    if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"])){
        
        UIFont *font = [UIFont fontWithName:CommonFont_New size:14];
        NSDictionary *attributes = @{NSFontAttributeName:font
                                     
                                     };
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[actionDetails objectForKey:@"gem_details"] attributes:attributes];
        cell.lblDescription.attributedText = attributedText;
        strDescription = [actionDetails objectForKey:@"gem_details"];
    }
    
    if (NULL_TO_NIL([actionDetails objectForKey:@"action_status"])){
        cell.isStatusPending = ![[actionDetails objectForKey:@"action_status"] boolValue];
    }
    if (strDescription) {
        NSString *string = strDescription;
        NSError *error = nil;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                   error:&error];
        NSArray *matches = [detector matchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
        if (matches.count > 0){
            cell.vwURLPreview.hidden = false;
            cell.bottomForStatus.constant = 110;
            NSTextCheckingResult *match = [matches firstObject];
            [MTDURLPreview loadPreviewWithURL:[match URL] completion:^(MTDURLPreview *preview, NSError *error) {
                [cell.previewIndicator stopAnimating];
                cell.lblPreviewTitle.text = preview.title;
                cell.lblPreviewDescription.text = preview.content;
                cell.lblPreviewTitle.text = preview.title;
                cell.lblPreviewDomain.text = preview.domain;
                [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                   placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                          }];
            }];
            
        }
    }
    
    cell.delegate = self;
    [cell setUp];
    return cell;
    
}

-(void)showMediaDetailsWithCell:(GemDetailsCustomTableViewCell*)cell andDetails:(NSDictionary*)mediaInfo index:(NSInteger)index{
    
    NSString *mediaType ;
    if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
        mediaType = [mediaInfo objectForKey:@"media_type"];
    }
    
    if (mediaType) {
        
        if ([mediaType isEqualToString:@"image"]) {
            
            // Type Image
           // [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage.png"]];
            [cell.activityIndicator startAnimating];
            [cell.imgGemMedia sd_setImageWithURL:[NSURL URLWithString:[mediaInfo objectForKey:@"gem_media"]]
                                placeholderImage:[UIImage imageNamed:@""]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           [cell.activityIndicator stopAnimating];
                                       }];
        }
        
        else if ([mediaType isEqualToString:@"audio"]) {
            
            // Type Audio
            [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
            if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                [[cell btnAudioPlay]setHidden:false];
                
            }
            
        }
        
        else if ([mediaType isEqualToString:@"video"]) {
            
            // Type Video
            if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                NSString *videoURL = [mediaInfo objectForKey:@"gem_media"];
                if (videoURL.length){
                    [[cell btnVideoPlay]setHidden:false];
                }
            }
            
            if (NULL_TO_NIL([mediaInfo objectForKey:@"video_thumb"])) {
                NSString *videoThumb = [mediaInfo objectForKey:@"video_thumb"];
                if (videoThumb.length) {
                    [cell.activityIndicator startAnimating];
                    [cell.imgGemMedia sd_setImageWithURL:[NSURL URLWithString:videoThumb]
                                        placeholderImage:[UIImage imageNamed:@""]
                                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                   [cell.activityIndicator stopAnimating];
                                                }];
                }
                
            }
            
        }
        
        float imageHeight = 0;
        float width = [[mediaInfo objectForKey:@"image_width"] floatValue];
        float height = [[mediaInfo objectForKey:@"image_height"] floatValue];
        float ratio = width / height;
        imageHeight = (self.view.frame.size.width - 10) / ratio;
        cell.constraintForHeight.constant = imageHeight;
        
    }
    
}


-(void)resetCellVariables:(GemDetailsCustomTableViewCell*)cell{
    
    [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
    [cell.activityIndicator stopAnimating];
    [[cell btnVideoPlay]setHidden:YES];
    [[cell btnAudioPlay]setHidden:YES];
    
}
#pragma mark - Custom Cell Delegates

-(void)resetPlayerVaiablesForIndex:(NSInteger)tag{
    
    if (tag < arrDataSource.count){
        
        NSDictionary * mediaInfo = arrDataSource[tag];
        NSString *mediaType ;
        if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
            mediaType = [mediaInfo objectForKey:@"media_type"];
        }
        
        if (mediaType) {
            
            if ([mediaType isEqualToString:@"image"]) {
                
            }
            else if ([mediaType isEqualToString:@"audio"]) {
                
                NSString *audioURL = [mediaInfo objectForKey:@"gem_media"];
                if (audioURL.length){
                    
                    NSURL *movieURL = [NSURL URLWithString:audioURL];
                    [self showAudioPlayerWithURL:movieURL];

                }
            }
            
            else if ([mediaType isEqualToString:@"video"]) {
                
                // Type Video
                if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                    NSString *videoURL = [mediaInfo objectForKey:@"gem_media"];
                    if (videoURL.length){
                         NSError* error;
                        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
                        [[AVAudioSession sharedInstance] setActive:NO error:&error];
                        NSURL *movieURL = [NSURL URLWithString:videoURL];
                        AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
                        playerViewController.player = [AVPlayer playerWithURL:movieURL];
                         [playerViewController.player play];
                        [self presentViewController:playerViewController animated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] addObserver:self
                                                                 selector:@selector(videoDidFinish:)
                                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                                   object:[playerViewController.player currentItem]];
                        
                    }
                }
                
            }
        }
    }
    
}

- (void)videoDidFinish:(id)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //fade out / remove subview
}

-(void)updateGoalActionStatus{
    
    if (goalID) {
        
        [APIMapper updateGoalActionStatusWithActionID:nil goalID:goalID goalActionID:goalActionID andUserID:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (responseObject && [responseObject objectForKey:@"text"]) {
                AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [ALToastView toastInView:delegate.window.rootViewController.view withText:[responseObject objectForKey:@"text"]];
            }
            if ([self.delegate respondsToSelector:@selector(refershGoalsAndDreamsAfterUpdate)]) {
                [self.delegate refershGoalsAndDreamsAfterUpdate];
            }
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
        }];
    }
    
}

-(void)moreButtonClicked{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"ACTION"
                                                                   message:@"Choose an action"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Edit"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                              [self editButtonClicked];
                                                              [alert dismissViewControllerAnimated:YES completion:^{
                                                                  
                                                              }];
                                                              
                                                              
                                                          }];
    UIAlertAction *second = [UIAlertAction actionWithTitle:@"Delete"
                                                     style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                         
                                                         [self deleteGoalClicked];
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:^{
                                                         }];
                                                     }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    [alert addAction:firstAction];
    [alert addAction:second];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}

-(void)deleteGoalClicked{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Action"
                                                                   message:@"Do you really want to delete the Action?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"CANCEL"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"DELETE"
                                                     style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                         
                                                         [self showLoadingScreen];
                                                         [APIMapper hideAGoalWithGoalID:goalID goalActionID:goalActionID success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                             
                                                             [self hideLoadingScreen];
                                                             if ([self.delegate respondsToSelector:@selector(refershGoalsAndDreamsAfterUpdate)]) {
                                                                 [self.delegate refershGoalsAndDreamsAfterUpdate];
                                                             }
                                                             [self goBack:nil];
                                                             
                                                         } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                                             
                                                             [self hideLoadingScreen];
                                                             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Action"
                                                                                                                            message:[error localizedDescription]
                                                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                                             
                                                             UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK"
                                                                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                                  
                                                                                                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                                              }];
                                                             
                                                             [alert addAction:cancel];
                                                             [self presentViewController:alert animated:YES completion:nil];
                                                         }];
                                                     }];
    
    [alert addAction:cancel];
    [alert addAction:delete];
    [self presentViewController:alert animated:YES completion:nil];

}
-(IBAction)editButtonClicked{
    
      if ([lblShare.text isEqualToString:@"Shared"]) {
          
          AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
          UINavigationController *nav = delegate.navGeneral;
          
          UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"EDIT ACTION"
                                                                         message:@"This is a shared Action you still want to change it?"
                                                                  preferredStyle:UIAlertControllerStyleAlert];
          UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"YES"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                    
                                                                    CreateActionInfoViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCreateActionMedias];
                                                                    detailPage.actionType = eActionTypeActions;
                                                                    detailPage.strTitle = @"EDIT ACTION";
                                                                    detailPage.delegate = self;
                                                                    [[self navigationController]pushViewController:detailPage animated:YES];
                        [detailPage getMediaDetailsForGemsToBeEditedWithGEMID:goalActionID GEMType:@"action"];
                                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                                                    
                                                                }];
          UIAlertAction *second = [UIAlertAction actionWithTitle:@"NO"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               
                                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
          
          [alert addAction:firstAction];
          [alert addAction:second];
          [nav presentViewController:alert animated:YES completion:nil];
          
      }else{
          
          CreateActionInfoViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCreateActionMedias];
          detailPage.actionType = eActionTypeActions;
          detailPage.strTitle = @"EDIT ACTION";
          detailPage.delegate = self;
          [[self navigationController]pushViewController:detailPage animated:YES];
          [detailPage getMediaDetailsForGemsToBeEditedWithGEMID:goalActionID GEMType:@"action"];
      }

    
}
-(void)actionStatusChanged{
    
    if ([[actionDetails objectForKey:@"action_status"] integerValue] == 1) {
        [actionDetails setObject:[NSNumber numberWithInteger:0] forKey:@"action_status"];
    }else{
         [actionDetails setObject:[NSNumber numberWithInteger:1] forKey:@"action_status"];
    }
}

-(void)newActionCreatedWithActionTitle:(NSString*)actionTitle actionID:(NSInteger)_actionID{
    
    [self getActionDetailsByGoalActionID:goalActionID actionID:nil goalID:goalID];
    if ([self.delegate respondsToSelector:@selector(refershGoalsAndDreamsAfterUpdate)]) {
        [self.delegate refershGoalsAndDreamsAfterUpdate];
    }
    // Refresh the Goal deatil page to reflect the changes
}

#pragma mark - Generic Methods

- (void)openPreviewURL
{
    NSString *string;
    if (NULL_TO_NIL([actionDetails objectForKey:@"preview_url"])){
        string = [actionDetails objectForKey:@"preview_url"];
        
    }else if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"])){
        string = [actionDetails objectForKey:@"gem_details"];
    }
    if (string.length) {
        NSError *error = nil;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                   error:&error];
        NSArray *matches = [detector matchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
        if (matches.count > 0) {
            NSTextCheckingResult *match = [matches firstObject];
            if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                [[UIApplication sharedApplication] openURL:[match URL] options:@{}
                                         completionHandler:^(BOOL success) {
                                         }];
            }
        }

    }
    
}


- (void)attemptOpenURL:(NSURL *)url
{
    
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (!safariCompatible) {
        
        NSString *urlString = url.absoluteString;
        urlString = [NSString stringWithFormat:@"http://%@",url.absoluteString];
        url = [NSURL URLWithString:urlString];
        
    }
    safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:url options:@{}
                                     completionHandler:^(BOOL success) {
                                     }];
        }
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem"
                                                                       message:@"The selected link cannot be opened."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dismiss"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:cancel];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
    }
}


-(IBAction)previewClickedWithGesture:(UIButton*)btn{
    
    if (strDescription){
        
        NSString *string = strDescription;
        NSError *error = nil;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                   error:&error];
        NSArray *matches = [detector matchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
        if (matches.count > 0) {
            NSTextCheckingResult *match = [matches firstObject];
            if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                [[UIApplication sharedApplication] openURL:[match URL] options:@{}
                                         completionHandler:^(BOOL success) {
                                         }];
            }
            
        }
    }
}


-(void)showAudioPlayerWithURL:(NSURL*)url{
    
    if (!vwAudioPlayer) {
        vwAudioPlayer = [[[NSBundle mainBundle] loadNibNamed:@"CustomAudioPlayerView" owner:self options:nil] objectAtIndex:0];
        vwAudioPlayer.delegate = self;
    }
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = vwAudioPlayer;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer setupAVPlayerForURL:url];
    }];
    
}

-(void)closeAudioPlayerView{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwAudioPlayer.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer removeFromSuperview];
        vwAudioPlayer = nil;
        NSError* error;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        [[AVAudioSession sharedInstance] setActive:NO error:&error];
    }];
    
    
    
}



#pragma mark - Comment Showing and its Delegate

-(IBAction)commentComposeView{
    
    CommentComposeViewController *composeComment =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCommentCompose];
    composeComment.delegate = self;
    NSDictionary *gemDetails = actionDetails;
    composeComment.dictGemDetails = gemDetails;
    composeComment.isFromCommunityGem = false;
    [self.navigationController pushViewController:composeComment animated:YES];
    
}

-(void)commentPopUpCloseAppplied{
    

    
}

#pragma mark - Photo Browser & Deleagtes

- (void)presentGalleryWithImages:(NSArray*)images
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}

#pragma mark - Add to Favourite Showing

-(void)shareButtonClicked{
    
     [btnShare setEnabled:false];
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SharePost"
                                                          owner:nil
                                                        options:nil];
    
    SharePost *vwPopUP = [arrayOfViews objectAtIndex:0];
    vwPopUP.delegate = self;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.4 delay:.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
}


-(void)sharePostTextPopUpCloseApppliedAtIndex:(NSInteger)index{
   // [self sharePostToCommunityWithMessage:nil atIndex:index];
     [btnShare setEnabled:true];
}

-(void)sharePostSubmitAppliedWithText:(NSString*)shareTxt AtIndex:(NSInteger)index{
    [self sharePostToCommunityWithMessage:shareTxt atIndex:index];
}


-(void)sharePostToCommunityWithMessage:(NSString*)message atIndex:(NSInteger)index{
    
    [btnShare setEnabled:true];
    [self showLoadingScreen];
    NSDictionary *gemDetails =  actionDetails;
    NSString *gemID;
    NSString *gemType;
    
    if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
        gemID = [gemDetails objectForKey:@"gem_id"];
    
    if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
        gemType = [gemDetails objectForKey:@"gem_type"];
    
    if (gemID && gemType) {
        
        [APIMapper shareAGEMToCommunityWith:[User sharedManager].userId gemID:gemID gemType:gemType shareMsg:message success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self hideLoadingScreen];
            if ([[responseObject objectForKey:@"code"]integerValue] == kSuccessCode){
                if ( NULL_TO_NIL( [responseObject objectForKey:@"text"])){
                   // [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
                }
                [btnShare setImage:[UIImage imageNamed:@"Shared.png"] forState:UIControlStateNormal];
                [btnShare setTitle:@"Shared" forState:UIControlStateNormal];
                [btnShare setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
            }
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            if (error && error.localizedDescription){
                [ALToastView toastInView:self.view withText:error.localizedDescription];
            }
            [self hideLoadingScreen];
        }];
    }
    
    
}

#pragma mark - Add to Favourite Showing

-(IBAction)addToFavouriteClicked{
    
    NSMutableDictionary *gemDetails = [NSMutableDictionary dictionaryWithDictionary:actionDetails];
    NSString *gemID;
    NSString *gemType;
    
    if (![[actionDetails objectForKey:@"favourite_status"] boolValue]){
        imgFavourite.image = [UIImage imageNamed:@"Favourite_Active.png"];
        [gemDetails setValue:[NSNumber numberWithBool:1] forKey:@"favourite_status"];
    }
    
    if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
        gemID = [gemDetails objectForKey:@"gem_id"];
    
    if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
        gemType = [gemDetails objectForKey:@"gem_type"];
    
    
    if (gemID && gemType) {
        [APIMapper addGemToFavouritesWith:[User sharedManager].userId gemID:gemID gemType:gemType success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
        }];
    }
}



-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(IBAction)hashTagClickedWithTag:(NSString*)tag{
    
    HashTagGemListingViewController *hashListing =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForHashTagListings];
    hashListing.strTagName = tag;
    [[self navigationController]pushViewController:hashListing animated:YES];
}



-(IBAction)goBack:(id)sender{
    
    [[self navigationController] popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
