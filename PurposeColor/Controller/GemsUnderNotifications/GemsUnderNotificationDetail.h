//
//  GEMSListingsViewController.h
//  PurposeColor
//
//  Created by Purpose Code on 28/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GemsUnderNotificationDetail : UIViewController
-(void)getGemDetailsWithGemID:(NSString*)gemID gemType:(NSString*)gemType;

@end
