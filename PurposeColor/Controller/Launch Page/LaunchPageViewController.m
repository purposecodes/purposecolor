//
//  LaunchPageViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 15/11/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "LaunchPageViewController.h"
#import "AppDelegate.h"
#import "MenuViewController.h"
#import "Constants.h"
#import "CommunityGEMListingViewController.h"
#import "EmotionalAwarenessViewController.h"
#import "GEMSListingsViewController.h"
#import "GoalsAndDreamsListingViewController.h"
#import "EmotionalIntelligenceViewController.h"
#import "HomePageViewController.h"
#import "GEMSListingsViewController.h"
#import "AMPopTip.h"
#import "MyMemmoriesViewController.h"
#import "HideAccntPopUp.h"
#import "NotificationsListingViewController.h"
#import "ReviewPopUp.h"
#import "IntelligenceJournalViewController.h"
#import "ChatUserListingViewController.h"
#import "SearchUserListingViewController.h"

@import Firebase;

@interface LaunchPageViewController () <SWRevealViewControllerDelegate>{
    
    IBOutlet UIView *vwContainer;
    UINavigationController *navController;
    
    IBOutlet UIView *vwOverLay;
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblNotfnCount;
    IBOutlet UILabel *lblChatCount;
    
    IBOutlet UIButton *btnHome;
    IBOutlet UIButton *btnAwareness;
    IBOutlet UIButton *btnIntelligence;
    IBOutlet UIButton *btnGoalsAndDreams;
    IBOutlet UIButton *btnGEMs;
    IBOutlet UIButton *btnCommunityGems;
    IBOutlet UIButton *btnSlideMenu;
    IBOutlet UIButton *btnSearch;
    IBOutlet NSLayoutConstraint *widthForSearch;
    
    AMPopTip *popTip;
    HideAccntPopUp *hideAccntPopUp;
}

@end

@implementation LaunchPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
    [self launchHomePage:nil];
    [self checkIsAccountHidden];
    [self getNotificationCount];
    
    // Do any additional setup after loading the view.
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if (app.navGeneral && [[[app.navGeneral viewControllers] firstObject] isKindOfClass:[LaunchPageViewController class]]) {
        LaunchPageViewController *launchPage = [[app.navGeneral viewControllers] firstObject];
        [launchPage getNotificationCount];
    }
    
}

-(void)refreshCommunityPage{
    
    for (UIViewController *_vc in navController.viewControllers) {
        
        if ([_vc isKindOfClass:[CommunityGEMListingViewController class]]) {
            CommunityGEMListingViewController *vc =  (CommunityGEMListingViewController*)_vc;
            [vc refreshData:YES];
        }
        if ([_vc isKindOfClass:[HomePageViewController class]]) {
            HomePageViewController *vc =  (HomePageViewController*)_vc;
            [vc refreshData:YES];
        }
    }
   
}


-(void)showSurveyPopUpWithInfo:(NSDictionary*)details{
    
    for (UIViewController *_vc in navController.viewControllers) {
        
        if ([_vc isKindOfClass:[HomePageViewController class]]) {
            HomePageViewController *vc =  (HomePageViewController*)_vc;
            [vc showSurveyPopUpWithInfo:details];
        }
    }
}

-(void)refreshBdayBadge{
    
    for (UIViewController *_vc in navController.viewControllers) {
        
        if ([_vc isKindOfClass:[HomePageViewController class]]) {
            HomePageViewController *vc =  (HomePageViewController*)_vc;
            [vc getBdayCount];
            break;
        }
    }
    
}


-(void)checkIsAccountHidden{
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"ACCOUNT_ENABLED"]) {
        BOOL isEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"ACCOUNT_ENABLED"];
        if (!isEnabled) {
             [self showHideAccountPopUp];
        }
    }
}

- (void)customSetup
{
    lblNotfnCount.hidden = true;
    lblChatCount.hidden = true;
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.purposecodes.purposecolor"];
    [sharedDefaults setObject: [User sharedManager].token forKey:@"TOKEN"];
    [sharedDefaults synchronize];
    vwOverLay.hidden = true;
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    if ( revealViewController )
    {
        [btnSlideMenu addTarget:self.revealViewController action:@selector(rightRevealToggle:)forControlEvents:UIControlEventTouchUpInside];
        [vwOverLay addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
}

-(void)resetChatNotificationCount{
    
    lblChatCount.text = @"";
    lblChatCount.hidden = true;
}

-(void)resetNotificationCount{
    
    lblNotfnCount.text = @"";
    lblNotfnCount.hidden = true;
}
-(void)getNotificationCount{
    
    [APIMapper getNotificationCountOnsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject objectForKey:@"count"]) {
            
            NSInteger count = [[responseObject objectForKey:@"count"] integerValue];
            lblNotfnCount.text = @"";
            lblNotfnCount.hidden = true;
            if (count > 0) {
                lblNotfnCount.text = [NSString stringWithFormat:@"%ld",[[responseObject objectForKey:@"count"] integerValue]];
                lblNotfnCount.hidden = false;
            }
            count = [[responseObject objectForKey:@"chat_count"] integerValue];
            lblChatCount.text = @"";
            lblChatCount.hidden = true;
            if (count > 0) {
                lblChatCount.text = [NSString stringWithFormat:@"%ld",[[responseObject objectForKey:@"chat_count"] integerValue]];
                lblChatCount.hidden = false;
            }
           
        }
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
    }];
    
}

-(IBAction)showSearchPage:(UIButton*)sender{
    
    SearchUserListingViewController *searchPost =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForSearchPosts];
    AppDelegate *deleagte = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [deleagte.navGeneral pushViewController:searchPost animated:NO];
}

-(IBAction)launchHomePage:(UIButton*)sender{
    
    widthForSearch.constant = 0;
    btnSearch.hidden = true;
    if (navController) {
        [UIView transitionWithView:lblTitle
                          duration:0.7
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                           lblTitle.text = @"PurposeColor";
                        } completion:nil];
     [navController popToRootViewControllerAnimated:YES];
    }else{
        HomePageViewController *gemsWithHeader =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGEMWithHeaderListings];
        navController = [[UINavigationController alloc] initWithRootViewController:gemsWithHeader];
        navController.view.frame = vwContainer.bounds;
        [vwContainer addSubview:navController.view];
        navController.navigationBarHidden = true;
        lblTitle.text = @"PurposeColor";
    }
    lblNotfnCount.layer.cornerRadius = 10;
    lblNotfnCount.layer.borderColor = [UIColor clearColor].CGColor;
    lblNotfnCount.layer.borderWidth = 0.f;
    
    lblChatCount.layer.cornerRadius = 10;
    lblChatCount.layer.borderColor = [UIColor clearColor].CGColor;
    lblChatCount.layer.borderWidth = 0.f;


}


-(IBAction)showEmoitonalAwareness:(UIButton*)sender{
    
    EmotionalAwarenessViewController *journalList =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForImotionalAwareness];
   // EmotionalAwarenessViewController *journalList =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:@"EmotionalAwarenessNew"];
    [self.navigationController pushViewController:journalList animated:YES];
    [FIRAnalytics logEventWithName:@"Module"
                        parameters:@{
                                     kFIRParameterItemName:@"Mental Strength",
                                     }];

}



-(IBAction)showEmoitonalIntelligence:(UIButton*)sender{

    
    EmotionalIntelligenceViewController *emotionalIntelligence =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForEmotionalIntelligence];
    [navController pushViewController:emotionalIntelligence animated:YES];
    [FIRAnalytics logEventWithName:@"Module"
                        parameters:@{
                                     kFIRParameterItemName:@"Smart Journal",
                                     }];
    [UIView transitionWithView:lblTitle
                      duration:0.7
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        lblTitle.text = @"Smart Journal";
                    } completion:nil];
    
    
}

-(IBAction)showIntelligenceJournalView:(UIButton*)sender{
    
    [UIView transitionWithView:lblTitle
                      duration:0.7
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        lblTitle.text = @"Smart Journal";
                    } completion:nil];
    
    IntelligenceJournalViewController *emotionalIntelligence =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForIntelligenceJournalView];
    [navController pushViewController:emotionalIntelligence animated:YES];
        
}

-(IBAction)showGoalsAndDreams:(UIButton*)sender{
    
    widthForSearch.constant = 0;
    btnSearch.hidden = true;
     GoalsAndDreamsListingViewController *goalsAndDreams =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGoalsDreams];
    [navController pushViewController:goalsAndDreams animated:YES];
    [FIRAnalytics logEventWithName:@"Module"
                        parameters:@{
                                     kFIRParameterItemName:@"Goals & Dreams",
                                     }];
    [UIView transitionWithView:lblTitle
                      duration:0.7
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        lblTitle.text = @"Goals & Dreams";
                    } completion:nil];
}

-(IBAction)showCommunityGems:(UIButton*)sender{
    
    [UIView transitionWithView:lblTitle
                      duration:0.7
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                       lblTitle.text = @"Self-help Networking";
                    } completion:nil];
    
    widthForSearch.constant = 35;
    btnSearch.hidden = false;
     CommunityGEMListingViewController *gemListingVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCommunityGEMListings];
    [navController pushViewController:gemListingVC animated:YES];
    [FIRAnalytics logEventWithName:@"Module"
                        parameters:@{
                                     kFIRParameterItemName:@"Self-help Networking",
                                     }];
}

-(IBAction)showChatList:(id)sender{
    
    ChatUserListingViewController *chatUser =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatUserListings];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:chatUser animated:YES];
}

-(IBAction)showNotifications:(UIButton*)sender{
    
     NotificationsListingViewController *notifications =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForNotificationsListing];
    [self.navigationController pushViewController:notifications animated:YES];
    
}

#pragma mark - Slider View Setup and Delegates Methods

- (void)revealController:(SWRevealViewController *)revealController animateToPosition:(FrontViewPosition)position{
    UINavigationController *nav = (UINavigationController*)revealController.rearViewController;
    if ([[nav.viewControllers objectAtIndex:0] isKindOfClass:[MenuViewController class]]) {
        MenuViewController *root = (MenuViewController*)[nav.viewControllers objectAtIndex:0];
        [root resetTable];
    }
    if (position == FrontViewPositionLeft) {
        [self setVisibilityForOverLayIsHide:YES];
    }else{
        [self setVisibilityForOverLayIsHide:NO];
    }
    
}
-(IBAction)hideSlider:(id)sender{
    [self.revealViewController rightRevealToggle:nil];
}

-(void)setVisibilityForOverLayIsHide:(BOOL)isHide{
    
    if (isHide) {
        [UIView transitionWithView:vwOverLay
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            vwOverLay.alpha = 0.0;
                        }
                        completion:^(BOOL finished) {
                            
                            vwOverLay.hidden = true;
                        }];
        
        
    }else{
        
        vwOverLay.hidden = false;
        [UIView transitionWithView:vwOverLay
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            vwOverLay.alpha = 0.7;
                        }
                        completion:^(BOOL finished) {
                            
                        }];
        
    }
}


#pragma mark - Hide Account PopUp


-(IBAction)showHideAccountPopUp{
    
    if (!hideAccntPopUp) {
        
        hideAccntPopUp = [HideAccntPopUp new];
        [self.view addSubview:hideAccntPopUp];
        hideAccntPopUp.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[hideAccntPopUp]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(hideAccntPopUp)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[hideAccntPopUp]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(hideAccntPopUp)]];
        hideAccntPopUp.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            hideAccntPopUp.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
        
    }
    
    [hideAccntPopUp setUp];
}



-(void)closeForgotPwdPopUp{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        hideAccntPopUp.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [hideAccntPopUp removeFromSuperview];
        hideAccntPopUp = nil;
    }];
}



#pragma mark state preservation / restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}


- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}


- (void)applicationFinishedRestoringState
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Call whatever function you need to visually restore
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
