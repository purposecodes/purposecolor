//
//  LaunchPageViewController.h
//  PurposeColor
//
//  Created by Purpose Code on 15/11/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaunchPageViewController : UIViewController


-(IBAction)showEmoitonalAwareness:(UIButton*)sender;

-(IBAction)showEmoitonalIntelligence:(UIButton*)sender;

-(IBAction)showGoalsAndDreams:(UIButton*)sender;

-(IBAction)showCommunityGems:(UIButton*)sender;

-(void)launchHomePage;

-(void)resetChatNotificationCount;

-(void)resetNotificationCount;

-(IBAction)showIntelligenceJournalView:(UIButton*)sender;

-(void)getNotificationCount;

-(void)refreshCommunityPage;

-(void)refreshBdayBadge;

-(void)showSurveyPopUpWithInfo:(NSDictionary*)details;

@end
