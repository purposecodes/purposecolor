//
//  GEMDetailViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 11/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eFollow = 0,
    eFollowPending = 1,
    eFollowed = 2,
    
} eFollowStat;

#define kCellHeight             300;
#define kHeightForHeader        100;
#define kHeightForFooter        .001;
#define kNumberOfSections       3;
#define kHeightPercentage       80;
#define OneK                    1000

#import "GEMDetailViewController.h"
#import "GemDetailsCustomTableViewCell.h"
#import "Constants.h"
#import <MediaPlayer/MediaPlayer.h>
#import "CommentComposeViewController.h"
#import <AVKit/AVKit.h>
#import "CustomAudioPlayerView.h"
#import "PhotoBrowser.h"
#import "CreateActionInfoViewController.h"
#import "ACRObservingPlayerItem.h"
#import "KILabel.h"
#import "MTDURLPreview.h"
#import "GemDetailsCustomCellTitle.h"
#import "GemDetailsCustomCellDescription.h"
#import "GemDetailsCustomCellLocation.h"
#import "GemDetailsCustomCellContact.h"
#import "GemDetailsCustomCellPreview.h"
#import "HashTagGemListingViewController.h"
#import "GemDetailsCustomCellShareText.h"
#import "GemDetailsCustomCellJournal.h"
#import "HashTagListingForSavedPostViewController.h"

@interface GEMDetailViewController ()<GemDetailPageCellDelegate,CommentActionDelegate,CustomAudioPlayerDelegate,PhotoBrowserDelegate,ACRObservingPlayerItemDelegate,CreateMediaInfoDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblShareTitle;
    IBOutlet UIImageView *imgShareImage;
    IBOutlet UIView *vwFooter;
    NSArray *arrGemMedias;
    
    NSInteger playingIndex;
    BOOL isScrolling;
    BOOL isPlaying;
    
    IBOutlet UIButton *btnLike;
    
    CustomAudioPlayerView *vwAudioPlayer;
    PhotoBrowser *photoBrowser;
    
    AVPlayer *videoPlayer;
    AVPlayerItem *videoPlayerItem;
    UIActivityIndicatorView *videoIndicator;
    ACRObservingPlayerItem *playerItem;
}

@end

@implementation GEMDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self updateViewCount];
   
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)setUp{
    
    [vwFooter.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwFooter.layer setBorderWidth:1.f];
    // drop shadow
    [vwFooter.layer setShadowColor:[UIColor blackColor].CGColor];
    [vwFooter.layer setShadowOpacity:0.3];
    [vwFooter.layer setShadowRadius:2.0];
    [vwFooter.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 20;
    playingIndex = -1;
    arrGemMedias = [NSArray new];
    if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_media"]))
        arrGemMedias = [_gemDetails objectForKey:@"gem_media"];
    [btnLike setImage:[UIImage imageNamed:@"Like_Buton.png"] forState:UIControlStateNormal];
    if ([[_gemDetails objectForKey:@"like_status"] boolValue])
         [btnLike setImage:[UIImage imageNamed:@"Like_Active.png"] forState:UIControlStateNormal];
    if ([[_gemDetails objectForKey:@"gem_type"] isEqualToString:@"event"]) {
        lblTitle.text = [[NSString stringWithFormat:@"%@ DETAILS",@"MOMENT"] capitalizedString];
    }else{
         //lblTitle.text = [[NSString stringWithFormat:@"%@ DETAIL",[_gemDetails objectForKey:@"gem_type"]] capitalizedString];
         lblTitle.text = [[NSString stringWithFormat:@"POST DETAILS"] capitalizedString];
    }
    
    
    if (_canSave) {
        //lblShareTitle.text = @"Save";
       // imgShareImage.image = [UIImage imageNamed:@"Save_Gray.png"];
    }
    [tableView reloadData];
    
}

-(void)updateViewCount{
    
    NSString *gemID;
    NSString *gemType;
    
    if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_id"]))
        gemID = [_gemDetails objectForKey:@"gem_id"];
    
    if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_type"]))
        gemType = [_gemDetails objectForKey:@"gem_type"];
    
    if (gemType && gemID) {
       
        [APIMapper updatePostDetailPageWithGemID:gemID gemType:gemType onSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if (responseObject && [[responseObject objectForKey:@"code"] integerValue] == 200) {
                if ([self.delegate respondsToSelector:@selector(updateGemViewCountWithIndex:andCount:)]) {
                    [self.delegate updateGemViewCountWithIndex:_clickedIndex andCount:[[responseObject objectForKey:@"count"] integerValue]];
                }
            }
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
        }];
    }
    
}


#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumberOfSections;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        NSInteger rows = 5;
        
        NSString *string;
        if (NULL_TO_NIL([_gemDetails objectForKey:@"preview_url"])){
            string = [_gemDetails objectForKey:@"preview_url"];
            
        }else if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_details"])){
            string = [_gemDetails objectForKey:@"gem_details"];
        }
        if (string.length) {
            NSError *error = nil;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                       error:&error];
            NSArray *matches = [detector matchesInString:string
                                                 options:0
                                                   range:NSMakeRange(0, [string length])];
            if (matches.count > 0) {
                rows += 1;
            }

        }
        return rows;
    }
    else if (section == 1){
        NSInteger rows = 0;
        if ([[_gemDetails objectForKey:@"gem_type"] isEqualToString:@"moment"]) {
            rows  = 1;
            if ([_gemDetails objectForKey:@"action_title"]) {
                NSString *Actions = [_gemDetails objectForKey:@"action_title"];
                NSArray *_count = [Actions componentsSeparatedByString:@","];
                rows += _count.count;
            }
        }
        return rows;
    }else{
        return arrGemMedias.count;
    }
    return 0;
    
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
           GemDetailsCustomCellShareText *cell = (GemDetailsCustomCellShareText *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellShare"];
           cell.lblDescription.text = [NSString stringWithFormat:@"\'%@\'",[_gemDetails objectForKey:@"share_msg"]];
           return cell;
        }
        
        if (indexPath.row == 1) {
            GemDetailsCustomCellTitle *cell = (GemDetailsCustomCellTitle *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellTitle"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.lbltTitle.text = [_gemDetails objectForKey:@"gem_title"];
            cell.btnEdit.hidden = true;
            cell.btnDelete.hidden = true;
            cell.titleRightConstraint.constant = 10;
            if (_canDelete){
                //cell.titleRightConstraint.constant = 40;
                //cell.btnDelete.hidden = false;
            }
            if (_isFromGEM) {
                //cell.titleRightConstraint.constant = 40;
                //cell.btnEdit.hidden = false;
            }
            cell.lblDate.text = [Utility getDaysBetweenTwoDatesWith:[[_gemDetails objectForKey:@"gem_datetime"] doubleValue]];
            [self setFollowButtonWithCell:cell];
            return cell;
        }
        
        if (indexPath.row == 2) {
            GemDetailsCustomCellLocation *cell = (GemDetailsCustomCellLocation *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellLocation"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (NULL_TO_NIL([_gemDetails objectForKey:@"location_name"])){
                cell.lblLocation.text = [_gemDetails objectForKey:@"location_name"];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
        if (indexPath.row == 3) {
            GemDetailsCustomCellContact *cell = (GemDetailsCustomCellContact *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellContact"];
            if (NULL_TO_NIL([_gemDetails objectForKey:@"contact_name"])){
                cell.lblContact.text = [_gemDetails objectForKey:@"contact_name"];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
        if (indexPath.row == 4) {
            GemDetailsCustomCellDescription *cell = (GemDetailsCustomCellDescription *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellDescription"];
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:CommonFont_New size:14],
                                         };
            if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_details"])){
                NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[_gemDetails objectForKey:@"gem_details"] attributes:attributes];
                if (NULL_TO_NIL([_gemDetails objectForKey:@"hash_tags"])) {
                    NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[_gemDetails objectForKey:@"hash_tags"]] attributes:nil];
                    [attributedText appendAttributedString:newAttString];
                }
                cell.lbltDescription.attributedText = attributedText;
                cell.lbltDescription.systemURLStyle = YES;
                cell.lbltDescription.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
                    // Open URLs
                    [self attemptOpenURL:[NSURL URLWithString:string]];
                };
                cell.lbltDescription.hashtagLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
                    // Open URLs
                    [self hashTagClickedWithTag:string];
                    
                };
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row == 5) {
            GemDetailsCustomCellPreview *cell = (GemDetailsCustomCellPreview *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellPreview"];
            BOOL isPreview = false;
            NSString *string;
            if (NULL_TO_NIL([_gemDetails objectForKey:@"preview_url"])){
                string = [_gemDetails objectForKey:@"preview_url"];
                isPreview = true;
                
            }else if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_details"])){
                string = [_gemDetails objectForKey:@"gem_details"];
            }
            
            if (string.length) {
                NSError *error = nil;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                           error:&error];
                NSArray *matches = [detector matchesInString:string
                                                     options:0
                                                       range:NSMakeRange(0, [string length])];
                if (matches.count > 0) {
                    if (!isPreview) {
                        cell.heightForPreview.constant = 105;
                        cell.previewImageHeight.constant = 0;
                        cell.previewImageTop.constant = 0;
                    }else{
                        cell.heightForPreview.constant = 305;
                        cell.previewImageHeight.constant = 200;
                        cell.previewImageTop.constant = 5;
                    }
                    
                    if ([[CashedURL sharedManager].dictCashedURLS objectForKey:string]) {
                        MTDURLPreview *preview = [[CashedURL sharedManager].dictCashedURLS objectForKey:string];
                        [cell.indicator stopAnimating];
                        cell.lblTitle.text = preview.title;
                        cell.lblDescription.text = preview.content;
                        cell.lblDomain.text = preview.domain;
                        [cell.imgPreview setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
                        if (preview.imageURL) {
                            [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                               placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                          
                                                      }];
                    
                        }
                    }else{
                        NSTextCheckingResult *match = [matches firstObject];
                        [cell.indicator startAnimating];
                        [cell.imgPreview setImage:[UIImage imageNamed:@""]];
                        [MTDURLPreview loadPreviewWithURL:[match URL] completion:^(MTDURLPreview *preview, NSError *error) {
                            if (!error) {
                                if (preview) [[CashedURL sharedManager].dictCashedURLS setObject:preview forKey:string];
                            }
                            [cell.indicator stopAnimating];
                            cell.lblTitle.text = preview.title;
                            cell.lblDescription.text = preview.content;
                            cell.lblDomain.text = preview.domain;
                            [cell.imgPreview setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
                            if (preview.imageURL) {
                                [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                                   placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                              
                                                              
                                                              
                                                          }];
                            }
                            
                        }];
                    }
                    
                }
                
            }
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
    }
    else if (indexPath.section == 1){
        //Moment Info
         if (indexPath.row == 0) {
             GemDetailsCustomCellJournal *cell = (GemDetailsCustomCellJournal *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellJournal"];
             [self configureMomentDetailsWith:_gemDetails cell:cell];
             cell.selectionStyle = UITableViewCellSelectionStyleNone;
             return cell;
         }else{
             GemDetailsCustomCellTitle *cell = (GemDetailsCustomCellTitle *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellActionTitle"];
             cell.selectionStyle = UITableViewCellSelectionStyleNone;
             if ([_gemDetails objectForKey:@"action_title"]) {
                 NSString *Actions = [_gemDetails objectForKey:@"action_title"];
                 NSArray *_count = [Actions componentsSeparatedByString:@","];
                 if (indexPath.row - 1 < _count.count) {
                     cell.lbltTitle.text = _count[indexPath.row - 1];
                 }
             }
             return  cell;
         }
    }
    
    else{
        static NSString *CellIdentifier = @"reuseIdentifier";
        GemDetailsCustomTableViewCell *cell = (GemDetailsCustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        [self resetCellVariables:cell];
        [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];
        if (indexPath.row < arrGemMedias.count)
            [self showMediaDetailsWithCell:cell andDetails:arrGemMedias[indexPath.row] index:indexPath.row];
        return cell;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        if (indexPath.row == 0) {
            if (NULL_TO_NIL([_gemDetails objectForKey:@"share_msg"]))
                return UITableViewAutomaticDimension;
            else
                return 0;
        }
        if (indexPath.row == 2) {
            if (NULL_TO_NIL([_gemDetails objectForKey:@"location_name"]))
                return UITableViewAutomaticDimension;
            else
                return 0;
        }
        else if (indexPath.row == 3) {
            if (NULL_TO_NIL([_gemDetails objectForKey:@"contact_name"]))
                return UITableViewAutomaticDimension;
            else
                return 0;
        }
        
        else if (indexPath.row == 4) {
            if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_details"]))
                return UITableViewAutomaticDimension;
            else
                return 0;
        }
        else if (indexPath.row == 7) {
             return UITableViewAutomaticDimension;
        }
        
    }
    
    return UITableViewAutomaticDimension;
    
}
- (CGFloat)tableView:(UITableView *)_tableView heightForHeaderInSection:(NSInteger)section{

    return 0.001;

}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (section == 1) {
        return 5;
    }
    return kHeightForFooter;
}



- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return nil;
    
}

-(void)showMediaDetailsWithCell:(GemDetailsCustomTableViewCell*)cell andDetails:(NSDictionary*)mediaInfo index:(NSInteger)_index{
    
    NSString *mediaType ;
    if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
        mediaType = [mediaInfo objectForKey:@"media_type"];
    }
    
    if (mediaType) {
        
        if ([mediaType isEqualToString:@"image"]) {
            
            // Type Image
            //[cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage.png"]];
            [cell.activityIndicator startAnimating];
            [cell.imgGemMedia sd_setImageWithURL:[NSURL URLWithString:[mediaInfo objectForKey:@"gem_media"]]
                                placeholderImage:[UIImage imageNamed:@""]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             [cell.activityIndicator stopAnimating];
                                       }];
        }
        
        else if ([mediaType isEqualToString:@"audio"]) {
            
            // Type Audio
            
            [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
            if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                [[cell btnAudioPlay]setHidden:false];
               
            }
            
        }
        
        else if ([mediaType isEqualToString:@"video"]) {
            
            // Type Video
            
            if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                NSString *videoURL = [mediaInfo objectForKey:@"gem_media"];
                if (videoURL.length){
                    [[cell btnVideoPlay]setHidden:false];
                   // [self setUpAutoPlayWithURL:[NSURL URLWithString:videoURL] withCell:cell];
                }
            }
            
            if (NULL_TO_NIL([mediaInfo objectForKey:@"video_thumb"])) {
                NSString *videoThumb = [mediaInfo objectForKey:@"video_thumb"];
                if (videoThumb.length) {
                    [cell.activityIndicator startAnimating];
                    [cell.imgGemMedia sd_setImageWithURL:[NSURL URLWithString:videoThumb]
                                        placeholderImage:[UIImage imageNamed:@""]
                                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                   [cell.activityIndicator stopAnimating];
                                               }];
                }
                
            }
            
        }
        
        float imageHeight = 0;
        float width = [[mediaInfo objectForKey:@"image_width"] floatValue];
        float height = [[mediaInfo objectForKey:@"image_height"] floatValue];
        float ratio = width / height;
        imageHeight = (self.view.frame.size.width - 10) / ratio;
        cell.constraintForHeight.constant = imageHeight;
        
    }
    
    
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    isScrolling = NO;
   // [tableView reloadData];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)aScrollView
{
    [videoPlayer pause];
    isPlaying = false;
    isScrolling = YES;
  //  [tableView reloadData];
    playingIndex = -1;
}


- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 && indexPath.row == 5) {
        [self openPreviewURL];
       
    }else if (indexPath.section == 2){
        
       // [tableView reloadData];
       // [videoPlayer pause];
      //  videoPlayer = nil;
        
        NSMutableArray *images = [NSMutableArray new];
        NSString *mediaType ;;
        if (indexPath.row < arrGemMedias.count) {
            NSDictionary *mediaInfo = arrGemMedias[indexPath.row];
            if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
                mediaType = [mediaInfo objectForKey:@"media_type"];
            }
            if (mediaType) {
                if ([mediaType isEqualToString:@"image"]) {
                    NSURL *url =  [NSURL URLWithString:[mediaInfo objectForKey:@"gem_media"]];
                    [images addObject:url];
                    for (NSDictionary *details in arrGemMedias) {
                        if (NULL_TO_NIL([details objectForKey:@"media_type"])) {
                            mediaType = [details objectForKey:@"media_type"];
                        }
                        if (mediaType) {
                            if ([mediaType isEqualToString:@"image"]) {
                                NSURL *url =  [NSURL URLWithString:[details objectForKey:@"gem_media"]];
                                if (![images containsObject:url]) {
                                    [images addObject:url];
                                }
                                
                            }
                            
                        }
                    }
                    if (images.count) {
                        [self presentGalleryWithImages:images];
                    }
                }
                else if ([mediaType isEqualToString:@"video"]){
                    if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                        NSString *videoURL = [mediaInfo objectForKey:@"gem_media"];
                        if (videoURL.length){
                            NSError* error;
                            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
                            [[AVAudioSession sharedInstance] setActive:NO error:&error];
                            NSURL *movieURL = [NSURL URLWithString:videoURL];
                            AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
                            [playerViewController.player play];
                            playerViewController.player = [AVPlayer playerWithURL:movieURL];
                            [self presentViewController:playerViewController animated:YES completion:nil];
                            [[NSNotificationCenter defaultCenter] addObserver:self
                                                                     selector:@selector(videoDidFinish:)
                                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                                       object:[playerViewController.player currentItem]];
                            
                        }
                    }
                }
                
            }
        }
    }
    
    
}

- (void)videoDidFinish:(id)notification
{
    [self dismissViewControllerAnimated:YES completion:nil];
   [[NSNotificationCenter defaultCenter] removeObserver:self];

    
    //fade out / remove subview
}

-(void)resetCellVariables:(GemDetailsCustomTableViewCell*)cell{
    
    [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
    [cell.activityIndicator stopAnimating];
    [[cell btnVideoPlay]setHidden:YES];
    [[cell btnAudioPlay]setHidden:YES];
    [videoPlayer pause];
    [cell.videoActivity stopAnimating];
    cell.videoPlayer = nil;
    [cell.videoPlayer pause];
    [cell.avLayer removeFromSuperlayer];
    cell.videoItem = nil;
    cell.imgGemMedia.hidden = false;
    [videoIndicator stopAnimating];
    videoIndicator = nil;
    
}
-(void)configureMomentDetailsWith:(NSDictionary*)details cell:(GemDetailsCustomCellJournal*)cell{
    
    NSInteger emotionValue = [[details objectForKey:@"emotion_value"] integerValue] + 3;
    NSInteger driveValue = [[details objectForKey:@"drive_value"] integerValue] + 3;
    
    NSMutableAttributedString *myString = [NSMutableAttributedString new];
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    UIImage *icon = [UIImage imageNamed:[NSString stringWithFormat:@"%ld_Star_Small",(long)emotionValue]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblFeel.font.descender + 2), icon.size.width, icon.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    [myString appendAttributedString:attachmentString];
    NSMutableAttributedString *strEmotion = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" Feeling %@",[details objectForKey:@"emotion_title"]]];
    [strEmotion addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:CommonFontBold_New size:14]
                       range:NSMakeRange(8, strEmotion.length - 8)];
    [strEmotion addAttribute:NSForegroundColorAttributeName
                       value:[UIColor blackColor]
                       range:NSMakeRange(8, strEmotion.length - 8)];
    [myString appendAttributedString:strEmotion];
    cell.lblFeel.attributedText = myString;
    
    attachment = [[NSTextAttachment alloc] init];
    myString = [NSMutableAttributedString new];
    icon = [UIImage imageNamed:[NSString stringWithFormat:@"%ld_Star_Small",(long)driveValue]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblReaction.font.descender + 2), icon.size.width, icon.size.height);
    attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    [myString appendAttributedString:attachmentString];
    
    if ([details objectForKey:@"goal_title"]) {
        NSMutableAttributedString *goalTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" Goal %@",[details objectForKey:@"goal_title"]]];
        [goalTitle addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:CommonFontBold_New size:14]
                          range:NSMakeRange(6, goalTitle.length - 6)];
        [goalTitle addAttribute:NSForegroundColorAttributeName
                          value:[UIColor blackColor]
                          range:NSMakeRange(6, goalTitle.length - 6)];
        [myString appendAttributedString:goalTitle];
    }
    if (NULL_TO_NIL([details objectForKey:@"action_title"])) {
        NSString *Actions = [details objectForKey:@"action_title"];
        NSArray *count = [Actions componentsSeparatedByString:@","];
        NSMutableAttributedString *goalTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" &\n%lu Action(s)",(unsigned long)count.count]];
        
        [goalTitle addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:CommonFontBold_New size:14]
                          range:NSMakeRange(3, goalTitle.length - 3)];
        [goalTitle addAttribute:NSForegroundColorAttributeName
                          value:[UIColor blackColor]
                          range:NSMakeRange(3, goalTitle.length - 3)];
        
        [myString appendAttributedString:goalTitle];
    }
    
    cell.lblReaction.attributedText = myString;
    
}

-(void)setFollowButtonWithCell:(GemDetailsCustomCellTitle*)cell{
    
    cell.titleRightConstraint.constant = 10;
    cell.folwHeightConstraint.constant = 0;
    cell.btnFollow.hidden = true;
    [cell.btnFollow setEnabled:false];
    BOOL canFollow = true;
    if (NULL_TO_NIL([_gemDetails objectForKey:@"can_follow"]))
        canFollow = [[_gemDetails objectForKey:@"can_follow"] boolValue];
    if ([[User sharedManager].userId isEqualToString:[_gemDetails objectForKey:@"user_id"]]) {
        canFollow = false;
    }
    if (canFollow) {
        NSInteger followStatus = 0;
        if (NULL_TO_NIL([_gemDetails objectForKey:@"follow_status"]))
            followStatus =  [[_gemDetails objectForKey:@"follow_status"] intValue];
        if (followStatus < eFollowed) {
            cell.btnFollow.hidden = true;
            switch (followStatus) {
                case eFollowPending:
                    [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
                    [cell.btnFollow setBackgroundColor:[UIColor lightGrayColor]];
                    [cell.btnFollow setTitleColor:[UIColor whiteColor ] forState:UIControlStateNormal];
                    cell.btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
                    cell.btnFollow.hidden = false;
                    [cell.btnFollow setEnabled:false];
                    cell.titleRightConstraint.constant = 80;
                    cell.folwHeightConstraint.constant = 35;
                    break;
                    
                case eFollow:
                    [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
                    [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
                    [cell.btnFollow setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
                    cell.btnFollow.layer.borderColor = [UIColor getThemeColor].CGColor;
                    cell.btnFollow.hidden = false;
                    [cell.btnFollow setEnabled:true];
                    cell.titleRightConstraint.constant = 80;
                    cell.folwHeightConstraint.constant = 35;
                    break;
                    
                default:
                    break;
            }
            
        }
    }
}


#pragma mark - Player Methods and  Delegates

-(void)setUpAutoPlayWithURL:(NSURL*)videoURL withCell:(GemDetailsCustomTableViewCell*)cell{
    
    if (!isScrolling && !isPlaying) {
        [cell.videoActivity startAnimating];
        isPlaying = true;
        cell.imgGemMedia.hidden = true;
        [cell.btnVideoPlay setHidden:true];
        NSURL *url = videoURL;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            dispatch_sync(dispatch_get_main_queue(), ^{
                AVAsset *avAsset = [AVAsset assetWithURL:url];
                playerItem = [[ACRObservingPlayerItem alloc] initWithAsset:avAsset];
                cell.videoPlayer = [AVPlayer playerWithPlayerItem:playerItem];
                cell.videoPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                videoPlayer =  cell.videoPlayer;
                cell.avLayer = [AVPlayerLayer playerLayerWithPlayer:cell.videoPlayer];
                cell.avLayer.frame = CGRectMake(5, 5, cell.imgGemMedia.frame.size.width, 245);
                cell.avLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                [cell.contentView.layer addSublayer:cell.avLayer];
                [cell.contentView bringSubviewToFront:cell.videoActivity];
                videoIndicator = cell.videoActivity;
                playerItem.delegate = self;
                
            });
        });
    }
}

- (void)bufferEmpty{
    [videoIndicator startAnimating];
}
- (void)playerResumeAfterBuffer{
    [videoPlayer play];
    [videoIndicator stopAnimating];
}
- (void)playerItemReachedEnd{
    
    [videoIndicator stopAnimating];
    [tableView reloadData];
}
- (void)playerItemStalled{
    
}
- (void)playerItemReadyToPlay{
    [videoPlayer play];
    [videoIndicator stopAnimating];
}
- (void)playerItemPlayFailed{
    [videoIndicator stopAnimating];
}
- (void)playerItemRemovedObservation{
    
}

#pragma mark - Custom Cell Delegates

-(void)resetPlayerVaiablesForIndex:(NSInteger)tag{
    
    [tableView reloadData];
    [videoPlayer pause];
    videoPlayer = nil;
    
    if (tag < arrGemMedias.count){
        
        NSDictionary * mediaInfo = arrGemMedias[tag];
        NSString *mediaType ;
        if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
            mediaType = [mediaInfo objectForKey:@"media_type"];
        }
        
        if (mediaType) {
            
            if ([mediaType isEqualToString:@"image"]) {
                
            }
            
            else if ([mediaType isEqualToString:@"audio"]) {
                
                NSString *audioURL = [mediaInfo objectForKey:@"gem_media"];
                if (audioURL.length){
                    NSURL *movieURL = [NSURL URLWithString:audioURL];
                    [self showAudioPlayerWithURL:movieURL];
                }
            }
            
            else if ([mediaType isEqualToString:@"video"]) {
                // Type Video
                if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                    NSString *videoURL = [mediaInfo objectForKey:@"gem_media"];
                    if (videoURL.length){
                        NSError* error;
                        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
                        [[AVAudioSession sharedInstance] setActive:NO error:&error];
                        NSURL *movieURL = [NSURL URLWithString:videoURL];
                        AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
                        playerViewController.player = [AVPlayer playerWithURL:movieURL];
                         [playerViewController.player play];
                        [[NSNotificationCenter defaultCenter] addObserver:self
                                                                 selector:@selector(videoDidFinish:)
                                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                                   object:[playerViewController.player currentItem]];
                        [self presentViewController:playerViewController animated:YES completion:nil];
                        
                    }
                }
                
            }
            
        }
    }

}

-(void)showAudioPlayerWithURL:(NSURL*)url{
    

    if (!vwAudioPlayer) {
        vwAudioPlayer = [[[NSBundle mainBundle] loadNibNamed:@"CustomAudioPlayerView" owner:self options:nil] objectAtIndex:0];
        vwAudioPlayer.delegate = self;
    }
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = vwAudioPlayer;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer setupAVPlayerForURL:url];
    }];
    
}

-(void)closeAudioPlayerView{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwAudioPlayer.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer removeFromSuperview];
        vwAudioPlayer = nil;
        NSError* error;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        [[AVAudioSession sharedInstance] setActive:NO error:&error];
    }];
    
    
    
}

#pragma mark - Photo Browser & Deleagtes

- (void)presentGalleryWithImages:(NSArray*)images
{
    [videoPlayer pause];
    videoPlayer = nil;
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}


#pragma mark - Media share/ Like / Comment / Follow Methods

-(IBAction)followUserClicked{
    
    if (NULL_TO_NIL([_gemDetails objectForKey:@"user_id"])) {
        NSString *followerID = [_gemDetails objectForKey:@"user_id"];
        [self showLoadingScreen];
        [APIMapper sendFollowRequestWithUserID:[User sharedManager].userId followerID:followerID success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self hideLoadingScreen];
            if ([[responseObject objectForKey:@"code"] integerValue] == 401) {
                UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Follow" message:[responseObject objectForKey:@"text"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* copy = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    
                    [alert dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alert addAction:copy];
                [self.navigationController presentViewController:alert animated:YES completion:nil];
                
                if ([self.delegate respondsToSelector:@selector(updateGemsWithFollowStatusWithIndex:latestFollowStatus:followstatus:)]) {
                    [self.delegate updateGemsWithFollowStatusWithIndex:_clickedIndex latestFollowStatus:false followstatus:0];
                }
                [self updateDetailPageWithFollowStatusWithlatestFollowStatus:false followstatus:0];
            }else{
                if ([self.delegate respondsToSelector:@selector(updateGemsWithFollowStatusWithIndex:latestFollowStatus:followstatus:)]) {
                    [self.delegate updateGemsWithFollowStatusWithIndex:_clickedIndex latestFollowStatus:true followstatus:[[responseObject objectForKey:@"follow_status"] integerValue] ];
                }
                [self updateDetailPageWithFollowStatusWithlatestFollowStatus:true followstatus:[[responseObject objectForKey:@"follow_status"] integerValue]];
            }
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            [self hideLoadingScreen];
        }];
    }
}


-(IBAction)likeMediaClicked{
    
    //Favourite / Like Clicked
    
    if ([self.delegate respondsToSelector:@selector(likeAppliedFromMediaPage:)]) {
        [self.delegate likeAppliedFromMediaPage:_clickedIndex];
        if (![[_gemDetails objectForKey:@"like_status"] boolValue]){
            [btnLike setImage:[UIImage imageNamed:@"Like_Active.png"] forState:UIControlStateNormal];
            [_gemDetails setValue:[NSNumber numberWithBool:1] forKey:@"like_status"];
        }else{
             [btnLike setImage:[UIImage imageNamed:@"Like_Buton.png"] forState:UIControlStateNormal];
            [_gemDetails setValue:[NSNumber numberWithBool:0] forKey:@"like_status"];
        }
        
    }
    
}

-(IBAction)shareButtonClicked{
    
    if ([self.delegate respondsToSelector:@selector(shareAppliedFromMediaPage:)]) {
        [self.delegate shareAppliedFromMediaPage:_clickedIndex];
    }
    
}

-(IBAction)commentButtonClicked{
    
    if ([self.delegate respondsToSelector:@selector(commentAppliedFromMediaPage:)]) {
        [self.delegate commentAppliedFromMediaPage:_clickedIndex];
    }

}

-(IBAction)deleteTheGem{
    
    if ([self.delegate respondsToSelector:@selector(deleteAppliedFromMediaPage:)]) {
        [self.delegate deleteAppliedFromMediaPage:_clickedIndex];
    }
    
}

-(IBAction)editAGEM{
    
    /*! Favourite Button Action!*/
    
//    NSString *gemID;
//    NSString *gemType;
//
//    if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_id"]))
//        gemID = [_gemDetails objectForKey:@"gem_id"];
//
//    if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_type"]))
//        gemType = [_gemDetails objectForKey:@"gem_type"];
//
//    CreateActionInfoViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCreateActionMedias];
//    detailPage.actionType = eActionTypeGoalsAndDreams;
//     if ([[_gemDetails objectForKey:@"gem_type"] isEqualToString:@"action"]) detailPage.actionType = eActionTypeActions;
//    if (_isFromGEM) detailPage.delegate = self;
//    if ([gemType isEqualToString:@"event"]) detailPage.actionType = eActionTypeEvent;
//    detailPage.strTitle = [[NSString stringWithFormat:@"EDIT AS %@",[_gemDetails objectForKey:@"gem_type"]] uppercaseString] ;
//    if ([[_gemDetails objectForKey:@"gem_type"] isEqualToString:@"community"]) {
//        detailPage.strTitle = @"SAVE POST";
//    }
//    [[self navigationController]pushViewController:detailPage animated:YES];
//    [detailPage getMediaDetailsForGemsToBeEditedWithGEMID:gemID GEMType:gemType];
    
}


-(void)newActionCreatedWithActionTitle:(NSString*)actionTitle actionID:(NSInteger)actionID;{
    
    // If the detail page comes from Saved gems , after edit , the page should be reloaded.
    
    [self getUpdatedGEMDetails];
    
}

-(void)goalsAndDreamsCreatedWithGoalTitle:(NSString*)goalTitle goalID:(NSInteger)goalID{

    [self getUpdatedGEMDetails];
}

-(void)getUpdatedGEMDetails{
    
    NSString *gemID;
    NSString *gemType;
    
    if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_id"]))
        gemID = [_gemDetails objectForKey:@"gem_id"];
    
    if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_type"]))
        gemType = [_gemDetails objectForKey:@"gem_type"];
    
    [self showLoadingScreen];
    [APIMapper getAnyGemDetailsWithGEMID:[_gemDetails objectForKey:@"gem_id"] gemType:gemType userID:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        [self refreshDetailPageWith:responseObject];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self hideLoadingScreen];
    }];

    
}

-(void)refreshDetailPageWith:(NSMutableDictionary*)details{
    
    _gemDetails = details;
    playingIndex = -1;
    arrGemMedias = [NSArray new];
    if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_media"]))
        arrGemMedias = [_gemDetails objectForKey:@"gem_media"];
    [tableView reloadData];
    if ([self.delegate respondsToSelector:@selector(refreshParentListings)]) {
        [self.delegate refreshParentListings];
    }
   

    
}

-(void)updateDetailPageWithFollowStatusWithlatestFollowStatus:(BOOL)latestFollowStatus followstatus:(NSInteger)followStatus{
    if (NULL_TO_NIL([_gemDetails objectForKey:@"user_id"])) {
        if ([_gemDetails objectForKey:@"follow_status"]) {
            if (!latestFollowStatus) {
                [_gemDetails setValue:[NSNumber numberWithBool:false] forKey:@"can_follow"];
            }else{
                [_gemDetails setObject:[NSNumber numberWithInteger:followStatus] forKey:@"follow_status"];
            }
            [tableView reloadData];
        }
    }
}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

- (void)openPreviewURL
{
    NSString *string;
    if (NULL_TO_NIL([_gemDetails objectForKey:@"preview_url"])){
        string = [_gemDetails objectForKey:@"preview_url"];
        
    }else if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_details"])){
        string = [_gemDetails objectForKey:@"gem_details"];
    }
    if (string.length) {
        NSError *error = nil;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                   error:&error];
        NSArray *matches = [detector matchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
        if (matches.count > 0) {
            NSTextCheckingResult *match = [matches firstObject];
            if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                [[UIApplication sharedApplication] openURL:[match URL] options:@{}
                                         completionHandler:^(BOOL success) {
                                         }];
            }
        }

    }
  

}
-(IBAction)goBack:(id)sender{
    
    [[self navigationController] popViewControllerAnimated:YES];

}

- (void)attemptOpenURL:(NSURL *)url
{
    
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (!safariCompatible) {
        
        NSString *urlString = url.absoluteString;
        urlString = [NSString stringWithFormat:@"http://%@",url.absoluteString];
        url = [NSURL URLWithString:urlString];
        
    }
    safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:url options:@{}
                                     completionHandler:^(BOOL success) {
                                     }];
        }
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem"
                                                                       message:@"The selected link cannot be opened."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dismiss"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:cancel];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
    }
}

-(IBAction)hashTagClickedWithTag:(NSString*)tag{
    
    if (_isFromGEM) {
        HashTagListingForSavedPostViewController *hashListing =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForHashTagForSavedPosts];
        //hashListing.delegate = self;
        hashListing.strTagName = tag;
        [[self navigationController] pushViewController:hashListing animated:YES];
    }else{
        HashTagGemListingViewController *hashListing =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForHashTagListings];
        hashListing.strTagName = tag;
        [self.navigationController pushViewController:hashListing animated:YES];
    }
   
}


- (void)dealloc {
    playerItem.delegate = nil;
    // or
    playerItem = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
