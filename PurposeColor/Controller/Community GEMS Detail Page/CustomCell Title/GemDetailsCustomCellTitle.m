//
//  GemDetailsCustomTableViewCell.m
//  PurposeColor
//
//  Created by Purpose Code on 18/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "GemDetailsCustomCellTitle.h"


@interface GemDetailsCustomCellTitle (){
    
}

@end

@implementation GemDetailsCustomCellTitle

- (void)awakeFromNib {
    // Initialization code
   
    [_btnFollow.layer setBorderColor:[UIColor getThemeColor].CGColor];
    [_btnFollow.layer setBorderWidth:1.f];
    _btnFollow.layer.cornerRadius = 5.f;
    // drop shadow
  
    
   
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


