//
//  GemDetailsCustomTableViewCell.h
//  PurposeColor
//
//  Created by Purpose Code on 18/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GemDetailsCustomCellJournal : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *lblFeel;
@property (nonatomic,weak) IBOutlet UILabel *lblReaction;


@end
