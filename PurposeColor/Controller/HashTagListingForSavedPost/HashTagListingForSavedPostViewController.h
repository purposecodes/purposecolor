//
//  MyFavouritesListingViewController.h
//  PurposeColor
//
//  Created by Purpose Code on 23/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

@protocol HashTagForSavedPostDelegte <NSObject>


@optional

/*!
 *This method is invoked when user Clicks "POST COMMENT" Button
 */
-(void)refreshPageAfterUpdate;



@end

#import <UIKit/UIKit.h>

@interface HashTagListingForSavedPostViewController : UIViewController

@property (nonatomic,strong) NSString *strTagName;
@property (nonatomic,weak)  id<HashTagForSavedPostDelegte>delegate;



@end
