//
//  CustomCellForType.m
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "NotificationListingCell.h"

@implementation NotificationListingCell

- (void)awakeFromNib {
    // Initialization code
    [self setUp];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUp{
    
    _imgTemplate.clipsToBounds = YES;
    _imgTemplate.layer.cornerRadius = 25.f;
    _imgTemplate.layer.borderColor = [UIColor clearColor].CGColor;
    _imgTemplate.contentMode = UIViewContentModeScaleAspectFill;
    
    _btnReject.layer.borderWidth = 1.f;
    _btnReject.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    _btnReject.layer.cornerRadius = 4.f;
    _btnAccept.layer.cornerRadius = 4.f;
 

}






@end
