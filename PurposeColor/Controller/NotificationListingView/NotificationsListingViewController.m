//
//  NotificationsListingViewController.m
//  SignSpot
//
//  Created by Purpose Code on 09/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kSectionCount               4
#define kDefaultCellHeight          95
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kWidthPadding               115
#define kFollowHeightPadding        85
#define kOthersHeightPadding        20

#import "NotificationsListingViewController.h"
#import "NotificationListingCell.h"
#import "Constants.h"
#import "MenuViewController.h"
#import "GemsUnderNotificationDetail.h"
#import "ProfilePageViewController.h"
#import "FollowersListingViewController.h"
#import "ChatUserListingViewController.h"
#import "BdayWishingViewController.h"
#import "LikedAndCommentedUserListings.h"

@interface NotificationsListingViewController ()<SWRevealViewControllerDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnSlideMenu;
    IBOutlet UIView *vwOverLay;
    BOOL isDataAvailable;
    NSInteger totalPages;
    NSInteger currentPage;
    BOOL isPageRefresing;
    NSMutableArray *arrNotifications;
    NSMutableArray *arrFollowReq;
    NSMutableArray *arrChatReq;
    NSMutableArray *arrBday;
    NSString *strNoDataText;
    UIRefreshControl *refreshControl;
}

@end

@implementation NotificationsListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self resetNotificationCount];
    [self setUp];
    [self loadAllMyNotificationsWithPageNo:currentPage isByPagination:NO shouldDisableCach:YES];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 200;
    currentPage = 1;
    totalPages = 0;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.alwaysBounceVertical = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    isPageRefresing = YES;
    isDataAvailable = false;
    [tableView setHidden:true];
    
}



-(void)resetNotificationCount{
    
    [APIMapper updateBadgeCountWithType:@"notification" Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [delegate.launchPage resetNotificationCount];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
    }];

}

-(void)refreshData{
    
    [self resetDataSource];
    [self loadAllMyNotificationsWithPageNo:currentPage isByPagination:NO shouldDisableCach:YES];
   
    
}

-(void)resetDataSource{
    
    currentPage = 1;
    arrFollowReq = nil;
    arrChatReq = nil;
    arrNotifications = nil;
    arrBday = nil;
}

-(void)loadAllMyNotificationsWithPageNo:(NSInteger)pageNo isByPagination:(BOOL)_isPagination shouldDisableCach:(BOOL)shouldDisableCach{
    
    
    if (!_isPagination) {
        [self resetDataSource];
        [self showLoadingScreen];
    }
    
    [APIMapper getAllMyNotificationsWith:[User sharedManager].userId pageNumber:pageNo shouldDisableCach:shouldDisableCach success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getNotificationsFromResponds:responseObject isPagination:_isPagination];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        strNoDataText = error.localizedDescription;
        if (error && error.localizedDescription) [ALToastView toastInView:self.view withText:NETWORK_ERROR_MESSAGE];
        isDataAvailable = false;
        isPageRefresing = false;
        [refreshControl endRefreshing];
        [self hideLoadingScreen];
        [tableView reloadData];
        [tableView setHidden:false];
    }];
        
      
}


-(void)getNotificationsFromResponds:(NSDictionary*)responseObject isPagination:(BOOL)_isPagination{
    
    [refreshControl endRefreshing];
    isDataAvailable = true;
    isPageRefresing = false;
    
    if (_isPagination) {
        
        [arrNotifications addObjectsFromArray:[responseObject objectForKey:@"notification"]];
        
    }else{
        
        if (NULL_TO_NIL([responseObject objectForKey:@"follow"])){
            arrFollowReq = [responseObject objectForKey:@"follow"];
        }
        if (NULL_TO_NIL([responseObject objectForKey:@"chat"])){
            arrChatReq = [responseObject objectForKey:@"chat"];
            
        }if (NULL_TO_NIL([responseObject objectForKey:@"notification"])){
            if (!arrNotifications) {
                arrNotifications = [NSMutableArray new];
            }
            [arrNotifications addObjectsFromArray:[responseObject objectForKey:@"notification"]];
        }
        if (NULL_TO_NIL([responseObject objectForKey:@"birthday"])){
            arrBday = [responseObject objectForKey:@"birthday"];
        }
    }
    
    
    if (NULL_TO_NIL([responseObject objectForKey:@"pageCount"]))
        totalPages =  [[responseObject objectForKey:@"pageCount"]integerValue];
    
    if (NULL_TO_NIL([responseObject objectForKey:@"currentPage"]))
        currentPage =  [[responseObject objectForKey:@"currentPage"]integerValue];
    
    if (arrFollowReq.count <= 0 && arrChatReq.count <= 0 && arrNotifications.count <= 0 && arrBday.count <= 0) {
        isDataAvailable = false;
    }
    
    [tableView setHidden:false];
    [tableView reloadData];
    
    
}



#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (!isDataAvailable) return kMinimumCellCount;
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    NSInteger rows = 0;
    if (section == 0) rows = arrFollowReq.count;
    if (section == 1) rows = arrChatReq.count;
    if (section == 2) rows = arrBday.count;
    if (section == 3) rows = arrNotifications.count;
   
    return rows;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (!isDataAvailable) {
        
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No records found!"];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        return cell;
    }

    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) {
        return nil;
    }
    
    if (section == 0 && arrFollowReq.count <= 0) {
        return nil;
    }
    if (section == 1 && arrChatReq.count <= 0) {
        return nil;
    }
    if (section == 2 && arrBday.count <= 0) {
        return nil;
    }
    if (section == 3 && arrNotifications.count <= 0) {
        return nil;
    }
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor whiteColor];
    vwHeader.clipsToBounds = YES;
    
    UIView *vwBg = [UIView new];
    vwBg.backgroundColor = [UIColor whiteColor];
    [vwHeader addSubview:vwBg];
    vwBg.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwBg]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwBg)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[vwBg]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwBg)]];
    
    UIView *vwBorder = [UIView new];
    vwBorder.backgroundColor = [UIColor getSeperatorColor];
    [vwHeader addSubview:vwBorder];
    vwBorder.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwBorder]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwBorder)]];
    [vwBorder addConstraint:[NSLayoutConstraint constraintWithItem:vwBorder
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.0
                                                          constant:0.5f]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:vwBorder
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:0]];


    
   
    UILabel *lblTitle= [UILabel new];
    lblTitle.numberOfLines = 1;
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[lblTitle]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    lblTitle.font = [UIFont fontWithName:CommonFontBold_New size:17];
    lblTitle.textColor = [UIColor getTitleBlackColor];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0]];
    lblTitle.text = @"Notifications";
    if (section == 0) {
        lblTitle.text = @"Follow Requests";
    }
    else if (section == 1) {
        lblTitle.text = @"Chat Requests";
    }
    else if (section == 2) {
        lblTitle.text = @"Birthday";
    }
  return vwHeader;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) {
        return 0.01;
    }
    if (section == 0) {
        if (arrFollowReq.count <= 0) {
            return 0.01;
        }
    }
    if (section == 1) {
        if (arrChatReq.count <= 0) {
            return 0.01;
        }
    }
    if (section == 2) {
        if (arrBday.count <= 0) {
            return 0.01;
        }
    }
    
    if (section == 3) {
        if (arrNotifications.count <= 0) {
            return 0.01;
        }
    }
   
    return 50;
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
        
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            NSInteger nextPage = currentPage ;
            nextPage += 1;
            if (nextPage  <= totalPages) {
                isPageRefresing = YES;
                [self loadAllMyNotificationsWithPageNo:nextPage isByPagination:YES shouldDisableCach:YES];
            }
            
        }
    }
    
}

-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    NSString *CellIdentifier = @"NotificationListingCell";
    NotificationListingCell *cell = (NotificationListingCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSMutableArray *dataSource;
    if (indexPath.section == 0){
        
        dataSource = arrFollowReq;
        CellIdentifier = @"NotificationTypeReq";
        cell = (NotificationListingCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [cell.btnAccept removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btnReject removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btnAccept addTarget:self action:@selector(acceptFollowClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnReject addTarget:self action:@selector(rejectFollowClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (indexPath.section == 1) {
        
        CellIdentifier = @"NotificationTypeReq";
        cell = (NotificationListingCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        dataSource = arrChatReq;
        [cell.btnAccept removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btnReject removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btnAccept addTarget:self action:@selector(acceptChatClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnReject addTarget:self action:@selector(rejectChatClicked:) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.section == 2){
         dataSource = arrBday;
    }
    else if(indexPath.section == 3){
        dataSource = arrNotifications;
    }
    
    cell.vwBackground.backgroundColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.btnReject.tag = indexPath.row;
    cell.btnAccept.tag = indexPath.row;
   
    if (indexPath.row < dataSource.count) {
        NSDictionary *productInfo = dataSource[[indexPath row]];
        NSString *message = [productInfo objectForKey:@"message"];
        if ([[productInfo objectForKey:@"type"] isEqualToString:@"follow"]) {
            if ([[productInfo objectForKey:@"follow_status"] integerValue] == 1){
                //cell.btnReject.hidden = false;
               // cell.btnAccept.hidden = false;
            }
        }
        if ([[productInfo objectForKey:@"type"] isEqualToString:@"chatrequest"]) {
            if ([[productInfo objectForKey:@"chat_status"] integerValue] == 1){
               // cell.btnReject.hidden = false;
               // cell.btnAccept.hidden = false;
            }
        }
        if ([productInfo objectForKey:@"datetime"]) {
            cell.lblDate.text = [Utility getDaysBetweenTwoDatesWith:[[productInfo objectForKey:@"datetime"] doubleValue]];
        }
        cell.imgType.image = [UIImage imageNamed:@""];
        if (NULL_TO_NIL([productInfo objectForKey:@"icon_url"])) {
            [cell.imgType sd_setImageWithURL:[NSURL URLWithString:[productInfo objectForKey:@"icon_url"]]
                            placeholderImage:[UIImage imageNamed:@""]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   }];
        }
        
        
        
        cell.lblDescription.text = message;
        if (message.length) {
            if ([productInfo objectForKey:@"firstname"]) {
                NSString *name = [productInfo objectForKey:@"firstname"];
                NSRange range = [message rangeOfString:name];
                if (range.location == NSNotFound) {
                    //The textfield was not selected.
                } else {
                    //There is a valid range..
                    NSMutableAttributedString *txtCombined = [[NSMutableAttributedString alloc] initWithString:message];
                    [txtCombined addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold_New size:14]
                                        range:range];
                    cell.lblDescription.attributedText = txtCombined;
                }
            }
        }
        
        NSString *urlImage;
        if (NULL_TO_NIL([productInfo objectForKey:@"profileimage"]))
            urlImage = [NSString stringWithFormat:@"%@",[productInfo objectForKey:@"profileimage"]];
        [cell.indicator stopAnimating];
        if (urlImage.length) {
            [cell.indicator startAnimating];
            [cell.imgTemplate sd_setImageWithURL:[NSURL URLWithString:urlImage]
                                placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           [cell.indicator stopAnimating];
                                           
                                       }];
        }
        cell.imgTemplate.tag = indexPath.row;
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserProfilePage:)];
        [cell.imgTemplate addGestureRecognizer:gestureRecognizer];
        gestureRecognizer.cancelsTouchesInView = NO;
        cell.imgTemplate.userInteractionEnabled = true;
        cell.bottomOfSuperVW.constant = 0;
        if ((indexPath.row == dataSource.count - 1) && indexPath.section < 3) {
            cell.bottomOfSuperVW.constant = 10;
        }
        
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 3) {
        if (indexPath.row < arrNotifications.count) {
            NSDictionary *productInfo = arrNotifications[[indexPath row]];
            NSString *type = [productInfo objectForKey:@"type"];
            if ((NULL_TO_NIL([productInfo objectForKey:@"type"])) && (NULL_TO_NIL([productInfo objectForKey:@"gem_type"]))) {
                if ([type isEqualToString:@"comment"] || [type isEqualToString:@"reply"]||[type isEqualToString:@"like"]||[type isEqualToString:@"share"]||[type isEqualToString:@"commentlike"]||[type isEqualToString:@"save"] || [type isEqualToString:@"replylike"]) {
                    GemsUnderNotificationDetail *notificationDetails =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGemsUnderNotification];
                    [notificationDetails getGemDetailsWithGemID:[productInfo objectForKey:@"gem_id"] gemType:[productInfo objectForKey:@"gem_type"]];
                    [self.navigationController pushViewController:notificationDetails animated:YES];
                }
            }else if ([type isEqualToString:@"following"]){
                FollowersListingViewController *followerList =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForFollowList];
                followerList.userID = [User sharedManager].userId;
                followerList.type = 2;
                [self.navigationController pushViewController:followerList animated:YES];
                
            }else if ([type isEqualToString:@"followaccept"]){
                FollowersListingViewController *followerList =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForFollowList];
                followerList.userID = [User sharedManager].userId;
                followerList.type = 10   ;
                [self.navigationController pushViewController:followerList animated:YES];
            }
            else if ([type isEqualToString:@"chataccept"]){
                ChatUserListingViewController *chatUser =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatUserListings];
                [[self navigationController]pushViewController:chatUser animated:YES];
            }
            else if ([type isEqualToString:@"birthday_wish"]){
                
                LikedAndCommentedUserListings *userListings =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForLikedAndCommentedUsers];
                [userListings loadUserListingsForBirthdayForTheUser:[User sharedManager].userId];
                [self.navigationController pushViewController:userListings animated:YES];
            }
            
        }
    }else if (indexPath.section == 2){
        
         BdayWishingViewController *bdayScreen =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForBdayVC];
         [[self navigationController]pushViewController:bdayScreen animated:YES];
    }
}

-(IBAction)acceptFollowClicked:(UIButton*)btn{
    
    if (btn.tag < arrFollowReq.count) {
        NSDictionary *productInfo = arrFollowReq[btn.tag];
        if (productInfo && [productInfo objectForKey:@"follow_id"]){
            NSString *status = @"2";
            NSInteger follow_id =  [[productInfo objectForKey:@"follow_id"] integerValue];
            [self updateFollowStatusWithFollowID:follow_id status:status isAccepted:YES index:btn.tag];
        }

    }
    
}

-(IBAction)rejectFollowClicked:(UIButton*)btn{
    
    if (btn.tag < arrFollowReq.count) {
        NSDictionary *productInfo = arrFollowReq[btn.tag];
        if (productInfo && [productInfo objectForKey:@"follow_id"]){
            NSString *status = @"0";
            NSInteger follow_id =  [[productInfo objectForKey:@"follow_id"] integerValue];
            [self updateFollowStatusWithFollowID:follow_id status:status isAccepted:NO index:btn.tag];
        }
        
    }
}

-(IBAction)acceptChatClicked:(UIButton*)btn{
    
    if (btn.tag < arrChatReq.count) {
        NSDictionary *productInfo = arrChatReq[btn.tag];
        if (productInfo && [productInfo objectForKey:@"chatrequest_id"]){
            NSString *status = @"2";
            NSInteger follow_id =  [[productInfo objectForKey:@"chatrequest_id"] integerValue];
            [self updateChatStatusWithchatID:follow_id status:status];
        }
        
    }
    
}

-(IBAction)rejectChatClicked:(UIButton*)btn{
    
    if (btn.tag < arrChatReq.count) {
        NSDictionary *productInfo = arrChatReq[btn.tag];
        if (productInfo && [productInfo objectForKey:@"chatrequest_id"]){
            NSString *status = @"0";
            NSInteger follow_id =  [[productInfo objectForKey:@"chatrequest_id"] integerValue];
            [self updateChatStatusWithchatID:follow_id status:status];
        }
        
    }
}

-(void)updateFollowStatusWithFollowID:(NSInteger)followID status:(NSString*)status isAccepted:(BOOL)isAccepted index:(NSInteger)index{
    
    [self showLoadingScreen];
    [APIMapper updateFollowRequestByUserID:[User sharedManager].userId followRequestID:followID status:status success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        if (isAccepted) {
            if (index < arrFollowReq.count) {
                NSDictionary *details = arrFollowReq[index];
                 [self initiateAReturnFollowRequestIfNeededAt:details];
            }
        }
        [self loadAllMyNotificationsWithPageNo:currentPage isByPagination:NO shouldDisableCach:YES];
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self showErrorMessage];
         [self hideLoadingScreen];
    }];
}

-(void)initiateAReturnFollowRequestIfNeededAt:(NSDictionary*)details{
    
    if ([[details objectForKey:@"can_follow"] boolValue] && ![[details objectForKey:@"following"]boolValue]) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Follow"
                                                                       message:[NSString stringWithFormat:@"Do you want to follow %@",[details objectForKey:@"firstname"]]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Follow"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  [self followUserWithUserID:[details objectForKey:@"user_id"]];
                                                                  
                                                              }];
        [alert addAction:firstAction];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                              style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                                  
                                                              }];
        [alert addAction:cancelAction];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
   

}

-(void)updateChatStatusWithchatID:(NSInteger)chatID status:(NSString*)status{
    
    [self showLoadingScreen];
    [APIMapper updateChatRequestByUserID:[User sharedManager].userId chatReqID:chatID status:status success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        [self loadAllMyNotificationsWithPageNo:currentPage isByPagination:NO shouldDisableCach:YES];
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self showErrorMessage];
        [self hideLoadingScreen];
    }];
}

-(void)followUserWithUserID:(NSString*)strUserID{
    
    [self showLoadingScreen];
    [APIMapper sendFollowRequestWithUserID:[User sharedManager].userId followerID:strUserID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[responseObject objectForKey:@"code"] integerValue] == 401) {
            UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Follow" message:[responseObject objectForKey:@"text"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* copy = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:copy];
            [self.navigationController presentViewController:alert animated:YES completion:nil];
        }else{
            
            if ([responseObject objectForKey:@"text"]) {
                [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
            }
            NSDictionary* userInfo = @{@"userID": strUserID,@"status":[NSNumber numberWithInteger:[[responseObject objectForKey:@"follow_status"] integerValue]]};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"FollowReqFromProfile" object:self userInfo:userInfo];
           
        }
      
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        if (error.localizedDescription) {
            [ALToastView toastInView:self.view  withText:error.localizedDescription];
        }
        [self hideLoadingScreen];
    }];

}

-(IBAction)showUserProfilePage:(UITapGestureRecognizer*)gesture{
    
    NSInteger tag = gesture.view.tag;
    CGPoint touchPoint = [gesture.view convertPoint:CGPointZero toView:tableView]; // maintable --> replace your tableview name
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:touchPoint];
    
    NSMutableArray *dataSource;
    if (indexPath.section == 0) dataSource = arrFollowReq;
    if (indexPath.section == 1) dataSource = arrChatReq;
    if (indexPath.section == 2) dataSource = arrBday;
    if (indexPath.section == 3) dataSource = arrNotifications;
    
    if (tag < dataSource.count) {
        NSDictionary *details = dataSource[tag];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
            [[self navigationController]pushViewController:profilePage animated:YES];
            profilePage.canEdit = false;
            if ([[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
                profilePage.canEdit = true;
            }
            [profilePage loadUserProfileWithUserID:[details objectForKey:@"user_id"]showBackButton:YES];
            
        }
    }
}


-(void)showErrorMessage{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Follow"
                                                                   message:@"Failed to update"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                   
                                               }];
    [alert addAction:firstAction];

}

-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
