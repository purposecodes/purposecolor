//
//  AwarenessMediaViewController.h
//  PurposeColor
//
//  Created by Purpose Code on 05/04/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@protocol AwarenessMediaDelegate <NSObject>



/*!
 *This method is invoked when user Clicks "PLAY MEDIA" Button
 */
-(void)saveMedias:(BOOL)shouldSave WithCoordinates:(CLLocationCoordinate2D)coordinates locationName:(NSString*)loc mediaCount:(NSInteger)count andCotactsObj:(NSArray*)contacts ;

@end



@interface AwarenessMediaViewController : UIViewController

@property (nonatomic,weak)  id<AwarenessMediaDelegate>delegate;
-(void)resetContactsIfEditedFromPopUp:(NSMutableArray*)conatcts;

@end
