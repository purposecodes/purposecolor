//
//  AwarenessMediaViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 05/04/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import "AwarenessMediaViewController.h"
#import "ActionMediaListCell.h"
#import "PhotoBrowser.h"
#import "CustomAudioPlayerView.h"
#import <AVKit/AVKit.h>

#import "Constants.h"
#import "AudioManagerView.h"
#import "GalleryManager.h"
#import "UIView+RNActivityView.h"
#import "CustomeImagePicker.h"
#import "CustomAudioPlayerView.h"
#import <AVKit/AVKit.h>
#import "UITextView+Placeholder.h"
#import "PhotoBrowser.h"
#import "SDAVAssetExportSession.h"
#import "ContactsPickerViewController.h"
#import <GooglePlaces/GooglePlaces.h>
#import <Photos/Photos.h>

@interface AwarenessMediaViewController () <ActionInfoCellDelegate,CustomAudioPlayerDelegate,PhotoBrowserDelegate,UIGestureRecognizerDelegate,ContactPickerDelegate,CustomeImagePickerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate>{
    
    IBOutlet UITableView *tableView;
    PhotoBrowser *photoBrowser;
    CustomAudioPlayerView *vwAudioPlayer;
    CGPoint viewStartLocation;
    BOOL isDataAvailable;
    
    AudioManagerView *vwRecordPopOver;
    CLLocationManager* locationManager;
    CLLocationCoordinate2D locationCordinates;
    NSString *strLocationAddress;
    NSString *strLocationName;
    NSMutableString *strContactName;
    NSArray *arrContacts;
    IBOutlet UIButton *btnAudioRecorder;
    IBOutlet UILabel *lblLocation;
    IBOutlet UILabel *lblContact;
    IBOutlet UIButton *btnDone;
    
    IBOutlet NSLayoutConstraint *locTop;
    IBOutlet NSLayoutConstraint *contTop;
    
}

@property (nonatomic,strong) NSIndexPath *draggingCellIndexPath;
@property (nonatomic,strong) UIView *cellSnapshotView;
@property (nonatomic,strong) NSMutableArray *arrMedias;



@end

@import GooglePlacePicker;

@implementation AwarenessMediaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    locTop.constant = 0;
    contTop.constant = 0;
    
    btnDone.layer.cornerRadius = 5.f;
    btnDone.layer.borderColor = [UIColor getThemeColor].CGColor;
    btnDone.layer.borderWidth = 1.f;
    
    _arrMedias = [NSMutableArray new];
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(dragCell:)];
    [longPressGestureRecognizer setDelegate:self];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [tableView addGestureRecognizer:longPressGestureRecognizer];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(recordMediaByView:)];
    longPress.minimumPressDuration = .5;
    [btnAudioRecorder addGestureRecognizer:longPress];
    
    
    isDataAvailable = false;
    if (_arrMedias.count)  isDataAvailable = true;
        
    
    [tableView reloadData];
    // Do any additional setup after loading the view.
}

-(void)resetContactsIfEditedFromPopUp:(NSMutableArray*)contactObjs{
    
    if (contactObjs.count) {
        [self pickedContactsList:contactObjs];

    }else{
        lblContact.text = @"";
        [strContactName setString: @""];
        contTop.constant = 0;
    }
    
}

#pragma mark - Journal Media Picker Menus

-(IBAction)showCamera:(id)sender{
    
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"CAPTURE"
                                                                   message:@"Capture Media"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"VIDEO"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                              [self pickIsVideo:YES];
                                                            
                                                            [alert dismissViewControllerAnimated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                              
                                                          }];
    UIAlertAction *second = [UIAlertAction actionWithTitle:@"IMAGE"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         
                                                        [self pickIsVideo:NO];
                                                        
                                                         [alert dismissViewControllerAnimated:YES completion:^{
                                                         }];
                                                     }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"CANCEL"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    [alert addAction:firstAction];
    [alert addAction:second];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}



-(IBAction)showGallery:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"UPLOAD"
                                                                   message:@"Upload Media"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"VIDEO"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                               [self showPhotoGallery:NO];
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                              
                                                          }];
    UIAlertAction *second = [UIAlertAction actionWithTitle:@"IMAGE"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         
                                                         [self showPhotoGallery:YES];
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"CANCEL"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    [alert addAction:firstAction];
    [alert addAction:second];
     [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(IBAction)showLocation:(id)sender{
    
    [self getNearBYLocations];
    [self.view endEditing:YES];
}

-(IBAction)showContacts:(id)sender{
    
    [self getAllContacts];
    [self.view endEditing:YES];
}

-(IBAction)recordAudio:(UIButton*)sender{
    
    [self showToast];
    
}

#pragma mark - Journal Video and Image Picker

-(void)pickIsVideo:(BOOL)isVideo{
    
    UIImagePickerController *picker= [[UIImagePickerController alloc] init];
    [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [picker setDelegate:self];
    picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
    if (isVideo) {
         picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie,nil];
    }
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    [self.view showActivityView];
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        
        [self compressVideoWithURL:[info objectForKey:UIImagePickerControllerMediaURL] onComplete:^(bool completed) {
            if (completed) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view hideActivityView];
                    [self reloadMediaLibraryTable];
                });
            }
        }];
    }else{
        [self.view hideActivityView];
        UIImage *image =[Utility fixrotation:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
        [GalleryManager saveJournalImageFileToFolderWithImage:image withCompresion:0.5f];
        [self reloadMediaLibraryTable];
        
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}

-(void)compressVideoWithURL:(NSURL*)videoURL onComplete:(void (^)(bool completed))completed{
    
    NSURL *outputURL = [NSURL fileURLWithPath:[GalleryManager getPathWhereJournalVideoNeedsToBeSaved]];
    SDAVAssetExportSession *encoder = [SDAVAssetExportSession.alloc initWithAsset:[AVAsset assetWithURL:videoURL]];
    NSURL *url = outputURL;
    encoder.outputURL=url;
    encoder.outputFileType = AVFileTypeMPEG4;
    encoder.shouldOptimizeForNetworkUse = YES;
    encoder.videoSettings = @
    {
    AVVideoCodecKey: AVVideoCodecH264,
    AVVideoWidthKey:[NSNumber numberWithInteger:360], // required
    AVVideoHeightKey:[NSNumber numberWithInteger:480], // required
    AVVideoCompressionPropertiesKey: @
        {
        AVVideoAverageBitRateKey: @500000, // Lower bit rate here
        AVVideoProfileLevelKey: AVVideoProfileLevelH264High40,
        },
    };
    
    encoder.audioSettings = @
    {
    AVFormatIDKey: @(kAudioFormatMPEG4AAC),
    AVNumberOfChannelsKey: @2,
    AVSampleRateKey: @44100,
    AVEncoderBitRateKey: @128000,
    };
    
    [encoder exportAsynchronouslyWithCompletionHandler:^
     {
         int status = encoder.status;
         if (status == AVAssetExportSessionStatusCompleted)
         {
             
             
         }
         else if (status == AVAssetExportSessionStatusCancelled)
         {
             NSLog(@"Video export cancelled");
         }
         else
         {
         }
         
         completed(YES);
     }];
}

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(AVAssetExportSession*))handler
{
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetLowQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         handler(exportSession);
     }];
}



#pragma mark - Record Audio actions

-(void)showToast{
    
    [self.view endEditing:YES];
    [ALToastView toastInView:self.view withText:@"Hold to record , Release to save."];
}

-(void)recordMediaByView:(UILongPressGestureRecognizer*)gesture{
    
    if (gesture.view) {
        
        if (gesture.state == UIGestureRecognizerStateBegan) {
            
            if (!vwRecordPopOver) {
                
                float xPadding = 10;
                float yPadding = 70;
                
                CGPoint point = [gesture.view.superview convertPoint:gesture.view.center toView:self.view];
                CGRect rect = CGRectMake(point.x - xPadding, point.y - yPadding, 100, 40);
                vwRecordPopOver = [AudioManagerView new];
                vwRecordPopOver.isJournal = true;
                vwRecordPopOver.frame = rect;
                [self.view addSubview:vwRecordPopOver];
                [vwRecordPopOver setUp];
                [vwRecordPopOver startRecording];
                
            }
        }
        
        if ((gesture.state == UIGestureRecognizerStateCancelled) || (gesture.state == UIGestureRecognizerStateFailed) || (gesture.state ==UIGestureRecognizerStateEnded)) {
            
            [vwRecordPopOver stopRecording];
            [vwRecordPopOver removeFromSuperview];
            vwRecordPopOver = nil;
            [self reloadMediaLibraryTable];
            
        }
        
    }
    
}

#pragma mark - Get All Contact Details

- (void)getAllContacts {
    
    ContactsPickerViewController *picker =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForContactsPickerVC];
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:NULL];
    
}


-(void)pickedContactsList:(NSMutableArray*)lists{
    
    arrContacts = [NSArray arrayWithArray:lists];
    NSMutableArray *names = [NSMutableArray new];
    for (NSDictionary *dict in lists) {
        if ([[dict objectForKey:@"isSelected"] boolValue]) {
            if ([dict objectForKey:@"name"]) {
                [names addObject:[dict objectForKey:@"name"]];
            }
        }
    }
    if (names.count) strContactName = [NSMutableString stringWithString:[names componentsJoinedByString:@","]];
    
    if (strContactName.length) {
        contTop.constant = 5;
        
        NSMutableAttributedString *myString= [NSMutableAttributedString new];
        NSInteger nextIndex = 0;
        NSArray *names = [strContactName componentsSeparatedByString:@","];
        NSInteger nextLength = strContactName.length;
        NSString *strCntact = strContactName;
        if (names.count > 1) {
            NSString *firstName = [names firstObject];
            nextLength = firstName.length;
            strCntact = [NSString stringWithFormat:@"With %@ and %lu other(s)",firstName,names.count - 1];
        }else{
            strCntact = [NSString stringWithFormat:@"With %@",strCntact];
        }
        NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",strCntact]];
        [myString appendAttributedString:mutableAttString];
        [myString addAttribute:NSForegroundColorAttributeName
                         value:[UIColor getThemeColor]
                         range:NSMakeRange(nextIndex + 5, nextLength)];
        lblContact.attributedText = myString;
    }
    
    
}

-(IBAction)clearContacts:(id)sender{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete"
                                  message:@"Do you want to delete Contacts?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"DELETE"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             lblContact.text = @"";
                             [strContactName setString: @""];
                             contTop.constant = 0;
                             arrContacts = nil;
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
   
}

#pragma mark - Location Picker

-(IBAction)getNearBYLocations{
    
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    acController.autocompleteFilter.type = kGMSPlacesAutocompleteTypeFilterCity;
    [self presentViewController:acController animated:YES completion:nil];
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    strLocationName = place.name;
    strLocationAddress = place.formattedAddress;
    locationCordinates = place.coordinate;
   
    NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"At %@",strLocationName]];
    [mutableAttString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor getThemeColor]
                             range:NSMakeRange(3, strLocationName.length)];
    lblLocation.attributedText = mutableAttString;
    locTop.constant = 5;

    
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(IBAction)clearLocations:(id)sender{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete"
                                  message:@"Do you want to delete Location?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"DELETE"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             lblLocation.text = @"";
                             strLocationName = @"";
                             strLocationAddress = @"";
                             locTop.constant = 0;
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
   
}

#pragma mark - Photo Gallery

-(IBAction)showPhotoGallery:(BOOL)isPhoto
{
    CustomeImagePicker *cip =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:StoryboardForLogin Identifier:StoryBoardIdentifierForImagePicker];
    
    cip.delegate = self;
    cip.isPhotos = isPhoto;
    [cip setHideSkipButton:NO];
    [cip setHideNextButton:NO];
    [cip setMaxPhotos:MAX_ALLOWED_PICK];
    
    [self presentViewController:cip animated:YES completion:^{
    }];
}

-(void)imageSelected:(NSArray*)arrayOfGallery isPhoto:(BOOL)isPhoto;
{
    float __block width = 300;
    [self.view showActivityView];
    width = self.view.frame.size.width;
    PHImageRequestOptions *requestOptions  = [[PHImageRequestOptions alloc] init];
    requestOptions.synchronous = YES;
    requestOptions.resizeMode = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    requestOptions.networkAccessAllowed = YES;
    
    __block NSInteger imageCount = 0;
    __block NSInteger videoCount = 0;
    
    for (PHAsset *asset in arrayOfGallery) {
        
        if (isPhoto) {
            
            PHImageManager *manager = [PHImageManager defaultManager];
            [manager requestImageForAsset:asset
                               targetSize:CGSizeMake(600, 600)
                              contentMode:PHImageContentModeDefault
                                  options:requestOptions
                            resultHandler:^void(UIImage *image, NSDictionary *info) {
                               [GalleryManager saveJournalImageFileToFolderWithImage:image withCompresion:1.0f];
                                imageCount ++;
                                if (imageCount == arrayOfGallery.count) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self performSelector:@selector(reloadMediaLibraryTable) withObject:self afterDelay:1];
                                    });
                                }
                                
                            }];
            
        }else{
            PHVideoRequestOptions *options = [PHVideoRequestOptions new];
            options.networkAccessAllowed = YES;
            options.deliveryMode = PHVideoRequestOptionsDeliveryModeMediumQualityFormat;
            
            [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset *_asset, AVAudioMix *audioMix, NSDictionary *info) {
                if ([_asset isKindOfClass:[AVURLAsset class]]) {
                    NSURL *URL = [(AVURLAsset *)_asset URL];
                    [self compressVideoWithURL:URL onComplete:^(bool completed) {
                        if (completed) {
                            videoCount ++;
                            if (imageCount + videoCount == arrayOfGallery.count) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self performSelector:@selector(reloadMediaLibraryTable) withObject:self afterDelay:1];
                                });
                            }
                        }
                    }];
                }
            }];
        }
    }

    
    
    /*
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view showActivityView];
        }); // Main Queue to Display the Activity View
        __block NSInteger imageCount = 0;
        __block NSInteger videoCount = 0;
        
        for(NSString *imageURLString in arrayOfGallery)
        {
            // Asset URLs
            ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
            [assetsLibrary assetForURL:[NSURL URLWithString:imageURLString] resultBlock:^(ALAsset *asset) {
                ALAssetRepresentation *representation = [asset defaultRepresentation];
                if (isPhoto) {
                    
                    // IMAGE
                    
                    CGImageRef imageRef = [representation fullScreenImage];
                    UIImage *image = [UIImage imageWithCGImage:imageRef];
                    if (imageRef) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [GalleryManager saveJournalImageFileToFolderWithImage:image];
                        });
                        imageCount ++;
                        
                        if (imageCount + videoCount == arrayOfGallery.count) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self performSelector:@selector(reloadMediaLibraryTable) withObject:self afterDelay:1];
                            });
                        }
                    }
                }else{
                    
                    //VIDEO
                    
                    [self compressVideoWithURL:[NSURL URLWithString:imageURLString] onComplete:^(bool completed) {
                        
                        if (completed) {
                            
                            videoCount ++;
                            
                            if (imageCount + videoCount == arrayOfGallery.count) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self performSelector:@selector(reloadMediaLibraryTable) withObject:self afterDelay:1];
                                });
                            }
                        }
                    }];
                    
                    
                }
                
                
                // Valid Image URL
            } failureBlock:^(NSError *error) {
            }];
        } // All Images I got
        
        
    });
    */
    
}
-(void) imageSelectionCancelled
{
    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView {
    
    
    return 1;
}


-(NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return 1;
    
   return  _arrMedias.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MediaList";
    ActionMediaListCell *cell = (ActionMediaListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [[cell btnVideoPlay]setHidden:true];
    [[cell btnAudioPlay]setHidden:true];
    cell.delegate = self;
    [cell.indicator stopAnimating];
    [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];
    
    if (!isDataAvailable) {
        
        static NSString *MyIdentifier = @"MyIdentifier";
        UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No Medias Available."];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.userInteractionEnabled = false;
        cell.textLabel.textColor = [UIColor colorWithRed:0.30 green:0.33 blue:0.38 alpha:1.0];
        return cell;
    }

    if (indexPath.row < _arrMedias.count) {
        id object = _arrMedias[indexPath.row];
        NSString *strFile;
        if ([object isKindOfClass:[NSString class]]) {
            
            //When Create
            
            strFile = (NSString*)_arrMedias[indexPath.row];
            cell.lblTitle.text = strFile;
            cell.imgMediaThumbnail.backgroundColor = [UIColor blackColor];
            NSString *fileName = _arrMedias[indexPath.row];
            if ([[fileName pathExtension] isEqualToString:@"jpeg"]) {
                //This is Image File with .png Extension , Photos.
                NSString *filePath = [Utility getJournalMediaSaveFolderPath];
                NSString *imagePath = [[filePath stringByAppendingString:@"/"] stringByAppendingString:fileName];
                if (imagePath.length) {
                    [cell.indicator startAnimating];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                        NSData *data = [[NSFileManager defaultManager] contentsAtPath:imagePath];
                        UIImage *image = [UIImage imageWithData:data];
                        dispatch_sync(dispatch_get_main_queue(), ^(void) {
                            //cell.imgMediaThumbnail.image = image;
                            [cell.indicator stopAnimating];
                            [UIView transitionWithView:cell.imgMediaThumbnail
                                              duration:0.4f
                                               options:UIViewAnimationOptionTransitionCrossDissolve
                                            animations:^{
                                                cell.imgMediaThumbnail.image = image;
                                            } completion:nil];
                        });
                    });
                    
                }
            }
            else if ([[fileName pathExtension] isEqualToString:@"mp4"]) {
                //This is Image File with .mp4 Extension , Video Files
                NSString *filePath = [Utility getJournalMediaSaveFolderPath];
                NSString *imagePath = [[filePath stringByAppendingString:@"/"] stringByAppendingString:fileName];
                [[cell btnVideoPlay]setHidden:false];
                if (imagePath.length){
                    [cell.indicator startAnimating];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                        UIImage *thumbnail = [Utility getThumbNailFromVideoURL:imagePath];
                        dispatch_sync(dispatch_get_main_queue(), ^(void) {
                            cell.imgMediaThumbnail.image = thumbnail;
                            [cell.indicator stopAnimating];
                        });
                    });
                    cell.imgMediaThumbnail.image = [UIImage imageNamed:@"Video_Play_Button.png"];
                }
            }
            else if ([[fileName pathExtension] isEqualToString:@"aac"]){
                // Recorded Audio
                [cell.indicator startAnimating];
                [[cell btnAudioPlay]setHidden:false];
                cell.imgMediaThumbnail.image = [UIImage imageNamed:@"NoImage_Goals_Dreams"];
                [cell.indicator stopAnimating];
            }
            
        }
        
        
    }
    
    cell.btnDelete.tag = indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(deleteSelectedMedia:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 250;
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray *images = [NSMutableArray new];
    if (indexPath.row < _arrMedias.count) {
        NSString *strURL = (NSString*)_arrMedias[indexPath.row];
        if ([strURL hasPrefix:@"PurposeColorImage"]) {
            if (![images containsObject:strURL]) {
               [images addObject:strURL];
            }
        }
    }
    
    if (images.count) {
        for (id details in _arrMedias) {
            NSString *strURL = (NSString*)details;
            if ([strURL hasPrefix:@"PurposeColorImage"]) {
                if (![images containsObject:strURL]) {
                    [images addObject:strURL];
                }
            }
        }
    }
    
    if (images.count) {
        [self presentGalleryWithImages:images];
    }

    
}

#pragma mark - Media Drag and Drop Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] || [gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
        return YES;
    }
    if ([touch.view isDescendantOfView:tableView])
        return NO;
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}



-(void)dragCell:(UILongPressGestureRecognizer *)panner
{
    if(panner.state == UIGestureRecognizerStateBegan)
    {
        viewStartLocation = [panner locationInView:tableView];
        tableView.scrollEnabled = NO;
        
        //if needed do some initial setup or init of views here
    }
    else if(panner.state == UIGestureRecognizerStateChanged)
    {
        //move your views here.
        if (! self.cellSnapshotView) {
            CGPoint loc = [panner locationInView:tableView];
            self.draggingCellIndexPath = [tableView indexPathForRowAtPoint:loc];
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:self.draggingCellIndexPath];
            if (cell){
                
                UIGraphicsBeginImageContextWithOptions(cell.bounds.size, NO, 0);
                [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
                UIImage *cellImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                // create and image view that we will drag around the screen
                self.cellSnapshotView = [[UIImageView alloc] initWithImage:cellImage];
                self.cellSnapshotView.alpha = 0.8;
                self.cellSnapshotView.layer.borderColor = [UIColor redColor].CGColor;
                self.cellSnapshotView.layer.borderWidth = 1;
                self.cellSnapshotView.frame =  cell.frame;
                [tableView addSubview:self.cellSnapshotView];
                
                //[tableView reloadRowsAtIndexPaths:@[self.draggingCellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            // replace the cell with a blank one until the drag is over
        }
        
        CGPoint location = [panner locationInView:tableView];
        CGPoint translation;
        translation.x = location.x - viewStartLocation.x;
        translation.y = location.y - viewStartLocation.y;
        //  NSIndexPath *current =  [tableView indexPathForRowAtPoint:location];
        //        if (current.row  < arrDataSource.count) {
        //            NSIndexPath *next =  [NSIndexPath indexPathForRow:current.row inSection:eSectionTwo];
        //            [tableView scrollToRowAtIndexPath:next atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        //        }
        
        CGPoint cvCenter = self.cellSnapshotView.center;
        cvCenter.x = location.x;
        cvCenter.y = location.y;
        self.cellSnapshotView.center = cvCenter;
        
        
        
    }
    else if(panner.state == UIGestureRecognizerStateEnded || (panner.state == UIGestureRecognizerStateCancelled) || (panner.state == UIGestureRecognizerStateFailed))
    {
        tableView.scrollEnabled = YES;
        UITableViewCell *droppedOnCell;
        CGRect largestRect = CGRectZero;
        for (UITableViewCell *cell in tableView.visibleCells) {
            CGRect intersection = CGRectIntersection(cell.frame, self.cellSnapshotView.frame);
            if (intersection.size.width * intersection.size.height >= largestRect.size.width * largestRect.size.height) {
                largestRect = intersection;
                droppedOnCell =  cell;
            }
        }
        
        NSIndexPath *droppedOnCellIndexPath = [tableView indexPathForCell:droppedOnCell];
        [UIView animateWithDuration:.2 animations:^{
            self.cellSnapshotView.center = droppedOnCell.center;
        } completion:^(BOOL finished) {
            [self.cellSnapshotView removeFromSuperview];
            self.cellSnapshotView = nil;
            NSIndexPath *savedDraggingCellIndexPath = self.draggingCellIndexPath;
            if (![self.draggingCellIndexPath isEqual:droppedOnCellIndexPath]) {
                //self.draggingCellIndexPath = [NSIndexPath indexPathForRow:-1 inSection:1];
                [_arrMedias exchangeObjectAtIndex:savedDraggingCellIndexPath.row withObjectAtIndex:droppedOnCellIndexPath.row];
                [tableView reloadRowsAtIndexPaths:@[savedDraggingCellIndexPath, droppedOnCellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            }else{
                // self.draggingCellIndexPath = [NSIndexPath indexPathForRow:-1 inSection:1];
                [tableView reloadRowsAtIndexPaths:@[savedDraggingCellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
        }];
        
    }
}


-(void)deleteSelectedMedia:(UIButton*)btnDelete{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete"
                                  message:@"Delete the selected media ?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             if (btnDelete.tag < _arrMedias.count) {
                                 id object = _arrMedias[btnDelete.tag];
                                 if ([object isKindOfClass:[NSString class]]) {
                                     // Created medias
                                     NSString *fileName = _arrMedias[btnDelete.tag];
                                     [self removeAMediaFileWithName:fileName];
                                 }else{
                                     //Editing medias
                                    
                                     
                                 }
                                 
                             }
                             
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)removeAMediaFileWithName:(NSString *)filename
{
    NSString *dataPath = [Utility getJournalMediaSaveFolderPath];
    NSString *filePath = [dataPath stringByAppendingPathComponent:filename];
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success){
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    [self reloadMediaLibraryTable];
}

-(void)reloadMediaLibraryTable{
    
    NSMutableArray *mediaForEdit = [NSMutableArray new];
    for (id object in _arrMedias) {
        if ([object isKindOfClass:[NSDictionary class]]) {
            [mediaForEdit addObject:object];
        }
        
    }
    [_arrMedias removeAllObjects];
    for (NSDictionary *dict in mediaForEdit) {
        [_arrMedias addObject:dict];
    }
    [self getAllMediaFiles];
    [tableView reloadData];
    [self.view hideActivityView];
    
    
    
}
-(void)getAllMediaFiles{
    
    NSString *dataPath = [Utility getJournalMediaSaveFolderPath];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    NSError* error = nil;
    // sort by creation date
    NSMutableArray* filesAndProperties = [NSMutableArray arrayWithCapacity:[directoryContent count]];
    for(NSString* file in directoryContent) {
        
        if (![file isEqualToString:@".DS_Store"]) {
            NSString* filePath = [dataPath stringByAppendingPathComponent:file];
            NSDictionary* properties = [[NSFileManager defaultManager]
                                        attributesOfItemAtPath:filePath
                                        error:&error];
            NSDate* modDate = [properties objectForKey:NSFileCreationDate];
            
            [filesAndProperties addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                           file, @"path",
                                           modDate, @"lastModDate",
                                           nil]];
            
        }
    }
    // sort using a block
    // order inverted as we want latest date first
    NSArray* sortedFiles = [filesAndProperties sortedArrayUsingComparator:
                            ^(id path1, id path2)
                            {
                                // compare
                                NSComparisonResult comp = [[path1 objectForKey:@"lastModDate"] compare:
                                                           [path2 objectForKey:@"lastModDate"]];
                                // invert ordering
                                if (comp == NSOrderedDescending) {
                                    comp = NSOrderedAscending;
                                }
                                else if(comp == NSOrderedAscending){
                                    comp = NSOrderedDescending;
                                }
                                return comp;
                            }];
    
    for (NSInteger i = sortedFiles.count - 1; i >= 0; i --) {
        NSDictionary *dict = sortedFiles[i];
        [_arrMedias insertObject:[dict objectForKey:@"path" ] atIndex:0];
    }
    isDataAvailable = false;
    if (_arrMedias.count) isDataAvailable = true;
        
    
    
}


-(void)removeAllContentsInMediaFolder{
    
    NSString *dataPath = [Utility getJournalMediaSaveFolderPath];
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager removeItemAtPath:dataPath error:&error];
    if (success){
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
}


#pragma mark - Photo Browser & Deleagtes

- (void)presentGalleryWithImages:(NSArray*)images
{
    [self.view endEditing:YES];
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    photoBrowser.isFromAwareness = true;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}

-(void)playSelectedMediaWithIndex:(NSInteger)tag {
    
    if (tag < _arrMedias.count){
        
        // When Create
        
        id object = _arrMedias[tag];
        if ([object isKindOfClass:[NSString class]]) {
            NSString *fileName = _arrMedias[tag];
            if ([[fileName pathExtension] isEqualToString:@"jpeg"]) {
                //This is Image File with .png Extension , Photos.
                //NSString *filePath = [Utility getMediaSaveFolderPath];
                
            }
            else if ([[fileName pathExtension] isEqualToString:@"mp4"]) {
                //This is Image File with .mp4 Extension , Video Files
                NSError* error;
                [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
                [[AVAudioSession sharedInstance] setActive:NO error:&error];
                NSString *filePath = [Utility getJournalMediaSaveFolderPath];
                NSString *path = [[filePath stringByAppendingString:@"/"] stringByAppendingString:fileName];
                NSURL  *videourl =[NSURL fileURLWithPath:path];
                AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
                playerViewController.player = [AVPlayer playerWithURL:videourl];
                [playerViewController.player play];
                [self presentViewController:playerViewController animated:YES completion:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(videoDidFinish:)
                                                             name:AVPlayerItemDidPlayToEndTimeNotification
                                                           object:[playerViewController.player currentItem]];
                
            }
            else if ([[fileName pathExtension] isEqualToString:@"aac"]){
                // Recorded Audio
                
                NSString *filePath = [Utility getJournalMediaSaveFolderPath];
                NSString *path = [[filePath stringByAppendingString:@"/"] stringByAppendingString:fileName];
                NSURL  *audioURL = [NSURL fileURLWithPath:path];
                [self showAudioPlayerWithURL:audioURL];
                
            }
            
        }
        
    }
    
}

- (void)videoDidFinish:(id)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //fade out / remove subview
}
-(void)showAudioPlayerWithURL:(NSURL*)url{
    
    if (!vwAudioPlayer) {
        vwAudioPlayer = [[[NSBundle mainBundle] loadNibNamed:@"CustomAudioPlayerView" owner:self options:nil] objectAtIndex:0];
        vwAudioPlayer.delegate = self;
    }
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = vwAudioPlayer;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer setupAVPlayerForURL:url];
    }];
    
}

-(void)closeAudioPlayerView{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwAudioPlayer.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer removeFromSuperview];
        vwAudioPlayer = nil;
        NSError* error;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        [[AVAudioSession sharedInstance] setActive:NO error:&error];
    }];
    
}

-(IBAction)doneApplied:(id)sender{
    
    if (_arrMedias.count || strContactName.length || strLocationName.length){
        
        if ([self.delegate respondsToSelector:@selector(saveMedias:WithCoordinates:locationName:mediaCount:andCotactsObj:)]) {
            [self.delegate saveMedias:YES WithCoordinates:locationCordinates locationName:strLocationName mediaCount:_arrMedias.count andCotactsObj:arrContacts];
        }
    }else{
        
        if ([self.delegate respondsToSelector:@selector(saveMedias:WithCoordinates:locationName:mediaCount:andCotactsObj:)]) {
            [self.delegate saveMedias:NO WithCoordinates:locationCordinates locationName:strLocationName mediaCount:_arrMedias.count andCotactsObj:arrContacts];
        }
    }
        
    
}

-(IBAction)goBack:(id)sender{
    
    
    if (_arrMedias.count || strContactName.length || strLocationName.length) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Media"
                                                                       message:@"Save this media?"
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Save"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  if ([self.delegate respondsToSelector:@selector(saveMedias:WithCoordinates:locationName:mediaCount:andCotactsObj:)]) {
                                                                      [self.delegate saveMedias:YES WithCoordinates:locationCordinates locationName:strLocationName mediaCount:_arrMedias.count andCotactsObj:arrContacts];
                                                                  }
                                                                  
                                                                  [alert dismissViewControllerAnimated:YES completion:^{
                                                                      
                                                                  }];
                                                              }];
        
        
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Discard"
                                                         style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                             if ([self.delegate respondsToSelector:@selector(saveMedias:WithCoordinates:locationName:mediaCount: andCotactsObj:)]) {
                                                                 [self.delegate saveMedias:NO WithCoordinates:locationCordinates locationName:strLocationName mediaCount:_arrMedias.count andCotactsObj:arrContacts];
                                                             }
                                                              [self removeAllContentsInMediaFolder];
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:firstAction];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        
        [self removeAllContentsInMediaFolder];
        if ([self.delegate respondsToSelector:@selector(saveMedias:WithCoordinates:locationName:mediaCount: andCotactsObj:)]) {
            [self.delegate saveMedias:NO WithCoordinates:locationCordinates locationName:strLocationName mediaCount:_arrMedias.count andCotactsObj:arrContacts];
        }
    }
    
    
    
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
