//
//  SelectYourFeel.m
//  PurposeColor
//
//  Created by Purpose Code on 16/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "SelectYourDrive.h"
#import "Constants.h"
#import "EDStarRating.h"

@interface SelectYourDrive () <EDStarRatingProtocol>{
    
    IBOutlet EDStarRating *rateStar;
    NSInteger rateValue;
}



@end

@implementation SelectYourDrive

-(void)configureDriveRating{
    
    rateStar.backgroundColor  = [UIColor clearColor];
    rateStar.starImage = [UIImage imageNamed:@"star-template"];
    rateStar.starHighlightedImage = [UIImage imageNamed:@"star-highlighted-template"];
    rateStar.maxRating = 5.0;
    rateStar.delegate = self;
    rateStar.horizontalMargin = 15.0;
    rateStar.editable=YES;
    rateStar.rating = 0;
    rateStar.displayMode=EDStarRatingDisplayFull;
    [rateStar  setNeedsDisplay];
}

-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating{
    
    rateValue = rating;
    [self closePopUp];
}

-(IBAction)closePopUp{
    
    if ([_delegate respondsToSelector:@selector(driveSelectedWithEmotionType:)]) {
        [_delegate driveSelectedWithEmotionType:rateValue];
    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
