//
//  SelectYourFeel.m
//  PurposeColor
//
//  Created by Purpose Code on 16/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kTagForTextField                1
#define kCellMinimumCount               1
#define kSectionCount                   1
#define kPadding                        60
#define kHeightForFooter                0.1
#define kSuccessCode                    200

#import "SelectedContacts.h"
#import "Constants.h"
#import "ContactsCell.h"

@interface SelectedContacts ()<UIGestureRecognizerDelegate>{
    
    IBOutlet UIImageView *imgArrow;
    NSMutableArray *arrDataSource;
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnClose;
    IBOutlet UIView *vwBG;
    BOOL isDataAvailable;
    NSInteger selectedIndex;
    IBOutlet NSLayoutConstraint *heightForContainer;
    BOOL isEdited;
    
}

@end

@implementation SelectedContacts


-(void)showContactNameWith:(NSArray*)contacts{

    [vwBG setAlpha:0.0f];

    btnClose.layer.cornerRadius = 5;
    btnClose.layer.borderWidth = 1.f;
    btnClose.layer.borderColor = [UIColor clearColor].CGColor;
    
    vwBG.layer.cornerRadius = 5;
    vwBG.layer.borderWidth = 1.f;
    vwBG.layer.borderColor = [UIColor clearColor].CGColor;
    isDataAvailable = true;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopUp)];
    [tapGesture setNumberOfTapsRequired:1];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
    
    
    
    arrDataSource = [NSMutableArray arrayWithArray:contacts];
    float height = arrDataSource.count * 60;
    float padding = 150;
    float screenHeight = self.frame.size.height - padding;
    if (height < screenHeight) {
        heightForContainer.constant = height;
    }else{
        heightForContainer.constant = screenHeight;
    }
    [tableView reloadData];
    
    [UIView animateWithDuration:0.5f animations:^{
        [vwBG setAlpha:1.0f];
    } completion:^(BOOL finished) {
    }];
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:tableView])
        return NO;
    return YES;
}

-(IBAction)deleteAContact:(UIButton*)sender{
    
    if (sender.tag < arrDataSource.count) {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Delete"
                                      message:@"Do you really want to delete?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"DELETE"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [self deleteContactAtIndex:sender.tag];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"CANCEL"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)deleteContactAtIndex:(NSInteger)index{
    
    [arrDataSource removeObjectAtIndex:index];
    [tableView reloadData];
    isEdited = true;
    
    float height = arrDataSource.count * 60;
    float padding = 150;
    float screenHeight = self.frame.size.height - padding;
    [self layoutIfNeeded];

    if (height < screenHeight) {
        heightForContainer.constant = height;
    }else{
        heightForContainer.constant = screenHeight;
    }
    [UIView animateWithDuration:.5
                     animations:^{
                         [self layoutIfNeeded];
                         // Called on parent view
                     }completion:^(BOOL finished) {
                     }];
    
    if (arrDataSource.count <= 0) {
        [self closePopUp];
    }

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ContactsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContactsCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.userInteractionEnabled = true;
    cell.btnDelete.tag = indexPath.row;
    
    if (indexPath.row < arrDataSource.count) {
        
        NSDictionary *details = arrDataSource[indexPath.row];
        if (NULL_TO_NIL([details objectForKey:@"name"])) {
            cell.lblName.text = [details objectForKey:@"name"];
            cell.lblNameIcon.text = [[[details objectForKey:@"name"] substringToIndex:1] uppercaseString];
        }
        if (NULL_TO_NIL([details objectForKey:@"Phone"])) {
            cell.lblPhoneNumber.text = [details objectForKey:@"Phone"];
        }
        if (NULL_TO_NIL([details objectForKey:@"contact_image"])) {
            cell.lblNameIcon.hidden = true;
            cell.imgUser.image = (UIImage*) [details objectForKey:@"contact_image"];
        }
        
    }else{
        
        UITableViewCell *cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No contacts found!."];
        return cell;
    }
    return cell;
       
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 60;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return  kHeightForFooter;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return nil;
}

-(IBAction)closePopUp{
    
    [UIView animateWithDuration:0.5f animations:^{
        [vwBG setAlpha:0.0f];
    } completion:^(BOOL finished) {
        if ([self.delegate respondsToSelector:@selector(selectContactsListPopUpCloseAppplied)]) {
            [self.delegate selectContactsListPopUpCloseAppplied];
        }
        if (isEdited) {
            if ([self.delegate respondsToSelector:@selector(showEditedContacts:)]) {
                [self.delegate showEditedContacts:arrDataSource];
            }
            
        }
        

    }];
    

    
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
