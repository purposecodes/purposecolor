//
//  SelectYourFeel.h
//  PurposeColor
//
//  Created by Purpose Code on 16/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SelectedConactsDelegate <NSObject>


@optional


/*!
 *This method is invoked when user Clicks "CLOSE" Button
 */
-(void)selectContactsListPopUpCloseAppplied;

/*!
 *This method is invoked when user Clicks "EMOTION" Button
 */
-(void)showEditedContacts:(NSMutableArray*)conatcts;


@end

@interface SelectedContacts : UIView <UIGestureRecognizerDelegate>

@property (nonatomic,weak)  id<SelectedConactsDelegate>delegate;
@property (nonatomic,assign)  NSInteger goalID;
@property (nonatomic,assign)  NSDictionary *selectedActions;

-(void)showContactNameWith:(NSArray*)contacts;

@end
