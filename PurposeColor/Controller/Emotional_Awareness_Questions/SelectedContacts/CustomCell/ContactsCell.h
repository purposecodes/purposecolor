//
//  GoalsCustomCell.h
//  PurposeColor
//
//  Created by Purpose Code on 18/10/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *imgUser;
@property (nonatomic,weak) IBOutlet UILabel *lblName;
@property (nonatomic,weak) IBOutlet UILabel *lblPhoneNumber;
@property (nonatomic,weak) IBOutlet UILabel *lblNameIcon;
@property (nonatomic,weak) IBOutlet UIButton *btnDelete;


@end
