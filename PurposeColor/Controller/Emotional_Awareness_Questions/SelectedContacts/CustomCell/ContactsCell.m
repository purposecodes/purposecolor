//
//  GoalsCustomCell.m
//  PurposeColor
//
//  Created by Purpose Code on 18/10/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "ContactsCell.h"

@implementation ContactsCell

- (void)awakeFromNib {
    
    _imgUser.layer.borderColor = [UIColor colorWithRed:0.83 green:0.83 blue:0.83 alpha:1.0].CGColor;
    _imgUser.layer.borderWidth = 0.f;
    _imgUser.layer.cornerRadius = 20.f;
    _imgUser.clipsToBounds = YES;
    // Initialization code
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
