//
//  UserListTableViewCell.m
//  PurposeColor
//
//  Created by Purpose Code on 14/06/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import "UserListTableViewCell.h"

@implementation UserListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imgProfile.layer.borderColor = [UIColor colorWithRed:0.83 green:0.83 blue:0.83 alpha:1.0].CGColor;
    _imgProfile.layer.borderWidth = 1.f;
    _imgProfile.layer.cornerRadius = 25.f;
    
    _btnFollow.layer.borderWidth = 1.f;
    _btnFollow.layer.borderColor = [UIColor getThemeColor].CGColor;
    _btnFollow.layer.cornerRadius = 5.f;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
