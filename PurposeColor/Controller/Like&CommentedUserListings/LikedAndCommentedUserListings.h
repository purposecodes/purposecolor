//
//  MenuViewController.h
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface LikedAndCommentedUserListings : UIViewController

@property (nonatomic,assign) BOOL isBirthday;
-(void)loadUserListingsForType:(NSString*)type selectedID:(NSString*)gemID gemType:(NSString*)gemType;
-(void)loadUserListingsForBirthdayForTheUser:(NSString*)userID;

@end
