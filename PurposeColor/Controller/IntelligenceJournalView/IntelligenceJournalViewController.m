//
//  IntelligenceJournalViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 02/02/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "IntelligenceJournalViewController.h"
#import "EmotionalIntelligenceViewController.h"
#import "Constants.h"
#import "JournalListViewController.h"

@interface IntelligenceJournalViewController (){
    
    IBOutlet UIView *vwContainer;
    IBOutlet UISegmentedControl *segmentControl;
    UINavigationController *navController;
}

@end

@implementation IntelligenceJournalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self showJournalListView];
    // Do any additional setup after loading the view.
}

-(void)setUp{
 
    [segmentControl setSelectedSegmentIndex:0];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:CommonFont size:15], NSFontAttributeName,
                                [UIColor getThemeColor], NSForegroundColorAttributeName, nil];
    [segmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                  [UIFont fontWithName:CommonFont size:15], NSFontAttributeName,
                  [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [segmentControl setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
}

-(void)showIntelligenceView{
    
    EmotionalIntelligenceViewController *emotionalIntelligence =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForEmotionalIntelligence];
    [navController pushViewController:emotionalIntelligence animated:YES];
}

-(void)showJournalListView{
    
    // Journal list
    
    JournalListViewController *journalList =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForJournalListVC];
    journalList.shouldHideNavBar = YES;
    journalList.view.alpha = 0.0f;
    navController = [[UINavigationController alloc] initWithRootViewController:journalList];
    navController.view.frame = vwContainer.bounds;
    [vwContainer addSubview:navController.view];
    navController.navigationBarHidden = true;
    [UIView animateWithDuration:0.5 animations:^(void) {
         journalList.view.alpha = 1;
    }];
    
}

-(IBAction)switchView:(id)sender{
    if ([segmentControl selectedSegmentIndex] == 0) {
        [navController popViewControllerAnimated:YES];
    }else{
        [self showIntelligenceView];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
