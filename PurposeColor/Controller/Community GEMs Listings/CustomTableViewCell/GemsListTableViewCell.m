//
//  ProductCollectionViewCell.m
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "GemsListTableViewCell.h"


@implementation GemsListTableViewCell


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // Make sure the contentView does a layout pass here so that its subviews have their frames set, which we
    // need to use to set the preferredMaxLayoutWidth below.
    //[self.contentView setNeedsLayout];
   // [self.contentView layoutIfNeeded];
    //[self.contentView setNeedsUpdateConstraints];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [_vwBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwBg.layer setBorderWidth:1.f];
    // drop shadow
    [_vwBg.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwBg.layer setShadowOpacity:0.3];
    [_vwBg.layer setShadowRadius:2.0];
    [_vwBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    [_vwPormoitonBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwPormoitonBg.layer setBorderWidth:1.f];
    // drop shadow
    [_vwPormoitonBg.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwPormoitonBg.layer setShadowOpacity:0.3];
    [_vwPormoitonBg.layer setShadowRadius:2.0];
    [_vwPormoitonBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
    
    _btnGoalShare.layer.borderWidth = 1.f;
    _btnGoalShare.layer.borderColor = [UIColor clearColor].CGColor;
    _btnGoalShare.layer.cornerRadius = 5;
    
    _vwOnlyMe.layer.borderWidth = 1.f;
    _vwOnlyMe.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    _vwOnlyMe.layer.cornerRadius = 5.f;
    
    _imgProfile.layer.borderColor = [UIColor colorWithRed:0.83 green:0.83 blue:0.83 alpha:1.0].CGColor;
    _imgProfile.layer.borderWidth = 2.f;
    _imgProfile.layer.cornerRadius = 25.f;
    _imgProfile.clipsToBounds = YES;
    
    _btnBanner.layer.borderWidth = 1.f;
    
    _btnBanner.layer.borderColor = [UIColor colorWithRed:0.83 green:0.83 blue:0.83 alpha:1.0].CGColor;
    _btnBanner.layer.cornerRadius = 3.f;
    _lblDescription.systemURLStyle = YES;
    
    NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] initWithString:@"Only Me | Stay connected with your Goal"];
    [mutableAttString addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 7)];
    _lblOnlyMe.attributedText = mutableAttString;
    
    
}

-(void)setUpIndexPathWithRow:(NSInteger)row section:(NSInteger)section;{
    
    self.row = row;
    self.section = section;

    _lblDescription.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        // Open URLs
        [self attemptOpenURL:[NSURL URLWithString:string]];
    };
    
    _lblDescription.allOtherClicks = ^(KILabel *label, NSString *string, NSRange range) {
        // Open URLs
        if ([self.delegate respondsToSelector:@selector(showDetailPageWithIndex:)]) {
            [self.delegate showDetailPageWithIndex:self.row];
        }
        
    };
    
    

}

- (void)attemptOpenURL:(NSURL *)url
{
    

    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (!safariCompatible) {
        
        NSString *urlString = url.absoluteString;
        urlString = [NSString stringWithFormat:@"http://%@",url.absoluteString];
        url = [NSURL URLWithString:urlString];
        
    }
    safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem"
                                                                       message:@"The selected link cannot be opened."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dismiss"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:cancel];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
    }
}



-(IBAction)likeGemsApplied:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(likeGemsClicked:)]) {
        [self.delegate likeGemsClicked:self.row];
    }

}

-(IBAction)commentComposeApplied:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(commentComposeViewClickedBy:)]) {
        [self.delegate commentComposeViewClickedBy:self.row];
    }
    
}

-(IBAction)moreGalleryButtonApplied:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(moreGalleryPageClicked:)]) {
        [self.delegate moreGalleryPageClicked:self.row];
    }
    
}


-(IBAction)followButtonClicked:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(followButtonClickedWith:)]) {
        [self.delegate followButtonClickedWith:self.row];
    }
    
}

-(IBAction)deleteButtonClicked:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(deleteAGemWithIndex:)]) {
        [self.delegate deleteAGemWithIndex:self.row];
    }
    
}


-(IBAction)hideButtonClicked:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(moreButtonClickedWithIndex:view:)]) {
        [self.delegate moreButtonClickedWithIndex:self.row view:_btnMore];
    }
    
}

-(IBAction)editButtonClicked:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(editAGemWithIndex:)]) {
        [self.delegate editAGemWithIndex:self.row];
    }
    
}

-(IBAction)showButtonClicked:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(showGEMSInCommunity:)]) {
        [self.delegate showGEMSInCommunity:self.row];
    }
    
}

-(IBAction)showAllGemLikedUsers:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(showAllLikedUsers:)]) {
        [self.delegate showAllLikedUsers:self.row];
    }
    
}


-(IBAction)showAllGemCommentedUsers:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(showAllCommentedUsers:)]) {
        [self.delegate showAllCommentedUsers:self.row];
    }
    
}

-(IBAction)shareGEMsToSocialSites:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(shareGEMsToSocialMedia:)]) {
        [self.delegate shareGEMsToSocialMedia:self.row];
    }
    
}

-(IBAction)shareGoalToCommunity:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(shareGoalToCommunityWith:)]) {
        [self.delegate shareGoalToCommunityWith:self.row];
    }
    
}


@end
