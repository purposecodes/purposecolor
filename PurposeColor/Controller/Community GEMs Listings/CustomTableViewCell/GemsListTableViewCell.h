//
//  ProductCollectionViewCell.h
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "KILabel.h"

@protocol GemsListTableViewCellDelegate <NSObject>


@optional

/*!
 *This method is invoked when user Clicks "LIKE" Button
 */
-(void)likeGemsClicked:(NSInteger)index ;
/*!
 *This method is invoked when user Clicks "COMMENT" Button
 */
-(void)commentComposeViewClickedBy:(NSInteger)index ;


/*!
 *This method is invoked when user Clicks "+02" Button
 */
-(void)moreGalleryPageClicked:(NSInteger)index ;

/*!
 *This method is invoked when user Clicks "Follow" Button
 */
-(void)followButtonClickedWith:(NSInteger)index ;

/*!
 *This method is invoked when user Clicks "Delete" Button
 */
-(void)deleteAGemWithIndex:(NSInteger)index ;


/*!
 *This method is invoked when user Clicks "Hide" Button
 */
-(void)moreButtonClickedWithIndex:(NSInteger)index view:(UIView*)sender;


/*!
 *This method is invoked when user Clicks "Edit" Button
 */
-(void)editAGemWithIndex:(NSInteger)index;

/*!
 *This method is invoked when user Clicks "Show" Button
 */
-(void)showGEMSInCommunity:(NSInteger)index;


/*!
 *This method is invoked when user Clicks "Show Liked Users" Button
 */
-(void)showAllLikedUsers:(NSInteger)index;

/*!
 *This method is invoked when user Clicks "Show Commeneted Users" Button
 */
-(void)showAllCommentedUsers:(NSInteger)index;

/*!
 *This method is invoked when user Clicks "Show Commeneted Users" Button
 */
-(void)showDetailPageWithIndex:(NSInteger)index;

/*!
 *This method is invoked when user Clicks "Share Publc Media" Button
 */
-(void)shareGEMsToSocialMedia:(NSInteger)index;

/*!
 *This method is invoked when user Clicks "Share Goal Template" Button
 */
-(void)shareGoalToCommunityWith:(NSInteger)index;


@end



@interface GemsListTableViewCell : UITableViewCell{
    
}

@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) NSInteger row;
@property (nonatomic,weak)  id<GemsListTableViewCellDelegate>delegate;

@property (nonatomic,weak) IBOutlet UIButton *btnBanner;
@property (nonatomic,weak) IBOutlet UIImageView *imgGemMedia;
@property (nonatomic,weak) IBOutlet UIImageView *imgTransparentVideo;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) IBOutlet UIView *vwBg;
@property (nonatomic, weak) IBOutlet UIButton *btnMore;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *bgToSuperVw;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *bgToSharePromotion;
@property (nonatomic, weak) IBOutlet UILabel *lblSharePromotion;
@property (nonatomic, weak) IBOutlet UIView *vwPormoitonBg;

@property (nonatomic, weak) IBOutlet UIButton *btnLike;
@property (nonatomic, weak) IBOutlet UIButton *btnComment;
@property (nonatomic, weak) IBOutlet UIButton *btnShare;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (nonatomic,weak) IBOutlet  UIButton *btnFollow;
@property (nonatomic,weak) IBOutlet  UIButton *btnEdit;
@property (nonatomic,weak) IBOutlet  UIButton *btnViews;

@property (nonatomic, weak) IBOutlet UILabel *lbldddddd;
@property (nonatomic, weak) IBOutlet KILabel *lblDescription;
@property (nonatomic, weak) IBOutlet UILabel *lblShareOrSave;
@property (nonatomic, weak) IBOutlet UILabel *lblShareCaption;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblTime;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblLikeCnt;
@property (nonatomic, weak) IBOutlet UILabel *lblCmntCount;
@property (nonatomic,weak) IBOutlet  UIImageView *imgProfile;
@property (nonatomic, weak) IBOutlet UILabel *lblMediaCount;
@property (nonatomic, weak) IBOutlet UIButton *bnExpandGallery;
@property (nonatomic, weak) IBOutlet UIButton *btnShowInCommunity;
@property (nonatomic,weak) IBOutlet  UIImageView *imgVerified;
@property (nonatomic,weak) IBOutlet  UIView *vwShareHolder;
@property (nonatomic, weak) IBOutlet UILabel *lblFollowStaticTxt;
@property (nonatomic, weak) IBOutlet UILabel *lblShareText;

@property (nonatomic, weak) IBOutlet UILabel *lblGoalTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblGoalStartEnd;
@property (nonatomic, weak) IBOutlet UIView *vwOnlyMe;
@property (nonatomic, weak) IBOutlet UIButton *btnGoalShare;
@property (nonatomic, weak) IBOutlet UILabel *lblOnlyMe;


@property (nonatomic, weak) IBOutlet UIView *vwURLPreview;
@property (nonatomic, weak) IBOutlet UILabel *lblPreviewTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblPreviewDescription;
@property (nonatomic, weak) IBOutlet UILabel *lblPreviewDomain;
@property (nonatomic, weak) IBOutlet UIImageView *imgPreview;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *previewIndicator;
@property (nonatomic, weak) IBOutlet UIButton *btnShowPreviewURL;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *previewImageHeight;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *previewImageTop;


@property (nonatomic,weak) IBOutlet  UIButton *btnVideoPlay;
@property (nonatomic,weak) IBOutlet  UIButton *btnAudioPlay;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *constraintForAdHeight;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *topForShareTxt;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *trailingForName;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *constraintForHeight;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *imageTopCosntraint;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *previewTopCosntraint;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *previewPanelHeightConstraint;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *titleHeight;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *titleTopConstraint;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *descriptionTopConstraint;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *contraintForLocTop;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *contraintForContTop;
@property (nonatomic, weak) IBOutlet UILabel *lblLocation;
@property (nonatomic, weak) IBOutlet UILabel *lblContact;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *contraintForFeelTop;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *contraintForDriveTop;
@property (nonatomic, weak) IBOutlet UILabel *lblFeel;
@property (nonatomic, weak) IBOutlet UILabel *lblDrive;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage1Top;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage1Left;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage1Width;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage1Height;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage2Top;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage2Left;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage2Width;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage2Height;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage3Top;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage3Left;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage3Width;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage3Height;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage4Top;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage4Left;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage4Width;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *gridImage4Height;
@property (nonatomic, weak) IBOutlet UIView *vwGridContainer;
@property (nonatomic, weak) IBOutlet UIView *vwGridCountOverLay;

@property (nonatomic,strong) NSString *strURL;

-(void)setUpIndexPathWithRow:(NSInteger)row section:(NSInteger)section;
- (void)layoutSubviews;


@end
