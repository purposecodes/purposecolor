//
//  CommentComposeViewController.h
//  PurposeColor
//
//  Created by Purpose Code on 11/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

@protocol ReplyActionDelegate <NSObject>


@optional


-(void)updateCommentPageWithIndex:(NSInteger)row reply:(NSDictionary*)reply replyCount:(NSInteger)count isEdited:(BOOL)isEdited;


@end



#import <UIKit/UIKit.h>

@interface ReplyComposeViewController : UIViewController

@property (nonatomic,strong) NSString *strGemType;
@property (nonatomic,strong) NSDictionary *dictCommentInfo;
@property (nonatomic,weak)  id<ReplyActionDelegate>delegate;
@property (nonatomic,assign) NSInteger index;



@end
