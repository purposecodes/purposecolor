//
//  CommentComposeViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 11/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kHeightForHeader        65
#define kMinimumCellCount       1
#define kSuccessCode            200
#define kDefaultCellHeight      150
#define kHeightForFooter        0.001
#define kSuccessCode            200

#import "ReplyComposeViewController.h"
#include "CommentCustomCell.h"
#import  "Constants.h"
#import "HPGrowingTextView.h"
#import "EMEmojiableBtn.h"
#import "LikedAndCommentedUserListings.h"
#import "ProfilePageViewController.h"
#import "ReportAbuseViewController.h"

@interface ReplyComposeViewController ()<HPGrowingTextViewDelegate,CommentCellDelegate,EMEmojiableBtnDelegate,UIGestureRecognizerDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnCancelEdit;
    NSMutableArray *arrComments;
    UIView *containerView;
    HPGrowingTextView *textView;
    BOOL isDataAvailable;
    UIButton *btnDone;
    NSString *strNoDataText;
    NSInteger totalPages;
    NSInteger currentPage;
    BOOL isUnderPagination;
    
    NSString *replyID;
    NSInteger indexOfReply;
    
    IBOutlet NSLayoutConstraint *bottomForContainer;
    IBOutlet NSLayoutConstraint *heightForContainer;
}

@end

@implementation ReplyComposeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self getAllRepliesWithPageNum:currentPage isPagination:NO];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)setUp{
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    [tableView addGestureRecognizer:lpgr];
     lpgr.delegate = self;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
    [tapGesture setNumberOfTapsRequired:1];
    [tableView addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;

    btnCancelEdit.hidden = true;
    btnCancelEdit.layer.cornerRadius = 5.f;
    btnCancelEdit.layer.borderColor = [UIColor getThemeColor].CGColor;
    btnCancelEdit.layer.borderWidth = 1.f;
    
    isUnderPagination = false;
    currentPage = 1;
    totalPages = 0;
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 100;
    arrComments = [NSMutableArray new];
    self.automaticallyAdjustsScrollViewInsets = NO;
    tableView.hidden = true;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification 
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setUpGrowingTextView];
}

-(void)getAllRepliesWithPageNum:(NSInteger)pageNum isPagination:(BOOL)isPagination{
    
    if (_dictCommentInfo) {
        NSString *gemID;
        NSString *commentID;
        if (NULL_TO_NIL([_dictCommentInfo objectForKey:@"gem_id"]))
            gemID = [_dictCommentInfo objectForKey:@"gem_id"];
        commentID = [_dictCommentInfo objectForKey:@"comment_id"];
        
            [self showLoadingScreenWithTitle:@"Loading.."];
        isUnderPagination = true;
            [APIMapper getAllRepliesForACommentWith:gemID commentID:commentID pageNo:pageNum success:^(AFHTTPRequestOperation *operation, id responseObject) {
                isUnderPagination = false;
                [self parseResponds:responseObject isPagination:isPagination];
                [self hideLoadingScreen];
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                isUnderPagination = false;
                [self hideLoadingScreen];
                if (error && error.localizedDescription){
                    [ALToastView toastInView:self.view withText:NETWORK_ERROR_MESSAGE];
                    strNoDataText = error.localizedDescription;
                    
                }
                tableView.hidden = false;
                [tableView reloadData];
            }];
            
        }
    
}

-(void)parseResponds:(NSDictionary*)responds isPagination:(BOOL)isPagination{
    
    if ([[responds objectForKey:@"code"] integerValue] == kSuccessCode) {
        NSArray *result = [responds objectForKey:@"resultarray"];
        if (isPagination) {
            NSMutableArray *indexPaths = [NSMutableArray array];
            for (int i = 0; i < result.count; i++) {
                [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                [arrComments insertObject:result[i] atIndex:i];
            }
            [tableView beginUpdates];
            [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [tableView endUpdates];
        }else{
            [arrComments addObjectsFromArray:result];
            [tableView reloadData];
        }
        if (NULL_TO_NIL([responds objectForKey:@"currentPage"])) {
            currentPage = [[responds objectForKey:@"currentPage"] integerValue];
        }
        if (NULL_TO_NIL([responds objectForKey:@"pageCount"])) {
            totalPages = [[responds objectForKey:@"pageCount"] integerValue];
        }

    }
    
    else if ([[responds objectForKey:@"code"]integerValue] == kUnauthorizedCode){
        
        if (NULL_TO_NIL([responds objectForKey:@"text"])) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Reply"
                                                                message:[responds objectForKey:@"text"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate clearUserSessions];
            
        }
        
    }else{
        
        if ([responds objectForKey:@"text"]) {
            strNoDataText = [responds objectForKey:@"text"];
        }
        
    }
    
    if (arrComments.count) isDataAvailable = true;
    tableView.hidden = false;
    [tableView reloadData];
}

#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
     if (!isDataAvailable) return kMinimumCellCount;
    return arrComments.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        static NSString *CellIdentifier = @"NoComments";
        UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        return cell;
    }
    static NSString *CellIdentifier = @"CommentCustomCell";
    CommentCustomCell *cell = (CommentCustomCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell.imgProfilePic setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
    cell.index = indexPath.row;
    cell.vwBg.tag = indexPath.row;
    cell.btnRply.tag = indexPath.row;
    cell.btnLike.tag = indexPath.row;
    cell.btnLike.delegate = self;
    cell.btnLike.vwBtnSuperView = self.view;
    [cell.btnLike privateInit];
    
    cell.delegate = self;
    [self setUpCommentWith:cell index:indexPath.row];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;{
    
   [self.view endEditing:YES];    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    // remove bottom extra 20px space.
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    // remove bottom extra 20px space.
    return CGFLOAT_MIN;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (isUnderPagination) return;
    /**! Pagination call !**/
    if (scrollView.contentOffset.y <= 0){
        
        NSInteger nextPage = currentPage ;
        nextPage += 1;
        if (nextPage  <= totalPages) {
            [self getAllRepliesWithPageNum:nextPage isPagination:YES];
        }
    }
    
}



-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:p];
    if (indexPath.row < arrComments.count) {
        if (indexPath.row < arrComments.count) {
            NSDictionary *gemDetails = arrComments[indexPath.row];
            BOOL isOthers = [[gemDetails objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId] ? false : true;
            
            UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Reply" message:@"Choose an action" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction* hide;
            UIAlertAction* copy;
            UIAlertAction* delete;
            UIAlertAction* edit;
            if (isOthers) {
                hide = [UIAlertAction actionWithTitle:@"Report Abuse" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    [alert dismissViewControllerAnimated:YES completion:nil];
                     [self reportAbuseWithIndex:indexPath.row];
                    
                }];
                [alert addAction:hide];
                
            }else{
                
                edit = [UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    [self editReply:indexPath.row];
                    
                }];
                [alert addAction:edit];
                
                delete = [UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    [self deleteCommentClicked:indexPath.row];
                    
                }];
                [alert addAction:delete];
                
               
            }
            copy = [UIAlertAction actionWithTitle:@"Copy" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                [alert dismissViewControllerAnimated:YES completion:nil];
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = [gemDetails objectForKey:@"reply_txt"];
                [ALToastView toastInView:self.view withText:@"Reply has been copied to clipboard"];
                
            }];
            
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                     {
                                     }];
            
            [alert addAction:copy];
            
            [alert addAction:cancel];
            [self presentViewController:alert animated:YES completion:nil];            
        }
        
        
        
    }
    
    
}



-(void)setUpCommentWith:(CommentCustomCell*)cell index:(NSInteger)row{
    
    if (row < arrComments.count) {
        NSDictionary *comment = arrComments[row];
        if (NULL_TO_NIL([comment objectForKey:@"reply_datetime"])) {
            double serverTime = [[comment objectForKey:@"reply_datetime"] doubleValue];
            cell.lblDate.text = [Utility getCommentedTimeWith:serverTime];
        }
        
        if (NULL_TO_NIL([comment objectForKey:@"firstname"])) cell.lblName.text = [comment objectForKey:@"firstname"];
        if (NULL_TO_NIL([comment objectForKey:@"reply_txt"])){
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineHeightMultiple = 1.2f;
            NSDictionary *attributes = @{NSParagraphStyleAttributeName:paragraphStyle,
                                         };
            NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[comment objectForKey:@"reply_txt"] attributes:attributes];
            //cell.lblComment.attributedText = attributedText;
            cell.lblComment.text = [comment objectForKey:@"reply_txt"];
            
        }
        
        if (NULL_TO_NIL([comment objectForKey:@"profileurl"])) {
            NSString *videoThumb = [comment objectForKey:@"profileurl"];
            if (videoThumb.length) {
                [cell.imgProfilePic sd_setImageWithURL:[NSURL URLWithString:videoThumb]
                                    placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           }];
            }
        }
        
        NSInteger likeStatus = [[comment objectForKey:@"like_status"] integerValue];
        if (likeStatus < 0) {
            [cell.btnLike setTitleColor:[UIColor colorWithRed:0.56 green:0.58 blue:0.62 alpha:1.0] forState:UIControlStateNormal];
            [cell.btnLike setTitle:@"Like" forState:UIControlStateNormal];
        }else{
            EMEmojiableOption *option = [cell.btnLike.dataset objectAtIndex:likeStatus];
            [cell.btnLike setTitle:option.name forState:UIControlStateNormal];
            [cell.btnLike setTitleColor:option.color forState:UIControlStateNormal];
        }
        cell.btnLikeCount.hidden = true;
        if ([[comment objectForKey:@"total_like"] integerValue] > 0) {
            cell.btnLikeCount.hidden = false;
            [cell.btnLikeCount setTitle:[NSString stringWithFormat:@" %d",[[comment objectForKey:@"total_like"] integerValue]] forState:UIControlStateNormal];
        }
        cell.btnDelete.hidden = true;
        if ([[comment objectForKey:@"user_id"] integerValue] == [[User sharedManager].userId integerValue]) {
            cell.btnDelete.hidden = false;
        }
        
        cell.imgVerified.hidden = true;
        if ([[comment objectForKey:@"verify_status"] integerValue] == 1){
            cell.imgVerified.hidden = false;
        }
        
        cell.lblComment.allOtherClicks = ^(KILabel *label, NSString *string, NSRange range) {
            [self.view endEditing:YES];
        };
    }
}


#pragma mark - Like  Comment


- (void)EMEmojiableBtn:( EMEmojiableBtn* _Nonnull)button selectedOption:(NSUInteger)emojiCode{
    
    if (button.tag < arrComments.count) {
        NSMutableDictionary *comment = [NSMutableDictionary dictionaryWithDictionary:arrComments[button.tag]];
        NSInteger count = [[comment objectForKey:@"total_like"] integerValue];
        NSInteger likeStatus = [[comment objectForKey:@"like_status"] integerValue];
        [comment setObject:[NSNumber numberWithInteger:emojiCode] forKey:@"like_status"];
        if (likeStatus >= 0) {
            
        }else{
            count += 1;
        }
        if (count < 0) {
            count = 0;
        }
        EMEmojiableOption *option = [button.dataset objectAtIndex:emojiCode];
        [button setTitle:option.name forState:UIControlStateNormal];
        [button setTitleColor:option.color forState:UIControlStateNormal];
        
        [comment setObject:[NSNumber numberWithInteger:count] forKey:@"total_like"];
        [arrComments replaceObjectAtIndex:button.tag withObject:comment];
        [tableView reloadData];
        [self likeAReplyWithIndex:button.tag andEmoji:emojiCode];
    }
    
    
    
}
- (void)EMEmojiableBtnCanceledAction:(EMEmojiableBtn* _Nonnull)button{
    
}
- (void)EMEmojiableBtnSingleTap:(EMEmojiableBtn* _Nonnull)button{
    
    if (button.tag < arrComments.count) {
        NSMutableDictionary *comment = [NSMutableDictionary dictionaryWithDictionary:arrComments[button.tag]];
        NSInteger status = 0;
        NSInteger count = [[comment objectForKey:@"total_like"] integerValue];
        if ([[comment objectForKey:@"like_status"] integerValue] >= 0) {
            status = -1;
            count -= 1;
        }else{
            count += 1;
        }
        if (count < 0) {
            count = 0;
        }
        [comment setObject:[NSNumber numberWithInteger:count] forKey:@"total_like"];
        [comment setObject:[NSNumber numberWithInteger:status] forKey:@"like_status"];
        [arrComments replaceObjectAtIndex:button.tag withObject:comment];
        [tableView reloadData];
        [self likeAReplyWithIndex:button.tag andEmoji:status];
        
    }
    
    
}


-(void)likeAReplyWithIndex:(NSInteger)index andEmoji:(NSInteger)emoji{
    
    if (index < arrComments.count) {
        NSDictionary *comment = arrComments[index];
        [APIMapper likeReplyOrCommentWithType:@"reply" gemID:[comment objectForKey:@"gem_id"] selectedID:[comment objectForKey:@"reply_id"] emojiCode:emoji gemType: _strGemType success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
        }];
    }
}


#pragma mark - Growing Text View

- (void)setUpGrowingTextView {
    
    containerView = [[UIView alloc] init];
    [self.view addSubview:containerView];
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[containerView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(containerView)]];
    heightForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                      attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil
                                                      attribute:NSLayoutAttributeHeight
                                                     multiplier:1.0
                                                       constant:50];
    [containerView addConstraint:heightForContainer];
    
    if (@available(iOS 11, *)){
        
        bottomForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0];
        
    }else{
        
        bottomForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0];
        
    }
    
    [self.view addConstraint:bottomForContainer];
    containerView.backgroundColor = [UIColor whiteColor];
    UIView *vwBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , 0.5)];
    vwBorder.backgroundColor = [UIColor colorWithRed:0.92 green:0.92 blue:0.93 alpha:1.0];
    [containerView addSubview:vwBorder];
    
    textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(5, 10, self.view.frame.size.width - 50, 40)];
    textView.isScrollable = NO;
    textView.contentInset = UIEdgeInsetsMake(5, 5, 0, 5);
    textView.layer.borderWidth = 1.f;
    textView.layer.borderColor = [UIColor clearColor].CGColor;
    textView.minNumberOfLines = 1;
    textView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
    textView.returnKeyType = UIReturnKeyGo; //just as an example
    textView.font = [UIFont fontWithName:CommonFont_New size:15];
    textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
    textView.placeholder = @"Write a reply..";
    textView.internalTextView.autocorrectionType = UITextAutocorrectionTypeYes;
    textView.textColor = [UIColor getTitleBlackColor];
    //textView.keyboardType=UIKeyboardTypeASCIICapable;
    
    // textView.text = @"test\n\ntest";
    // textView.animateHeightChange = NO; //turns off animation
    
    
    // textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:textView];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    doneBtn.frame = CGRectMake(self.view.frame.size.width - 45, 5, 40, 40);
    [doneBtn setImage:[UIImage imageNamed:@"Comment_Send"] forState:UIControlStateNormal];
    // [doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn.titleLabel.font = [UIFont fontWithName:CommonFontBold size:16];
    [doneBtn addTarget:self action:@selector(postReply) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:doneBtn];
    btnDone = doneBtn;
    [btnDone setEnabled:FALSE];
}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    bottomForContainer.constant =  - (keyboardBounds.size.height - bottomPadding) ;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    
    // commit animations
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    bottomForContainer.constant = 0;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    // commit animations
    [UIView commitAnimations];
}
-(void) keyboardWillChangeFrame:(NSNotification *)note{
    
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    bottomForContainer.constant =  - (keyboardBounds.size.height - bottomPadding) ;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    
    // commit animations
    [UIView commitAnimations];
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    heightForContainer.constant = r.size.height;

}

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView{
    
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (trimmedString.length > 0) [btnDone setEnabled:TRUE];
    else [btnDone setEnabled:FALSE];
    
}

- (void)growingTextViewDidEndEditing:(HPGrowingTextView *)growingTextView{
    
}


-(void)profileBtnClicked:(NSInteger)index{
    
    if (index < arrComments.count) {
        NSMutableDictionary *details = arrComments[index];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
            
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            if (!app.navGeneral) {
                app.navGeneral = [[UINavigationController alloc] initWithRootViewController:profilePage];
                app.navGeneral.navigationBarHidden = true;
                [UIView transitionWithView:app.window
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{  app.window.rootViewController = app.navGeneral; }
                                completion:nil];
            }else{
                [app.navGeneral pushViewController:profilePage animated:YES];
            }
            profilePage.canEdit = false;
            if ([[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
                profilePage.canEdit = true;
            }
            [profilePage loadUserProfileWithUserID:[details objectForKey:@"user_id"]showBackButton:YES];
            
        }
        
    }
    
}



#pragma mark - Post Reply

-(void)reportAbuseWithIndex:(NSInteger)index{
    
    if (index < arrComments.count) {
        
        NSDictionary *gemDetails = arrComments[index];
        ReportAbuseViewController *reportAbuseVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForReportAbuse];
        reportAbuseVC.gemDetails = gemDetails;
        [self.navigationController pushViewController:reportAbuseVC animated:YES];
    }
    
}
-(void)postReply
{
    [textView resignFirstResponder];
    [self.view endEditing:YES];
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (trimmedString.length > 0) {
        
        if (_dictCommentInfo) {
            NSString *gemID;
             NSString *commentID;
            if (NULL_TO_NIL([_dictCommentInfo objectForKey:@"gem_id"]))
                gemID = [_dictCommentInfo objectForKey:@"gem_id"];
                commentID = [_dictCommentInfo objectForKey:@"comment_id"];
                [self showLoadingScreenWithTitle:@"Posting.."];
                NSString *comment = textView.internalTextView.text;
            
            [APIMapper postReplyForACommentWithCommentID:commentID gemID:gemID reply:comment replyID:replyID gemType:_strGemType success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    if ([[responseObject objectForKey:@"code"] integerValue] == kSuccessCode) {
                        
                        if (!replyID) {
                            if ([responseObject objectForKey:@"reply"]) {
                                [arrComments addObject:[responseObject objectForKey:@"reply"]];
                                tableView.hidden = false;
                            }
                            if (arrComments.count > 0)isDataAvailable = true;
                            [tableView reloadData];
                            [self tableScrollToLastCell];
                            [self updateCommentPageWithReply:[responseObject objectForKey:@"reply"] count:[[responseObject objectForKey:@"reply_count"] integerValue] isaEdited:false];
                        }else{
                            
                            if (indexOfReply < arrComments.count) [arrComments replaceObjectAtIndex:indexOfReply withObject:[responseObject objectForKey:@"reply"]];
                            if (arrComments.count > 0)isDataAvailable = true;
                            BOOL edited = false;
                            if (indexOfReply == arrComments.count - 1) edited = true;
                            [self updateCommentPageWithReply:[responseObject objectForKey:@"reply"] count:[[responseObject objectForKey:@"reply_count"] integerValue]isaEdited:edited];
                            [tableView reloadData];
                        }
                        btnCancelEdit.hidden = true;
                       
                       
                    }
                    else if ([[responseObject objectForKey:@"code"]integerValue] == kUnauthorizedCode){
                        
                        if (NULL_TO_NIL([responseObject objectForKey:@"text"])) {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Reply"
                                                                                message:[responseObject objectForKey:@"text"]
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                            [alertView show];
                            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                            [delegate clearUserSessions];
                            
                        }
                        
                    }
                    
                    [self hideLoadingScreen];
                    textView.text = @"";
                    
                    
                } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                    
                    [self showMessage:@"Failed to add reply!"];
                    [self hideLoadingScreen];
                }];
        }
    }
}


-(void)tableScrollToLastCell{
    
    if (arrComments.count > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:arrComments.count - 1 inSection:0];
        [tableView scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionTop
                                 animated:YES];
    }
   
    
}

-(void)editReply:(NSInteger)index {
    
    btnCancelEdit.hidden = false;
    if (index < arrComments.count) {
        
        NSDictionary *comment = arrComments[index];
        if (NULL_TO_NIL([comment objectForKey:@"reply_id"])) {
            NSString *_commentID = [comment objectForKey:@"reply_id"];
            if (_commentID) {
                replyID = _commentID;
                textView.text = [comment objectForKey:@"reply_txt"];
                indexOfReply = index;
                [textView becomeFirstResponder];
            }
            
        }
    }
    
}

-(IBAction)cancelEdit:(id)sender{
    
    btnCancelEdit.hidden = true;
    replyID = nil;
    textView.text = @"";
    [self.view endEditing:YES];
    
}




-(void)deleteCommentClicked:(NSInteger)index {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete"
                                  message:@"Delete the selected Reply?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"DELETE"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             if (index < arrComments.count) {
                                 
                                 NSDictionary *comment = arrComments[index];
                                 if (NULL_TO_NIL([comment objectForKey:@"comment_id"])) {
                                     NSString *commentID = [comment objectForKey:@"comment_id"];
                                     if (commentID) {
                                         [self showLoadingScreenWithTitle:@"Deleting.."];
                                         [APIMapper removeReplyWithCommentID:commentID replyID:[comment objectForKey:@"reply_id"] success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             if ([[responseObject objectForKey:@"code"] integerValue] == kSuccessCode){
                                                 
                                                 isDataAvailable = true;
                                                 if (index < arrComments.count) {
                                                     [arrComments removeObjectAtIndex:index];
                                                     if (arrComments.count <= 0) isDataAvailable = false;
                                                     //[tableView reloadData];
                                                     NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [tableView numberOfSections])];
                                                     [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
                                                 }
                                                 [self hideLoadingScreen];
                                                 if (arrComments.count)
                                                     [self updateCommentPageWithReply:[arrComments lastObject] count:arrComments.count isaEdited:false];
                                                 else
                                                      [self updateCommentPageWithReply:nil count:arrComments.count isaEdited:false];
                                                 
                                             }
                                             else if ([[responseObject objectForKey:@"code"]integerValue] == kUnauthorizedCode){
                                                 
                                                 if (NULL_TO_NIL([responseObject objectForKey:@"text"])) {
                                                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Reply"
                                                                                                         message:[responseObject objectForKey:@"text"]
                                                                                                        delegate:nil
                                                                                               cancelButtonTitle:@"OK"
                                                                                               otherButtonTitles:nil];
                                                     [alertView show];
                                                     AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                                     [delegate clearUserSessions];
                                                     
                                                 }
                                             }
                                             
                                                 
                                            
                                             
                                             
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                             
                                             [self showMessage:@"Failed to delete reply!"];
                                             [self hideLoadingScreen];
                                         }];
                                     }
                                 }
                             }
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


-(void)updateCommentPageWithReply:(NSDictionary*)reply count:(NSInteger)count isaEdited:(BOOL)isEdited{
    
    if ([self.delegate respondsToSelector:@selector(updateCommentPageWithIndex:reply:replyCount:isEdited:)]) {
        [self.delegate updateCommentPageWithIndex:_index reply:reply replyCount:count isEdited:isEdited];
    }
}


-(void)showMessage:(NSString*)message{
    
    [[[UIAlertView alloc] initWithTitle:@"Reply" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)closeKeyboard{
    
    [self.view endEditing:YES];
}

-(IBAction)closePopUp{
    
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    //if ([self.delegate respondsToSelector:@selector(commentPopUpCloseAppplied)]) {
        //[self.delegate commentPopUpCloseAppplied];
    //}
    [[self navigationController] popViewControllerAnimated:YES];
   
 
}

-(void)updateCommentCountWithGemID:(NSString*)gemID commentCount:(NSInteger)count isAddComment:(BOOL)isAddComment{
    
    //if ([self.delegate respondsToSelector:@selector(commentPostedSuccessfullyWithGemID:commentCount:index:isAddComment:)]) {
       // [self.delegate commentPostedSuccessfullyWithGemID:gemID commentCount:count index:_selectedIndex isAddComment:isAddComment];
    //}
}

-(void)likedUserListClicked:(NSInteger)index{
    if (index < arrComments.count) {
        NSDictionary *comment = arrComments[index];
        NSInteger count = [[comment objectForKey:@"total_like"] integerValue];
        if (count <= 0) {
            return;
        }
        LikedAndCommentedUserListings *userListings =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForLikedAndCommentedUsers];
        [userListings loadUserListingsForType:@"reply" selectedID:[comment objectForKey:@"reply_id"]gemType:nil];
        [self.navigationController pushViewController:userListings animated:YES];
        
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    if ([[touch view] isKindOfClass:[KILabel class]]) {
        return NO;
    }
    
    return YES;
}

-(void)dealloc{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
