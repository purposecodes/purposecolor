//
//  EmotionalAwarenessViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 20/03/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//


#define EachQuestionBlock       40
#define ExpanedAnswerBlock      80
#define TotalBlocks             4
#define NavPanelHeight          65

typedef enum{
    
    eTypeEvent = -1,
    eTypeFeel = 0,
    eTypeEmotion = 1,
    eTypeDrive = 2,
    eTypeGoalsAndDreams = 3,
    eTypeAction = 4,
    eTypeDate = 5
    
}ESelectedMenuType;

#define kSectionCount               2
#define kDefaultCellHeight          95
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kWidthPadding               115
#define kFollowHeightPadding        85
#define kOthersHeightPadding        20

#import "EDStarRating.h"
#import "EmotionalAwarenessViewController.h"
#import "CellForEventDisplay.h"
#import "CellForMenus.h"
#import "SelectYourEvent.h"
#import "EventTitleCell.h"
#import "EventDescriptionCell.h"

#import "Constants.h"
#import "AudioManagerView.h"
#import "GalleryManager.h"
#import "UIView+RNActivityView.h"
#import "CustomeImagePicker.h"
#import "CustomAudioPlayerView.h"
#import <AVKit/AVKit.h>
#import "UITextView+Placeholder.h"
#import "PhotoBrowser.h"
#import "SDAVAssetExportSession.h"
#import "ContactsPickerViewController.h"
#import "ActionMediaListCell.h"
#import "SelectYourFeel.h"
#import "SelectYourEmotion.h"
#import "SelectYourDrive.h"
#import "SelectYourGoalsAndDreams.h"
#import "SelectActions.h"
#import "CellForContactAndLoc.h"
#import "AMPopTip.h"
#import "AwarenessHeader.h"
#import "SelectedValue_1.h"
#import "SelectedValue_2.h"
#import "AwarenessMediaViewController.h"
#import "WebBrowserViewController.h"

@import GooglePlacePicker;

@interface EmotionalAwarenessViewController () <SelectYourEventDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CustomeImagePickerDelegate,CLLocationManagerDelegate,ContactPickerDelegate,ActionInfoCellDelegate,PhotoBrowserDelegate,CustomAudioPlayerDelegate,SelectYourFeelingDelegate,SelectYourEmotionDelegate,SelectYourDriveDelegate,SelectYourGoalsAndDreamsDelegate,SelectYourActionsDelegate,UIGestureRecognizerDelegate,UITextViewDelegate,AwarenessMediaDelegate,EDStarRatingProtocol>{
    
    IBOutlet UIButton *btnPost;
    UIView *inputAccView;
    
    BOOL isCycleCompleted;
   
    NSMutableArray *arrFeelImages;
    NSMutableArray *arrDriveImages;
    NSMutableDictionary *dictSelectedSteps;
    NSMutableDictionary *dictSuccessSteps;
    
    SelectYourEvent *vwEventSelection;
    NSString  *eventTitle;
    NSInteger eventValue;
    NSInteger selectedEventValue;
    CGPoint viewStartLocation;
    
    SelectYourFeel *vwFeelSelection;
    NSInteger selectedFeelValue;
    
    SelectYourEmotion *vwEmotionSelection;
    NSString  *selectedEmotionTitle;
    NSInteger selectedEmotionValue;
    
    SelectYourDrive *vwDriveSelection;
    NSInteger selectedDriveValue;
    
    SelectYourGoalsAndDreams *vwGoalsSelection;
    NSString  *selectedGoalsTitle;
    NSInteger selectedGoalsValue;
    
    SelectActions *vwActions;
    NSString  *selectedActionTitle;
    NSDictionary* selectedActions;

    NSString  *strDescription;
    
    CLLocationManager* locationManager;
    CLLocationCoordinate2D locationCordinates;
    NSString *strLocationAddress;
    NSString *strLocationName;
    NSString *strContactName;
    
    AMPopTip *popTip;
    
    NSString *strDate;
    IBOutlet UILabel *lblDate;
    IBOutlet UIView *vwPickerOverLay;
    IBOutlet UIDatePicker *datePicker;
    
    float firstX;
    float firstY;
    IBOutlet UIButton *btnHelp;
    IBOutlet NSLayoutConstraint *btnHelpTop;
    IBOutlet NSLayoutConstraint *btnHelpLeft;
    
    /********************** NEWLY ADDED ************************/
    
    AwarenessMediaViewController *mediasVC;
    
    IBOutlet NSLayoutConstraint *questionHlderTop;
    BOOL isSkipApplied;
    IBOutlet UIView *vwDateView;
    IBOutlet UIView *vwMediaAttachmnt;
    IBOutlet UIButton *btnMedia;
    
    IBOutlet NSLayoutConstraint *descPanelHeight;
    IBOutlet NSLayoutConstraint *descQuestonTop;
    BOOL isDescOpen;
    IBOutlet UILabel *lblDescriptionSelected;
    IBOutlet UIImageView *imgDescTick;
    IBOutlet UIButton *btnNextDesc;
    IBOutlet UILabel *lblDescQstn;
    IBOutlet UITextView *txtView;
    
    IBOutlet NSLayoutConstraint *ratePanelHeight;
    IBOutlet NSLayoutConstraint *rateQuestonTop;
    IBOutlet UIImageView *imgRateValue;
    IBOutlet UIImageView *imgRateTick;
    IBOutlet UIButton *btnNextFeel;
    BOOL isRatePopOpen;
    IBOutlet EDStarRating *rateStar;
    IBOutlet UILabel *lblRateQstn;
    IBOutlet UIImageView *imgRateIcon;
    IBOutlet UIView *vwRateView;
    
    IBOutlet NSLayoutConstraint *emotionPanelHeight;
    IBOutlet NSLayoutConstraint *emotionQuestonTop;
    IBOutlet UIView *emotionListHolder;
    BOOL isEmotionPopOpen;
    IBOutlet UILabel *lblEmotionSelected;
    IBOutlet UILabel *lblEmotionQstn;
    IBOutlet UIButton *btnNextEmotion;
    IBOutlet UIImageView *imgEmoiotnIcon;
    IBOutlet UIImageView *imgEmotionTick;
    IBOutlet UIView *vwEmotionView;
    
    IBOutlet NSLayoutConstraint *reactionPanelHeight;
    IBOutlet NSLayoutConstraint *reactionQuestonTop;
    IBOutlet EDStarRating *reactionStar;
    IBOutlet UIImageView *imgReactionValue;
    BOOL isReactionPopOpen;
    IBOutlet UILabel *lblReacitonQstn;
    IBOutlet UIButton *btnNextReaciton;
    IBOutlet UIImageView *imgReacitonIcon;
    IBOutlet UIImageView *imgReacitonTick;
    IBOutlet UIView *vwReactionView;
    
    IBOutlet UIView *goalListHolder;
    IBOutlet NSLayoutConstraint *goalPanelHeight;
    IBOutlet NSLayoutConstraint *goalQuestonTop;
    BOOL isgoalPopOpen;
    IBOutlet UILabel *lblGoalSelected;
    IBOutlet UILabel *lblGoalQstn;
    IBOutlet UIButton *btnNextGoal;
    IBOutlet UIImageView *imgGoalIcon;
    IBOutlet UIImageView *imgGoalTick;
    IBOutlet UIView *vwGoalBottomBorder;
    IBOutlet UIView *vwGoalView;
    
    IBOutlet UIView *actionViewPanel;
    IBOutlet UIView *actionListHolder;
    IBOutlet NSLayoutConstraint *actionPanelHeight;
    IBOutlet NSLayoutConstraint *actionQuestonTop;
    BOOL isActionPopOpen;
    IBOutlet UILabel *lblActionSelected;
    IBOutlet UIImageView *imgActionTick;
    IBOutlet UIButton *btnNextActin;
    IBOutlet UILabel *lblActionQstn;
    IBOutlet UIView *vwActionBottomBorder;
    
}

@property (nonatomic,strong) NSIndexPath *draggingCellIndexPath;
@property (nonatomic,strong) UIView *cellSnapshotView;

@end

@implementation EmotionalAwarenessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fadeOutAllInactiveBoxes];
    [self setUp];
    [self removeAllContentsInMediaFolder];
    [self getCurrentDate];
    [self createInputAccessoryView];
    [self enableFloaitngMenu];
    [self showWhatsOnYourMind:nil];
    
   // if (_shouldOpenFirstMenu) [self performSelector:@selector(showEventSelectionView) withObject:self afterDelay:0.7];
    
        // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
   
}


-(void)setUp{
    
    
    
    btnHelpLeft.constant = self.view.frame.size.width - 50;
    btnHelpTop.constant = self.view.frame.size.height - 120;;
    
    actionViewPanel.hidden = true;
    vwGoalBottomBorder.hidden = false;
    vwActionBottomBorder.hidden = true;
    vwPickerOverLay.alpha = 0;
    vwDateView.alpha = 0;
    
    [btnMedia setTitle:@"   MEDIA   " forState:UIControlStateNormal];
    btnMedia.layer.cornerRadius = 5.f;
    btnMedia.layer.borderColor = [UIColor clearColor].CGColor;
    btnMedia.layer.borderWidth = 1.f;
    [btnMedia addTarget:self action:@selector(showUploadingMedias:) forControlEvents:UIControlEventTouchUpInside];
    vwMediaAttachmnt.alpha = 0;
    
    btnPost.layer.cornerRadius = 5.f;
    btnPost.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    btnPost.layer.borderWidth = 1.f;
    btnPost.enabled = false;
    btnPost.alpha = 0.3;
    
    isCycleCompleted = false;
    dictSelectedSteps = [NSMutableDictionary new];
    dictSuccessSteps = [NSMutableDictionary new];
    arrFeelImages = [[NSMutableArray alloc] initWithObjects:@"Strongly_Agree_Blue",@"Agree_Blue",@"Neutral_Blue",@"Disagree_Blue",@"Strongly_DisAgree_Blue", nil];
    arrDriveImages = [[NSMutableArray alloc] initWithObjects:@"Blue_1_Star",@"Blue_2_Star",@"Blue_3_Star",@"Blue_4_Star",@"Blue_5_Star", nil];
    
    btnHelp.layer.cornerRadius = 20.f;
    btnHelp.layer.borderWidth = 1.f;
    btnHelp.layer.borderColor = [UIColor clearColor].CGColor;
    
    btnHelp.hidden = true;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"SHOULD_SHOW_HELP"]){
        NSInteger type = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SHOULD_SHOW_HELP"] integerValue];
        if (type == 1) {
            btnHelp.hidden = false;
        }
    }
    
    questionHlderTop.constant = 0; //[self getQuestionHolderIntialPosiotion];
    imgRateValue.hidden = true;
    imgReactionValue.hidden = true;
       

}




-(IBAction)showUploadingMedias:(id)sender{
    
    [self.view endEditing:YES];
    if (!mediasVC) {
        mediasVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForImotionalAwarenessMedia];
        [self addChildViewController:mediasVC];
        mediasVC.delegate = self;
        
    }
    
    UIView *vwPopUP = mediasVC.view;
    [self.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
   
    vwPopUP.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    
    [UIView animateWithDuration:0.8 animations:^{
        vwPopUP.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            vwPopUP.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                vwPopUP.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
   
    
   
}
-(void)saveMedias:(BOOL)shouldSave WithCoordinates:(CLLocationCoordinate2D)coordinates locationName:(NSString*)loc contacts:(NSString*)contacts mediaCount:(NSInteger)count{
    
    if (count > 0) [btnMedia setTitle:[NSString stringWithFormat:@"   MEDIA (%ld)   ",(long)count] forState:UIControlStateNormal];
    else  [btnMedia setTitle:@"   MEDIA   " forState:UIControlStateNormal];
    
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        mediasVC.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.8 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            mediasVC.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished){
            
            [mediasVC.view removeFromSuperview];
            locationCordinates = coordinates;
            strLocationName = loc;
            strLocationAddress = loc;
            strContactName = contacts;
            if (!shouldSave) {
                
                [self removeAllContentsInMediaFolder];
                strContactName = nil;
                strLocationName = nil;
                strLocationAddress = nil;
                
                locationCordinates = CLLocationCoordinate2DMake(0, 0);;
                [mediasVC willMoveToParentViewController:nil];
                [mediasVC.view removeFromSuperview];
                [mediasVC removeFromParentViewController];
                mediasVC = nil;
            }
            
            // if you want to do something once the animation finishes, put it here
            
            
        }];

    }];
    
    
    
   
    
}



#pragma mark - UITextView delegate methods


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    [textView setInputAccessoryView:inputAccView];
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
   
}

- (void)textViewDidEndEditing:(UITextView *)textView{
   

    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
    [self getTextFromField:textView.text];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    
}

-(void)getTextFromField:(NSString*)string{
    
    
    if ((([string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length) > 0)) {
        strDescription = string;
        lblDescriptionSelected.text = string;
        descQuestonTop.constant = -3;
        [dictSelectedSteps setObject:[NSNumber numberWithBool:YES] forKey:[NSNumber numberWithInteger:eTypeEvent]];
        [UIView animateWithDuration:0.5
                         animations:^{
                             [self.view layoutIfNeeded]; // Called on parent view
                         }completion:^(BOOL finished) {
                             //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                         }];
    }
   
    
}

-(void)createInputAccessoryView{
    
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 45.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 1];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 85, 5.0f, 80.0f, 35.0f)];
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor getThemeColor]];
    btnDone.layer.cornerRadius = 5.f;
    btnDone.layer.borderWidth = 1.f;
    btnDone.layer.borderColor = [UIColor whiteColor].CGColor;
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    btnDone.titleLabel.font = [UIFont fontWithName:CommonFont_New size:14];
    [inputAccView addSubview:btnDone];
    
}
-(void)doneTyping{
    
    if (!txtView.text.length) {
        [ALToastView toastInView:self.view withText:@"Please enter description.."];
        return;
    }
    [self.view endEditing:YES];
    if (strDescription.length)[self fadeInActiveBox];
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) [self showRateOptions:nil];else  [self showWhatsOnYourMind:nil];
    [self closeAllOtherQuestionsIfNeededFrmDescription];
    
}

#pragma mark - SELECT YOUR EVENT Actions

-(IBAction)showEventSelectionView{
    
    [self.view endEditing:YES];
    vwEventSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourEvent" owner:self options:nil] objectAtIndex:0];
    vwEventSelection.translatesAutoresizingMaskIntoConstraints = NO;
    vwEventSelection.delegate = self;
    vwEventSelection.eventID = eventValue;
    [self.view addSubview:vwEventSelection];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[vwEventSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwEventSelection)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwEventSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwEventSelection)]];
    [vwEventSelection showSelectionPopUp];
    
}


-(void)selectYourEventPopUpCloseAppplied{
    
    [vwEventSelection removeFromSuperview];
    vwEventSelection.delegate = nil;
    vwEventSelection = nil;
}


-(void)eventSelectedWithEventTitle:(NSString*)_eventTitle eventID:(NSInteger)eventID;{
    eventValue = eventID;
    selectedEventValue = eventID;
    eventTitle = _eventTitle;
    [dictSelectedSteps setObject:[NSNumber numberWithInteger:eventID] forKey:[NSNumber numberWithInteger:eTypeEvent]];
    [self shouldeablePostButton:YES];
    

}

#pragma mark - Whats On Your Mind Actions

-(IBAction)showWhatsOnYourMind:(id)sender{
    
    [self.view endEditing:YES];
    if (strDescription.length)[self fadeInActiveBox];
    [self toggleAllQuestionBoxesFrom:eTypeEvent];
    [self.view layoutIfNeeded];
    [self resetAllQuestionBoxes];
    float answerHeight = 15;
    float feelViewHeight = 40;
    float reactionViewHeight = 40;
    float goalViewHeight = 40;
    float actionViewHeight = 40;
    float emotionViewHeight = 40;
    float postViewHeight = 40;
    
    float parentHeight = self.view.frame.size.height - 65;
    
    BOOL isDescAvailable = false;
    BOOL isFeelAvailable = false;
    BOOL isEmotionAvailable = false;
    BOOL isReactionAvailable = false;
    BOOL isGoalAvailable = false;
    BOOL isActionAvailable = false;
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEvent]]) isDescAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) isFeelAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) isEmotionAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) isReactionAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isGoalAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isActionAvailable = true;
    
    if (isDescOpen) {
        
        
        [UIView animateWithDuration:.5f animations:^{
            vwMediaAttachmnt.alpha = 0;
        } completion:^(BOOL finished) {
        }];
        
        [self.view endEditing:YES];
        descPanelHeight.constant = EachQuestionBlock;
        questionHlderTop.constant = 0;
      
        
        if (isDescAvailable) {
            descPanelHeight.constant = EachQuestionBlock + answerHeight;
          //  questionHlderTop.constant = parentHeight - feelViewHeight - reactionViewHeight - goalViewHeight - (emotionViewHeight + answerHeight);
        }
        
        if (isEmotionAvailable) {
          //  emotionPanelHeight.constant = EachQuestionBlock + answerHeight;
           // questionHlderTop.constant = parentHeight - feelViewHeight - reactionViewHeight - goalViewHeight - (emotionViewHeight + answerHeight);
        }
        if (isReactionAvailable) {
           // questionHlderTop.constant -= (answerHeight);
        }
        if (isGoalAvailable) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (isActionAvailable) {
           // questionHlderTop.constant -= (EachQuestionBlock) ;
        }
        
        if (selectedActions.count) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (!selectedActions.count) {
            if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
              //  questionHlderTop.constant -= (answerHeight) ;
            }
        }
        
        if (isCycleCompleted) {
           // questionHlderTop.constant -= postViewHeight;
        }
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
    }else{
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        
        [UIView animateWithDuration:.5f animations:^{
            vwMediaAttachmnt.alpha = 1;
        } completion:^(BOOL finished) {
        }];
        
        questionHlderTop.constant = 0;
        descPanelHeight.constant = parentHeight - feelViewHeight - emotionViewHeight - reactionViewHeight - goalViewHeight ;
       
        if (isDescAvailable) {
            descPanelHeight.constant = parentHeight;
            descPanelHeight.constant = parentHeight - EachQuestionBlock;
        }
        
        if (isFeelAvailable) {
            
            descPanelHeight.constant -=  EachQuestionBlock;
            descPanelHeight.constant -= (answerHeight) ;
        }
        if (isEmotionAvailable) {
            
            descPanelHeight.constant -= EachQuestionBlock;
            descPanelHeight.constant -= (answerHeight) ;
        }
        if (isReactionAvailable) {
            descPanelHeight.constant -= EachQuestionBlock;
            descPanelHeight.constant -= (answerHeight);
        }
        if (isGoalAvailable) {
            
            descPanelHeight.constant -= actionViewHeight ;
            descPanelHeight.constant -= (answerHeight) ;
            
        }
        if (!isGoalAvailable) {
            if ([lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
               descPanelHeight.constant -= (answerHeight) ;
            }
        }
        
        if (selectedActions.count) {
            descPanelHeight.constant -= (answerHeight) ;
        }
        if (!selectedActions.count) {
            if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
                descPanelHeight.constant -= (answerHeight) ;
            }
        }
        
        if (isCycleCompleted) {
            descPanelHeight.constant -= postViewHeight;
        }
    }
    
    
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                       //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                         isDescOpen = !isDescOpen;
                     }];
    
    
    
    
    
}

-(void)closeAllOtherQuestionsIfNeededFrmDescription{
    
    vwRateView.hidden = false;
    vwEmotionView.hidden = true;
    vwReactionView.hidden = true;
    vwGoalView.hidden = true;
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]])  vwEmotionView.hidden = false;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive ]])   vwReactionView.hidden = false;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams ]])   vwGoalView.hidden = false;
        
    
}


#pragma mark - RATE YOUR FEEL Actions

-(void)showRateSelection{
    
    vwFeelSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourFeel" owner:self options:nil] objectAtIndex:0];
    vwFeelSelection.translatesAutoresizingMaskIntoConstraints = NO;
    vwFeelSelection.delegate = self;
    [self.view addSubview:vwFeelSelection];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[vwFeelSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwFeelSelection)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwFeelSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwFeelSelection)]];
    [vwFeelSelection showSelectionPopUp];
    
    
}

-(void)selectYourFeelingPopUpCloseAppplied{
    
    [vwFeelSelection removeFromSuperview];
    vwFeelSelection.delegate = nil;
    vwFeelSelection = nil;
}

-(void)feelingsSelectedWithEmotionType:(NSInteger)emotionType{
    
    selectedFeelValue = emotionType;
    [dictSelectedSteps setObject:[NSNumber numberWithInteger:emotionType] forKey:[NSNumber numberWithInteger:eTypeFeel]];
    
}

-(IBAction)showRateOptions:(id)sender{
    
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEvent]]) return;
    [self toggleAllQuestionBoxesFrom:eTypeFeel];
    [self configureStarRating];
    [self.view layoutIfNeeded];
    [self resetAllQuestionBoxes];
    
    float parentHeight = self.view.frame.size.height - 65;
    float answerHeight = 15;
    float feelViewHeight = 40;
    float emotionViewHeight = 40;
    float reactionViewHeight = 40;
    float goalViewHeight = 40;
     float postViewHeight = 40;
    
    BOOL isFeelAvailable = false;
    BOOL isEmotionAvailable = false;
    BOOL isReactionAvailable = false;
    BOOL isGoalAvailable = false;
    BOOL isActionAvailable = false;
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) isFeelAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) isEmotionAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) isReactionAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isGoalAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isActionAvailable = true;
    
    if (isRatePopOpen) {
        
        ratePanelHeight.constant = feelViewHeight;
       // questionHlderTop.constant = parentHeight -  emotionViewHeight - reactionViewHeight - goalViewHeight - feelViewHeight ;
      // questionHlderTop.constant = 0;
        
        if (isFeelAvailable) {
           // questionHlderTop.constant = parentHeight - emotionViewHeight - reactionViewHeight - goalViewHeight - (feelViewHeight + answerHeight);
            ratePanelHeight.constant = feelViewHeight + answerHeight;
        }
        
        if (isEmotionAvailable) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (isReactionAvailable) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (isGoalAvailable) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        
        if (!isGoalAvailable) {
            if ([lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
               // questionHlderTop.constant -= (answerHeight) ;
            }
        }
        if (isActionAvailable) {
            //questionHlderTop.constant -= (EachQuestionBlock) ;
        }
        if (selectedActions.count) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (!selectedActions.count) {
            if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
               // questionHlderTop.constant -= (answerHeight) ;
            }
        }
        
        if (isCycleCompleted) {
            //questionHlderTop.constant -= postViewHeight;
        }
        
        
    }else{
        
        ratePanelHeight.constant = EachQuestionBlock + ExpanedAnswerBlock;
       // questionHlderTop.constant = parentHeight -  emotionViewHeight - reactionViewHeight - goalViewHeight - (EachQuestionBlock + ExpanedAnswerBlock);
        
        if (isFeelAvailable) {
            //questionHlderTop.constant = parentHeight - emotionViewHeight - reactionViewHeight - goalViewHeight - (feelViewHeight + ExpanedAnswerBlock) ;
            ratePanelHeight.constant = feelViewHeight + ExpanedAnswerBlock;
        }
        
        if (isEmotionAvailable) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (isReactionAvailable) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (isGoalAvailable) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (!isGoalAvailable) {
            if ([lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
                //questionHlderTop.constant -= (answerHeight) ;
            }
        }
        if (isActionAvailable) {
           // questionHlderTop.constant -= (EachQuestionBlock) ;
        }
        if (selectedActions.count) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (!selectedActions.count) {
            if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
               // questionHlderTop.constant -= (answerHeight) ;
            }
        }
        if (isCycleCompleted) {
            //questionHlderTop.constant -= postViewHeight;
        }

        
    }
    
    isRatePopOpen = !isRatePopOpen;
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
    
    
    
}

-(void)configureStarRating{
    
    rateStar.backgroundColor  = [UIColor whiteColor];
    rateStar.starImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    rateStar.starHighlightedImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    rateStar.maxRating = 5.0;
    rateStar.delegate = self;
    rateStar.horizontalMargin = 15.0;
    rateStar.editable=YES;
    rateStar.rating= 0;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) rateStar.rating = selectedFeelValue;
    
    rateStar.displayMode=EDStarRatingDisplayFull;
    [rateStar  setNeedsDisplay];
}

-(void)showRateSelectionWithRating:(float)rating{
    
    rateQuestonTop.constant = -3;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                     }];
    imgRateValue.hidden = false;
    if (rating < arrDriveImages.count) {
        imgRateValue.image = [UIImage imageNamed:[arrDriveImages objectAtIndex:rating]];
        
    }
    [self showRateOptions:nil];
    [self fadeInActiveBox];
  }


#pragma mark - SELECT YOUR EMOTIONS Actions

-(IBAction)showEmotionsOptions:(id)sender{
    
    vwEmotionView.hidden = false;
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) return;
    
    [self toggleAllQuestionBoxesFrom:eTypeEmotion];
    [self.view layoutIfNeeded];
    [self resetAllQuestionBoxes];
    
    float answerHeight = 15;
    float descViewHeight = 50;
    float feelViewHeight = 50;
    float reactionViewHeight = 40;
    float goalViewHeight = 40;
    float actionViewHeight = 40;
    float emotionViewHeight = 40;
    float postViewHeight = 40;
    
    float parentHeight = self.view.frame.size.height - 65;
    
    BOOL isEmotionAvailable = false;
    BOOL isReactionAvailable = false;
    BOOL isGoalAvailable = false;
    BOOL isActionAvailable = false;
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) isEmotionAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) isReactionAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isGoalAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isActionAvailable = true;
    
    if (isEmotionPopOpen) {
        
        questionHlderTop.constant = 0;
        emotionPanelHeight.constant = EachQuestionBlock;
       // questionHlderTop.constant = parentHeight - feelViewHeight - reactionViewHeight - goalViewHeight - emotionViewHeight;
        
        if (isEmotionAvailable) {
            emotionPanelHeight.constant = EachQuestionBlock + answerHeight;
            //questionHlderTop.constant = parentHeight - feelViewHeight - reactionViewHeight - goalViewHeight - (emotionViewHeight + answerHeight);
        }
        if (isReactionAvailable) {
            //questionHlderTop.constant -= (answerHeight);
        }
        if (isGoalAvailable) {
            //questionHlderTop.constant -= (answerHeight) ;
        }
        if (isActionAvailable) {
            //questionHlderTop.constant -= (EachQuestionBlock) ;
        }
        
        if (selectedActions.count) {
            //questionHlderTop.constant -= (answerHeight) ;
        }
        if (!selectedActions.count) {
            if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
                //questionHlderTop.constant -= (answerHeight) ;
            }
        }
        
        if (isCycleCompleted) {
            //questionHlderTop.constant -= postViewHeight;
        }
       
        
    }else{
        
        questionHlderTop.constant = -(2*EachQuestionBlock + 2 * answerHeight);
        //emotionPanelHeight.constant = parentHeight - descViewHeight - feelViewHeight - reactionViewHeight - goalViewHeight ;
        emotionPanelHeight.constant = parentHeight;
        /*
        
        if (isReactionAvailable) {
           emotionPanelHeight.constant -= (answerHeight);
        }
        if (isGoalAvailable) {
            
          emotionPanelHeight.constant -= actionViewHeight ;
          emotionPanelHeight.constant -= (answerHeight) ;
        
        }
        if (!isGoalAvailable) {
            if ([lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
                emotionPanelHeight.constant -= (answerHeight) ;
            }
        }
       
        if (selectedActions.count) {
          emotionPanelHeight.constant -= (answerHeight) ;
        }
        if (!selectedActions.count) {
            if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
                emotionPanelHeight.constant -= (answerHeight) ;
            }
        }*/
        
        if (isCycleCompleted) {
           emotionPanelHeight.constant -= postViewHeight;
        }
        
    }
    
    
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         if (!isEmotionPopOpen) [self showEmotionSelection];
                          isEmotionPopOpen = !isEmotionPopOpen;
                     }];
   
  
    
    
}
-(void)showEmotionSelection{
    
    vwEmotionSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourEmotion" owner:self options:nil] objectAtIndex:0];
    vwEmotionSelection.translatesAutoresizingMaskIntoConstraints = NO;
    vwEmotionSelection.delegate = self;
    vwEmotionSelection.selectedEmotionID = selectedEmotionValue;
    [emotionListHolder addSubview:vwEmotionSelection];
    [emotionListHolder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[vwEmotionSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwEmotionSelection)]];
    [emotionListHolder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwEmotionSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwEmotionSelection)]];
    [vwEmotionSelection showSelectionPopUp];
    
}

-(void)emotionSelectedWithEmotionTitle:(NSString*)emotionTitle emotionID:(NSInteger)emotionID;{
    
    lblEmotionSelected.text = emotionTitle;
    selectedEmotionValue = emotionID;
    selectedEmotionTitle = emotionTitle;
    [dictSelectedSteps setObject:[NSNumber numberWithInteger:emotionID] forKey:[NSNumber numberWithInteger:eTypeEmotion]];


    emotionQuestonTop.constant = -3;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                     }];
    [self resetAllQuestionBoxes];
   
    [self fadeInActiveBox];
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) [self showReactionsOptions:nil]; else  [self showEmotionsOptions:nil];
    
    
}


-(void)SelectYourEmotionPopUpCloseAppplied{
    
    [vwEmotionSelection removeFromSuperview];
    vwEmotionSelection.delegate = nil;
    vwEmotionSelection = nil;
}


#pragma mark - SELECT YOUR DRIVE Actions

-(void)showDriveSelection{
    
    vwDriveSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourDrive" owner:self options:nil] objectAtIndex:0];
    vwDriveSelection.translatesAutoresizingMaskIntoConstraints = NO;
    vwDriveSelection.delegate = self;
    [self.view addSubview:vwDriveSelection];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[vwDriveSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwDriveSelection)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwDriveSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwDriveSelection)]];
    [vwDriveSelection showSelectionPopUp];
    
    
}

-(void)selectYourDrivePopUpCloseAppplied{
    
    [vwDriveSelection removeFromSuperview];
    vwDriveSelection.delegate = nil;
    vwDriveSelection = nil;
}

-(void)driveSelectedWithEmotionType:(NSInteger)emotionType{
    
    selectedDriveValue = emotionType;
    [dictSelectedSteps setObject:[NSNumber numberWithInteger:emotionType] forKey:[NSNumber numberWithInteger:eTypeDrive]];
    
    
}

-(IBAction)showReactionsOptions:(id)sender{
    
    vwReactionView.hidden = false;
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) return;
    
    [self toggleAllQuestionBoxesFrom:eTypeDrive];
    [self configureReactionRating];
    [self.view layoutIfNeeded];
    [self resetAllQuestionBoxes];
    
    float parentHeight = self.view.frame.size.height - 65;
    float answerHeight = 15;
    float feelViewHeight = 50;
    float emotionViewHeight = 50;
    float reactionViewHeight = 40;
    float goalViewHeight = 40;
     float postViewHeight = 40;
    
    
    BOOL isReactionAvailable = false;
    BOOL isGoalAvailable = false;
    BOOL isActionAvailable = false;
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) isReactionAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isGoalAvailable = true;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isActionAvailable = true;
    
    if (isReactionPopOpen) {
        
        reactionPanelHeight.constant = reactionViewHeight;
        //questionHlderTop.constant = parentHeight -  emotionViewHeight - feelViewHeight - goalViewHeight - reactionViewHeight ;
        
        if (isReactionAvailable) {
            //questionHlderTop.constant = parentHeight - emotionViewHeight - feelViewHeight - goalViewHeight - (reactionViewHeight + answerHeight);
            reactionPanelHeight.constant = reactionViewHeight + answerHeight;
        }
        if (isGoalAvailable) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        
        if (!isGoalAvailable) {
            if ([lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
                //questionHlderTop.constant -= (answerHeight) ;
            }
        }
        
        if (isActionAvailable) {
            //questionHlderTop.constant -= (EachQuestionBlock) ;
        }
        if (selectedActions.count) {
           // questionHlderTop.constant -= (answerHeight) ;
        }
        if (!selectedActions.count) {
            if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
                //questionHlderTop.constant -= (answerHeight) ;
            }
        }
        
        if (isCycleCompleted) {
           // questionHlderTop.constant -= postViewHeight;
        }
        
    }else{
        
        reactionPanelHeight.constant = EachQuestionBlock + ExpanedAnswerBlock;
        //questionHlderTop.constant = parentHeight -  emotionViewHeight - feelViewHeight - goalViewHeight - (EachQuestionBlock + ExpanedAnswerBlock);
        
        if (isReactionAvailable) {
            //questionHlderTop.constant = parentHeight - emotionViewHeight - feelViewHeight - goalViewHeight - (reactionViewHeight + ExpanedAnswerBlock) ;
            reactionPanelHeight.constant = reactionViewHeight + ExpanedAnswerBlock;
        }
        if (isGoalAvailable) {
            //questionHlderTop.constant -= (answerHeight) ;
        }
        if (!isGoalAvailable) {
            if ([lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
                //questionHlderTop.constant -= (answerHeight) ;
            }
        }
        
        if (isActionAvailable) {
            //questionHlderTop.constant -= (EachQuestionBlock) ;
        }
        if (selectedActions.count) {
            //questionHlderTop.constant -= (answerHeight) ;
        }
        if (!selectedActions.count) {
            if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
                //questionHlderTop.constant -= (answerHeight) ;
            }
        }
        if (isCycleCompleted) {
            //questionHlderTop.constant -= postViewHeight;
        }
    }
    
    isReactionPopOpen = !isReactionPopOpen;
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
    
    
}
-(void)configureReactionRating{
    
    reactionStar.backgroundColor  = [UIColor whiteColor];
    reactionStar.starImage = [UIImage imageNamed:@"star-template"];
    reactionStar.starHighlightedImage = [UIImage imageNamed:@"star-highlighted-template"];
    reactionStar.maxRating = 5.0;
    reactionStar.delegate = self;
    reactionStar.horizontalMargin = 15.0;
    reactionStar.editable=YES;
    reactionStar.rating = 0;
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) reactionStar.rating = selectedDriveValue;
    reactionStar.displayMode=EDStarRatingDisplayFull;
    [reactionStar  setNeedsDisplay];
}

-(void)showReactionSelectionWithRating:(float)rating{
    
    reactionQuestonTop.constant = -3;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                     }];
    imgReactionValue.hidden = false;
    if (rating < arrDriveImages.count) {
        imgReactionValue.image = [UIImage imageNamed:[arrDriveImages objectAtIndex:rating]];
    }
    [self showReactionsOptions:nil];
    [self fadeInActiveBox];
}



#pragma mark - SELECT YOUR GOALS and DREAMS Actions

-(IBAction)showGoalsList:(id)sender{
    
    vwGoalView.hidden = false;
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) return;
    [self toggleAllQuestionBoxesFrom:eTypeGoalsAndDreams];
    [self.view layoutIfNeeded];
    [self resetAllQuestionBoxes];
    float answerHeight = 15;
    float descViewHeight = 50;
    float feelViewHeight = 50;
    float reactionViewHeight = 50;
    float emotionViewHeight = 50;
    float goalViewHeight = 40;
    float actionViewHeight = 40;
    float postViewHeight = 40;
    if(isSkipApplied) actionViewHeight = 0;
    
    float parentHeight = self.view.frame.size.height - 65;
    
    BOOL isGoalAvailable = false;
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) isGoalAvailable = true;
    if ([lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
        isGoalAvailable = true;
    }
    
    if (isgoalPopOpen) {
        
        goalPanelHeight.constant = goalViewHeight;
        questionHlderTop.constant = 0;
       actionViewPanel.hidden = true;
       vwGoalBottomBorder.hidden = false;
        vwActionBottomBorder.hidden = true;
        if (isGoalAvailable) {
            goalPanelHeight.constant = goalViewHeight + answerHeight;
            //questionHlderTop.constant = parentHeight - feelViewHeight - reactionViewHeight - emotionViewHeight - (goalViewHeight + answerHeight) - actionViewHeight;
            if (selectedActions.count) {
                //questionHlderTop.constant -= (answerHeight) ;
            }
            
            if (![lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
                 actionViewPanel.hidden = false;
                vwGoalBottomBorder.hidden = true;
                vwActionBottomBorder.hidden = false;
            }
           
            
        }
        if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
            //questionHlderTop.constant -= (answerHeight) ;
        }
        
        if (isCycleCompleted) {
           // questionHlderTop.constant -= postViewHeight;
        }
        
       
    }else{
        
        questionHlderTop.constant = -(4*EachQuestionBlock + 4 * answerHeight);
       // goalPanelHeight.constant = parentHeight - descViewHeight - feelViewHeight - reactionViewHeight - emotionViewHeight ;
        goalPanelHeight.constant = parentHeight;
        if (isGoalAvailable) {
                
            //goalPanelHeight.constant = parentHeight - descViewHeight - feelViewHeight - reactionViewHeight - emotionViewHeight - actionViewHeight;
            if (selectedActions.count) {
                //goalPanelHeight.constant -= (answerHeight) ;
            }
            
        }
        if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
            //goalPanelHeight.constant -= (answerHeight) ;
        }
        
        if (isCycleCompleted) {
            goalPanelHeight.constant -= postViewHeight;
        }
       
        
        
    }
    
    
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         if (!isgoalPopOpen) [self showGoalsAndDreamsSelection];
                         isgoalPopOpen = !isgoalPopOpen;
                     }];
    
    
}

-(void)showGoalsAndDreamsSelection{
    
   
    vwGoalsSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourGoalsAndDreams" owner:self options:nil] objectAtIndex:0];
    vwGoalsSelection.translatesAutoresizingMaskIntoConstraints = NO;
    vwGoalsSelection.delegate = self;
    vwGoalsSelection.selectedGoalID = selectedGoalsValue;
    [goalListHolder addSubview:vwGoalsSelection];
    [goalListHolder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[vwGoalsSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwGoalsSelection)]];
    [goalListHolder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwGoalsSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwGoalsSelection)]];
    [vwGoalsSelection showSelectionPopUp];
    
}



-(void)goalsAndDreamsSelectedWithTitle:(NSString*)title goalId:(NSInteger)goalsId{
    
    actionPanelHeight.constant = 40;
    actionQuestonTop.constant = 0;
    isCycleCompleted = false;
    imgActionTick.alpha = 0;
    btnNextActin.hidden = false;
    
    [UIView animateWithDuration:.3f animations:^{
        vwDateView.alpha = 0;
    } completion:^(BOOL finished) {
    }];
    
    isSkipApplied = false;
    selectedActionTitle = @"";
    selectedGoalsTitle = title;
    selectedGoalsValue = goalsId;
    [dictSelectedSteps setObject:[NSNumber numberWithInteger:goalsId] forKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]];
    selectedActionTitle = @"";
    [dictSelectedSteps removeObjectForKey:[NSNumber numberWithInt:eTypeAction]];
    lblGoalSelected.text = title;
    goalQuestonTop.constant = 0;
    if (title.length) {
        goalQuestonTop.constant = -3;
    }
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                     }];
    [self resetActions];
    [self fadeInActiveBox];
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeAction]]) [self showActionsList:nil]; else  [self showGoalsList:nil];
    
    
}

-(void)skipButtonApplied{
    
    [UIView animateWithDuration:.3f animations:^{
        vwDateView.alpha = 1;
    } completion:^(BOOL finished) {
    }];
    
    actionViewPanel.hidden = true;
    vwGoalBottomBorder.hidden = false;
    vwActionBottomBorder.hidden = true;
    isSkipApplied = true;
    isCycleCompleted = true;
    selectedActionTitle = @"";
    selectedGoalsTitle = @"";
    lblGoalSelected.text = @"No Goal selected";
    lblActionSelected.text = @"";
    goalQuestonTop.constant = -3;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                     }];
    [dictSelectedSteps removeObjectForKey:[NSNumber numberWithInt:eTypeAction]];
    [dictSelectedSteps removeObjectForKey:[NSNumber numberWithInt:eTypeGoalsAndDreams]];
    selectedGoalsValue = -1;
    selectedActions = nil;
    [self shouldeablePostButton:YES];
    [self showGoalsList:nil];
    [self fadeInActiveBox];
}



-(void)selectYourGoalsAndDreamsPopUpCloseAppplied{
    
    [vwGoalsSelection removeFromSuperview];
    vwGoalsSelection.delegate = nil;
    vwGoalsSelection = nil;
}

#pragma mark - SELECT YOUR ACTIONS methods

-(IBAction)showActionsList:(id)sender{
    
    [self toggleAllQuestionBoxesFrom:eTypeAction];
    [self.view layoutIfNeeded];
    [self resetAllQuestionBoxes];
    float answerHeight = 15;
    float descViewHeight = 50;
    float feelViewHeight = 50;
    float reactionViewHeight = 50;
    float emotionViewHeight = 50;
    float goalViewHeight = 50;
    float actionViewHeight = 40;
    float postViewHeight = 40;
    
    float parentHeight = self.view.frame.size.height - 65;
    
    BOOL isActionAvailable = false;
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeAction]]) isActionAvailable = true;
    
    if (isActionPopOpen) {
        questionHlderTop.constant = 0;
        actionPanelHeight.constant = actionViewHeight;
        //questionHlderTop.constant = parentHeight - feelViewHeight - reactionViewHeight - goalViewHeight - emotionViewHeight - actionViewHeight ;
        
        if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
            isActionAvailable = true;
        }
        
        if (isActionAvailable) {
            actionPanelHeight.constant = actionViewHeight + answerHeight;
           // questionHlderTop.constant = parentHeight - feelViewHeight - reactionViewHeight - emotionViewHeight - goalViewHeight - (actionViewHeight + answerHeight);
        }
        if (isCycleCompleted) {
            //questionHlderTop.constant -= postViewHeight;
        }
        
    }else{
        
        questionHlderTop.constant = -(5*EachQuestionBlock + 5 * answerHeight);
       // actionPanelHeight.constant = parentHeight - descViewHeight - feelViewHeight - reactionViewHeight - emotionViewHeight - goalViewHeight ;
        actionPanelHeight.constant = parentHeight;
        
        if (isCycleCompleted) {
            actionPanelHeight.constant -= postViewHeight;
        }
        
    }
    
    
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         if (!isActionPopOpen) [self showActionSelection];
                         isActionPopOpen = !isActionPopOpen;
                     }];
    
    
}

-(void)showActionSelection{
    
    
    vwActions = [[[NSBundle mainBundle] loadNibNamed:@"SelectActions" owner:self options:nil] objectAtIndex:0];
    vwActions.translatesAutoresizingMaskIntoConstraints = NO;
    vwActions.delegate = self;
    vwActions.goalID = selectedGoalsValue;
    vwActions.selectedActions = selectedActions;
    [actionListHolder addSubview:vwActions];
    [actionListHolder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[vwActions]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwActions)]];
    [actionListHolder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwActions]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwActions)]];
    [vwActions showSelectionPopUp];
    
    
}

-(void)selectYourActionsPopUpCloseAppplied{
    
    [vwActions removeFromSuperview];
    vwActions.delegate = nil;
    vwActions = nil;
}

-(void)actionsSelectedWithTitle:(NSString*)title actionIDs:(NSDictionary*)selectedAcitons{
    
    btnNextActin.hidden = true;
    imgActionTick.alpha = 1;
    
    [UIView animateWithDuration:.3f animations:^{
        vwDateView.alpha = 1;
    } completion:^(BOOL finished) {
    }];
    [dictSuccessSteps setObject:[NSNumber numberWithBool:YES] forKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]];
    isCycleCompleted = true;
    selectedActionTitle = title;
    selectedActions = selectedAcitons;
    [dictSelectedSteps removeObjectForKey:[NSNumber numberWithInteger:eTypeAction]];
    if (selectedAcitons.count) {
         [dictSelectedSteps setObject:selectedAcitons forKey:[NSNumber numberWithInteger:eTypeAction]];
    }
    if (title.length) {
        lblActionSelected.text = title;
    }else{
        lblActionSelected.text = @"No Action selected";
    }
    
    actionQuestonTop.constant = -3;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                     }];
   
    [self showActionsList:nil];
    [self shouldeablePostButton:YES];
    [self fadeInActiveBox];
    
}

-(void)shouldeablePostButton:(BOOL)shouldEnable{
    
    if (!isCycleCompleted) {
        btnPost.alpha = 0.3;
        btnPost.enabled = false;
        return;
    }
    if (shouldEnable) {
        btnPost.alpha = 1;
        btnPost.enabled = true;
    }else{
        btnPost.alpha = 0.3;
        btnPost.enabled = false;
    }
}

#pragma mark - Star Rating Protocols Methods

-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating{
    
    if (control == rateStar){
        
        selectedFeelValue = rating;
        [dictSelectedSteps setObject:[NSNumber numberWithInteger:rating] forKey:[NSNumber numberWithInteger:eTypeFeel]];
        [self showRateSelectionWithRating:rating - 1];
        if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) [self showEmotionsOptions:nil];;
        
    }
    if (control == reactionStar){
        
        selectedDriveValue = rating;
        [dictSelectedSteps setObject:[NSNumber numberWithInteger:rating] forKey:[NSNumber numberWithInteger:eTypeDrive]];
        [self showReactionSelectionWithRating:rating -  1];
        if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) [self showGoalsList:nil];
    }
    
    
    
}



#pragma mark - Date Methods

-(IBAction)showDatePicker:(id)sender{
    
    datePicker.maximumDate = [NSDate date];
    
    datePicker.date = [NSDate date];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"d MMM,yyyy h:mm a"];
    strDate = [dateformater stringFromDate:datePicker.date];
    [UIView animateWithDuration:.4 animations:^{
       vwPickerOverLay.alpha = 1.0;
    } completion:^(BOOL finished) {
       
    }];
}

-(IBAction)getSelectedDate{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"d MMM,yyyy h:mm a"];
    strDate = [dateformater stringFromDate:datePicker.date];
}

-(IBAction)hidePicker:(id)sender{
    
    [UIView animateWithDuration:.4 animations:^{
        vwPickerOverLay.alpha = 0.0;
    } completion:^(BOOL finished) {
        
    }];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"d MMM,yyyy h:mm a"];
    strDate = [dateformater stringFromDate:datePicker.date];
    lblDate.text = strDate;
}

-(void)getCurrentDate{
    
    datePicker.date = [NSDate date];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"d MMM,yyyy h:mm a"];
    strDate = [dateformater stringFromDate:[NSDate date]];
    lblDate.text = strDate;
}

#pragma mark - Floating HelP Menu

-(IBAction)enableFloaitngMenu {
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [btnHelp addGestureRecognizer:panRecognizer];
}

-(void)move:(UIPanGestureRecognizer*)sender {
    [self.view bringSubviewToFront:sender.view];
    CGPoint translatedPoint = [sender translationInView:sender.view.superview];
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        firstX = sender.view.center.x;
        firstY = sender.view.center.y;
    }
    
    translatedPoint = CGPointMake(sender.view.center.x+translatedPoint.x, sender.view.center.y+translatedPoint.y);
    
    [sender.view setCenter:translatedPoint];
    [sender setTranslation:CGPointZero inView:sender.view];
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        CGFloat velocityX = 0;
        CGFloat velocityY = 0;
        
        CGFloat finalX = translatedPoint.x + velocityX;
        CGFloat finalY = translatedPoint.y + velocityY;// translatedPoint.y + (.35*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
        
        if (finalX < 0) {
            finalX = btnHelp.frame.size.width / 2;
        } else if (finalX + 0 > self.view.frame.size.width) {
            finalX = self.view.frame.size.width - btnHelp.frame.size.width / 2;
        }
        
        if (finalY < 20) { // to avoid status bar
            finalY = 20;;
        } else if (finalY > self.view.frame.size.height) {
            finalY = self.view.frame.size.height - btnHelp.frame.size.width / 2;;
        }
        
        [btnHelp setCenter:CGPointMake(finalX, finalY)];
        
        CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;
        
        
       
       // [self.view layoutIfNeeded];
        [UIView animateWithDuration:animationDuration
                              delay:0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             btnHelpLeft.constant = btnHelp.frame.origin.x;
                             btnHelpTop.constant = btnHelp.frame.origin.y;
                            [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished){
                         }];
        
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDuration:animationDuration];
//        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//       // [[sender view] setCenter:CGPointMake(finalX, finalY)];
//        btnHelpLeft.constant = finalX;
//        btnHelpTop.constant = finalY;
//         [self.view layoutIfNeeded];
//        [UIView commitAnimations];
    }
}

#pragma mark - Keyboard Manager

-(void) keyboardWillShow:(NSNotification *)note{
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    float accessoryHeight = 55;
    if (strDescription.length) {
        accessoryHeight += 5;
    }
    float y = self.view.bounds.size.height  - (keyboardBounds.size.height + accessoryHeight);
    descPanelHeight.constant = y;
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         //  if (!isEmotionPopOpen) [self showWhatsOnYourMind:nil];
                     }];
    
    
    
    // animations settings
}

#pragma mark - GENERIC Actions

-(void)resetActions{
    
    lblActionSelected.text = @"";
    selectedActionTitle = @"";
    [dictSelectedSteps removeObjectForKey:[NSNumber numberWithInt:eTypeAction]];
    selectedActions = nil;
}

-(void)fadeOutAllInactiveBoxes{
    
    imgRateIcon.alpha = 0.5;
    imgRateTick.alpha = 0;
    lblRateQstn.alpha = 0.5;
    btnNextFeel .alpha = 0.5;;
    lblRateQstn.font = [UIFont fontWithName:CommonFont_New size:14];
    
    imgEmotionTick.alpha = 0;
    lblEmotionQstn.alpha = 0.5;
    btnNextEmotion.alpha = 0.5;;
    imgEmoiotnIcon.alpha = 0.5;
    lblEmotionQstn.font = [UIFont fontWithName:CommonFont_New size:14];
    
    imgReacitonTick.alpha = 0;
    lblReacitonQstn.alpha = 0.5;
    btnNextReaciton.alpha = 0.5;
    imgReacitonIcon.alpha = 0.5;
    lblReacitonQstn.font = [UIFont fontWithName:CommonFont_New size:14];
    
    imgGoalTick.alpha = 0;
    lblGoalQstn.alpha = 0.5;
    btnNextGoal.alpha = 0.5;
    imgGoalIcon.alpha = 0.5;
    lblGoalQstn.font = [UIFont fontWithName:CommonFont_New size:14];
    
    imgActionTick.alpha = 0;
    lblActionQstn.font = [UIFont fontWithName:CommonFont_New size:14];

}

-(void)fadeInActiveBox{
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEvent]] || lblDescriptionSelected.text.length){
        
        lblDescQstn.font = [UIFont fontWithName:CommonFont_New size:14];
        btnNextDesc.hidden = true;
        lblRateQstn.alpha = 1;
        btnNextFeel.alpha = 1;;
        imgRateIcon.alpha = 1;
        imgDescTick.alpha = 1;
        if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) lblRateQstn.font = [UIFont fontWithName:CommonFontBold_New size:14];
    }
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]){
        
        lblRateQstn.font = [UIFont fontWithName:CommonFont_New size:14];
        btnNextFeel.hidden = true;
        lblEmotionQstn.alpha = 1;
        btnNextEmotion.alpha = 1;;
        imgEmoiotnIcon.alpha = 1;
        imgRateTick.alpha = 1;
        if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) lblEmotionQstn.font = [UIFont fontWithName:CommonFontBold_New size:14];
    }
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]){
        
        lblEmotionQstn.font = [UIFont fontWithName:CommonFont_New size:14];
        lblReacitonQstn.alpha = 1;
        btnNextReaciton.alpha = 1;
        imgReacitonIcon.alpha = 1;
        btnNextEmotion.hidden = true;
        imgEmotionTick.alpha = 1;
        if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) lblReacitonQstn.font = [UIFont fontWithName:CommonFontBold_New size:14];
    }
    
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]){
        
        lblReacitonQstn.font = [UIFont fontWithName:CommonFont_New size:14];
        lblGoalQstn.alpha = 1;
        btnNextGoal.alpha = 1;
        imgGoalIcon.alpha = 1;
        btnNextReaciton.hidden = true;
        imgReacitonTick.alpha = 1;
        if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) lblGoalQstn.font = [UIFont fontWithName:CommonFontBold_New size:14];
    }
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]){
        
        lblGoalQstn .font = [UIFont fontWithName:CommonFont_New size:14];
        btnNextGoal.hidden = true;
        imgGoalTick.alpha = 1;
        if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeAction]]) lblActionQstn.font = [UIFont fontWithName:CommonFontBold_New size:14];
        
    }
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) {
        
        if ([lblGoalSelected.text isEqualToString:@"No Goal selected"]) {
            lblGoalQstn.font = [UIFont fontWithName:CommonFont_New size:14];
            btnNextGoal.hidden = true;
            imgGoalTick.alpha = 1;
        }
    }
    if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeAction]]){
        
        lblActionQstn.font = [UIFont fontWithName:CommonFont_New size:14];
        btnNextActin.hidden = true;
        imgActionTick.alpha = 1;
        if ([dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeAction]]) lblActionQstn.font = [UIFont fontWithName:CommonFont_New size:14];
    }
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeAction]]){
        
        if ([lblActionSelected.text isEqualToString:@"No Action selected"]) {
            lblActionQstn.font = [UIFont fontWithName:CommonFont_New size:14];
            btnNextActin.hidden = true;
            imgActionTick.alpha = 1;
        }
        
    }
    
}


-(void)toggleAllQuestionBoxesFrom:(ESelectedMenuType)type{
    
    if (type == eTypeEvent) {
        
        if (isRatePopOpen) {
            [self showRateOptions:nil];
        }
        if (isEmotionPopOpen) {
            [self showEmotionsOptions:nil];
        }
        if (isReactionPopOpen) {
            [self showReactionsOptions:nil];
        }
        if (isgoalPopOpen) {
            [self showGoalsList:nil];
        }
        if (isActionPopOpen) {
            [self showActionsList:nil];
        }
    }
    if (type == eTypeFeel) {
        
        if (isDescOpen) {
            [self showWhatsOnYourMind:nil];
        }
        if (isEmotionPopOpen) {
            [self showEmotionsOptions:nil];
        }
        if (isReactionPopOpen) {
            [self showReactionsOptions:nil];
        }
        if (isgoalPopOpen) {
            [self showGoalsList:nil];
        }
        if (isActionPopOpen) {
            [self showActionsList:nil];
        }
    }
    if (type == eTypeEmotion) {
        
        if (isDescOpen) {
            [self showWhatsOnYourMind:nil];
        }
        if (isRatePopOpen) {
            [self showRateOptions:nil];
        }
        if (isReactionPopOpen) {
            [self showReactionsOptions:nil];
        }
        if (isgoalPopOpen) {
            [self showGoalsList:nil];
        }
        if (isActionPopOpen) {
            [self showActionsList:nil];
        }
    }
    if (type == eTypeDrive) {
        
        if (isDescOpen) {
            [self showWhatsOnYourMind:nil];
        }
        
        if (isRatePopOpen) {
            [self showRateOptions:nil];
        }
        if (isEmotionPopOpen) {
            [self showEmotionsOptions:nil];
        }
        if (isgoalPopOpen) {
            [self showGoalsList:nil];
        }
        if (isActionPopOpen) {
            [self showActionsList:nil];
        }
    }
    if (type == eTypeGoalsAndDreams) {
        
        if (isDescOpen) {
            [self showWhatsOnYourMind:nil];
        }
        
        if (isRatePopOpen) {
            [self showRateOptions:nil];
        }
        if (isEmotionPopOpen) {
            [self showEmotionsOptions:nil];
        }
        if (isReactionPopOpen) {
            [self showReactionsOptions:nil];
        }
        if (isActionPopOpen) {
            [self showActionsList:nil];
        }
    }
    if (type == eTypeAction) {
        
        if (isDescOpen) {
            [self showWhatsOnYourMind:nil];
        }
        if (isRatePopOpen) {
            [self showRateOptions:nil];
        }
        if (isEmotionPopOpen) {
            [self showEmotionsOptions:nil];
        }
        if (isReactionPopOpen) {
            [self showReactionsOptions:nil];
        }
        if (isgoalPopOpen) {
            [self showGoalsList:nil];
        }
    }

   
    
}

-(void)resetAllQuestionBoxes{
    
    [self SelectYourEmotionPopUpCloseAppplied];
    [self selectYourGoalsAndDreamsPopUpCloseAppplied];
    [self selectYourActionsPopUpCloseAppplied];
    
}

-(float)getQuestionHolderIntialPosiotion{
    
    float parentHeight = self.view.frame.size.height - 65;
    float panelHeight = TotalBlocks * EachQuestionBlock;
    float reqiuredTop = parentHeight - panelHeight;
    return reqiuredTop;
    
}

-(IBAction)showHelpPage{
    
    WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
    browser.strTitle = @"HELP";
    browser.strHelpType = @"journal";
    [self.navigationController pushViewController:browser animated:YES];
}


-(void)emotionalIntelligencePage{
    
    [self goBack:nil];
    AppDelegate *appdelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appdelegate emotionalIntelligencePage];
    
}


-(NSMutableArray*)getAllMediaFiles{
    
    NSMutableArray *arrMedias = [NSMutableArray new];
    NSString *dataPath = [Utility getJournalMediaSaveFolderPath];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    NSError* error = nil;
    // sort by creation date
    NSMutableArray* filesAndProperties = [NSMutableArray arrayWithCapacity:[directoryContent count]];
    for(NSString* file in directoryContent) {
        
        if (![file isEqualToString:@".DS_Store"]) {
            NSString* filePath = [dataPath stringByAppendingPathComponent:file];
            NSDictionary* properties = [[NSFileManager defaultManager]
                                        attributesOfItemAtPath:filePath
                                        error:&error];
            NSDate* modDate = [properties objectForKey:NSFileCreationDate];
            
            [filesAndProperties addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                           file, @"path",
                                           modDate, @"lastModDate",
                                           nil]];
            
        }
    }
    // sort using a block
    // order inverted as we want latest date first
    NSArray* sortedFiles = [filesAndProperties sortedArrayUsingComparator:
                            ^(id path1, id path2)
                            {
                                // compare
                                NSComparisonResult comp = [[path1 objectForKey:@"lastModDate"] compare:
                                                           [path2 objectForKey:@"lastModDate"]];
                                // invert ordering
                                if (comp == NSOrderedDescending) {
                                    comp = NSOrderedAscending;
                                }
                                else if(comp == NSOrderedAscending){
                                    comp = NSOrderedDescending;
                                }
                                return comp;
                            }];
    
    for (NSInteger i = sortedFiles.count - 1; i >= 0; i --) {
        NSDictionary *dict = sortedFiles[i];
        [arrMedias insertObject:[dict objectForKey:@"path" ] atIndex:0];
    }
    return arrMedias;
}


-(void)removeAllContentsInMediaFolder{
    
    NSString *dataPath = [Utility getJournalMediaSaveFolderPath];
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager removeItemAtPath:dataPath error:&error];
    if (success){
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
}

-(IBAction)submitJournals:(id)sender{
    
    [self showLoadingScreen];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"d MMM,yyyy h:mm a"];
    NSDate *dateFromString = [dateFormatter dateFromString:strDate];
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    NSInteger feelValue = 0;
    switch (selectedFeelValue) {
        case 1:
            feelValue = -2;
            break;
        case 2:
            feelValue = -1;
            break;
        case 3:
            feelValue = 0;
            break;
        case 4:
            feelValue = 1;
            break;
        case 5:
            feelValue = 2;
            break;
            
        default:
            break;
    }
    
    NSInteger driveValue = 0;
    switch (selectedDriveValue) {
        case 1:
            driveValue = -2;
            break;
        case 2:
            driveValue = -1;
            break;
        case 3:
            driveValue = 0;
            break;
        case 4:
            driveValue = 1;
            break;
        case 5:
            driveValue = 2;
            break;
            
        default:
            break;
    }
    
    [params setObject:[NSNumber numberWithInteger:feelValue] forKey:@"emotion_value"];
    [params setObject:[NSNumber numberWithInteger:selectedEmotionValue] forKey:@"emotion_id"];
    [params setObject:[NSNumber numberWithInteger:selectedEventValue] forKey:@"event_id"];
    [params setObject:[NSNumber numberWithInteger:driveValue] forKey:@"drive_value"];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    [params setObject:[NSNumber numberWithDouble:[dateFromString timeIntervalSince1970] ] forKey:@"journal_date"];
    if (selectedGoalsValue > 0) [params setObject:[NSNumber numberWithInteger:selectedGoalsValue] forKey:@"goal_id"];
    if ([selectedActions allKeys].count > 0) [params setObject:[NSNumber numberWithInteger:[[selectedActions allKeys] count]] forKey:@"action_count"];
    if ([selectedActions allKeys].count > 0){
        NSString * result = [[selectedActions allKeys] componentsJoinedByString:@","];
        [params setObject:result forKey:@"goalaction_id"];
    }
    
    
    [APIMapper createAJournelWith:[self getAllMediaFiles] description:strDescription latitude:locationCordinates.latitude longitude:locationCordinates.longitude locName:strLocationAddress address:strLocationAddress contactName:strContactName emotionAwarenssValues:params OnSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        
        if ([[responseObject objectForKey:@"code"]integerValue] == kUnauthorizedCode){
            
            if (NULL_TO_NIL([responseObject objectForKey:@"text"])) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"EMOTIONAL AWARENESS"
                                                                               message:[responseObject objectForKey:@"text"]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                                                          [delegate clearUserSessions];
                                                                          [self removeAllContentsInMediaFolder];
                                                                      }];
                [alert addAction:firstAction];
                [[self navigationController] presentViewController:alert animated:YES completion:nil];
                                
                
            }
            
        }
        else if ([[responseObject objectForKey:@"code"]integerValue] == kRequestFail){
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"EMOTIONAL AWARENESS"
                                                                           message:[responseObject objectForKey:@"text"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      
                                                                      
                                                                  }];
            [alert addAction:firstAction];
            [[self navigationController] presentViewController:alert animated:YES completion:nil];
            
        }else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"EMOTIONAL AWARENESS"
                                                                           message:[responseObject objectForKey:@"text"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      
                                                                      [self removeAllContentsInMediaFolder];
                                                                      [self emotionalIntelligencePage];
                                                                      
                                                                  }];
            [alert addAction:firstAction];
            [[self navigationController] presentViewController:alert animated:YES completion:nil];
        }
        
        
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self hideLoadingScreen];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"EMOTIONAL AWARENESS"
                                                                       message:[error localizedDescription]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                              }];
        
        [alert addAction:firstAction];
        [[self navigationController] presentViewController:alert animated:YES completion:nil];
        
    } progress:^(long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"loading...";
    hud.removeFromSuperViewOnHide = YES;
}

-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(IBAction)goBack:(id)sender{
    
    BOOL shouldRemovescreen = [self collapseOpenedSectionIfAny];
    if (shouldRemovescreen) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [[self navigationController] popViewControllerAnimated:YES];
    }
    
}

-(BOOL)collapseOpenedSectionIfAny{
    
    BOOL shouldGoBack = true;
    if (isEmotionPopOpen) {
        [self showEmotionsOptions:nil];
        shouldGoBack = false;
    }
    
    if (isgoalPopOpen) {
        [self showGoalsList:nil];
        shouldGoBack = false;
    }
    if (isActionPopOpen) {
        [self showActionsList:nil];
        shouldGoBack = false;
    }
    return shouldGoBack;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
