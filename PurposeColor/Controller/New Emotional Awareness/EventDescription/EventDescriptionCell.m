//
//  EventDescriptionCell.m
//  PurposeColor
//
//  Created by Purpose Code on 20/03/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import "EventDescriptionCell.h"
#import "Constants.h"

@implementation EventDescriptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.txtVwDescription.font = [UIFont fontWithName:CommonFont_New size:25];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
