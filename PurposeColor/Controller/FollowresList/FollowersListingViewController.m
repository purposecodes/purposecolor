//
//  GoalsAndDreamsListingViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 21/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eFollowing = 1,
    eFollow = 2
    
} eType;

typedef enum{
    
    eUserFollow = 0,
    eUserFollowPending = 1,
    eUserFollowed = 2
    
} eFollowStatus;


#define kSectionCount               1
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kHeaderHeight               0.001
#define kCellHeight                 395
#define kEmptyHeaderAndFooter       0
#define kUnAuthorized               403

#import "FollowersListingViewController.h"
#import "Constants.h"
#import "UserListTableViewCell.h"
#import "ProfilePageViewController.h"

@interface FollowersListingViewController (){
    
    IBOutlet UITableView *tableView;
    IBOutlet UIView *vwSegmentSelection;
    IBOutlet UIButton *btnFollow;
    IBOutlet UIButton *btnFollowing;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UIButton *btnCancel;
    IBOutlet NSLayoutConstraint *tableBottomConstraint;
    IBOutlet NSLayoutConstraint *rightForSearchBar;
    
    UIRefreshControl *refreshControl;
    BOOL isDataAvailable;
    BOOL isPageRefresing;
    BOOL isLoaded;
    NSInteger totalPages;
    NSInteger currentPage;
    
    NSString *strNoDataText;
    NSMutableArray *arrFiltered;
    NSMutableArray *arrFollow;
    NSMutableArray *arrFollowing;
    NSMutableArray *arrDataSource;
    
    eType EType;
   
    BOOL isCachedData;

}

@end

@implementation FollowersListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
   
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (isLoaded) return;
    isLoaded = true;
    EType = _type;
    if (_type == eFollow)
        [self performSelector:@selector(segmentChanged:) withObject:btnFollow];
    else
        [self performSelector:@selector(segmentChanged:) withObject:btnFollowing];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)setUp{

    currentPage = 1;
    arrFollow          = [NSMutableArray new];
    arrFollowing       = [NSMutableArray new];
    arrDataSource      = [NSMutableArray new];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    isDataAvailable = false;
    tableView.hidden = true;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 300;
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,tableView.bounds.size.width, 0.01f)];

    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    searchBar.tintColor = [UIColor getThemeColor];
    UITextField *searchTextField = [searchBar valueForKey:@"_searchField"];
    if ([searchTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor whiteColor];
        [searchTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"Search Users" attributes:@{NSForegroundColorAttributeName: color}]];
    }
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setFont:[UIFont fontWithName:CommonFont size:15]];
    searchBar.hidden = true;
    btnCancel.hidden = true;
    [btnCancel setTitle:@"" forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}


-(IBAction)segmentChanged:(UIButton*)sender{
    
    [arrFiltered removeAllObjects];
    [arrDataSource removeAllObjects];
    [arrFollow removeAllObjects];
    [arrFollowing removeAllObjects];
    if (sender.tag == eFollow) {
        EType = eFollow;
        _type = eFollow;
        [self loadAllFollowsListWithPageNumber:currentPage isPagination:NO];
        [self changeAnimatedSelectionToCompleted:NO];
    }else{
        EType = eFollowing;
        _type = eFollowing;
        [self loadAllFollowsListWithPageNumber:currentPage isPagination:NO];
        [self changeAnimatedSelectionToCompleted:YES];
    }
    [self configureDataSource];
    
  }

-(void)changeAnimatedSelectionToCompleted:(BOOL)toCompeleted{
    
    if (toCompeleted) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = vwSegmentSelection.frame;
            frame.origin.x = self.view.frame.size.width / 2;
            vwSegmentSelection.frame = frame;
        }completion:^(BOOL finished) {
            [btnFollow setAlpha:1];
            [btnFollowing setAlpha:0.7];
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = vwSegmentSelection.frame;
            frame.origin.x = 0;
            vwSegmentSelection.frame = frame;
        }completion:^(BOOL finished) {
            [btnFollow setAlpha:0.7];
            [btnFollowing setAlpha:1];
        }];
    }
    
}


#pragma mark - API Integration

-(void)loadAllFollowsListWithPageNumber:(NSInteger)pageNo isPagination:(BOOL)isPagination{
    
    if (!isPagination)[self showLoadingScreen];
    [APIMapper getFollowingUserList:EType forTheUser:_userID pageNo:pageNo Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        tableView.hidden = false;
        isPageRefresing = NO;
        [refreshControl endRefreshing];
        [self getUserListFrom:responseObject];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        if (error && error.localizedDescription){
            [ALToastView toastInView:self.view withText:error.localizedDescription];
            strNoDataText = error.localizedDescription;
        }
        isPageRefresing = NO;
        [refreshControl endRefreshing];
        tableView.hidden = false;
        searchBar.hidden = true;
        [self hideLoadingScreen];
        [tableView reloadData];
        
        
    }];
}

-(void)refreshData{
    
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    [self clearSearch];
    [arrFiltered removeAllObjects];
    [arrFollowing removeAllObjects];
    [arrFollow removeAllObjects];
    [arrDataSource removeAllObjects];
    [tableView reloadData];
    isPageRefresing = YES;
    [self loadAllFollowsListWithPageNumber:currentPage isPagination:NO];
    
    
}

-(void)getUserListFrom:(NSDictionary*)responds{
    
    if (NULL_TO_NIL([responds objectForKey:@"resultarray"])){
        NSArray *goals;
        if (EType == eFollow) {
            EType = eFollow;
            if (NULL_TO_NIL([responds objectForKey:@"resultarray"])) {
                goals = [responds objectForKey:@"resultarray"];
                if (goals.count) [arrFollow addObjectsFromArray:goals];
                if (arrFollow.count > 0) {
                    arrDataSource = [NSMutableArray arrayWithArray:arrFollow];
                }
            }
        }else{
            EType = eFollowing;
            if (NULL_TO_NIL([responds objectForKey:@"resultarray"])) {
                goals = [responds objectForKey:@"resultarray"];
                if (goals.count) [arrFollowing addObjectsFromArray:goals];
                if (arrFollowing.count > 0) {
                    arrDataSource = [NSMutableArray arrayWithArray:arrFollowing];
                }
            }
        }
        if (NULL_TO_NIL([responds objectForKey:@"pageCount"]))
            totalPages =  [[responds objectForKey:@"pageCount"]integerValue];
        
        if (NULL_TO_NIL([responds objectForKey:@"currentPage"]))
            currentPage =  [[responds objectForKey:@"currentPage"]integerValue];
    }else{
        
        if ([responds objectForKey:@"text"]) {
            strNoDataText = [responds objectForKey:@"text"];
        }
    }
    [self configureDataSource];
}

-(void)configureDataSource{
    
    if (EType == eFollow) {
        EType = eFollow;
        if (arrFollow.count > 0) {
            arrDataSource = [NSMutableArray arrayWithArray:arrFollow];
        }
    }else{
        
         EType = eFollowing;
        if (arrFollowing.count > 0) {
            arrDataSource = [NSMutableArray arrayWithArray:arrFollowing];
        }
    }
    arrFiltered = [NSMutableArray arrayWithArray:arrDataSource];
    isDataAvailable = false;
    if (arrDataSource.count) {
        isDataAvailable = true;
        searchBar.hidden = false;
    }
    [tableView reloadData];


}



#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    return arrFiltered.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:strNoDataText];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}



-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    static NSString *CellIdentifier = @"UserListTableViewCell";
    UserListTableViewCell *cell = (UserListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row < arrFiltered.count){
        NSDictionary *userInfo = arrFiltered[indexPath.row];
        cell.btnFollow.tag = indexPath.row;
        cell.lblName.text = [userInfo objectForKey:@"firstname"];
        cell.lblDate.text = [Utility getDaysBetweenTwoDatesWith:[[userInfo objectForKey:@"display_datetime"]doubleValue] ] ;
        [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[userInfo objectForKey:@"profileimg"]]
                      placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 
                             }];
        cell.imgVerified.hidden = true;
        if (NULL_TO_NIL([userInfo objectForKey:@"verify_status"])){
           NSInteger isVerified = [[userInfo objectForKey:@"verify_status"] integerValue];
            if (isVerified == 1) {
                cell.imgVerified.hidden = false;
            }
        }
        BOOL canFollow = true;
        if (NULL_TO_NIL([userInfo objectForKey:@"can_follow"]))
            canFollow =  [[userInfo objectForKey:@"can_follow"] boolValue];
        
        eFollowStatus followStatus = 0 ;
        if ([userInfo objectForKey:@"follow_status"])
            followStatus =  [[userInfo objectForKey:@"follow_status"] intValue];
        
         [cell.btnFollow setTitle:@"" forState:UIControlStateNormal];
         cell.btnFollow.hidden = TRUE;
        
        switch (followStatus) {
            case eUserFollowed:
                cell.btnFollow.hidden = false;
                [cell.btnFollow setEnabled:true];
                [cell.btnFollow setTitle:@"Unfollow" forState:UIControlStateNormal];
                [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
                [cell.btnFollow setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
                cell.btnFollow.layer.borderColor = [UIColor getThemeColor].CGColor;
                break;
                
            case eUserFollow:
                if (canFollow) {
                    cell.btnFollow.hidden = false;
                    [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
                    [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
                    [cell.btnFollow setEnabled:true];
                    [cell.btnFollow setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
                     cell.btnFollow.layer.borderColor = [UIColor getThemeColor].CGColor;
                }
                break;
                
            case eUserFollowPending:
                cell.btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
                cell.btnFollow.hidden = false;
                [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
                [cell.btnFollow setBackgroundColor:[UIColor lightGrayColor]];
                [cell.btnFollow setEnabled:false];
                [cell.btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                break;
                
            default:
                break;
        }
        
        if ([[userInfo objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
            [cell.btnFollow setTitle:@"" forState:UIControlStateNormal];
            cell.btnFollow.hidden = TRUE;
        }
   
   }
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeaderHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < arrFiltered.count) {
         NSDictionary *userInfo = arrFiltered[indexPath.row];
        ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.navGeneral pushViewController:profilePage animated:YES];
        profilePage.canEdit = false;
        if ([[User sharedManager].userId isEqualToString:[userInfo objectForKey:@"user_id"]]) {
             profilePage.canEdit = true;
        }
        [profilePage loadUserProfileWithUserID:[userInfo objectForKey:@"user_id"] showBackButton:YES];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
        
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            NSInteger nextPage = currentPage ;
            nextPage += 1;
            if (nextPage  <= totalPages) {
                isPageRefresing = YES;
                [self loadAllFollowsListWithPageNumber:nextPage isPagination:YES];
            }
            
        }
    }
    
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(IBAction)followButtonClickedWith:(UIButton*)btn{
    
    NSString *strCurrentFollow = btn.currentTitle;
    if ([strCurrentFollow isEqualToString:@"Unfollow"]) {
        
        UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Unfollow" message:@"Do you really want to unfollow this user?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* block = [UIAlertAction actionWithTitle:@"Unfollow" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
            [alert dismissViewControllerAnimated:YES completion:nil];
            [self changeFollowStatusWithIndex:btn.tag];
            
        }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:block];
        [alert addAction:cancel];
        [self.navigationController presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        [self changeFollowStatusWithIndex:btn.tag];
    }
    
}

-(void)changeFollowStatusWithIndex:(NSInteger)index{
    
    if (index < arrFiltered.count) {
        NSDictionary *gemDetails = arrFiltered[index];
        if (NULL_TO_NIL([gemDetails objectForKey:@"user_id"])) {
            NSString *followerID = [gemDetails objectForKey:@"user_id"];
            [self showLoadingScreen];
            [APIMapper sendFollowRequestWithUserID:[User sharedManager].userId followerID:followerID success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [self hideLoadingScreen];
                if ([[responseObject objectForKey:@"code"] integerValue] == 401) {
                    UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Follow" message:[responseObject objectForKey:@"text"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* copy = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                        
                        [alert dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alert addAction:copy];
                    [self.navigationController presentViewController:alert animated:YES completion:nil];
                }else{
                    
                    [self updateFollowStatusWithResponds:responseObject atIndex:index];
                }
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                [self hideLoadingScreen];
            }];
        }
    }
}

-(void)updateFollowStatusWithResponds:(NSDictionary*)responds atIndex:(NSInteger)index{

    if (index < arrFiltered.count) {
        NSMutableDictionary *gemDetails = [NSMutableDictionary dictionaryWithDictionary:arrFiltered[index]];
        if ([responds objectForKey:@"follow_status"]) {
            NSInteger followStatus = [[responds objectForKey:@"follow_status"] integerValue];
            [gemDetails setObject:[NSNumber numberWithInteger:followStatus] forKey:@"follow_status"];
            [arrFiltered replaceObjectAtIndex:index withObject:gemDetails];
            
            if (NULL_TO_NIL([gemDetails objectForKey:@"user_id"])) {
                NSString *userID = [gemDetails objectForKey:@"user_id"];
                NSInteger _index = 0;
                for (NSDictionary *dict in arrDataSource) {
                    if ([[dict objectForKey:@"user_id"] isEqualToString:userID]) {
                        gemDetails = [NSMutableDictionary dictionaryWithDictionary:arrDataSource[_index]];
                        break;
                    }
                    _index ++;
                }
                [gemDetails setObject:[NSNumber numberWithInteger:followStatus] forKey:@"follow_status"];
                [arrDataSource replaceObjectAtIndex:_index withObject:gemDetails];
            }
            [tableView reloadData];
        }
    }
}

#pragma mark - Keyboard Methods

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    tableBottomConstraint.constant = (keyboardBounds.size.height - bottomPadding) ;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    
    
    // commit animations
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    tableBottomConstraint.constant = 0;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    // commit animations
    [UIView commitAnimations];
}

#pragma mark - Search Methods and Delegates

- (void)searchBar:(UISearchBar *)_searchBar textDidChange:(NSString *)searchString{
    
    btnCancel.hidden = false;
    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    rightForSearchBar.constant = 60;
    [arrFiltered removeAllObjects];
    if (searchString.length > 0) {
        if (arrDataSource.count > 0) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSString *regexString  = [NSString stringWithFormat:@".*\\b%@.*", searchString];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstname matches[cd] %@", regexString];
                // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"ANY words BEGINSWITH[c] %@",searchString];
                arrFiltered = [NSMutableArray arrayWithArray:[arrDataSource filteredArrayUsingPredicate:predicate]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    isDataAvailable = true;
                    if (arrFiltered.count <= 0)isDataAvailable = false;
                    [tableView reloadData];
                });
            });
            
        }
    }else{
        if (arrDataSource.count > 0) {
            if (searchBar.text.length > 0) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    NSString *regexString  = [NSString stringWithFormat:@".*\\b%@.*", searchString];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstname matches[cd] %@", regexString];
                    arrFiltered = [NSMutableArray arrayWithArray:[arrDataSource filteredArrayUsingPredicate:predicate]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        isDataAvailable = true;
                        if (arrFiltered.count <= 0)isDataAvailable = false;
                        [tableView reloadData];
                    });
                });
            }else{
                
                arrFiltered = [NSMutableArray arrayWithArray:arrDataSource];
                dispatch_async(dispatch_get_main_queue(), ^{
                    isDataAvailable = true;
                    if (arrFiltered.count <= 0)isDataAvailable = false;
                    [tableView reloadData];
                });
            }
            
        }
    }
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    [_searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar{
    
    rightForSearchBar.constant = 5;
    [searchBar resignFirstResponder];
    arrFiltered = [NSMutableArray arrayWithArray:arrDataSource];
    [tableView reloadData];
    searchBar.text = @"";
    btnCancel.hidden = true;
    [btnCancel setTitle:@"" forState:UIControlStateNormal];
}

-(IBAction)clearSearch{
    
    rightForSearchBar.constant = 5;
    [btnCancel setTitle:@"" forState:UIControlStateNormal];
    btnCancel.hidden = true;
    [searchBar resignFirstResponder];
    arrFiltered = [NSMutableArray arrayWithArray:arrDataSource];
    if (arrFiltered.count) isDataAvailable = true;
    [tableView reloadData];
    searchBar.text = @"";
}


#pragma mark - CustomCell Delegte Methods

-(IBAction)goBack:(id)sender{
    
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
