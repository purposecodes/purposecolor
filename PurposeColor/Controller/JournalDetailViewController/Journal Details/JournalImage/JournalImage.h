//
//  JournalDateInfo.h
//  PurposeColor
//
//  Created by Purpose Code on 22/02/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalImage : UITableViewCell


@property (nonatomic,weak) IBOutlet UILabel *lblEmotion;
@property (nonatomic,weak) IBOutlet UIImageView *imgJournal;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *imgHeight;
@property (nonatomic,weak) IBOutlet UIButton *bnExpandGallery;

@end
