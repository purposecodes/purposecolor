//
//  JournalDateInfo.h
//  PurposeColor
//
//  Created by Purpose Code on 22/02/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalBasicInfo : UITableViewCell


@property (nonatomic,weak) IBOutlet UILabel *lblDate;
@property (nonatomic,weak) IBOutlet UILabel *lblMonth;
@property (nonatomic,weak) IBOutlet UILabel *lblTime;
@property (nonatomic,weak) IBOutlet UILabel *lblTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblContact;
@property (nonatomic,weak) IBOutlet UILabel *lblLoc;

@property (nonatomic,weak) IBOutlet NSLayoutConstraint *contraintForLocTop;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *contraintForContTop;


@end
