//
//  JournalDetailPageViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 21/02/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#define kSuccessCode            200

#import "JournalDetailPageViewController.h"
#import "JournalBasicInfo.h"
#import "Constants.h"
#import "JournalGalleryViewController.h"
#import "Journal_CommentViewController.h"
#import "JournalDetails.h"
#import "GoalDetailViewController.h"
#import "JournalImage.h"
#import "SharePost.h"
#import "GemDetailsCustomCellTitle.h"
#import "ActionDetailPageViewController.h"

@interface JournalDetailPageViewController () <Journal_CommentActionDelegate,SharePopUpTextDelegate,ActionDetailsDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnShare;
    IBOutlet UIButton *btnNotes;
    IBOutlet UIView *vwFooter;
    Journal_CommentViewController *composeComment;
    BOOL isDataAvailable;
    NSString *strNoDataText;
}

@end

@implementation JournalDetailPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
   
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[tableView reloadData];
}

-(void)setUp{
    
    isDataAvailable = true;
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 50;
    
    [btnShare setImage:[UIImage imageNamed:@"Share.png"] forState:UIControlStateNormal];
    [btnShare setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnShare setTitle:@"Share" forState:UIControlStateNormal];
    
    if ([[_journalDetails objectForKey:@"share_status"] boolValue]) {
        [btnShare setImage:[UIImage imageNamed:@"Shared.png"] forState:UIControlStateNormal];
        [btnShare setTitle:@"Shared" forState:UIControlStateNormal];
        [btnShare setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
        
    }
    NSInteger notesCount = [[_journalDetails objectForKey:@"note_count"] integerValue];
    [btnNotes setTitle:[NSString stringWithFormat:@"Notes(%ld)",(long)notesCount]forState:UIControlStateNormal];
    
    [vwFooter.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwFooter.layer setBorderWidth:1.f];
    [vwFooter.layer setShadowColor:[UIColor blackColor].CGColor];
    [vwFooter.layer setShadowOpacity:0.3];
    [vwFooter.layer setShadowRadius:2.0];
    [vwFooter.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (!isDataAvailable) {
        return 1;
    }
    return 3;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) {
        return 1;
    }
    
    if (section == 0) {
        return 1;
    }else if (section == 1){
        NSInteger rows = 1;
        if (NULL_TO_NIL([_journalDetails objectForKey:@"action"])) {
            NSArray *Actions = [_journalDetails objectForKey:@"action"];
            rows += Actions.count;
        }
        return rows;
    }else{
        NSInteger count = 0;
        if (NULL_TO_NIL([_journalDetails objectForKey:@"display_image"])) {
            count ++;
        }
        return count;
    }
    
    return 0;
}



-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:strNoDataText];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    if (indexPath.section == 0) {
        JournalBasicInfo *cell = (JournalBasicInfo *)[aTableView dequeueReusableCellWithIdentifier:@"JournalBasicInfo"];
        cell = [self configureJournalBasicInfoWithCell:cell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if (indexPath.section == 1){
        if (indexPath.row == 0){
            JournalDetails *cell = (JournalDetails *)[aTableView dequeueReusableCellWithIdentifier:@"JournalInfo"];
            cell = [self configureJournalDetailsWithCell:cell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else{
            GemDetailsCustomCellTitle *cell = (GemDetailsCustomCellTitle *)[tableView dequeueReusableCellWithIdentifier:@"GemDetailsCustomCellActionTitle"];
            cell = [self configureActionTitleWithCell:cell atIndex:indexPath.row];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return  cell;
        }
    }else{
        JournalImage *cell = (JournalImage *)[aTableView dequeueReusableCellWithIdentifier:@"JournalImage"];
        cell = [self configureJournalImagesWithCell:cell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return cell;
}

-(IBAction)showGoalDetails{
    
    GoalDetailViewController *goalDetailsVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForGoalDetails];
    goalDetailsVC.isFromJournal = YES;
    [goalDetailsVC getGoaDetailsByGoalID:[_journalDetails objectForKey:@"goal_id"]];
    [[self navigationController]pushViewController:goalDetailsVC animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 2) {
         [self showGallery:nil];
    }else if (indexPath.section == 1){
        if (indexPath.row > 0) {
            [self showActionDetailsAtIndex:indexPath.row - 1];
        }
    }
  
}

-(void)closeJournalCommentPopUpClicked{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        composeComment.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [composeComment.view removeFromSuperview];
        [composeComment removeFromParentViewController];
        composeComment = nil;
        
    }];
    
}

-(void)notesUpdatedByNewNoteCount:(NSInteger)noteCount{
    
    [btnNotes setTitle:[NSString stringWithFormat:@"Notes(%ld)",(long)noteCount]forState:UIControlStateNormal];
    if ([self.delegate respondsToSelector:@selector(notesUpdatedByNewNoteCountFromDetailView:)])
        [self.delegate notesUpdatedByNewNoteCountFromDetailView:noteCount];
    
}

#pragma mark - Configure Cells

-(JournalBasicInfo*)configureJournalBasicInfoWithCell:(JournalBasicInfo*)cell{
    
    cell.lblTitle.text = [_journalDetails objectForKey:@"journal_desc"];
    
    NSDate *currentDate = [NSDate dateWithTimeIntervalSince1970:[[_journalDetails objectForKey:@"journal_datetime"] doubleValue]];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-YYYY"];
    NSString * dateString = [dateFormatter stringFromDate:currentDate];
    NSArray *components = [dateString componentsSeparatedByString:@"-"];
    
    cell.lblDate.text = components[0];
    cell.lblMonth.text = [[NSString stringWithFormat:@"%@ %@",components[1],components[2]] uppercaseString];;
    
    [dateFormatter setDateFormat:@"hh:mm a"];
    dateString = [dateFormatter stringFromDate:currentDate];
    cell.lblTime.text = dateString;
    
    cell.contraintForLocTop.constant = 0;
    cell.contraintForContTop.constant = 0;
    if (NULL_TO_NIL([_journalDetails objectForKey:@"location_name"])) {
        cell.contraintForLocTop.constant = 10;
        NSMutableAttributedString *myString = [NSMutableAttributedString new];
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        UIImage *icon = [UIImage imageNamed:@"Loc_Small"];
        attachment.image = icon;
        attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblLoc.font.descender + 2), icon.size.width, icon.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        [myString appendAttributedString:attachmentString];
        NSAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",[_journalDetails objectForKey:@"location_name"]]];
        [myString appendAttributedString:myText];
        cell.lblLoc.attributedText = myString;
    }
    
    if (NULL_TO_NIL([_journalDetails objectForKey:@"contact_name"])) {
        cell.contraintForContTop.constant = 10;
        NSMutableAttributedString *myString = [NSMutableAttributedString new];
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        UIImage *icon = [UIImage imageNamed:@"contact_icon"];
        attachment.image = icon;
        attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblContact.font.descender + 2), icon.size.width, icon.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        [myString appendAttributedString:attachmentString];
        NSAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",[_journalDetails objectForKey:@"contact_name"]]];
        [myString appendAttributedString:myText];
        cell.lblContact.attributedText = myString;
        
    }
    return cell;
}

-(JournalDetails*)configureJournalDetailsWithCell:(JournalDetails*)cell{
    
    NSInteger emotionValue = [[_journalDetails objectForKey:@"emotion_value"] integerValue] + 3;
    NSInteger driveValue = [[_journalDetails objectForKey:@"drive_value"] integerValue] + 3;
    
    NSMutableAttributedString *myString = [NSMutableAttributedString new];
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    UIImage *icon = [UIImage imageNamed:[NSString stringWithFormat:@"%ld_Star_Filled",(long)emotionValue]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblEmotion.font.descender + 2), icon.size.width, icon.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    [myString appendAttributedString:attachmentString];
    NSMutableAttributedString *strEmotion = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" Feeling %@",[_journalDetails objectForKey:@"emotion_title"]]];
    [strEmotion addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:CommonFontBold_New size:14]
                       range:NSMakeRange(8, strEmotion.length - 8)];
    [strEmotion addAttribute:NSForegroundColorAttributeName
                       value:[UIColor blackColor]
                       range:NSMakeRange(8, strEmotion.length - 8)];
    [myString appendAttributedString:strEmotion];
    cell.lblEmotion.attributedText = myString;
    
    myString = [NSMutableAttributedString new];
    icon = [UIImage imageNamed:[NSString stringWithFormat:@"%ld_Star_Filled",(long)driveValue]];
    attachment = [[NSTextAttachment alloc] init];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblGoal.font.descender + 2), icon.size.width, icon.size.height);
    attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    [myString appendAttributedString:attachmentString];
    
    if ([_journalDetails objectForKey:@"goal_title"]) {
        NSMutableAttributedString *goalTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" Goal %@",[_journalDetails objectForKey:@"goal_title"]]];
        [goalTitle addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:CommonFontBold_New size:14]
                          range:NSMakeRange(6, goalTitle.length - 6)];
        [goalTitle addAttribute:NSForegroundColorAttributeName
                          value:[UIColor blackColor]
                          range:NSMakeRange(6, goalTitle.length - 6)];
        [myString appendAttributedString:goalTitle];
        
    }
    
    if (NULL_TO_NIL([_journalDetails objectForKey:@"action"])) {
        NSArray *Actions = [_journalDetails objectForKey:@"action"];
        NSMutableAttributedString *goalTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" &\n%lu Action(s)",(unsigned long)Actions.count]];
        
        [goalTitle addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:CommonFontBold_New size:14]
                          range:NSMakeRange(3, goalTitle.length - 3)];
        [goalTitle addAttribute:NSForegroundColorAttributeName
                          value:[UIColor blackColor]
                          range:NSMakeRange(3, goalTitle.length - 3)];
        [myString appendAttributedString:goalTitle];
        
    }
    
    cell.lblGoal.attributedText = myString;
    UITapGestureRecognizer* _gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedGoalsLabel:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [cell.lblGoal setUserInteractionEnabled:YES];
    [cell.lblGoal addGestureRecognizer:_gesture];
    [cell.lblGoal setUserInteractionEnabled:YES];
    
    return cell;
}

-(JournalImage*)configureJournalImagesWithCell:(JournalImage*)cell{
    
    float imageHeight = 0;
    cell.bnExpandGallery.hidden = true;
    float width = [[_journalDetails objectForKey:@"image_width"] floatValue];
    float height = [[_journalDetails objectForKey:@"image_height"] floatValue];
    [cell.activityIndicator stopAnimating];
    if (width && height > 0) {
        float ratio = width / height;
        imageHeight = (self.view.frame.size.width) / ratio;
        [cell.activityIndicator startAnimating];
        [cell.imgJournal sd_setImageWithURL:[NSURL URLWithString:[_journalDetails objectForKey:@"display_image"]]
                           placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      
                                      [cell.activityIndicator stopAnimating];
                                      
                                  }];
    }
    cell.imgHeight.constant = imageHeight;
    if (NULL_TO_NIL([_journalDetails objectForKey:@"journal_media"])){
        NSArray *gallery =  [_journalDetails objectForKey:@"journal_media"];
        if (gallery.count > 1) {
            cell.bnExpandGallery.hidden = FALSE;
            [cell.bnExpandGallery setTitle:[NSString stringWithFormat:@"+%02lu",(unsigned long)(gallery.count) - 1] forState:UIControlStateNormal];
        }
    }
    return cell;
}

-(GemDetailsCustomCellTitle*)configureActionTitleWithCell:(GemDetailsCustomCellTitle*)cell atIndex:(NSInteger)index{
    if ([_journalDetails objectForKey:@"action"]) {
        NSArray *Actions = [_journalDetails objectForKey:@"action"];
        if (index - 1 < Actions.count) {
            NSDictionary *action =  Actions[index - 1];
            cell.lbltTitle.text = [action objectForKey:@"action_title"];
        }
    }
    return cell;
}

- (void)tappedGoalsLabel:(UITapGestureRecognizer *)tapGesture {
   
    if ([_journalDetails objectForKey:@"goal_title"]) {
        UILabel *lblGoal = (UILabel*)tapGesture.view;
        NSString *goalTitle = [_journalDetails objectForKey:@"goal_title"];
        NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
        NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
        NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:lblGoal.attributedText];
        
        // Configure layoutManager and textStorage
        [layoutManager addTextContainer:textContainer];
        [textStorage addLayoutManager:layoutManager];
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0;
        textContainer.lineBreakMode = lblGoal.lineBreakMode;
        textContainer.maximumNumberOfLines = lblGoal.numberOfLines;
        
        CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
        CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x,
                                                             locationOfTouchInLabel.y);
        NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer
                                                           inTextContainer:textContainer
                                  fractionOfDistanceBetweenInsertionPoints:nil];
        
        NSRange range = NSMakeRange(0, 0);
        if (goalTitle.length) {
            range = [lblGoal.attributedText.string rangeOfString:goalTitle];
        }
        if (indexOfCharacter <= 0) {
            NSLog(@"1");
            //[self showDriveSelection];
        }else if (NSLocationInRange(indexOfCharacter, range)){
            NSLog(@"2");
            [self showGoalDetails];
        }else if (!goalTitle){
            NSLog(@"3");
            //[self showGoalsAndDreamsSelection];
        }else{
            //[self showActionSelection];
            NSLog(@"4");
        }
    }
    
}

-(void)showActionDetailsAtIndex:(NSInteger)index{
    
    if ([_journalDetails objectForKey:@"action"]) {
        NSArray *actions = [_journalDetails objectForKey:@"action"];
        if (index < actions.count) {
            NSDictionary *_actionDetails = actions[index];
            ActionDetailPageViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGoalsDreamsDetailPage];
            NSString *goalActionID = [_actionDetails objectForKey:@"action_id"];
            detailPage.isFromJournal = YES;
            [self.navigationController pushViewController:detailPage animated:YES];
            [detailPage getActionDetailsByGoalActionID:goalActionID actionID:nil goalID:[_journalDetails objectForKey:@"goal_id"]];
        }
    }
}
#pragma mark - Generic Methods

-(void)shareGEMsToCommunityWith:(NSInteger)index{
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SharePost"
                                                          owner:nil
                                                        options:nil];
    
    SharePost *vwPopUP = [arrayOfViews objectAtIndex:0];
    vwPopUP.delegate = self;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.4 delay:.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
}


-(void)sharePostTextPopUpCloseApppliedAtIndex:(NSInteger)index{
    //[self sharePostToCommunityWithMessage:nil atIndex:index];
}

-(void)sharePostSubmitAppliedWithText:(NSString*)shareTxt AtIndex:(NSInteger)index{
    [self sharePostToCommunityWithMessage:shareTxt atIndex:index];
}

-(void)sharePostToCommunityWithMessage:(NSString*)mesage atIndex:(NSInteger)index{
    
    NSString *gemID;
    NSString *gemType;
    if (NULL_TO_NIL([_journalDetails objectForKey:@"journal_id"]))
        gemID = [_journalDetails objectForKey:@"journal_id"];
    gemType = @"moment";
    if (gemID && gemType) {
        [self showLoadingScreen];
        [APIMapper shareAGEMToCommunityWith:[User sharedManager].userId gemID:gemID gemType:gemType shareMsg:mesage success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self hideLoadingScreen];
            if ([[responseObject objectForKey:@"code"]integerValue] == kSuccessCode){
                if ([[responseObject objectForKey:@"code"]integerValue] == kSuccessCode){
                    if ( NULL_TO_NIL( [responseObject objectForKey:@"text"])){
                       // [ALToastView toastInView:self.view withText: [responseObject objectForKey:@"text"]];
                    }
                    [btnShare setImage:[UIImage imageNamed:@"Shared.png"] forState:UIControlStateNormal];
                    [btnShare setTitle:@"Shared" forState:UIControlStateNormal];
                    [btnShare setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
                    if ([self.delegate respondsToSelector:@selector(journalSharedFromDetailViewWithIndex:)])
                        [self.delegate journalSharedFromDetailViewWithIndex:_indexObj];
                    
                }
            }
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            [self hideLoadingScreen];
            if (error && error.localizedDescription){
                [ALToastView toastInView:self.view withText:error.localizedDescription];
            }
        }];
    }
    
    
}


-(IBAction)shareJournalToCommunity:(id)sender{
    
    
    UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Share" message:@"Do you really want to share this journal to community?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* block = [UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self shareGEMsToCommunityWith:0];
    
        
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:block];
    [alert addAction:cancel];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
    
    
}

-(IBAction)deleteJournal:(id)sender{
    
    UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Delete" message:@"Do you really want to delete this journal?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* block = [UIAlertAction actionWithTitle:@"DELETE" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self showLoadingScreen];
        [APIMapper deleteJournalWithJournalID:[_journalDetails objectForKey:@"journal_id"] userID:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([self.delegate respondsToSelector:@selector(journalDeletedFromDetailViewWithIndex:)])
                [self.delegate journalDeletedFromDetailViewWithIndex:_indexObj];
            [self hideLoadingScreen];
            [self goBack:nil];
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            [ALToastView toastInView:self.view withText:error.localizedDescription];
            [self hideLoadingScreen];
        }];
        
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:block];
    [alert addAction:cancel];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
    
    
}

-(IBAction)addNotes:(id)sender{
    
    // To show notes adding popup
    
    composeComment =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForJournalCommentView];
    composeComment.dictJournal = _journalDetails;
    composeComment.delegate = self;
    float strtPoint = 50;
    
    [self addChildViewController:composeComment];
    UIView *vwPopUP = composeComment.view;
    [self.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:vwPopUP
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self.view
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1.0
                                                            constant:strtPoint];
    [self.view addConstraint:top];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:vwPopUP
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1.0
                                                               constant:100];
    [vwPopUP addConstraint:height];
    [self.view layoutIfNeeded];
    top.constant = 0;
    height.constant = self.view.frame.size.height;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         [composeComment showNavBar];
                     }];
}

-(IBAction)showGallery:(id)sender{
    
    // Show image gallery
    
    JournalGalleryViewController *galleryVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForJournalGallery];
    galleryVC.arrMedia = [_journalDetails objectForKey:@"journal_media"];
    [self.navigationController pushViewController:galleryVC animated:YES];
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(IBAction)goBack:(id)sender{
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
