//
//  JournalDateInfo.h
//  PurposeColor
//
//  Created by Purpose Code on 22/02/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalDetails : UITableViewCell


@property (nonatomic,weak) IBOutlet UILabel *lblGoal;
@property (nonatomic,weak) IBOutlet UILabel *lblEmotion;
@property (nonatomic,weak) IBOutlet UIImageView *imgFeelView;
@property (nonatomic,weak) IBOutlet UIImageView *imgReactionView;



@end
