//
//  UserListTableViewCell.h
//  PurposeColor
//
//  Created by Purpose Code on 14/06/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatUserListTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@property (nonatomic, weak) IBOutlet UIView *vwBg;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *lblMsg;
@property (nonatomic, weak) IBOutlet UIButton *btnInviteChat;
@property (nonatomic, weak) IBOutlet UILabel *lblChatCount;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *widthForChatCount;
@end
