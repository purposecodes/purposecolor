//
//  UserListTableViewCell.m
//  PurposeColor
//
//  Created by Purpose Code on 14/06/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import "ChatUserListTableViewCell.h"

@implementation ChatUserListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _imgView.layer.borderColor = [UIColor colorWithRed:0.83 green:0.83 blue:0.83 alpha:1.0].CGColor;
    _imgView.layer.borderWidth = 1.f;
    _imgView.layer.cornerRadius = 25.f;
    _imgView.clipsToBounds = YES;
    
    _lblChatCount.layer.borderColor = [UIColor clearColor].CGColor;
    _lblChatCount.layer.borderWidth = 1.f;
    _lblChatCount.layer.cornerRadius = 10.f;
    _lblChatCount.clipsToBounds = YES;

    _btnInviteChat.layer.borderColor = [UIColor getThemeColor].CGColor;
    _btnInviteChat.layer.borderWidth = 1.f;
    _btnInviteChat.layer.cornerRadius = 5.f;
    
//    [_vwBg.layer setBorderColor:[UIColor clearColor].CGColor];
//    [_vwBg.layer setBorderWidth:1.f];
//    // drop shadow
//    [_vwBg.layer setShadowColor:[UIColor blackColor].CGColor];
//    [_vwBg.layer setShadowOpacity:0.3];
//    [_vwBg.layer setShadowRadius:2.0];
//    [_vwBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
