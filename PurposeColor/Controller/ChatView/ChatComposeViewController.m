//
//  ChatComposeViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 02/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kSectionCount               1
#define kDefaultCellHeight          70
#define kSuccessCode                200
#define kMinimumCellCount           0
#define kHeightForHeader            40

#define kTagForImage                1
#define kTagForName                 2


#import "ChatComposeViewController.h"
#import "Constants.h"
#import "ComposeMessageTableViewCell.h"
#import "HPGrowingTextView.h"
#import "ProfilePageViewController.h"
#import <CoreData/CoreData.h>
#import "Chat+CoreDataClass.h"

@interface ChatComposeViewController () <HPGrowingTextViewDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *lblTitleName;
    IBOutlet UIView *vwLoadHistory;
    IBOutlet UIView *vwDeletePanel;
    IBOutlet UIImageView *imgProfilePic;
    IBOutlet NSLayoutConstraint *tableConstraint;
    IBOutlet UIButton *btnCancel;
    IBOutlet NSLayoutConstraint *bottomForContainer;
    NSLayoutConstraint *heightForContainer;
    
    
    IBOutlet UIButton *btnDeleteAPI;
    IBOutlet UILabel *lblDeleteCount;
    
    NSMutableArray *arrMessages;
    UIButton *btnDone;
    
    NSMutableDictionary *dictDeleteIDs;
    BOOL isPageRefresing;
    NSInteger totalPages;
    HPGrowingTextView *textView;
    UIView *containerView;
    NSInteger currentPage;
    BOOL isDeleting;
    BOOL isDataAvailable;
    NSString *strErroMsg;
    NSInteger dbLstIndex;
    
}

@end

@implementation ChatComposeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self getAllOfflineChat];
    [self loadChatHistoryWithPageNo:currentPage isByPagination:NO];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setUpGrowingTextView];
}

-(void)setUp{
    
    vwLoadHistory.hidden = true;
    isDataAvailable = false;
    self.view.backgroundColor = [UIColor colorWithRed:0.92 green:0.91 blue:0.87 alpha:1.0];
    currentPage = 1;
    totalPages = 1;
    btnCancel.hidden = true;
    arrMessages = [NSMutableArray new];
    dictDeleteIDs = [NSMutableDictionary new];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.hidden = true;
    imgProfilePic.layer.cornerRadius = 20.f;
    tableView.tableHeaderView = nil;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserProfilePage:)];
    [imgProfilePic addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
    imgProfilePic.userInteractionEnabled = true;
    if (_chatUserInfo) {
        if (NULL_TO_NIL([_chatUserInfo objectForKey:@"firstname"])) {
            lblTitleName.text = [_chatUserInfo objectForKey:@"firstname"];
        }
        if (NULL_TO_NIL([_chatUserInfo objectForKey:@"profileimage"])) {
            NSString *urlImage =[_chatUserInfo objectForKey:@"profileimage"];
            if (urlImage.length) {
                [imgProfilePic sd_setImageWithURL:[NSURL URLWithString:urlImage]
                                 placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            
                                        }];
            }
            lblTitleName.text = [_chatUserInfo objectForKey:@"firstname"];
        }
    }
    
    tableView.estimatedRowHeight = 150;
    tableView.rowHeight = UITableViewAutomaticDimension;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController) {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}


-(void)loadChatHistoryWithPageNo:(NSInteger)pageNo isByPagination:(BOOL)isPagination{
    
    NSString *toUserID;
    if ([_chatUserInfo objectForKey:@"chatuser_id"]) {
        toUserID =[_chatUserInfo objectForKey:@"chatuser_id"];
    }
    
    if (!isPagination){
        [self hideLoadingScreen];
        [self showLoadingScreen];
    }
    
    else
        vwLoadHistory.hidden = false;
    [APIMapper loadChatHistoryWithFrom:[User sharedManager].userId toUserID:toUserID page:pageNo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self showAllChatHistoryMessagesWith:responseObject isPagination:isPagination];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        if (arrMessages.count <= 0) isDataAvailable = false;
        strErroMsg = error.localizedDescription;
        [self hideLoadingScreen];
        [tableView reloadData];
        [tableView setHidden:false];
        isPageRefresing = NO;
        vwLoadHistory.hidden = true;
    }];
    
    
}

-(void)showAllChatHistoryMessagesWith:(NSDictionary*)responds isPagination:(BOOL)isPagination{
    
    vwLoadHistory.hidden = true;
    if ([[responds objectForKey:@"code"] integerValue] == kSuccessCode) {
        if (NULL_TO_NIL([responds objectForKey:@"resultarray"])){
            NSArray *result = [responds objectForKey:@"resultarray"];
            if (result.count) {
                isDataAvailable = true;
                if (isPagination) {
                    NSMutableArray *indexPaths = [NSMutableArray array];
                    for (int i = 0; i < result.count; i++) {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        [arrMessages insertObject:result[i] atIndex:i];
                    }
                    [tableView beginUpdates];
                    [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
                    [tableView endUpdates];
                }else{
                    [arrMessages addObjectsFromArray:result];
                    [tableView reloadData];
                    [self tableScrollToLastCell];
                }
            }
        }
    }else{
        
        if ([responds objectForKey:@"text"]) {
            isDataAvailable = false;
            strErroMsg = [responds objectForKey:@"text"];
            [tableView reloadData];
        }
    }
    if (NULL_TO_NIL([responds objectForKey:@"pageCount"]))
        totalPages =  [[responds objectForKey:@"pageCount"]integerValue];
    
    if (NULL_TO_NIL([responds  objectForKey:@"currentPage"]))
        currentPage =  [[responds objectForKey:@"currentPage"]integerValue];
    
    tableView.hidden = false;
    isPageRefresing = NO;
    
    
}



-(void)newChatHasReceivedWithDetails:(NSDictionary*)chatInfo{
        
    NSString *message;
    NSString *sender;
    NSString *chat_id;
    double serverTime = 0;
    
    if (NULL_TO_NIL([[chatInfo objectForKey:@"aps"] objectForKey:@"alert"]))
        message = [[chatInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if (NULL_TO_NIL([[chatInfo objectForKey:@"aps"] objectForKey:@"from_user"]))
        sender = [[chatInfo objectForKey:@"aps"] objectForKey:@"from_user"];
    
    if (NULL_TO_NIL([[chatInfo objectForKey:@"aps"] objectForKey:@"chat_datetime"]))
        serverTime =  [[[chatInfo objectForKey:@"aps"] objectForKey:@"chat_datetime"] doubleValue];
    
    if (NULL_TO_NIL([[chatInfo objectForKey:@"aps"] objectForKey:@"chat_id"]))
        chat_id = [[chatInfo objectForKey:@"aps"] objectForKey:@"chat_id"];
    
    if (message && sender) {
        NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:sender,@"from_id",message,@"msg",[NSNumber numberWithDouble:serverTime],@"chat_datetime",chat_id,@"chat_id", nil];
        [arrMessages addObject:details];
    }
    
    if (arrMessages.count) isDataAvailable = true;
    [tableView reloadData];
    [self tableScrollToLastCell];
    
    
}



#pragma mark - UITableViewDataSource Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    return arrMessages.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:strErroMsg];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}
-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    NSString *CellIdentifier = @"ChatComposeCellOthers";
    ComposeMessageTableViewCell *cell = (ComposeMessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.row < arrMessages.count ) {
        NSDictionary *chatInfo = (NSDictionary *) [arrMessages objectAtIndex:indexPath.row];
        NSString *sender = @"OTHERS";
        NSString *localDateTime;
        BOOL isLocal = false;
        if ([[chatInfo objectForKey:@"in_out"] integerValue] == 1) {
            sender = @"YOU";
            CellIdentifier = @"ChatComposeCellYou";
            cell = (ComposeMessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell.imgStatus.image = [UIImage imageNamed:@"Tick_Sent"];
            if ([chatInfo objectForKey:@"isDelivered"] && ![[chatInfo objectForKey:@"isDelivered"] boolValue]) {
                cell.imgStatus.image = [UIImage imageNamed:@"Tick_NotSent"];
                isLocal = true;
            }
        }
        cell.btnDelete.tag = indexPath.row;
        [cell.btnDelete addTarget:self action:@selector(deleteSelectedChat:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDelete setImage:[UIImage imageNamed:@"Chat_Message_Delete_Inactive"] forState:UIControlStateNormal];
        cell.leftForDelete.constant = -40;
        if (isDeleting) {
            cell.leftForDelete.constant = 0;
            if ([dictDeleteIDs objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                [cell.btnDelete setImage:[UIImage imageNamed:@"Chat_Message_Delete_Active"] forState:UIControlStateNormal];
            }
            
        }
        [UIView animateWithDuration:.3
                         animations:^{
                             [self.view layoutIfNeeded];
                             // Called on parent view
                         }completion:^(BOOL finished) {
                             
                         }];
        
        NSString *message = [chatInfo objectForKey:@"msg"];
        if ([chatInfo objectForKey:@"chat_datetime"]) {
            double serverTime = [[chatInfo objectForKey:@"chat_datetime"] doubleValue];
            if (isLocal) {
                localDateTime = [self getLocalDateTime:serverTime];
            }else{
                localDateTime = [Utility getDaysBetweenTwoDatesWith:serverTime];
            }
        }
        cell.lblTime.text = localDateTime;
        UIImage *bgImage = nil;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:CommonFont_New size:15],
                                     };
        if (message && message.length) {
            NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:message attributes:attributes];
            cell.lblMessage.attributedText = attributedText;
        }
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        [cell.contentView addGestureRecognizer:longPress];
        cell.contentView.tag = indexPath.row;
        
        if ([sender isEqualToString:@"OTHERS"])
            bgImage = [[UIImage imageNamed:@"Chat_White_buble.png"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
        else
            bgImage = [[UIImage imageNamed:@"Chat_Green_buble.png"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
        cell.imgBg.image = bgImage;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self resetHeightConstraints];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    if (scrollView.contentOffset.y <= 0){
        
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            NSInteger nextPage = currentPage ;
            nextPage += 1;
            if (nextPage  <= totalPages) {
                isPageRefresing = YES;
                [self loadChatHistoryWithPageNo:nextPage isByPagination:YES];
            }
            
        }
        
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
}


#pragma mark - Growing Text View


- (void)setUpGrowingTextView {
    
    
    containerView = [[UIView alloc] init];
    [self.view addSubview:containerView];
    containerView.backgroundColor = [UIColor colorWithRed:245/255.f green:245/255.f blue:245/255.f alpha:1];
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[containerView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(containerView)]];
    heightForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                      attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil
                                                      attribute:NSLayoutAttributeHeight
                                                     multiplier:1.0
                                                       constant:50];
    [containerView addConstraint:heightForContainer];
    
    if (@available(iOS 11, *)){
        
        bottomForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0];
        
    }else{
        
        bottomForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0];
        
    }
    
    [self.view addConstraint:bottomForContainer];
    
    
    textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(5, 8, self.view.frame.size.width - 60, 40)];
    textView.isScrollable = NO;
    textView.contentInset = UIEdgeInsetsMake(5, 5, 0, 5);
    textView.layer.borderWidth = 1.f;
    textView.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    textView.layer.cornerRadius = 5;
    textView.minNumberOfLines = 1;
    textView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
    textView.returnKeyType = UIReturnKeyGo; //just as an example
    textView.font = [UIFont fontWithName:CommonFont_New size:15];
    textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
    textView.placeholder = @"Message..";
    textView.internalTextView.autocorrectionType = UITextAutocorrectionTypeYes;
    textView.textColor = [UIColor getTitleBlackColor];
    
    //textView.keyboardType = UIKeyboardTypeASCIICapable;
    
    // textView.animateHeightChange = NO; //turns off animation
    
    [self.view addSubview:containerView];
    [containerView addSubview:textView];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    doneBtn.frame = CGRectMake(self.view.frame.size.width - 45, 5, 40, 40);
    [doneBtn setImage:[UIImage imageNamed:@"Comment_Send"] forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(postMessage) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:doneBtn];
    btnDone = doneBtn;
    btnDone.alpha = 0.5;
    [btnDone setEnabled:FALSE];
    
}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
    
    CGRect containerFrame = containerView.frame;
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    bottomForContainer.constant =  - (keyboardBounds.size.height - bottomPadding) ;
    
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    // set views with new info
    containerView.frame = containerFrame;
    // commit animations
    [UIView commitAnimations];
    [self.view layoutIfNeeded];
    float constant =  -(keyboardBounds.size.height + containerFrame.size.height) + bottomPadding;
    tableConstraint.constant = constant;
    [UIView animateWithDuration:.5
                     animations:^{
                         [self.view layoutIfNeeded];
                         // Called on parent view
                     }];
    if (arrMessages.count) [self tableScrollToLastCell];
    
    
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    // set views with new info
    bottomForContainer.constant = 0;
    // commit animations
    [UIView commitAnimations];
    [self resetHeightConstraints];
    
    
}

-(void) keyboardWillChangeFrame:(NSNotification *)note{
    
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
    
    CGRect containerFrame = containerView.frame;
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    bottomForContainer.constant =  - (keyboardBounds.size.height - bottomPadding) ;
    
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    // set views with new info
    containerView.frame = containerFrame;
    // commit animations
    [UIView commitAnimations];
    [self.view layoutIfNeeded];
    float constant =  -(keyboardBounds.size.height + containerFrame.size.height) + bottomPadding;
    tableConstraint.constant = constant;
    [UIView animateWithDuration:.5
                     animations:^{
                         [self.view layoutIfNeeded];
                         // Called on parent view
                     }];
    
}



- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    heightForContainer.constant = r.size.height;
}
- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView{
    
    [btnDone setEnabled:TRUE];
    btnDone.enabled = YES;
    btnDone.alpha = 1;
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (trimmedString.length > 0) [btnDone setEnabled:TRUE];
    else{
        
        btnDone.enabled = NO;
        btnDone.alpha = 0.5;
        [btnDone setEnabled:FALSE];
    }
    
}
#pragma mark -  Chat Operations

-(IBAction)deleteSelectedChat:(UIButton*)_btnDelete{
    if (_btnDelete.tag < arrMessages.count) {
        NSDictionary *details = arrMessages[_btnDelete.tag];
        if ([dictDeleteIDs objectForKey:[NSString stringWithFormat:@"%ld",(long)_btnDelete.tag]]) {
            [dictDeleteIDs removeObjectForKey:[NSString stringWithFormat:@"%ld",(long)_btnDelete.tag]];
            [_btnDelete setImage:[UIImage imageNamed:@"Chat_Message_Delete_Inactive"] forState:UIControlStateNormal];
        }else{
            if([details objectForKey:@"chat_id"]  || [details objectForKey:@"isDelivered"]){
                [dictDeleteIDs setObject:details forKey:[NSString stringWithFormat:@"%ld",(long)_btnDelete.tag]];
                [_btnDelete setImage:[UIImage imageNamed:@"Chat_Message_Delete_Active"] forState:UIControlStateNormal];
            }
        }
        
    }
    
    btnDeleteAPI.enabled = true;
    lblDeleteCount.text = [NSString stringWithFormat:@"%lu Selected",(unsigned long)[dictDeleteIDs count]];
    if (dictDeleteIDs.count <= 0) {
        btnDeleteAPI.enabled = false;
    }
    
}

-(void)postMessage{
    
    if (textView.text.length > 0) {
        NSString *message = textView.text;
        NSString *toUserID;
        if ([_chatUserInfo objectForKey:@"chatuser_id"]) {
            toUserID =[_chatUserInfo objectForKey:@"chatuser_id"];
        }
        [APIMapper postChatMessageWithUserID:[User sharedManager].userId toUserID:toUserID message:message indexOfDB:-1 success:^(AFHTTPRequestOperation *operation, id responseObject){
            
            if ([[responseObject objectForKey:@"code"] integerValue] == kSuccessCode) {
                
                if (NULL_TO_NIL([responseObject objectForKey:@"chat"])) {
                    [arrMessages addObject:[responseObject objectForKey:@"chat"]];
                    if (arrMessages.count) isDataAvailable = true;
                    [tableView reloadData];
                    [self tableScrollToLastCell];
                }
            }else{
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Chat"
                                              message:[responseObject objectForKey:@"text"]
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            CFAbsoluteTime timeInSeconds = CFAbsoluteTimeGetCurrent();
            NSMutableDictionary *chatDetails = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithLong:timeInSeconds],@"chat_datetime",message,@"msg",[NSNumber numberWithInteger:1],@"in_out",[NSNumber numberWithBool:false],@"isDelivered",[NSNumber numberWithInteger:dbLstIndex],@"index", nil];
            if (chatDetails) {
                
                [self writeChatToDB:chatDetails];
                [arrMessages addObject:chatDetails];
                if (arrMessages.count) isDataAvailable = true;
                [tableView reloadData];
                [self tableScrollToLastCell];
                
            }
            
            
            
        }];
        
        textView.text = @"";
    }
}

-(IBAction)deleteChat:(NSInteger)tag{
    
    [self.view endEditing:YES];
    btnCancel.hidden = false;
    containerView.alpha = 0;
    isDeleting = true;
    if (tag < arrMessages.count) {
        NSDictionary *details = arrMessages[tag];
        if([details objectForKey:@"chat_id"] || [details objectForKey:@"isDelivered"]){
            [dictDeleteIDs setObject:details forKey:[NSString stringWithFormat:@"%ld",(long)tag]];
        }
    }
    
    [UIView animateWithDuration:0.3 animations:^(void) {
        vwDeletePanel.alpha = 1;
    }];
    btnDeleteAPI.enabled = true;
    lblDeleteCount.text = [NSString stringWithFormat:@"%lu Selected",(unsigned long)[dictDeleteIDs count]];
    if (dictDeleteIDs.count <= 0) {
        btnDeleteAPI.enabled = false;
    }
    [UIView animateWithDuration:.3
                     animations:^{
                         [self.view layoutIfNeeded];
                         // Called on parent view
                     }completion:^(BOOL finished) {
                         
                     }];
    
    [tableView reloadData];
    
    
}

-(IBAction)cancelDelete:(id)sender{
    
    isDeleting = false;
    btnCancel.hidden = true;
    [dictDeleteIDs removeAllObjects];
    [UIView animateWithDuration:0.3 animations:^(void) {
        vwDeletePanel.alpha = 0;
    }];
    [UIView animateWithDuration:.3
                     animations:^{
                         [self.view layoutIfNeeded];
                         containerView.alpha = 1.f;
                         // Called on parent view
                     }completion:^(BOOL finished) {
                         
                     }];
    
    [tableView reloadData];
}

-(void)handleLongPress:(UILongPressGestureRecognizer*)longPress{
    
    [self.view endEditing:YES];
    NSInteger tag = 0;
    if (longPress && longPress.view) {
        tag = longPress.view.tag;
    }
    if (longPress.state == UIGestureRecognizerStateBegan) {
        UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Chat" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction* delete = [UIAlertAction actionWithTitle:@"Delete chat" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
            
            [self deleteChat:tag];
            [alert dismissViewControllerAnimated:YES completion:nil];
            
            
        }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:cancel];
        [alert addAction:delete];
        [self.navigationController presentViewController:alert animated:YES completion:nil];
        
        
    }
    
}


-(IBAction)deleteAllSelectedChatMessages{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete"
                                  message:@"Delete the selected chat?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"DELETE"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             if (dictDeleteIDs.count) {
                                 
                                 NSArray *ids = [dictDeleteIDs allValues];
                                 NSMutableArray *arrIDS = [NSMutableArray new];
                                 NSMutableArray *arrDBIndex = [NSMutableArray new];
                                 for (NSDictionary *dict in ids) {
                                     if ([dict objectForKey:@"chat_id"]) {
                                         [arrIDS addObject:[dict objectForKey:@"chat_id"]];
                                     }else{
                                         [arrDBIndex addObject:[dict objectForKey:@"index"]];
                                     }
                                     
                                 }
                                 [self deleteOfflineRecordsFromIDs:arrDBIndex];
                                 [arrMessages removeObjectsInArray:ids];
                                 [dictDeleteIDs removeAllObjects];
                                 [tableView reloadData];
                                 [self cancelDelete:nil];
                                 [APIMapper deleteChatWithIDs:arrIDS success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     
                                     if (arrMessages.count <= 0) {
                                         isDataAvailable = false;
                                         strErroMsg = @"No chat done yet!";
                                         [tableView reloadData];
                                     }
                                 } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                     
                                 }];
                                 
                             }
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [self cancelDelete:nil];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}


#pragma mark - Generic Methods


-(IBAction)showUserProfilePage:(UITapGestureRecognizer*)gesture{
    
    if (NULL_TO_NIL([_chatUserInfo objectForKey:@"chatuser_id"])) {
        ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
        [[self navigationController]pushViewController:profilePage animated:YES];
        profilePage.canEdit = false;
        if ([[_chatUserInfo objectForKey:@"chatuser_id"] isEqualToString:[User sharedManager].userId]) {
            profilePage.canEdit = true;
        }
        [profilePage loadUserProfileWithUserID:[_chatUserInfo objectForKey:@"chatuser_id"] showBackButton:YES];
    }
    
    
}



-(void)resetHeightConstraints{
    
    [self.view endEditing:YES];
    tableConstraint.constant = -50;
    [UIView animateWithDuration:.5
                     animations:^{
                         [self.view layoutIfNeeded];
                         // Called on parent view
                     }];
    
}

-(void)tableScrollToLastCell{
    
    if (arrMessages.count - 1 > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:arrMessages.count - 1 inSection:0];
        [tableView scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionBottom
                                 animated:YES];
    }
    
    
}
-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(NSString*)getLocalDateTime:(double)dateTime{
    
    NSDate * today = [NSDate date];
    NSDate * refDate = [NSDate dateWithTimeIntervalSinceReferenceDate:dateTime];
    NSDate *fromDate;
    NSDate *toDate;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:refDate];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:today];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute
                                               fromDate:fromDate toDate:toDate options:0];
    
    NSString *msgDate;
    NSInteger days = [difference day];
    if (days > 7) {
        NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
        [dateformater setDateFormat:@"d MMM,yyyy"];
        msgDate = [dateformater stringFromDate:refDate];
    }
    else if (days <= 0) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"h:mm a"];
        NSDate *date = refDate;
        msgDate = [dateFormatter stringFromDate:date];
    }else{
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EE h:mm a"];
        msgDate = [dateFormatter stringFromDate:refDate];
    }
    
    return msgDate;
    
}

-(void)writeChatToDB:(NSDictionary*)details{
    
    if (details) {
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:details options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (myString) {
            NSManagedObjectContext* wContext = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
            Chat * chat = [[Chat alloc] initWithContext: wContext ];
            chat.responds = myString;
            chat.toUserID = [_chatUserInfo objectForKey:@"chatuser_id"];
            chat.index = dbLstIndex;
            dbLstIndex += 1;
            if (![wContext save:nil]) {
            }
        }
    }
    
}

-(void)getAllOfflineChat{
    
    NSMutableArray *arrOfflineRecords = [NSMutableArray new];
    NSError *error = nil;
    NSManagedObjectContext* context = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"Chat" inManagedObjectContext:context]];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sort]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"toUserID == %@", [_chatUserInfo objectForKey:@"chatuser_id"]];
    [fetch setPredicate:predicate];
    NSArray *results = [context executeFetchRequest:fetch error:&error];
    NSError * err;
    for (Chat *chat in results) {
        NSString *myString = chat.responds;
        NSData *data = [myString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * responds;
        if(data!=nil){
            responds = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            [arrOfflineRecords addObject:chat];
        }
    }
    if (arrOfflineRecords.count) {
        Chat *chat = [arrOfflineRecords lastObject];
        dbLstIndex = chat.index;
    }
    dbLstIndex += 1;
}

-(void)deleteOfflineRecordsFromIDs:(NSMutableArray*)ids{
    
    for (NSNumber *index in ids) {
        NSManagedObjectContext* context = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
        NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
        [fetch setEntity:[NSEntityDescription entityForName:@"Chat" inManagedObjectContext:context]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"toUserID == %@ AND index == %d", [_chatUserInfo objectForKey:@"chatuser_id"],[index integerValue]];
        [fetch setPredicate:predicate];
        NSArray * result = [context executeFetchRequest:fetch error:nil];
        for (id basket in result)
            [context deleteObject:basket];
    }
    
    
}

-(IBAction)goBack:(id)sender{
    
    [self resetBadge];
    [[self navigationController]popViewControllerAnimated:YES];
}

-(void)resetBadge{
    if ([_chatUserInfo objectForKey:@"chatuser_id"]) {
        [APIMapper clearChatBadgeWithUserID:[_chatUserInfo objectForKey:@"chatuser_id"] Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

