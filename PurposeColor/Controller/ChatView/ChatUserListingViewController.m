//
//  ChatUserListingViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 02/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eReqForChat = 0,
    eChatReqPending = 1,
    eCanChat = 2,
    eBlocked = 3
    
} eFollowStatus;

#define kSectionCount               1
#define kDefaultCellHeight          80
#define kSuccessCode                200
#define kMinimumCellCount           1

#define kTagForImage                1
#define kTagForName                 2
#define kTagForDate                 3
#define kTagForMsg                  4

#import "ChatUserListingViewController.h"
#import "Constants.h"
#import "ChatComposeViewController.h"
#import "ChatUserListTableViewCell.h"
#import "ProfilePageViewController.h"
#import "Chat+CoreDataClass.h"


@interface ChatUserListingViewController () <UISearchBarDelegate>{
    
    IBOutlet UITableView *tableView;
    NSMutableArray *arrChatUser;
    BOOL isDataAvailable;
    BOOL showIndicator;
    NSString *strNoDataText;
    UIRefreshControl *refreshControl;
    NSMutableArray *arrFiltered;
    IBOutlet UISearchBar *searchBar;
    IBOutlet NSLayoutConstraint *tableBottomConstraint;
}

@end

@implementation ChatUserListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
    // Do any additional setup after loading the view.
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadAllChatUsers];
    
}


-(void)setUp{
    
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    searchBar.tintColor = [UIColor getThemeColor];
    UITextField *searchTextField = [searchBar valueForKey:@"_searchField"];
    if ([searchTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor whiteColor];
        [searchTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"Search friends" attributes:@{NSForegroundColorAttributeName: color}]];
    }
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setFont:[UIFont fontWithName:CommonFont size:15]];
    searchBar.hidden = true;
    
    showIndicator = true;
    arrChatUser = [NSMutableArray new];
    isDataAvailable = false;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.hidden = true;
    
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [tableView addGestureRecognizer:longPressGestureRecognizer];
    longPressGestureRecognizer.minimumPressDuration = .3;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

-(void)refreshData{
    
    [self clearSearch];
    [arrChatUser removeAllObjects];
    [arrFiltered removeAllObjects];
    [tableView reloadData];
    showIndicator = true;
    [self loadAllChatUsers];
    
}

-(void)loadAllChatUsers{
    
    if (showIndicator)[self showLoadingScreen];
    [APIMapper getAllChatUsersWithUserID:[User sharedManager].userId pageNumber:1 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getAllUserListWith:responseObject];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        if (error && error.localizedDescription){
            [ALToastView toastInView:self.view withText:NETWORK_ERROR_MESSAGE];
            strNoDataText = error.localizedDescription;
            
        }
        [refreshControl endRefreshing];
        [self hideLoadingScreen];
        [tableView reloadData];
        [tableView setHidden:false];
        showIndicator = false;
        
    }];
    
}


-(void)getAllUserListWith:(NSDictionary*)responds{
    
    [refreshControl endRefreshing];
    isDataAvailable = false;
    if (NULL_TO_NIL([responds objectForKey:@"resultarray"])){
        arrChatUser = [NSMutableArray arrayWithArray:[responds objectForKey:@"resultarray"]];
        arrFiltered = [NSMutableArray arrayWithArray:[responds objectForKey:@"resultarray"]];
    }
    else{
        strNoDataText = [responds objectForKey:@"text"];
    }
    if (arrChatUser.count)
        isDataAvailable = true;
    
    tableView.hidden = false;
    showIndicator = false;
    searchBar.hidden = false;
    [tableView reloadData];
    
}

#pragma mark - Search Methods and Delegates

- (void)searchBar:(UISearchBar *)_searchBar textDidChange:(NSString *)searchString{
    
    [arrFiltered removeAllObjects];
    if (searchString.length > 0) {
        if (arrChatUser.count > 0) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSString *regexString  = [NSString stringWithFormat:@".*\\b%@.*", searchString];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstname matches[cd] %@", regexString];
                // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"ANY words BEGINSWITH[c] %@",searchString];
                arrFiltered = [NSMutableArray arrayWithArray:[arrChatUser filteredArrayUsingPredicate:predicate]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    isDataAvailable = true;
                    if (arrFiltered.count <= 0)isDataAvailable = false;
                    [tableView reloadData];
                });
            });
            
        }
    }else{
        if (arrChatUser.count > 0) {
            if (searchBar.text.length > 0) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    NSString *regexString  = [NSString stringWithFormat:@".*\\b%@.*", searchString];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstname matches[cd] %@", regexString];
                    arrFiltered = [NSMutableArray arrayWithArray:[arrChatUser filteredArrayUsingPredicate:predicate]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        isDataAvailable = true;
                        if (arrFiltered.count <= 0)isDataAvailable = false;
                        [tableView reloadData];
                    });
                });
            }else{
                
                arrFiltered = [NSMutableArray arrayWithArray:arrChatUser];
                dispatch_async(dispatch_get_main_queue(), ^{
                    isDataAvailable = true;
                    if (arrFiltered.count <= 0)isDataAvailable = false;
                    [tableView reloadData];
                });
            }
            
        }
    }
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    [_searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar{
    
    [searchBar resignFirstResponder];
    arrFiltered = [NSMutableArray arrayWithArray:arrChatUser];
    [tableView reloadData];
    searchBar.text = @"";
}



#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    return arrFiltered.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:strNoDataText];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor =  [UIColor clearColor];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kDefaultCellHeight;
}

-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    if (!isDataAvailable) {
        /*****! No listing found , default cell !**************/
        UITableViewCell *cell = [Utility getNoDataCustomCellWith:tableView withTitle:@"No Listing found"];
        return cell;
    }
    static NSString *CellIdentifier = @"ChatInviteCell";
    ChatUserListTableViewCell *cell = (ChatUserListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (indexPath.row < arrFiltered.count) {
        NSString *urlImage;
        NSDictionary *productInfo = arrFiltered[[indexPath row]];
        NSInteger chatStatus = eCanChat;
        if (NULL_TO_NIL([productInfo objectForKey:@"can_chat"])) {
            chatStatus = [[productInfo objectForKey:@"can_chat"] integerValue];
            if (chatStatus == eCanChat || chatStatus == eBlocked) {
                cell = (ChatUserListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ChatListingCell"];
            }else{
                switch (chatStatus) {
                    case eChatReqPending:
                        [cell.btnInviteChat setTitle:@"Invite Chat" forState:UIControlStateNormal];
                        [cell.btnInviteChat setBackgroundColor:[UIColor lightGrayColor]];
                        [cell.btnInviteChat setTitleColor:[UIColor whiteColor ] forState:UIControlStateNormal];
                        cell.btnInviteChat.layer.borderColor = [UIColor clearColor].CGColor;
                        [cell.btnInviteChat setEnabled:false];
                        break;
                        
                    case eReqForChat:
                        [cell.btnInviteChat setTitle:@"Invite Chat" forState:UIControlStateNormal];
                        [cell.btnInviteChat setBackgroundColor:[UIColor clearColor]];
                        [cell.btnInviteChat setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
                        cell.btnInviteChat.layer.borderColor = [UIColor getThemeColor].CGColor;
                        [cell.btnInviteChat setEnabled:true];
                        break;
                        
                    default:
                        break;
                }
                
            }
        }
        if (NULL_TO_NIL([productInfo objectForKey:@"profileimage"]))
            urlImage = [NSString stringWithFormat:@"%@",[productInfo objectForKey:@"profileimage"]];
        if (urlImage.length) {
            [cell.imgView sd_setImageWithURL:[NSURL URLWithString:urlImage]
                            placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                       
                                   }];
        }
        
        if (NULL_TO_NIL([productInfo objectForKey:@"firstname"]))
            cell.lblName.text = [productInfo objectForKey:@"firstname"];
        
        if (NULL_TO_NIL([productInfo objectForKey:@"chat_datetime"])) {
            double serverTime = [[productInfo objectForKey:@"chat_datetime"] doubleValue];
            if (serverTime > 0)cell.lblDate.text = [Utility getDaysBetweenTwoDatesWith:serverTime];
        }
        cell.lblMsg.text = @"";
        cell.lblChatCount.text = @"";
        cell.widthForChatCount.constant = 0;
        NSInteger chatCount = 0;
        if (NULL_TO_NIL([productInfo objectForKey:@"chat_count"]))
            chatCount = [[productInfo objectForKey:@"chat_count"] integerValue];
        
        if (chatCount > 0){
            cell.widthForChatCount.constant = 20;
            cell.lblChatCount.text = [NSString stringWithFormat:@"%ld",(long)chatCount];
        }
        
        
        cell.lblMsg.font = [UIFont fontWithName:CommonFont_New size:14];
        if ([[productInfo objectForKey:@"in_out"] integerValue] == 1) {
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"You : %@",[productInfo objectForKey:@"msg"]]];
            [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold size:14.0] range:NSMakeRange(0, 5)];
            cell.lblMsg.attributedText = str;
            
        }else if (NULL_TO_NIL([productInfo objectForKey:@"msg"])){
            cell.lblMsg.text = [productInfo objectForKey:@"msg"];
        }
        cell.lblMsg.textColor = [UIColor blackColor];
        if (chatStatus == eBlocked) {
            cell.lblMsg.text = @"You have blocked this user!";
            cell.lblMsg.textColor = [UIColor grayColor];
            cell.lblDate.text = @"";
        }
     
    }
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserProfilePage:)];
    [cell.imgView addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
    cell.imgView.userInteractionEnabled = true;
    cell.imgView.tag = indexPath.row;
    
    [cell.btnInviteChat addTarget:self action:@selector(inviteChat:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnInviteChat.tag = indexPath.row;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    if (indexPath.row < arrFiltered.count) {
        NSDictionary *details = arrFiltered[[indexPath row]];
        NSInteger chatStatus = eCanChat;
        if (NULL_TO_NIL([details objectForKey:@"can_chat"])) {
            chatStatus = [[details objectForKey:@"can_chat"] integerValue];
            if (chatStatus >= eCanChat) {
                 [self composeChat:details];
                [self clearChatBandgeForSelectedUser:details atIndex:indexPath.row];
            }
        }
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
        
    }
    
}

-(void)clearChatBandgeForSelectedUser:(NSDictionary*)info atIndex:(NSInteger)index{
    
    /*
    if (index < arrFiltered.count) {
        NSMutableDictionary *details = [NSMutableDictionary dictionaryWithDictionary:info];
        [details setObject:@"0" forKey:@"chat_count"];
        [arrFiltered replaceObjectAtIndex:index withObject:details];
        NSInteger position = 0;
        for (NSDictionary *dict in arrChatUser) {
            if ([[dict objectForKey:@"chatuser_id"] isEqualToString:[details objectForKey:@"chatuser_id"]]) {
                
            }
            position ++;
        }
        if (position < arrChatUser.count) {
            NSMutableDictionary *details = [NSMutableDictionary dictionaryWithDictionary:arrChatUser[position]];
            [details setObject:@"0" forKey:@"chat_count"];
            [arrChatUser replaceObjectAtIndex:position withObject:details];
        }
        [tableView reloadData];
        [APIMapper clearChatBadgeWithUserID:[details objectForKey:@"chatuser_id"] Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
           
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            
        }];
        
    }*/
    
    
}

-(void)composeChat:(NSDictionary*)info{
    
    ChatComposeViewController *chatCompose =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatComposer];
    chatCompose.chatUserInfo = info;
    [[self navigationController]pushViewController:chatCompose animated:YES];
    
}

-(IBAction)inviteChat:(UIButton*)sender{
    
    if (sender.tag < arrFiltered.count) {
        
        NSDictionary *chatInfo = arrFiltered[sender.tag];
        if (NULL_TO_NIL([chatInfo objectForKey:@"chatuser_id"])) {
            NSString *userID = [chatInfo objectForKey:@"chatuser_id"];
            [self showLoadingScreen];
            [APIMapper inviteToChatWithChatUserID:userID Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [self hideLoadingScreen];
                [self updateChatRequestStatusAt:sender.tag];
                if ([responseObject objectForKey:@"text"]) {
                     [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
                }
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
                if (error.localizedDescription) {
                    [ALToastView toastInView:self.view withText:error.localizedDescription];
                }
                 [self hideLoadingScreen];
            }];
        }
    }
}

-(void)updateChatRequestStatusAt:(NSInteger)index{
    
    if (index < arrFiltered.count) {
        NSMutableDictionary *chatInfo = [NSMutableDictionary dictionaryWithDictionary:arrFiltered[index]];
        [chatInfo setObject:[NSNumber numberWithInteger:1] forKey:@"can_chat"];
        [arrFiltered replaceObjectAtIndex:index withObject:chatInfo];
        if (NULL_TO_NIL([chatInfo objectForKey:@"chatuser_id"])) {
            NSString *userID = [chatInfo objectForKey:@"chatuser_id"];
            NSInteger _index = 0;
            for (NSDictionary *dict in arrChatUser) {
                if ([[dict objectForKey:@"chatuser_id"] isEqualToString:userID]) {
                    chatInfo = [NSMutableDictionary dictionaryWithDictionary:arrChatUser[_index]];
                    break;
                }
                _index ++;
            }
            [chatInfo setObject:[NSNumber numberWithInteger:1] forKey:@"can_chat"];
            [arrChatUser replaceObjectAtIndex:_index withObject:chatInfo];
        }
        [tableView reloadData];
       
    }
}

-(void)updateBlockRequestStatusAt:(NSInteger)index{
    
    if (index < arrFiltered.count) {
        NSMutableDictionary *chatInfo = [NSMutableDictionary dictionaryWithDictionary:arrFiltered[index]];
        NSInteger canChat = [[chatInfo objectForKey:@"can_chat"] integerValue];
        if (canChat == eCanChat) canChat = eBlocked;
        else canChat = eCanChat;
        
        [chatInfo setObject:[NSNumber numberWithInteger:canChat] forKey:@"can_chat"];
        [arrFiltered replaceObjectAtIndex:index withObject:chatInfo];
        if (NULL_TO_NIL([chatInfo objectForKey:@"chatuser_id"])) {
            NSString *userID = [chatInfo objectForKey:@"chatuser_id"];
            NSInteger _index = 0;
            for (NSDictionary *dict in arrChatUser) {
                if ([[dict objectForKey:@"chatuser_id"] isEqualToString:userID]) {
                    chatInfo = [NSMutableDictionary dictionaryWithDictionary:arrChatUser[_index]];
                    break;
                }
                _index ++;
            }
            [chatInfo setObject:[NSNumber numberWithInteger:canChat] forKey:@"can_chat"];
            [arrChatUser replaceObjectAtIndex:_index withObject:chatInfo];
        }
        [tableView reloadData];
        
    }
}

-(IBAction)showUserProfilePage:(UITapGestureRecognizer*)gesture{
    
    NSInteger tag = gesture.view.tag;
    if (tag < arrFiltered.count) {
        NSDictionary *details = arrFiltered[tag];
        if (NULL_TO_NIL([details objectForKey:@"chatuser_id"])) {
            ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app.navGeneral pushViewController:profilePage animated:YES];
            profilePage.canEdit = false;
            if ([[details objectForKey:@"chatuser_id"] isEqualToString:[User sharedManager].userId]) {
                profilePage.canEdit = true;
            }
            [profilePage loadUserProfileWithUserID:[details objectForKey:@"chatuser_id"]showBackButton:YES];
            
        }
    }
}

#pragma mark - Keyboard Methods

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    tableBottomConstraint.constant = (keyboardBounds.size.height - bottomPadding) ;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    
    
    // commit animations
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    tableBottomConstraint.constant = 0;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    // commit animations
    [UIView commitAnimations];
}


#pragma mark - Generic Methods

-(void)clearSearch{
    
    [searchBar resignFirstResponder];
    arrFiltered = [NSMutableArray arrayWithArray:arrChatUser];
    [tableView reloadData];
    searchBar.text = @"";
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
    CGPoint p = [gestureRecognizer locationInView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:p];
    if (indexPath.row < arrFiltered.count) {
        NSDictionary *details = arrFiltered[indexPath.row];
        NSInteger chatStatus = eCanChat;
        if (NULL_TO_NIL([details objectForKey:@"can_chat"])) {
            chatStatus = [[details objectForKey:@"can_chat"] integerValue];
            BOOL isModerator =  [[details objectForKey:@"is_moderator"] boolValue];
            if (!isModerator && chatStatus > eChatReqPending) {
                NSString *strBlock = @"Block";
                if (chatStatus == eBlocked) {
                    strBlock = @"UnBlock";
                    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
                        
                        UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Chat" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                        UIAlertAction* block;
                        
                        block = [UIAlertAction actionWithTitle:strBlock style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
                            
                            [alert dismissViewControllerAnimated:YES completion:nil];
                            [self blockTheUser:indexPath.row];
                            
                        }];
                        
                        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                            {
                                
                            }];
                        
                        [alert addAction:block];
                        [alert addAction:cancel];
                        [self.navigationController presentViewController:alert animated:YES completion:nil];
                        
                        
                    }
                }else{
                    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
                        
                        UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Chat" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                        UIAlertAction* block;
                        
                        block = [UIAlertAction actionWithTitle:strBlock style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
                            
                            [alert dismissViewControllerAnimated:YES completion:nil];
                            
                            UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Block" message:@"Do you really want to Block this User?" preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction* block = [UIAlertAction actionWithTitle:@"Block" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
                                
                                [alert dismissViewControllerAnimated:YES completion:nil];
                                [self blockTheUser:indexPath.row];
                            }];
                            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
                                
                                [alert dismissViewControllerAnimated:YES completion:nil];
                            }];
                            
                            [alert addAction:block];
                            [alert addAction:cancel];
                            [self.navigationController presentViewController:alert animated:YES completion:nil];
                            
                        }];
                        
                        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                                 {
                                                     
                                                 }];
                        
                        
                        [alert addAction:block];
                        [alert addAction:cancel];
                        [self.navigationController presentViewController:alert animated:YES completion:nil];
                        
                        
                    }
                }
               
            }
            
        }
        
    }
    
}

-(void)blockTheUser:(NSInteger)index{
    
    if (index< arrFiltered.count) {
        NSDictionary *details = arrFiltered[index];
        NSInteger chatStatus = eCanChat;
        if (NULL_TO_NIL([details objectForKey:@"can_chat"])) {
            chatStatus = [[details objectForKey:@"can_chat"] integerValue];
            NSString *block = @"block";
            if (chatStatus == eBlocked) {
                block = @"unblock";
            }
            [self showLoadingScreen];
            [APIMapper blockChatByUserID:[details objectForKey:@"chatuser_id"] status:block success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ( NULL_TO_NIL( [responseObject objectForKey:@"text"])) [ALToastView toastInView:self.view withText: [responseObject objectForKey:@"text"]];
                [self updateBlockRequestStatusAt:index];
                [self hideLoadingScreen];
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
                 [self hideLoadingScreen];
            }];
        }
    }
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
