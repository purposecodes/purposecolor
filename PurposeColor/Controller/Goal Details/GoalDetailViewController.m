//
//  GoalsAndDreamsDetailViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 21/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eSectionInfo = 0,
    eSectionMedia = 1,
    eSectionActions = 2,
    
} eSectionDetails;


#define kSectionCount               3
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kHeaderHeight               0
#define kDefaultCellHeight          300
#define kEmptyHeaderAndFooter       0
#define kPadding                    130;
#define kActionCellHeight           50;

#import "GoalDetailViewController.h"
#import "Constants.h"
#import "GemDetailsCustomTableViewCell.h"
#import "ActionDetailsCustomCell.h"
#import <AVKit/AVKit.h>
#import "CustomAudioPlayerView.h"
#import "CreateActionInfoViewController.h"
#import "CommentComposeViewController.h"
#import "PhotoBrowser.h"
#import "ActionCustomCell.h"
#import "ActionDetailPageViewController.h"
#import "MTDURLPreview.h"
#import "GoalDetailLocationCell.h"
#import "GoalDetailLocationContact.h"
#import "GoalDetailsCustomCellDescription.h"
#import "GalDetailsCustomCellPreview.h"
#import "HashTagGemListingViewController.h"
#import "SharePost.h"

@interface GoalDetailViewController ()<GemDetailPageCellDelegate,ActionDetailsCustomCellDelegate,CustomAudioPlayerDelegate,CommentActionDelegate,PhotoBrowserDelegate,CreateMediaInfoDelegate,ActionDetailsDelegate,SharePopUpTextDelegate>{
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UITableView *tableView;
    IBOutlet  UIImageView *imgFavourite;
    IBOutlet  UIButton *btnHeaderAction;
    IBOutlet  UIButton *btnCreateAction;
     IBOutlet  UIButton *btnShare;
    IBOutlet UIView *vwFooter;
    
    BOOL isDataAvailable;
    NSMutableArray *arrDataSource;
    NSMutableArray *arrActions;
    
    NSInteger playingIndex;
    BOOL isScrolling;
    BOOL isPlaying;
    BOOL isUpdated;
    
    NSString *strTitle;
    NSString *strDescription;
    
    NSString *goalID;
    
    NSDictionary *actionDetails;
    CustomAudioPlayerView *vwAudioPlayer;
    PhotoBrowser *photoBrowser;
    NSString *strErrorMsg;
    
}


@end

@implementation GoalDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
    
    // Do any additional setup after loading the view.
}

-(void)setUp{
    
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 20;
    arrDataSource = [NSMutableArray new];
    actionDetails = [NSDictionary new];
    tableView.hidden = true;
    btnHeaderAction.hidden = true;
    isDataAvailable = false;
    playingIndex = -1;
    btnCreateAction.hidden = true;
    strTitle = [NSString new];
    strDescription = [NSString new];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    btnCreateAction.layer.cornerRadius = 5.f;
    btnCreateAction.layer.borderColor = [UIColor clearColor].CGColor;
    btnCreateAction.layer.borderWidth = 1.f;
    
    [vwFooter.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwFooter.layer setBorderWidth:1.f];
    // drop shadow
    [vwFooter.layer setShadowColor:[UIColor blackColor].CGColor];
    [vwFooter.layer setShadowOpacity:0.3];
    [vwFooter.layer setShadowRadius:2.0];
    [vwFooter.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    vwFooter.hidden = true;
    
    btnHeaderAction.layer.cornerRadius = 5.f;
    btnHeaderAction.layer.borderColor = [UIColor whiteColor].CGColor;
    btnHeaderAction.layer.borderWidth = 1.f;
    
    
    
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)getGoaDetailsByGoalID:(NSString*)_goalID{
    
    goalID = _goalID;
    
    if (_goalID) {
        [self showLoadingScreen];
        [APIMapper getGoalDetailsWithGoalID:_goalID userID:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([[responseObject objectForKey:@"code"] integerValue] == kSuccessCode){
                [self showGemDetailsWith:responseObject];
            }
            else{
                tableView.hidden = false;
                vwFooter.hidden = true;
                isDataAvailable = false;
                strErrorMsg = @"Goal details not available";
                if ([responseObject objectForKey:@"text"]) {
                    strErrorMsg = [responseObject objectForKey:@"text"];
                    [tableView reloadData];
                }
            }
            [self hideLoadingScreen];
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            vwFooter.hidden = true;
            strErrorMsg = error.localizedDescription;
            [self hideLoadingScreen];
            tableView.hidden = false;
            isDataAvailable = false;
            [tableView reloadData];
            
        }];
        
    }
}

-(void)showGemDetailsWith:(NSDictionary*)details{
    
    vwFooter.hidden = false;
    [arrActions removeAllObjects];
    isDataAvailable = true;
    [arrDataSource removeAllObjects];
    if (NULL_TO_NIL([details objectForKey:@"gem_media"]))
        arrDataSource = [NSMutableArray arrayWithArray:[details objectForKey:@"gem_media"]];
    actionDetails = details;
    tableView.hidden = false;
    imgFavourite.image = [UIImage imageNamed:@"Favourite.png"];
    if ([[actionDetails objectForKey:@"favourite_status"] boolValue])
        imgFavourite.image = [UIImage imageNamed:@"Favourite_Active.png"];
    if (NULL_TO_NIL([details objectForKey:@"action"]))
        arrActions = [NSMutableArray arrayWithArray:[details objectForKey:@"action"]];
    lblTitle.text = [[NSString stringWithFormat:@"%@ DETAILS",[actionDetails objectForKey:@"gem_type"]] capitalizedString];
    
    [btnShare setImage:[UIImage imageNamed:@"Share.png"] forState:UIControlStateNormal];
    [btnShare setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnShare setTitle:@"Share" forState:UIControlStateNormal];
    
    if ([[actionDetails objectForKey:@"share_status"] boolValue]) {
        [btnShare setImage:[UIImage imageNamed:@"Shared.png"] forState:UIControlStateNormal];
        [btnShare setTitle:@"Shared" forState:UIControlStateNormal];
        [btnShare setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];

    }
    btnCreateAction.hidden = false;
    if ([[actionDetails objectForKey:@"goal_status"] boolValue]) btnCreateAction.hidden = true;
    [tableView reloadData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (!isDataAvailable) return kMinimumCellCount;
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    if (section == eSectionInfo){
        NSInteger rows = 4;
        
        NSString *string;
        if (NULL_TO_NIL([actionDetails objectForKey:@"preview_url"])){
            string = [actionDetails objectForKey:@"preview_url"];
            
        }else if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"])){
            string = [actionDetails objectForKey:@"gem_details"];
        }
        if (string.length) {
            NSError *error = nil;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                       error:&error];
            NSArray *matches = [detector matchesInString:string
                                                 options:0
                                                   range:NSMakeRange(0, [string length])];
            if (matches.count > 0) {
                rows = 5;
            }

        }
        return rows;

    }
    
    else if (section == eSectionActions){
        if (arrActions.count <= 0)  return 0;
        btnHeaderAction.hidden = false;
        return arrActions.count;
    }
    else
        return arrDataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        static NSString *CellIdentifier = @"ErrorDiaplay";
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        if ([[[cell contentView] viewWithTag:1] isKindOfClass:[UILabel class]]) {
            UILabel *lblError = (UILabel*)[[cell contentView] viewWithTag:1];
            lblError.text = strErrorMsg;
        }
        return cell;
    }
    if (indexPath.section == eSectionInfo){
        if (indexPath.row == 0) {
             cell = [self configureBasicInfoCell:indexPath];
        }
        if (indexPath.row == 1) {
            static NSString *CellIdentifier = @"GoalDetailLocationCell";
            GoalDetailLocationCell *cell = (GoalDetailLocationCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (NULL_TO_NIL([actionDetails objectForKey:@"location_name"])){
                cell.lblLocation.text = [actionDetails objectForKey:@"location_name"];
                
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row == 2) {
            static NSString *CellIdentifier = @"GoalDetailLocationContact";
            GoalDetailLocationContact *cell = (GoalDetailLocationContact*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (NULL_TO_NIL([actionDetails objectForKey:@"contact_name"])){
                cell.lblContact.text = [actionDetails objectForKey:@"contact_name"];
                
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
        if (indexPath.row == 3) {
            GoalDetailsCustomCellDescription *cell = (GoalDetailsCustomCellDescription *)[tableView dequeueReusableCellWithIdentifier:@"GoalDetailsCustomCellDescription"];
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:CommonFont_New size:14],
                                         };
            NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[actionDetails objectForKey:@"gem_details"] attributes:attributes];
            cell.lbltDescription.attributedText = attributedText;
            cell.lbltDescription.systemURLStyle = YES;
            cell.lbltDescription.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
                // Open URLs
                [self attemptOpenURL:[NSURL URLWithString:string]];
            };
            cell.lbltDescription.hashtagLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
                // Open URLs
                [self hashTagClickedWithTag:string];
                
            };
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row == 4) {
            GalDetailsCustomCellPreview *cell = (GalDetailsCustomCellPreview *)[tableView dequeueReusableCellWithIdentifier:@"GalDetailsCustomCellPreview"];
            
            NSString *string;
            BOOL isPreview = false;
            if (NULL_TO_NIL([actionDetails objectForKey:@"preview_url"])){
                string = [actionDetails objectForKey:@"preview_url"];
                isPreview = true;
                
            }else if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"])){
                string = [actionDetails objectForKey:@"gem_details"];
            }
            if (string.length) {
                NSError *error = nil;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                           error:&error];
                NSArray *matches = [detector matchesInString:string
                                                     options:0
                                                       range:NSMakeRange(0, [string length])];
                if (matches.count > 0) {
                    NSTextCheckingResult *match = [matches firstObject];
                    
                    if (!isPreview) {
                        cell.heightForPreview.constant = 105;
                        cell.previewImageHeight.constant = 0;
                        cell.previewImageTop.constant = 0;
                    }else{
                        cell.heightForPreview.constant = 305;
                        cell.previewImageHeight.constant = 200;
                        cell.previewImageTop.constant = 5;
                    }
                    
                    if ([[CashedURL sharedManager].dictCashedURLS objectForKey:string]) {
                        MTDURLPreview *preview = [[CashedURL sharedManager].dictCashedURLS objectForKey:string];
                        [cell.indicator stopAnimating];
                        cell.lblTitle.text = preview.title;
                        cell.lblDescription.text = preview.content;
                        cell.lblDomain.text = preview.domain;
                        [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                           placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  }];
                        
                    }else{
                        [MTDURLPreview loadPreviewWithURL:[match URL] completion:^(MTDURLPreview *preview, NSError *error) {
                            if (!error) {
                                if (preview) [[CashedURL sharedManager].dictCashedURLS setObject:preview forKey:string];
                            }
                            [cell.indicator stopAnimating];
                            cell.lblTitle.text = preview.title;
                            cell.lblDescription.text = preview.content;
                            cell.lblDomain.text = preview.domain;
                            [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                               placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                      }];
                            
                            
                        }];
                    }
                    
                }

            }
            
           
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
    }
    
    
    else if (indexPath.section == eSectionMedia){
        static NSString *CellIdentifier = @"mediaListingCell";
        GemDetailsCustomTableViewCell *cell = (GemDetailsCustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [self resetCellVariables:cell];
        cell.delegate = self;
        [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];
        if (indexPath.row < arrDataSource.count)
            [self showMediaDetailsWithCell:cell andDetails:arrDataSource[indexPath.row] index:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
        
    }else{
        if (arrActions.count <= 0) {
           UITableViewCell *cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No Actions Available."];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryNone;
            return cell;
        }
        static NSString *CellIdentifier = @"ActionCell";
        ActionCustomCell *cell = (ActionCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row < arrActions.count) {
            cell.btnStatus.tag = indexPath.row;
            [cell.btnStatus addTarget:self action:@selector(updateStatus:) forControlEvents:UIControlEventTouchUpInside];
            NSDictionary *goalsDetails = arrActions[indexPath.row];
            if (NULL_TO_NIL([goalsDetails objectForKey:@"action_title"]))
                cell.lblTitle.text = [goalsDetails objectForKey:@"action_title"];
            if (NULL_TO_NIL([goalsDetails objectForKey:@"action_status"])){
                [cell.btnStatus setImage:[UIImage imageNamed:@"CheckBox_Goals_Active.png"] forState:UIControlStateNormal];
                NSInteger status = [[goalsDetails objectForKey:@"action_status"] integerValue];
                if (status == 0) {
                    [cell.btnStatus setImage:[UIImage imageNamed:@"CheckBox_Goals.png"] forState:UIControlStateNormal];
                }
            }
        }
        return cell;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) {
         return  UITableViewAutomaticDimension;
    }
    if (indexPath.section == eSectionInfo) {
        
        if (indexPath.row == 1) {
            if (NULL_TO_NIL([actionDetails objectForKey:@"location_name"])) return UITableViewAutomaticDimension;
            else return 0;
        }
        if (indexPath.row == 2) {
            if (NULL_TO_NIL([actionDetails objectForKey:@"contact_name"])) return UITableViewAutomaticDimension;
            else return 0;
        }
        if (indexPath.row == 3) {
            if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"])) return UITableViewAutomaticDimension;
            else return 0;
        }
        if (indexPath.row == 4) return UITableViewAutomaticDimension;
        return  UITableViewAutomaticDimension;
    }
    else if (indexPath.section == eSectionMedia) {
      
        return UITableViewAutomaticDimension;
        
    }else{
        return UITableViewAutomaticDimension;
    }
    float height = kActionCellHeight;
    return height;

}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (arrActions.count <= 0) {
        return 0.01;
    }
    if (section == eSectionActions) {
        CGFloat height = 45;
        return height;
    }
    return kHeaderHeight;
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    playingIndex = -1;
    isScrolling = NO;
    isPlaying = FALSE;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)aScrollView
{
    isScrolling = YES;
    isPlaying = FALSE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 4) {
        [self openPreviewURL];
        
    }
    NSString *mediaType ;
    NSMutableArray *images = [NSMutableArray new];
    if (indexPath.section == eSectionMedia){
        if (indexPath.row < arrDataSource.count) {
            NSDictionary *mediaInfo = arrDataSource[indexPath.row];
            if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
                mediaType = [mediaInfo objectForKey:@"media_type"];
            }
            if (mediaType) {
                if ([mediaType isEqualToString:@"image"]) {
                    NSURL *url =  [NSURL URLWithString:[mediaInfo objectForKey:@"gem_media"]];
                    [images addObject:url];
                    for (NSDictionary *details in arrDataSource) {
                        if (NULL_TO_NIL([details objectForKey:@"media_type"])) {
                            mediaType = [details objectForKey:@"media_type"];
                        }
                        if (mediaType) {
                            if ([mediaType isEqualToString:@"image"]) {
                                NSURL *url =  [NSURL URLWithString:[details objectForKey:@"gem_media"]];
                                if (![images containsObject:url]) {
                                    [images addObject:url];
                                }
                                
                            }
                            
                        }
                    }
                    if (images.count) {
                        [self presentGalleryWithImages:images];
                    }
                }
                
            }
        }
        
    }
    else if (indexPath.section == eSectionActions){
        
        if (indexPath.row < arrActions.count){
            ActionDetailPageViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGoalsDreamsDetailPage];
            detailPage.delegate = self;
            [[self navigationController]pushViewController:detailPage animated:YES];
            NSDictionary *_actionDetails = arrActions[indexPath.row];
            NSString *actionID = [_actionDetails objectForKey:@"action_id"];
            NSString *goalActionID = [_actionDetails objectForKey:@"goalaction_id"];
            [detailPage getActionDetailsByGoalActionID:goalActionID actionID:actionID goalID:goalID];
                
        }
        
       
    }
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == eSectionActions){
        if (arrActions.count <= 0) {
            return nil;
        }
        UIView *vwHeader = [UIView new];
        
        UIView *vwBG = [UIView new];
        [vwHeader addSubview:vwBG];
        vwBG.translatesAutoresizingMaskIntoConstraints = NO;
        [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[vwBG]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwBG)]];
        [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[vwBG]-5-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwBG)]];
         vwBG.backgroundColor = [UIColor getThemeColor];
       
        UILabel *_lblTitle = [UILabel new];
        _lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
        [vwBG addSubview:_lblTitle];
        [vwBG addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[_lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_lblTitle)]];
        [vwBG addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_lblTitle)]];
        _lblTitle.text = @"ACTIONS";
        _lblTitle.font = [UIFont fontWithName:CommonFont size:14];
        _lblTitle.textColor = [UIColor whiteColor];
        vwHeader.backgroundColor = [UIColor clearColor];
        return vwHeader;
    }
    return nil;
   
}


- (void)openPreviewURL
{
    NSString *string;
    if (NULL_TO_NIL([actionDetails objectForKey:@"preview_url"])){
        string = [actionDetails objectForKey:@"preview_url"];
        
    }else if (NULL_TO_NIL([actionDetails objectForKey:@"gem_details"])){
        string = [actionDetails objectForKey:@"gem_details"];
    }
    if (string.length) {
        NSError *error = nil;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                   error:&error];
        NSArray *matches = [detector matchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
        if (matches.count > 0) {
            NSTextCheckingResult *match = [matches firstObject];
            if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                [[UIApplication sharedApplication] openURL:[match URL] options:@{}
                                         completionHandler:^(BOOL success) {
                                         }];
            }
        }

    }
    
}

#pragma mark - Custom Cell Customization Methods

-(ActionDetailsCustomCell*)configureBasicInfoCell:(NSIndexPath*)indexPath{
    
    static NSString *CellIdentifier = @"infoCustomCell";
    ActionDetailsCustomCell *cell = (ActionDetailsCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (NULL_TO_NIL([actionDetails objectForKey:@"gem_title"])){
        cell.lblTitle.text = [actionDetails objectForKey:@"gem_title"];
        cell.rightConstarint.constant = 40;
        if (_shouldHideEditBtn) {
            cell.rightConstarint.constant = 10;
        }
        strTitle = [actionDetails  objectForKey:@"gem_title"];
    }
   
    if (NULL_TO_NIL([actionDetails objectForKey:@"gem_datetime"]))
    cell.lblDate.text = [Utility getDaysBetweenTwoDatesWith:[[actionDetails objectForKey:@"gem_datetime"] doubleValue]];
    cell.delegate = self;
    cell.btnEdit.hidden = false;
    if (_shouldHideEditBtn) cell.btnEdit.hidden = true;
    if (_isFromJournal) cell.btnMore.hidden = true;
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
    
}

-(void)showMediaDetailsWithCell:(GemDetailsCustomTableViewCell*)cell andDetails:(NSDictionary*)mediaInfo index:(NSInteger)index{
    
    NSString *mediaType ;
    if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
        mediaType = [mediaInfo objectForKey:@"media_type"];
    }
    
    if (mediaType) {
        
        if ([mediaType isEqualToString:@"image"]) {
            
            // Type Image
            [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
            [cell.activityIndicator startAnimating];
            [cell.imgGemMedia sd_setImageWithURL:[NSURL URLWithString:[mediaInfo objectForKey:@"gem_media"]]
                                placeholderImage:[UIImage imageNamed:@""]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           
                                           [cell.activityIndicator stopAnimating];
                                       }];
        }
        
        else if ([mediaType isEqualToString:@"audio"]) {
            
            // Type Audio
            [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
            if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                [[cell btnAudioPlay]setHidden:false];
                
            }
            
        }
        
        else if ([mediaType isEqualToString:@"video"]) {
            
            // Type Video
            if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                NSString *videoURL = [mediaInfo objectForKey:@"gem_media"];
                if (videoURL.length){
                    [[cell btnVideoPlay]setHidden:false];
                }
            }
            
            if (NULL_TO_NIL([mediaInfo objectForKey:@"video_thumb"])) {
                NSString *videoThumb = [mediaInfo objectForKey:@"video_thumb"];
                if (videoThumb.length) {
                    [cell.activityIndicator startAnimating];
                    [cell.imgGemMedia sd_setImageWithURL:[NSURL URLWithString:videoThumb]
                                        placeholderImage:[UIImage imageNamed:@""]
                                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                   [cell.activityIndicator stopAnimating];
                                                  
                                               }];
                }
                
            }
            
        }
        
    }
    
    float imageHeight = 0;
    float width = [[mediaInfo objectForKey:@"image_width"] floatValue];
    float height = [[mediaInfo objectForKey:@"image_height"] floatValue];
    float ratio = width / height;
    imageHeight = (self.view.frame.size.width - 10) / ratio;
    cell.constraintForHeight.constant = imageHeight;
    
}


-(void)resetCellVariables:(GemDetailsCustomTableViewCell*)cell{
    
    [cell.imgGemMedia setImage:[UIImage imageNamed:@""]];
    [cell.activityIndicator stopAnimating];
    [[cell btnVideoPlay]setHidden:YES];
    [[cell btnAudioPlay]setHidden:YES];
    
}
#pragma mark - Custom Cell Delegates

-(void)resetPlayerVaiablesForIndex:(NSInteger)tag{
    
    if (tag < arrDataSource.count){
        
        NSDictionary * mediaInfo = arrDataSource[tag];
        NSString *mediaType ;
        if (NULL_TO_NIL([mediaInfo objectForKey:@"media_type"])) {
            mediaType = [mediaInfo objectForKey:@"media_type"];
        }
        
        if (mediaType) {
            
            if ([mediaType isEqualToString:@"image"]) {
                
            }
            else if ([mediaType isEqualToString:@"audio"]) {
                
                NSString *audioURL = [mediaInfo objectForKey:@"gem_media"];
                if (audioURL.length){
                    
                    NSURL *movieURL = [NSURL URLWithString:audioURL];
                    [self showAudioPlayerWithURL:movieURL];
                    
                }
            }
            
            else if ([mediaType isEqualToString:@"video"]) {
                
                // Type Video
                if (NULL_TO_NIL([mediaInfo objectForKey:@"gem_media"])) {
                    NSString *videoURL = [mediaInfo objectForKey:@"gem_media"];
                    if (videoURL.length){
                        NSError* error;
                        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
                        [[AVAudioSession sharedInstance] setActive:NO error:&error];
                        NSURL *movieURL = [NSURL URLWithString:videoURL];
                        AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
                        playerViewController.player = [AVPlayer playerWithURL:movieURL];
                         [playerViewController.player play];
                        [self presentViewController:playerViewController animated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] addObserver:self
                                                                 selector:@selector(videoDidFinish:)
                                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                                   object:[playerViewController.player currentItem]];
                        
                    }
                }
                
            }
        }
    }
    
}

- (void)videoDidFinish:(id)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //fade out / remove subview
}

-(void)moreButtonClicked{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"GOAL"
                                                                   message:@"Choose an action"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Edit"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                              [self editGoalSelected];
                                                              [alert dismissViewControllerAnimated:YES completion:^{
                                                                  
                                                              }];
                                                              
                                                              
                                                          }];
    UIAlertAction *second = [UIAlertAction actionWithTitle:@"Delete"
                                                     style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                         
                                                          [self deleteGoalClicked];
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:^{
                                                         }];
                                                     }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    [alert addAction:firstAction];
    [alert addAction:second];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
   
    
}

-(void)editGoalSelected{
    
   if ([[actionDetails objectForKey:@"share_status"] boolValue]) {
        
        UINavigationController *nav =self.navigationController;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"EDIT GOAL"
                                                                       message:@"This is a shared GOAL.Do you still want to change it?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"YES"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  NSString *gemType = @"goal";
                                                                  CreateActionInfoViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCreateActionMedias];
                                                                  detailPage.actionType = eActionTypeGoalsAndDreams;
                                                                  detailPage.delegate = self;
                                                                  detailPage.strTitle = @"EDIT GOALS&DREAMS";
                                                                  [[self navigationController]pushViewController:detailPage animated:YES];
                                                                  [detailPage getMediaDetailsForGemsToBeEditedWithGEMID:goalID GEMType:gemType];
                                                                  
                                                              }];
        UIAlertAction *second = [UIAlertAction actionWithTitle:@"NO"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         }];
        
        [alert addAction:firstAction];
        [alert addAction:second];
        [nav presentViewController:alert animated:YES completion:nil];
        
        
    }else{
        
        NSString *gemType = @"goal";
        CreateActionInfoViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCreateActionMedias];
        detailPage.actionType = eActionTypeGoalsAndDreams;
        detailPage.delegate = self;
        detailPage.strTitle = @"EDIT GOALS&DREAMS";
        [[self navigationController]pushViewController:detailPage animated:YES];
        [detailPage getMediaDetailsForGemsToBeEditedWithGEMID:goalID GEMType:gemType];
    }
    
}

-(void)deleteGoalClicked{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Goal"
                                                                   message:@"Do you really want to delete the Goal?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"CANCEL"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"DELETE"
                                                     style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                         
                                                         [self showLoadingScreen];
                                                         [APIMapper hideAGoalWithGoalID:goalID goalActionID:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                             [self hideLoadingScreen];
                                                             if ([self.delegate respondsToSelector:@selector(refershGoalsAndDreamsListingAfterUpdate)]) {
                                                                 [self.delegate refershGoalsAndDreamsListingAfterUpdate];
                                                             }
                                                             [self goBack:nil];
                                                             
                                                         } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                                             
                                                             [self hideLoadingScreen];
                                                             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Goal"
                                                                                                                            message:[error localizedDescription]
                                                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                                             
                                                             UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK"
                                                                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                                  
                                                                                                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                                              }];
                                                             
                                                             [alert addAction:cancel];
                                                             [self presentViewController:alert animated:YES completion:nil];
                                                         }];
                                                     }];
    
    [alert addAction:cancel];
    [alert addAction:delete];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

#pragma mark - Media Creation Page Delegates

-(void)goalsAndDreamsCreatedWithGoalTitle:(NSString*)goalTitle goalID:(NSInteger)_goalID{
    
    isUpdated = true;
    if ([self.delegate respondsToSelector:@selector(refershGoalsAndDreamsListingAfterUpdate)]) {
        [self.delegate refershGoalsAndDreamsListingAfterUpdate];
    }
    [self getGoaDetailsByGoalID:goalID];
    
}

-(void)newActionCreatedWithActionTitle:(NSString*)actionTitle actionID:(NSInteger)_actionID{
    
    isUpdated = true;
    if ([self.delegate respondsToSelector:@selector(refershGoalsAndDreamsListingAfterUpdate)]) {
        [self.delegate refershGoalsAndDreamsListingAfterUpdate];
    }
    [self getGoaDetailsByGoalID:goalID];
}

-(void)refershGoalsAndDreamsAfterUpdate{
    
    if ([self.delegate respondsToSelector:@selector(refershGoalsAndDreamsListingAfterUpdate)]) {
        [self.delegate refershGoalsAndDreamsListingAfterUpdate];
    }
    isUpdated = true;
    [self getGoaDetailsByGoalID:goalID];
}


#pragma mark - Generic Methods

- (void)attemptOpenURL:(NSURL *)url
{
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (!safariCompatible) {
        
        NSString *urlString = url.absoluteString;
        urlString = [NSString stringWithFormat:@"http://%@",url.absoluteString];
        url = [NSURL URLWithString:urlString];
        
    }
    safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:url options:@{}
                                     completionHandler:^(BOOL success) {
                                     }];
        }
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem"
                                                                       message:@"The selected link cannot be opened."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dismiss"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:cancel];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
       
    }
}



-(IBAction)previewClickedWithGesture:(UIButton*)btn{
    
    if (strDescription){
        
        NSString *string = strDescription;
        NSError *error = nil;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                   error:&error];
        NSArray *matches = [detector matchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
        if (matches.count > 0) {
            NSTextCheckingResult *match = [matches firstObject];
            if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                [[UIApplication sharedApplication] openURL:[match URL] options:@{}
                                         completionHandler:^(BOOL success) {
                                         }];
            }
            
        }
    }
}


-(IBAction)tableScrollToActions:(id)sender{
    
    if (arrActions.count){
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        [tableView scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionTop
                                 animated:YES];
        
       
    }
   
}
-(IBAction)createNewAction{
    
    CreateActionInfoViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCreateActionMedias];
    detailPage.strTitle = @"CREATE ACTION";
    detailPage.actionType = eActionTypeActions;
    detailPage.strGoalID = goalID;
    detailPage.delegate = self;
    [[self navigationController]pushViewController:detailPage animated:YES];
    
}
-(void)updateStatus:(UIButton*)btnStatus{
    
    isUpdated = true;
    NSInteger index = btnStatus.tag;
    
    NSString *strMessage = @"Do you want to change this action to Complete ?";
    if (arrActions.count > index) {
        NSMutableDictionary *_actionDetails = [NSMutableDictionary dictionaryWithDictionary:arrActions[index]];
        if (_actionDetails) {
            if ([[_actionDetails objectForKey:@"action_status"] integerValue] == 1) strMessage = @"Do you want to change this action to Pending ?";
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Action"
                                                                   message:strMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"YES"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                              if (arrActions.count > index) {
                                                                  NSMutableDictionary *_actionDetails = [NSMutableDictionary dictionaryWithDictionary:arrActions[index]];
                                                                  if (_actionDetails) {
                                                                      if ([[_actionDetails objectForKey:@"action_status"] integerValue] == 0)
                                                                          [_actionDetails setObject:[NSNumber numberWithInt:1] forKey:@"action_status"];
                                                                      else
                                                                          [_actionDetails setObject:[NSNumber numberWithInt:0] forKey:@"action_status"];
                                                                      
                                                                      [arrActions replaceObjectAtIndex:index withObject:_actionDetails];
                                                                      NSRange range = NSMakeRange(2, 1);
                                                                      NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
                                                                      [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
                                                                  }
                                                                  
                                                                  NSString *actionID = [_actionDetails objectForKey:@"action_id"];
                                                                  NSString *goalActionID = [_actionDetails objectForKey:@"goalaction_id"];
                                                                  [APIMapper updateGoalActionStatusWithActionID:actionID goalID:goalID goalActionID:goalActionID andUserID:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                      
                                                                  } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                                                      
                                                                  }];
                                                                  
                                                              }
                                                              
                                                              
                                                          }];
    UIAlertAction *second = [UIAlertAction actionWithTitle:@"NO"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                     }];
    
    [alert addAction:firstAction];
    [alert addAction:second];
    [self presentViewController:alert animated:YES completion:nil];
}



-(void)showAudioPlayerWithURL:(NSURL*)url{
    
    if (!vwAudioPlayer) {
        vwAudioPlayer = [[[NSBundle mainBundle] loadNibNamed:@"CustomAudioPlayerView" owner:self options:nil] objectAtIndex:0];
        vwAudioPlayer.delegate = self;
    }
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = vwAudioPlayer;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer setupAVPlayerForURL:url];
    }];
    
}

-(void)closeAudioPlayerView{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwAudioPlayer.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [vwAudioPlayer removeFromSuperview];
        vwAudioPlayer = nil;
        NSError* error;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        [[AVAudioSession sharedInstance] setActive:NO error:&error];
    }];
    
    
    
}


#pragma mark - Comment Showing and its Delegate

-(IBAction)commentComposeView{
    
    CommentComposeViewController*  composeComment =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCommentCompose];
        composeComment.delegate = self;
    NSDictionary *gemDetails = actionDetails;
    composeComment.dictGemDetails = gemDetails;
    composeComment.isFromCommunityGem = false;
    [self.navigationController pushViewController:composeComment animated:YES];
    
}


#pragma mark - Photo Browser & Deleagtes

- (void)presentGalleryWithImages:(NSArray*)images
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}


#pragma mark - Share Action 

-(IBAction)shareButtonClicked{
    
    [btnShare setEnabled:false];
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SharePost"
                                                          owner:nil
                                                        options:nil];
    
    SharePost *vwPopUP = [arrayOfViews objectAtIndex:0];
    vwPopUP.delegate = self;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.4 delay:.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
    
}

-(void)sharePostTextPopUpCloseApppliedAtIndex:(NSInteger)index{
    //[self sharePostToCommunityWithMessage:nil];
     [btnShare setEnabled:true];
}

-(void)sharePostSubmitAppliedWithText:(NSString*)shareTxt AtIndex:(NSInteger)index{
    [self sharePostToCommunityWithMessage:shareTxt];
}

-(void)sharePostToCommunityWithMessage:(NSString*)mesage{
    
    [btnShare setEnabled:true];
    NSDictionary *gemDetails =  actionDetails;
    NSString *gemID;
    NSString *gemType;
    
    if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
        gemID = [gemDetails objectForKey:@"gem_id"];
    
    if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
        gemType = [gemDetails objectForKey:@"gem_type"];
    
    if (gemID && gemType) {
        
        [self showLoadingScreen];
        
        [APIMapper shareAGEMToCommunityWith:[User sharedManager].userId gemID:gemID gemType:gemType shareMsg:mesage success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self hideLoadingScreen];
            if ([[responseObject objectForKey:@"code"]integerValue] == kSuccessCode){
                if ([[responseObject objectForKey:@"code"]integerValue] == kSuccessCode){
                    if ( NULL_TO_NIL( [responseObject objectForKey:@"text"])){
                        //[ALToastView toastInView:self.view withText: [responseObject objectForKey:@"text"]];
                    }
                }
                [btnShare setImage:[UIImage imageNamed:@"Shared.png"] forState:UIControlStateNormal];
                [btnShare setTitle:@"Shared" forState:UIControlStateNormal];
                [btnShare setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
            }
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            [self hideLoadingScreen];
            if (error && error.localizedDescription){
                [ALToastView toastInView:self.view withText:error.localizedDescription];
            }
        }];
    }
}

#pragma mark - Add to Favourite Showing

-(IBAction)addToFavouriteClicked{
    
    NSMutableDictionary *gemDetails = [NSMutableDictionary dictionaryWithDictionary:actionDetails];
    NSString *gemID;
    NSString *gemType;
    
    if (![[actionDetails objectForKey:@"favourite_status"] boolValue]){
        imgFavourite.image = [UIImage imageNamed:@"Favourite_Active.png"];
        [gemDetails setValue:[NSNumber numberWithBool:1] forKey:@"favourite_status"];
    }
    
    if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
        gemID = [gemDetails objectForKey:@"gem_id"];
    
    if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
        gemType = [gemDetails objectForKey:@"gem_type"];
    
    
    if (gemID && gemType) {
        [APIMapper addGemToFavouritesWith:[User sharedManager].userId gemID:gemID gemType:gemType success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
        }];
    }
}




-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(IBAction)hashTagClickedWithTag:(NSString*)tag{
    
    HashTagGemListingViewController *hashListing =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForHashTagListings];
    hashListing.strTagName = tag;
    [[self navigationController]pushViewController:hashListing animated:YES];
    
}

-(IBAction)goBack:(id)sender{
    /*
    if (isUpdated) {
        if ([self.delegate respondsToSelector:@selector(refershGoalsAndDreamsListingAfterUpdate)]) {
            [self.delegate refershGoalsAndDreamsListingAfterUpdate];
        }
    }*/
    [[self navigationController] popViewControllerAnimated:YES];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
