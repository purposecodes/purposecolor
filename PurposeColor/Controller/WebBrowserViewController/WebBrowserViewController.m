//
//  WebBrowserViewController.m
//  SignPost
//
//  Created by Purpose Code on 11/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kHelp                       @"HELP"
#define kPrivacyPolicy              @"PRIVACY POLICY"
#define kTermsOfService             @"TERMS OF SERVICE"


#import "WebBrowserViewController.h"
#import "MenuViewController.h"
#import "Constants.h"
#import <AVKit/AVKit.h>

@interface WebBrowserViewController () <SWRevealViewControllerDelegate>{
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UIWebView *webView;
    IBOutlet UIView *vwBg;
    NSString *strHelpURL;
    
}

@end

@implementation WebBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    strHelpURL = [BaseURLString stringByReplacingOccurrencesOfString:@"api.php?" withString:@""];

    lblTitle.text = [_strTitle capitalizedString];
    if ([_strTitle isEqualToString:kHelp]) {
        
        [self showLoadingScreen];
        [self loadHelpPage];
    }
    else if ([_strTitle isEqualToString:kPrivacyPolicy]) {
        [self loadContentWithType:@"Privacy"];
    }
    else if ([_strTitle isEqualToString:kTermsOfService]) {
        [self loadContentWithType:@"Terms"];
    }
    
    // Do any additional setup after loading the view.
}

-(IBAction)playVideo:(id)sender{
    
    NSError* error;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:NO error:&error];
    NSURL *movieURL = [NSURL URLWithString:@"http://purposecolor.com/assets/images/Purpose%20Color-Final.mp4"];
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    [playerViewController.player play];
    playerViewController.player = [AVPlayer playerWithURL:movieURL];
    [self presentViewController:playerViewController animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoDidFinish:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[playerViewController.player currentItem]];
}

- (void)videoDidFinish:(id)notification
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
    //fade out / remove subview
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideLoadingScreen];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:_strTitle message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [self hideLoadingScreen];
}

-(IBAction)goBack:(id)sender{
    
    if ([webView canGoBack]) {
        [self showLoadingScreen];
        [webView goBack];
        return;
    }
    
    [[self navigationController] popViewControllerAnimated:YES];

}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading..";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


#pragma mark - Cashing Methods

-(void)loadContentWithType:(NSString*)type{
    
    [self showLoadingScreen];
    [APIMapper getWebContentWithType:type success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        if ([responseObject objectForKey:@"text"]) {
            [self saveDetailsToFlder:[responseObject objectForKey:@"text"] type:type];
        }
        [self showContentFromFolder];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self showContentFromFolder];
        [self hideLoadingScreen];
    }];
    
}

-(void)loadHelpPage{
    
    if (!_strHelpType) {
         [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@help.php?type=%@",strHelpURL,_strHelpType]]]];
    }else{
        
        NSString *strURL = @"";
        
        if ([_strHelpType isEqualToString:@"inspire"])
            strURL = @"inspire-be-inspired.php";
        else if ([_strHelpType isEqualToString:@"journal"])
            strURL = @"smartjournal.php";
        else if ([_strHelpType isEqualToString:@"intelligence"])
            strURL = @"emotionalintelligence.php";
        else if ([_strHelpType isEqualToString:@"goal"])
            strURL = @"goalsdreams.php";
        else if ([_strHelpType isEqualToString:@"community"])
            strURL = @"community.php";
            
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strHelpURL,strURL]]]];
    }
   
}

-(void)saveDetailsToFlder:(NSString*)content type:(NSString*)type{
    
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",type];
    NSString *path = [[self applicationDocumentsDirectory].path
                      stringByAppendingPathComponent:fileName];
    [content writeToFile:path atomically:YES
                encoding:NSUTF8StringEncoding error:nil];
    
}


- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

-(void)showContentFromFolder{
    
    NSString *type;
    
    if ([_strTitle isEqualToString:kHelp]) {
        type = @"Help";
    }
    else if ([_strTitle isEqualToString:kPrivacyPolicy]) {
        type = @"Privacy";
    }
    else if ([_strTitle isEqualToString:kTermsOfService]) {
        type = @"Terms";
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt",type]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
        [webView loadHTMLString:content baseURL:nil];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
