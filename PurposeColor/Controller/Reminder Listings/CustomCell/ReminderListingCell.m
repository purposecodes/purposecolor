//
//  CustomCellForType.m
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "ReminderListingCell.h"

@implementation ReminderListingCell

- (void)awakeFromNib {
    
    [super awakeFromNib];

    [_vwBackground.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwBackground.layer setBorderWidth:1.f];
    _vwBackground.layer.cornerRadius = 5.f;
    
    // drop shadow
    [_vwBackground.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwBackground.layer setShadowOpacity:0.3];
    [_vwBackground.layer setShadowRadius:2.0];
    [_vwBackground.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUp{
    
   // _vwBackground.layer.borderColor = [UIColor getSeperatorColor].CGColor;
   // _vwBackground.layer.borderWidth = 1.f;
   

}






@end
