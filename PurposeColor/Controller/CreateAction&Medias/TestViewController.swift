//
//  TestViewController.swift
//  PurposeColor
//
//  Created by Purpose Code on 02/11/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

import UIKit
import algorithmia

class TestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //closure: () -> Void
   static func getTagsFromResponds(string:String,responds: @escaping (String) -> ()) {
        
        let input = string;
        let client = Algorithmia.client(simpleKey: "sim5O+pH6SG0KkJfAGlpPVWgQy51")
        let algo = client.algo(algoUri: "nlp/AutoTag/1.0.1")
        var arrTags = [String]()
        algo.pipe(text: input) { resp, error in
            if let array = resp.getJson() as? [String]{
                for _string in array {
                    let str = String(format: "#%@", _string)
                    arrTags.append(str)
                }
                var string = arrTags.joined(separator: ",")
                print(string)
                
                responds(string)
            }
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
