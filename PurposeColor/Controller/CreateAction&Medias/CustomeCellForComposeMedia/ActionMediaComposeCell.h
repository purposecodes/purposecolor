//
//  ActionMediaComposeCell.h
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionMediaComposeCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UITextField *txtTitleBg;
@property (nonatomic,weak) IBOutlet UITextField *txtTitle;
@property (nonatomic,weak) IBOutlet UITextView *txtDecsription;
@property (nonatomic,weak) IBOutlet UIView *vwBg;
@property (nonatomic,weak) IBOutlet UILabel *lblContactInfo;
@property (nonatomic,weak) IBOutlet UILabel *lblLocInfo;
@property (nonatomic,weak) IBOutlet UIView *vwFooter;
@property (nonatomic,weak) IBOutlet UIView *vwTitleBG;


@property (nonatomic,weak) IBOutlet NSLayoutConstraint *heightForPreview;
@property (nonatomic,weak)IBOutlet UIView *vwURLPreview;
@property (nonatomic,weak)IBOutlet UILabel *lblPreviewTitle;
@property (nonatomic,weak)IBOutlet UILabel *lblPreviewDescription;
@property (nonatomic,weak)IBOutlet UILabel *lblPreviewDomain;
@property (nonatomic,weak)IBOutlet UIImageView *imgPreview;
@property (nonatomic,weak)IBOutlet UIActivityIndicatorView *previewIndicator;
@property (nonatomic,weak)IBOutlet UIButton *btnShowPreviewURL;

@end
