//
//  ActionMediaComposeCell.m
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "ActionMediaComposeCell.h"

@implementation ActionMediaComposeCell

- (void)awakeFromNib {
    // Initialization code
    [self setUp];
}

-(void)setUp{
    
    _vwBg.layer.borderWidth = 1.f;
    _vwBg.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    //_txtTitle.layer.borderWidth = 1.f;
    //_txtTitle.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    [_txtTitleBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [_txtTitleBg.layer setBorderWidth:1.f];
    // drop shadow
    [_txtTitleBg.layer setShadowColor:[UIColor blackColor].CGColor];
    [_txtTitleBg.layer setShadowOpacity:0.3];
    [_txtTitleBg.layer setShadowRadius:2.0];
    [_txtTitleBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
