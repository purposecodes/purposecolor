//
//  GoalCategory.h
//  PurposeColor
//
//  Created by Purpose Code on 04/12/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoalCategory : NSObject

@property (nonatomic,strong) NSString *strCatID;
@property (nonatomic,strong) NSString *strCatName;
@property (nonatomic,strong) NSString *strCatImg;
@property (nonatomic,assign) BOOL  isSelected;

-(id)initWithName:(NSString*)name catID:(NSString*)catID image:(NSString*)img;
@end
