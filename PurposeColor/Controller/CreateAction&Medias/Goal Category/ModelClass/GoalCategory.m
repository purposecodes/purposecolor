//
//  GoalCategory.m
//  PurposeColor
//
//  Created by Purpose Code on 04/12/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "GoalCategory.h"

@implementation GoalCategory

-(id)initWithName:(NSString*)name catID:(NSString*)catID image:(NSString*)img{
    self = [super init];
    if (self) {
        self.strCatName = name;
        self.strCatID = catID;
        self.strCatImg = img;
        self.isSelected = false;
    }
     return  self;
}

@end
