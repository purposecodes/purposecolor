//
//  GoalCategoryCollectionViewCell.h
//  PurposeColor
//
//  Created by Purpose Code on 03/12/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalCategoryCollectionViewCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIView *vwBg;
@property (nonatomic,weak) IBOutlet UIImageView *imgVw;
@property (nonatomic,weak) IBOutlet UILabel *lblTitle;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;

@end
