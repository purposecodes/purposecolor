//
//  GoalCategoryViewController.h
//  PurposeColor
//
//  Created by Purpose Code on 03/12/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoalCategory.h"

@protocol GoalCategoryDelegte <NSObject>


@optional

/*!
 *This method is invoked when user Clicks "POST COMMENT" Button
 */
-(void)goalCategorySelectedCategoryID:(GoalCategory*)category;
-(void)savedCategoryIDs:(NSArray*)strIDS;


@end

@interface GoalCategoryViewController : UIViewController
@property (nonatomic,weak)  id<GoalCategoryDelegte>delegate;
@property (nonatomic,assign) BOOL isMultiSelectionAllowed;
@property (nonatomic,strong) NSString *savedIDS;

@end
