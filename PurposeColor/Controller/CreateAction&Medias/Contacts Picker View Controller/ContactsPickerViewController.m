//
//  ContactsPickerViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 15/03/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import "ContactsPickerViewController.h"
#import "ContactsTableViewCell.h"
#import "Constants.h"
#import <ContactsUI/ContactsUI.h>

@interface ContactsPickerViewController () <UISearchBarDelegate>{
    
    NSMutableArray *contactList;
    NSMutableArray *arrFiltered;
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnDone;
    IBOutlet UIView *vwLoading;
    IBOutlet UIView *vwInnerLoading;
    UISearchBar *searchBar;
    BOOL isDataAvailable;
}

@end

@implementation ContactsPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showLoadingScreen];
    [self setUp];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self contactScan];
}

-(void)setUp{
    
    vwInnerLoading.layer.cornerRadius = 5.f;
    vwInnerLoading.layer.borderWidth = 0.f;
    vwInnerLoading.layer.borderColor = [UIColor clearColor].CGColor;
    vwInnerLoading.clipsToBounds = YES;
    
    btnDone.hidden = true;
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [searchBar setShowsCancelButton:YES];
    searchBar.delegate = self;
    
    UIView *headerView = [[UIView alloc] initWithFrame:searchBar.frame];
    [headerView addSubview:searchBar];
    tableView.tableHeaderView = headerView;
    
    contactList = [[NSMutableArray alloc] init];

}

- (void) contactScan
{
    if ([CNContactStore class]) {
        //ios9 or later
        CNEntityType entityType = CNEntityTypeContacts;
        if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined)
        {
            CNContactStore * contactStore = [[CNContactStore alloc] init];
            [contactStore requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if(granted){
                    [self getAllContact];
                }
            }];
        }
        else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized)
        {
            [self getAllContact];
            
        }else{
            
            [self hideLoadingScreen];
            isDataAvailable = false;
            [tableView reloadData];
            searchBar.hidden = true;
        }
    }
}

-(void)getAllContact
{
    if([CNContactStore class])
    {
        //iOS 9 or later
        NSError* contactError;
        CNContactStore* addressBook = [[CNContactStore alloc]init];
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch =@[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey,CNContactThumbnailImageDataKey, CNContactGivenNameKey, CNContactPostalAddressesKey];
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
        BOOL success = [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
            [self parseContactWithContact:contact];
        }];
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                isDataAvailable = false;
                if (contactList.count) isDataAvailable = true;
                arrFiltered = [NSMutableArray arrayWithArray:contactList];
                [tableView reloadData];
                [self hideLoadingScreen];
                searchBar.hidden = false;
            });
        }else{
            [self hideLoadingScreen];
            isDataAvailable = false;
            [tableView reloadData];
            searchBar.hidden = true;
        }
      
    }
}

- (void)parseContactWithContact :(CNContact* )contact
{
    NSMutableDictionary *personInfo = [NSMutableDictionary dictionary];
    NSString * firstName =  contact.givenName;
    NSData *imageData = contact.thumbnailImageData;
    NSString * phone;
    NSArray *phones = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
    if (phones.count) {
        phone = [phones firstObject];
    }
    NSString * email = [contact.emailAddresses valueForKey:@"value"];
    if (phone && firstName) {
        [personInfo setObject:firstName forKey:@"name"];
        if (imageData.length) [personInfo setObject:[UIImage imageWithData:imageData] forKey:@"contact_image"];
        if (email) [personInfo setObject:email forKey:@"email"];
        if (phone) [personInfo setObject:phone forKey:@"Phone"];
        [personInfo setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        [contactList addObject:personInfo];
    }
    
    /*
    for (CNLabeledValue *label in contact.phoneNumbers) {
        NSString *phone = [label.value stringValue];
        if ([phone length] > 0) {
            //[contact.phones addObject:phone];
            NSLog(@"name phone %@ %@",firstName,phone);
        }
    }*/
}
     

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return 1;
    return arrFiltered.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsTableViewCell"];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnCheckBox.tag = indexPath.row;
    cell.imgUser.layer.cornerRadius = 25;
    cell.imgUser.layer.borderWidth = 1.f;
    cell.imgUser.layer.borderColor = [UIColor clearColor].CGColor;
    cell.imgUser.clipsToBounds = YES;
    cell.lblNameIcon.hidden = false;
    cell.imgUser.image = nil;
    [cell.btnCheckBox addTarget:self action:@selector(selectContact:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCheckBox setImage:[UIImage imageNamed:@"CheckBox_Goals"] forState:UIControlStateNormal];
    if (_isFromShare) {
         [cell.btnCheckBox setImage:[UIImage imageNamed:@"UnSelected"] forState:UIControlStateNormal];
    }
    
    if (!isDataAvailable) {
        static NSString *MyIdentifier = @"MyIdentifier";
        UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.userInteractionEnabled = false;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.text = @"No contacts found!";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        searchBar.hidden = true;
        return cell;
    }
    cell.lblName.text = @"";
    cell.lblNameIcon.text = @"";
    cell.lblPhoneNumber.text = @"";
    [cell.imgUser setImage:nil];

    if (indexPath.row < arrFiltered.count) {
        NSDictionary *details = arrFiltered[indexPath.row];
        if (NULL_TO_NIL([details objectForKey:@"name"])) {
            cell.lblName.text = [details objectForKey:@"name"];
            cell.lblNameIcon.text = [[[details objectForKey:@"name"] substringToIndex:1] uppercaseString];
        }
        if (NULL_TO_NIL([details objectForKey:@"Phone"])) {
            cell.lblPhoneNumber.text = [details objectForKey:@"Phone"];
        }
        if (NULL_TO_NIL([details objectForKey:@"contact_image"])) {
            cell.lblNameIcon.hidden = true;
            cell.imgUser.image = (UIImage*) [details objectForKey:@"contact_image"];
        }
        if ([[details objectForKey:@"isSelected"] boolValue]) {
            [cell.btnCheckBox setImage:[UIImage imageNamed:@"CheckBox_Goals_Active"] forState:UIControlStateNormal];
            if (_isFromShare) {
                [cell.btnCheckBox setImage:[UIImage imageNamed:@"Selected"] forState:UIControlStateNormal];
            }

        }
        
    }
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 80;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self selectContact:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return  0.1;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return nil;
}

-(IBAction)selectContact:(id)sender{
    NSInteger index = 0;
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *_sender = (UIButton*)sender;
        index = _sender.tag;
    }else{
        NSIndexPath *indexPath = (NSIndexPath*)sender;
        index = indexPath.row;
    }
    if (index < arrFiltered.count) {
        NSMutableDictionary *details = arrFiltered[index];
        if ([[details objectForKey:@"isSelected"] boolValue]) {
            [details setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        }else{
             [details setObject:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
        }
        
        [tableView reloadData];
        btnDone.hidden = true;
        for (NSDictionary *dict in arrFiltered) {
            if ([[dict objectForKey:@"isSelected"] boolValue]) {
                   btnDone.hidden = false;
            }
        }
       
        
    }

}

#pragma mark - Search Methods and Delegates


- (void)searchBar:(UISearchBar *)_searchBar textDidChange:(NSString *)searchString{
    
    [arrFiltered removeAllObjects];
    if (searchString.length > 0) {
        if (contactList.count > 0) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSString *regexString  = [NSString stringWithFormat:@".*\\b%@.*", searchString];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name matches[cd] %@", regexString];
                arrFiltered = [NSMutableArray arrayWithArray:[contactList filteredArrayUsingPredicate:predicate]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    isDataAvailable = true;
                    if (arrFiltered.count <= 0)isDataAvailable = false;
                    [tableView reloadData];
                });
            });
        }
    }else{
        if (contactList.count > 0) {
            if (searchBar.text.length > 0) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    NSString *regexString  = [NSString stringWithFormat:@".*\\b%@.*", searchString];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name matches[cd] %@", regexString];
                    arrFiltered = [NSMutableArray arrayWithArray:[contactList filteredArrayUsingPredicate:predicate]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        isDataAvailable = true;
                        if (arrFiltered.count <= 0)isDataAvailable = false;
                        [tableView reloadData];
                    });
                });
            }else{
                arrFiltered = [NSMutableArray arrayWithArray:contactList];
                dispatch_async(dispatch_get_main_queue(), ^{
                    isDataAvailable = true;
                    if (arrFiltered.count <= 0)isDataAvailable = false;
                    [tableView reloadData];
                });
            }
            
        }
    }
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    [_searchBar resignFirstResponder];
    
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar{
    
    [searchBar resignFirstResponder];
    arrFiltered = [NSMutableArray arrayWithArray:contactList];
    if (arrFiltered.count) {
        isDataAvailable = true;
    }
    [tableView reloadData];
    searchBar.text = @"";
    
    
}

-(void)showLoadingScreen{
    
    vwLoading.alpha = 1;
    
}
-(void)hideLoadingScreen{
    
    vwLoading.alpha = 0;
}

-(IBAction)done:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(pickedContactsList:)]) {
        [self.delegate pickedContactsList:arrFiltered];
    }
     [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)goBack:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
