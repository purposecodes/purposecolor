//
//  AudioPlayerView.m
//  PurposeColor
//
//  Created by Purpose Code on 24/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CustomAudioPlayerView.h"
#import "UIImage+animatedGIF.h"
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
#import "PrefixHeader.pch"
#import <MobileVLCKit/MobileVLCKit.h>

@interface CustomAudioPlayerView () <VLCMediaDelegate,VLCMediaPlayerDelegate> {
    
    NSString *strURL;
    IBOutlet UIImageView *imgAnimatedView;
    BOOL isStarted;
    VLCMediaPlayer *_mediaplayer;
    IBOutlet UIButton *btnPlay;
    IBOutlet UIActivityIndicatorView *indicator;
}


@end

@implementation CustomAudioPlayerView



-(void)setupAVPlayerForURL:(NSURL*)_url {
    
    [indicator startAnimating];
    NSURL *gifURL = [[NSBundle mainBundle] URLForResource:@"Equalizer" withExtension:@"gif"];
    UIImage *testImage = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:gifURL]];
    imgAnimatedView.animationImages = testImage.images;
    imgAnimatedView.animationDuration = testImage.duration;
    imgAnimatedView.animationRepeatCount = 0;
    imgAnimatedView.image = testImage.images.lastObject;
    
    _mediaplayer = [[VLCMediaPlayer alloc] init];
    _mediaplayer.delegate = self;
    
    /* create a media object and give it to the player */
    _mediaplayer.media = [VLCMedia mediaWithURL:_url];
    [self BtnPlay:btnPlay];

    
    
    /*
    self.player = [APAudioPlayer new];
    
    Somewhere else
    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"audio" withExtension:@"opus"];
    [self.player loadItemWithURL:fileURL autoPlay:YES];
     Do any additional setup after loading the view, typically from a nib.
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self showLoadingScreen];
        
        NSURL *gifURL = [[NSBundle mainBundle] URLForResource:@"Equalizer" withExtension:@"gif"];
        UIImage *testImage = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:gifURL]];
        imgAnimatedView.animationImages = testImage.images;
        imgAnimatedView.animationDuration = testImage.duration;
        imgAnimatedView.animationRepeatCount = 0;
        imgAnimatedView.image = testImage.images.lastObject;
       
        NSError* error;
        
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
        AVAudioSession *session = [AVAudioSession sharedInstance];
        NSError *setCategoryError = nil;
        if (![session setCategory:AVAudioSessionCategoryPlayback
                      withOptions:AVAudioSessionCategoryOptionMixWithOthers
                            error:&setCategoryError]) {
            // handle error
        }
        
        AVAsset *asset = [AVURLAsset URLAssetWithURL:url options:nil];
        AVPlayerItem *anItem = [AVPlayerItem playerItemWithAsset:asset];
        
        player = [AVPlayer playerWithPlayerItem:anItem];
        [player addObserver:self forKeyPath:@"status" options:0 context:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[player currentItem]];
        
       
                
    });
*/
    
    
    
}

- (void)mediaPlayerStateChanged:(NSNotification *)aNotification{
    
    if (_mediaplayer.isPlaying) {
        [indicator stopAnimating];
        [imgAnimatedView startAnimating];
    }else{
         [imgAnimatedView stopAnimating];
    }
    
    if (_mediaplayer.state == VLCMediaPlayerStateEnded || _mediaplayer.state == VLCMediaPlayerStateError)  {
        
        [self closePopUp:nil];
    }
    
}


-(IBAction) BtnPlay:(id)sender {
    if (btnPlay.tag == 0) {
         btnPlay.tag = 1;
         [_mediaplayer play];
         [btnPlay setImage:[UIImage imageNamed:@"Pause_Button.png"] forState:UIControlStateNormal];
         [imgAnimatedView startAnimating];
    }else{
         btnPlay.tag = 0;
         [_mediaplayer pause];
         [btnPlay setImage:[UIImage imageNamed:@"Video_Play_Button.png"] forState:UIControlStateNormal];
         [imgAnimatedView stopAnimating];
    }
    [btnPlay setSelected:false];
    
}



-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self animated:YES];
    
}

-(IBAction)closePopUp:(id)sender{
    
    [_mediaplayer stop];
    if ([self.delegate respondsToSelector:@selector(closeAudioPlayerView)]) {
        [self.delegate closeAudioPlayerView];
    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
