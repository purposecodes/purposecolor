//
//  ActionMediaComposeCell.m
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "DetailComposeCell.h"

@implementation DetailComposeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setUp];
}

-(void)setUp{
    
    [_vwTitleBG.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwTitleBG.layer setBorderWidth:1.f];
    // drop shadow
    [_vwTitleBG.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwTitleBG.layer setShadowOpacity:0.3];
    [_vwTitleBG.layer setShadowRadius:2.0];
    [_vwTitleBG.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    [_vwDateHalfBG.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwDateHalfBG.layer setBorderWidth:1.f];
    // drop shadow
    [_vwDateHalfBG.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwDateHalfBG.layer setShadowOpacity:0.3];
    [_vwDateHalfBG.layer setShadowRadius:2.0];
    [_vwDateHalfBG.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    [_vwStatusHalgBG.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwStatusHalgBG.layer setBorderWidth:1.f];
    // drop shadow
    [_vwStatusHalgBG.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwStatusHalgBG.layer setShadowOpacity:0.3];
    [_vwStatusHalgBG.layer setShadowRadius:2.0];
    [_vwStatusHalgBG.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
  
    [_vwDecsriptionBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwDecsriptionBg.layer setBorderWidth:1.f];
    // drop shadow
    [_vwDecsriptionBg.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwDecsriptionBg.layer setShadowOpacity:0.3];
    [_vwDecsriptionBg.layer setShadowRadius:2.0];
    [_vwDecsriptionBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];


    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
