//
//  ActionMediaComposeCell.h
//  PurposeColor
//
//  Created by Purpose Code on 25/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailComposeCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UITextField *txtTitle;
@property (nonatomic,weak) IBOutlet UIView *vwTitleBG;

@property (nonatomic,weak) IBOutlet UITextView *txtDecsription;
@property (nonatomic,weak) IBOutlet UIView *vwDecsriptionBg;

@property (nonatomic,weak) IBOutlet UIView *vwDateHalfBG;
@property (nonatomic,weak) IBOutlet UIView *vwStatusHalgBG;;
@property (nonatomic,weak) IBOutlet UITextField *txtDate;
@property (nonatomic,weak) IBOutlet UITextField *txtStatus;
@property (nonatomic,weak) IBOutlet UITextField *txtCategory;
@property (nonatomic,weak) IBOutlet UIButton *btnTag;


@property (nonatomic,weak) IBOutlet UILabel *lblContactInfo;
@property (nonatomic,weak) IBOutlet UILabel *lblLocInfo;
@property (nonatomic,weak) IBOutlet UIView *vwFooter;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *topForContact;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *topForLocation;


@property (nonatomic,weak) IBOutlet NSLayoutConstraint *heightForPreview;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *topForPreview;
@property (nonatomic,weak)IBOutlet UIView *vwURLPreview;
@property (nonatomic,weak)IBOutlet UILabel *lblPreviewTitle;
@property (nonatomic,weak)IBOutlet UILabel *lblPreviewDescription;
@property (nonatomic,weak)IBOutlet UILabel *lblPreviewDomain;
@property (nonatomic,weak)IBOutlet UIImageView *imgPreview;
@property (nonatomic,weak)IBOutlet UIActivityIndicatorView *previewIndicator;
@property (nonatomic,weak)IBOutlet UIButton *btnShowPreviewURL;

@end
