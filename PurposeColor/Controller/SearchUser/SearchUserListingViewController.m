//
//  GEMListingViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 09/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


#define OneK                    1000
#define kPadding                10
#define kSuccessCode            200
#define kUnAuthorized           403

#import "SearchUserListingViewController.h"
#import "Constants.h"
#import "ProfilePageViewController.h"
#import "BlockedUserListTableViewCell.h"
#import "SearchedUserPostListing.h"

@interface SearchUserListingViewController ()<UISearchBarDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UISearchBar *searchBar;
    IBOutlet NSLayoutConstraint *tableBottomConstraint;
    
    NSMutableArray *arrUsers;
    NSString *strNoDataText;
    
    BOOL isDataAvailable;
    NSString *strSeatchText;
    NSTimer *timer;

 
}

@end

@implementation SearchUserListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
   
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
}

-(void)setUp{
    
    searchBar.delegate = self;
    [searchBar becomeFirstResponder];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 300;
    
    arrUsers = [NSMutableArray new];
    
    tableView.alwaysBounceVertical = YES;
    tableView.hidden = false;
    isDataAvailable = false;
    
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    searchBar.tintColor = [UIColor getThemeColor];
    UITextField *searchTextField = [searchBar valueForKey:@"_searchField"];
    if ([searchTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor whiteColor];
        [searchTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"Search user.." attributes:@{NSForegroundColorAttributeName: color}]];
    }
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setFont:[UIFont fontWithName:CommonFont size:15]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}



-(NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    return arrUsers.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = @"UserList";
    BlockedUserListTableViewCell *cell = (BlockedUserListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    [self configureCellWithCell:cell indexPath:indexPath];
    return cell;
    
    
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Show particular user posts
    
    [self showUserPostsAtIndex:indexPath];
  
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0.001;
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
   
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
}

-(void)configureCellWithCell:(BlockedUserListTableViewCell*)cell indexPath:(NSIndexPath*)indexPath{
    
    if (indexPath.row < arrUsers.count) {
        NSDictionary *details = arrUsers[[indexPath row]];
        NSInteger verified = 0;
        cell.imgProfile.tag = indexPath.row;
        cell.imgVerified.hidden = true;
        cell.imgProfile.userInteractionEnabled = YES;
        if (NULL_TO_NIL([details objectForKey:@"verify_status"])){
            verified = [[details objectForKey:@"verify_status"] integerValue];
            if (verified == 1) {
                cell.imgVerified.hidden = false;
            }
        }
        cell.lblName.text = [details objectForKey:@"firstname"];
        if (NULL_TO_NIL ([details objectForKey:@"profileimg"]) && [[details objectForKey:@"profileimg"] length]){
            [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[details objectForKey:@"profileimg"]]
                               placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      }];
        }
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserProfilePage:)];
        [cell.imgProfile addGestureRecognizer:gestureRecognizer];
        
    }
    
    
}

#pragma mark - Search Methods and Delegates

- (BOOL)searchBar:(UISearchBar *)_searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    NSString *trimDot = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([trimDot isEqualToString:@"."]) {
        return YES;
    }
    NSString *appendStr;
    if([text length] == 0)
    {
        NSRange rangemak = NSMakeRange(0, [_searchBar.text length]-1);
        appendStr = [_searchBar.text substringWithRange:rangemak];
    }
    else
    {
        appendStr = [NSString stringWithFormat:@"%@%@",_searchBar.text,text];
        
    }
    [self performSelector:@selector(startTimerWithSearchText:) withObject:appendStr];
    
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)_searchBar
{
    [self performSelector:@selector(startTimerWithSearchText:) withObject:_searchBar.text];
    searchBar.showsCancelButton=NO;
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar
{
    [_searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""]) {
        [self performSelector:@selector(clearSearchList) withObject:nil];
    }
}

-(IBAction)cancelSearch:(id)sender{
    
    [self goBack:nil];

    
    
}

-(void)startTimerWithSearchText:(NSString*)searchTxt{
    
    // Call search API after every .6 seconds while type.
    
    if ([timer isValid]) {
        [timer invalidate];
        timer = nil;
    }
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:searchTxt,@"keyword", nil];
   timer = [NSTimer scheduledTimerWithTimeInterval:.6
                                     target:self
                                   selector:@selector(callSearchWebService)
                                   userInfo:userInfo
                                    repeats:NO];
    
}

-(void)callSearchWebService{
    
    if ([timer userInfo]) {
        NSDictionary *dict = [timer userInfo];
        strSeatchText = [dict objectForKey:@"keyword"];
        if (strSeatchText.length) {
            NSString *trimDot = [strSeatchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            trimDot = [trimDot stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            [APIMapper searchUserNameWithText:trimDot onSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [self praseResponds:responseObject];
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
            }];
        }
      
    }
    
}

-(void)praseResponds:(NSDictionary*)responseObject{
    
    isDataAvailable = false;
    if ( NULL_TO_NIL([responseObject objectForKey:@"resultarray"])) {
        NSArray *results = [responseObject objectForKey:@"resultarray"];
        arrUsers = [NSMutableArray arrayWithArray:results];
    }else{
        [arrUsers removeAllObjects];
        strNoDataText = [responseObject objectForKey:@"text"];
    }
    if (arrUsers.count) isDataAvailable = true;
    [tableView reloadData];
    
    
}

-(void)clearSearchList{
    
    if ([timer isValid]) {
        [timer invalidate];
        timer = nil;
    }
    [arrUsers removeAllObjects];
    isDataAvailable = false;
    [tableView reloadData];
}

#pragma mark - Keyboard Methods

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    tableBottomConstraint.constant = -(keyboardBounds.size.height - bottomPadding) ;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    
    // commit animations
    [UIView commitAnimations];
    
   
}

-(void) keyboardWillHide:(NSNotification *)note{
    
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    tableBottomConstraint.constant = 0;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    // commit animations
    [UIView commitAnimations];
    
  
}



#pragma mark - Generic Methods

-(void)showUserPostsAtIndex:(NSIndexPath*)indexPath{
    
    // Listing of searched user posts
  
    NSInteger tag = indexPath.row;
    if (tag < arrUsers.count) {
        NSDictionary *details = arrUsers[tag];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            SearchedUserPostListing *userPostList =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForSearchedUserPosts];
            userPostList.strToUserID = [details objectForKey:@"user_id"];
            userPostList.strUserName = [details objectForKey:@"firstname"];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app.navGeneral pushViewController:userPostList animated:YES];
          
        }
    }
 
}

-(void)showUserProfilePage:(UITapGestureRecognizer*)gesture{
    
    // Show user profie page
    
    NSInteger tag = gesture.view.tag;
    if (tag < arrUsers.count) {
        NSDictionary *details = arrUsers[tag];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app.navGeneral pushViewController:profilePage animated:YES];
            profilePage.canEdit = false;
            if ([[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
                profilePage.canEdit = true;
            }
            [profilePage loadUserProfileWithUserID:[details objectForKey:@"user_id"]showBackButton:YES];
        }
    }
}

-(void)showLoadingScreen{
    
    [self hideLoadingScreen];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


-(IBAction)goBack:(id)sender{
    
    [[self navigationController]popViewControllerAnimated:false];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    tableView = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
