//
//  CustomCellForType.m
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "BdayWishingViewCell.h"

@implementation BdayWishingViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self setUp];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUp{
    
    _vwBackground.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    _vwBackground.layer.borderWidth = 1.f;
    _vwBackground.layer.cornerRadius = 5.f;
    _vwBackground.clipsToBounds = YES;
    
    _imgUser.layer.borderColor = [UIColor clearColor].CGColor;
    _imgUser.layer.borderWidth = 1.f;
    _imgUser.layer.cornerRadius = 27.f;
    _imgUser.clipsToBounds = YES;
    
    _btnApplyWish.layer.borderColor = [UIColor clearColor].CGColor;
    _btnApplyWish.layer.borderWidth = 1.f;
    _btnApplyWish.layer.cornerRadius = 5.f;
    _btnApplyWish.clipsToBounds = YES;
   
 }






@end
