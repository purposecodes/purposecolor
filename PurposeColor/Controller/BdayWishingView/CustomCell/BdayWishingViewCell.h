//
//  CustomCellForType.h
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BdayWishingViewCellDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Share' Button.
 */

-(void)shareButtonTappedWithTag:(NSInteger)tag;

/*!
 *This method is invoked when user taps the 'Delete' Button.
 */


-(void)deleteSelectedCellWithTag:(NSInteger)tag;



@end


@interface BdayWishingViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIView *vwBgForColor;
@property (nonatomic,weak) IBOutlet UIView *vwBackground;
@property (nonatomic,weak) IBOutlet UIImageView *imgUser;
@property (nonatomic,weak) IBOutlet UIImageView *imgVerified;
@property (nonatomic,weak) IBOutlet UILabel *lblName;
@property (nonatomic,weak) IBOutlet UIButton *btnWishCount;
@property (nonatomic,weak) IBOutlet UILabel *lblDate;
@property (nonatomic,weak) IBOutlet UIButton *btnApplyWish;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *topForContainer;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *bottomForContainer;

@property (nonatomic,weak)  id<BdayWishingViewCellDelegate>delegate;



@end
