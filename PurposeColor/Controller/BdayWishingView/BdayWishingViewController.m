//
//  NotificationsListingViewController.m
//  SignSpot
//
//  Created by Purpose Code on 09/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kSectionCount               1
#define kDefaultCellHeight          95
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kWidthPadding               115
#define kFollowHeightPadding        85
#define kOthersHeightPadding        20

#import "BdayWishingViewController.h"
#import "BdayWishingViewCell.h"
#import "Constants.h"
#import "ProfilePageViewController.h"
#import "LikedAndCommentedUserListings.h"

@interface BdayWishingViewController ()<SWRevealViewControllerDelegate>{
    
    IBOutlet UITableView *tableView;
    BOOL isDataAvailable;
    NSInteger totalPages;
    NSInteger currentPage;
    BOOL isPageRefresing;
    NSMutableArray *dataSource;
    NSString *strNoDataText;
    UIRefreshControl *refreshControl;
    NSArray *arrColors;
}

@end

@import Firebase;

@implementation BdayWishingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self getBdayUserList];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    arrColors = [NSArray arrayWithObjects:[UIColor colorWithRed:0.96 green:0.65 blue:0.13 alpha:1.0],[UIColor colorWithRed:0.78 green:0.64 blue:0.79 alpha:1.0], nil];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.alwaysBounceVertical = YES;
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 100;
    self.automaticallyAdjustsScrollViewInsets = NO;
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    [tableView setHidden:true];
    [FIRAnalytics logEventWithName:@"Module"
                        parameters:@{
                                     kFIRParameterItemName:@"Birthday",
                                     }];
}

-(void)refreshData{
    
    [self resetDataSource];
    [self getBdayUserList];
    
}

-(void)resetDataSource{
    
    [dataSource removeAllObjects];
}

-(void)getBdayUserList{
    
    // Get list of users from API
    
    [self showLoadingScreen];
    [APIMapper getBirthDayUserListOnSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getUsersFromResponds:responseObject];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        strNoDataText = error.localizedDescription;
        if (error && error.localizedDescription) [ALToastView toastInView:self.view withText:NETWORK_ERROR_MESSAGE];
        isDataAvailable = false;
        isPageRefresing = false;
        [refreshControl endRefreshing];
        [self hideLoadingScreen];
        [tableView reloadData];
        [tableView setHidden:false];
    }];
        
      
}


-(void)getUsersFromResponds:(NSDictionary*)responseObject{
    
    // Prase responds
    
    [refreshControl endRefreshing];
    isPageRefresing = false;
    
    if (NULL_TO_NIL([responseObject objectForKey:@"resultarray"])){
        dataSource = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"resultarray"]];
    }
    
    if (NULL_TO_NIL([[responseObject objectForKey:@"header"] objectForKey:@"pageCount"]))
        totalPages =  [[[responseObject objectForKey:@"header"] objectForKey:@"pageCount"]integerValue];
    
    if (NULL_TO_NIL([[responseObject objectForKey:@"header"] objectForKey:@"currentPage"]))
        currentPage =  [[[responseObject objectForKey:@"header"] objectForKey:@"currentPage"]integerValue];

    if (dataSource.count) isDataAvailable = true;
    
    [tableView setHidden:false];
    [tableView reloadData];
    
    
}



#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (!isDataAvailable) return kMinimumCellCount;
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    NSInteger rows = dataSource.count;
    return rows;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(BdayWishingViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (dataSource.count <= 0) {
        return;
    }
    UIView *cellContentView = [cell contentView];
    CGFloat rotationAngleDegrees = -30;
    CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
    CGPoint offsetPositioning = CGPointMake(0, cell.contentView.frame.size.height*4);
    CATransform3D transform = CATransform3DIdentity;
    transform = CATransform3DRotate(transform, rotationAngleRadians, -50.0, 0.0, 1.0);
    transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50.0);
    cellContentView.layer.transform = transform;
    cellContentView.layer.opacity = 0.8;
    
    [UIView animateWithDuration:0.8 delay:00 usingSpringWithDamping:0.85 initialSpringVelocity:0.8 options:0 animations:^{
        cellContentView.layer.transform = CATransform3DIdentity;
        cellContentView.layer.opacity = 1;
    } completion:^(BOOL finished) {}];

    
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (!isDataAvailable) {
        
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No records found!"];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        return cell;
    }

    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return nil;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
     return 0.001;
   
    
}

-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    NSString *CellIdentifier = @"BdayWishingViewCell";
    BdayWishingViewCell *cell = (BdayWishingViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.row < dataSource.count) {
        NSDictionary *details = dataSource[indexPath.row];
        cell.lblName.text = [details objectForKey:@"firstname"];
        cell.lblDate.text = [self changeDateFormat:[details objectForKey:@"dob"]];
        cell.imgUser.userInteractionEnabled = true;
        cell.imgUser.tag = indexPath.row;
        cell.btnApplyWish.tag = indexPath.row;
        cell.btnWishCount.tag = indexPath.row;
        [cell.btnWishCount setTitle:[NSString stringWithFormat:@"%ld Wishe(s)",[[details objectForKey:@"wish_count"] integerValue]] forState:UIControlStateNormal];
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserProfilePage:)];
        [cell.imgUser addGestureRecognizer:gestureRecognizer];
        gestureRecognizer.cancelsTouchesInView = NO;
        [cell.indicator startAnimating];
        [cell.imgUser sd_setImageWithURL:[NSURL URLWithString:[details objectForKey:@"profileimg"]]
                        placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [cell.indicator stopAnimating];
                               }];
        [cell.btnApplyWish setEnabled:true];
        [cell.btnApplyWish setAlpha:1];
        [cell.btnApplyWish setTitle:@"  Wish  " forState:UIControlStateNormal];
        if ([[details objectForKey:@"wish_status"] boolValue]) {
             [cell.btnApplyWish setTitle:@"  Wished  " forState:UIControlStateNormal];
             [cell.btnApplyWish setEnabled:false];
             [cell.btnApplyWish setAlpha:0.4];
        }
        cell.imgVerified.hidden = true;
        NSInteger verified = [[details objectForKey:@"verify_status"] integerValue];
        if (verified == 1)cell.imgVerified.hidden = false;
        
        cell.topForContainer.constant = 10;
        cell.bottomForContainer.constant = 5;
        if (indexPath.row == 0) cell.topForContainer.constant = 15;
        if (indexPath.row == dataSource.count - 1) cell.bottomForContainer.constant = 15;
        
        NSInteger index = indexPath.row;
        if (index < arrColors.count) {
            cell.vwBgForColor.backgroundColor = arrColors[index];
        }else{
            NSInteger reminder = index % 2;
            if (reminder < arrColors.count) {
                cell.vwBgForColor.backgroundColor = arrColors[reminder];
            }
        }
    }

    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

-(NSString*)changeDateFormat:(NSString*)strDate{
    NSString *dateString = strDate;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [format dateFromString:dateString];
    [format setDateFormat:@"dd MMMM"];
    NSString* finalDateString = [format stringFromDate:date];
    return finalDateString;
}

-(IBAction)showWishedUsers:(UIButton*)sender{
    
    if (sender.tag < dataSource.count) {
        NSDictionary *details = dataSource[sender.tag];
        NSInteger wishCount = [[details objectForKey:@"wish_count"] integerValue];
        if (wishCount > 0) {
            LikedAndCommentedUserListings *userListings =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForLikedAndCommentedUsers];
            [userListings loadUserListingsForBirthdayForTheUser:[details objectForKey:@"user_id"]];
            [self.navigationController pushViewController:userListings animated:YES];
        }
       
    }
    
}


-(IBAction)wishBirthday:(UIButton*)sender{
    
    if (sender.tag < dataSource.count) {
        NSDictionary *details = dataSource[sender.tag];
        [self showLoadingScreen];
        [APIMapper postBdayWishesToUserID:[details objectForKey:@"user_id"] OnSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self updateWishStatusAtIndex:sender.tag];
            if ([responseObject objectForKey:@"text"]) [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
            [self hideLoadingScreen];
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
             if (error.localizedDescription) [ALToastView toastInView:self.view withText:error.localizedDescription];
            [self hideLoadingScreen];
        }];
    }
}

-(IBAction)showUserProfilePage:(UITapGestureRecognizer*)gesture{
    
    NSInteger tag = gesture.view.tag;
    if (tag < dataSource.count) {
        NSDictionary *details = dataSource[tag];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
            [[self navigationController]pushViewController:profilePage animated:YES];
            profilePage.canEdit = false;
            if ([[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
                profilePage.canEdit = true;
            }
            [profilePage loadUserProfileWithUserID:[details objectForKey:@"user_id"]showBackButton:YES];
            
        }
    }
}

-(void)updateWishStatusAtIndex:(NSInteger)index{
    
    if (index < dataSource.count) {
        NSMutableDictionary *details = [NSMutableDictionary dictionaryWithDictionary:dataSource[index]];
        if (![[details objectForKey:@"wish_status"] boolValue]) {
            NSInteger wishcount = [[details objectForKey:@"wish_count"]integerValue];
            wishcount ++;
            [details setObject:[NSNumber numberWithBool:true] forKey:@"wish_status"];
            [details setObject:[NSNumber numberWithInteger:wishcount] forKey:@"wish_count"];
            [dataSource replaceObjectAtIndex:index withObject:details];
            [tableView reloadData];
            for (UIViewController *_vc in self.navigationController.viewControllers) {
                if ([_vc isKindOfClass:[LaunchPageViewController class]]) {
                    LaunchPageViewController *vc =  (LaunchPageViewController*)_vc;
                    [vc refreshBdayBadge];
                    break;
                }
            }
        }
    }
}


-(IBAction)goBack:(id)sender{
    
   [self.navigationController popViewControllerAnimated:YES];
}
-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
