//
//  EmotionalAwarenessViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 19/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

typedef enum{
    
    eTypeEvent = -1,
    eTypeFeel = 0,
    eTypeEmotion = 1,
    eTypeDrive = 2,
    eTypeGoalsAndDreams = 3,
    eTypeAction = 4,
    eTypeDate = 5
    
}ESelectedMenuType;

typedef enum{
    
    eQuestionOne = 0,
    eQuestionTwo = 1,
    eQuestionThree = 2,
    eQuestionFour = 3,
    eQuestionFive = 4
    
}EField;

#define questionPanelWhenOpen  165
#define questionPanelWhenHide  80
#define questionImagePadding   25

#import "EmotionalAwarenessViewController.h"
#import "Constants.h"
#import "AwarenessMediaViewController.h"
#import "GalleryCollectionViewCell.h"
#import "SelectYourFeel.h"
#import "SelectYourEmotion.h"
#import "SelectYourDrive.h"
#import "SelectYourGoalsAndDreams.h"
#import "SelectActions.h"
#import "SelectedContacts.h"

@interface EmotionalAwarenessViewController
()<AwarenessMediaDelegate,UIGestureRecognizerDelegate,SelectYourFeelingDelegate,SelectYourEmotionDelegate,SelectYourDriveDelegate,SelectYourGoalsAndDreamsDelegate,SelectYourActionsDelegate,SelectedConactsDelegate>{
    
    UIView *inputAccView;
    NSString *strDescription;
    CLLocationCoordinate2D locationCordinates;
    NSString *strLocationAddress;
    NSString *strLocationName;
    NSString *strContactName;
    NSMutableArray *arrContacts;
    
    IBOutlet UILabel *lblDate;
    IBOutlet UILabel *lblTime;
    IBOutlet UILabel *lblNameAndDetails;
    IBOutlet UIButton *btnAddMedia;
    IBOutlet UIButton *btnQuestion;
    IBOutlet UIButton *btnDoneDate;
    IBOutlet UIButton *btnPost;
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *vwQuestionHolder;
    IBOutlet UIView *vwFifthQuestion;
    IBOutlet UIView *vwBottomPanel;
    IBOutlet UILabel *lblQuestion;
    IBOutlet UIImageView *imgTootlTip;
    IBOutlet NSLayoutConstraint *leftForToolTip;
    IBOutlet NSLayoutConstraint *heightForQuestionView;
    
    IBOutlet UITextView *txtView;
    IBOutlet NSLayoutConstraint *bottomForScrollView;
    
    IBOutlet NSLayoutConstraint *widthForQstnAction;
    IBOutlet NSLayoutConstraint *trailingForQuestinActon;
    
    IBOutlet UIButton *btnFirstQuestion;
    IBOutlet UIImageView *questnImgOne;
    IBOutlet UIImageView *questnImgTwo;
    IBOutlet UIImageView *questnImgThree;
    IBOutlet UIImageView *questnImgFour;
    IBOutlet UIImageView *questnImgFive;
    
    IBOutlet UILabel *lblFeelRateAndEmotion;
    IBOutlet UILabel *lblReactionAndGoals;
    
    AwarenessMediaViewController *mediasVC;
    IBOutlet NSLayoutConstraint *mediaVCLeftConstraint;
    IBOutlet NSLayoutConstraint *mediaVCTopConstraint;
    IBOutlet NSLayoutConstraint *mediaVCHeightConstraint;
    IBOutlet NSLayoutConstraint *mediaVCWidthConstraint;
    
    IBOutlet NSLayoutConstraint *heightForImagePannel;
    NSMutableArray *_arrMedias;
    IBOutlet UICollectionView *collectionView;
    float heightOfSingleImage;
    CAShapeLayer *arrowLayer;
    
    NSString *strDate;
    IBOutlet UIView *vwPickerOverLay;
    IBOutlet UIDatePicker *datePicker;
    
    NSMutableDictionary *dictSelectedSteps;
    
    SelectYourFeel *vwFeelSelection;
    NSInteger selectedFeelValue;
    
    SelectYourEmotion *vwEmotionSelection;
    NSString  *selectedEmotionTitle;
    NSInteger selectedEmotionValue;
    
    SelectYourDrive *vwDriveSelection;
    NSInteger selectedDriveValue;
    
    SelectYourGoalsAndDreams *vwGoalsSelection;
    NSString  *selectedGoalsTitle;
    NSInteger selectedGoalsValue;
    
    SelectedContacts *selectedContacts;
    
    SelectActions *vwActions;
    NSDictionary* selectedActions;
    
    NSMutableArray *arrDriveImages;
    BOOL isQuestionsReady;
    
}

@end

@implementation EmotionalAwarenessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self removeAllContentsInMediaFolder];
    [self setUp];
    [self setUpQuestions];
    [self toggleQuestionViewShow:NO];
    // Do any additional setup after loading the view.
}

-(void)setUp{
    
    btnPost.layer.cornerRadius = 5.f;
    btnPost.layer.borderColor = [UIColor whiteColor].CGColor;
    btnPost.layer.borderWidth = 1.f;
    btnPost.hidden = true;
    
    isQuestionsReady = false;
    arrDriveImages = [[NSMutableArray alloc] initWithObjects:@"Blue_1_Star",@"Blue_2_Star",@"Blue_3_Star",@"Blue_4_Star",@"Blue_5_Star", nil];
    [btnDoneDate setBackgroundColor:[UIColor getThemeColor]];
    btnDoneDate.layer.cornerRadius = 5.f;
    btnDoneDate.layer.borderWidth = 1.f;
    btnDoneDate.layer.borderColor = [UIColor whiteColor].CGColor;
    vwPickerOverLay.alpha = 0;
    
    dictSelectedSteps = [NSMutableDictionary new];
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    
    // Set the starting point of the shape.
    [aPath moveToPoint:CGPointMake(0.0, 22.0)];
    
    // Draw the lines.
    [aPath addLineToPoint:CGPointMake(10, 0.0)];
    [aPath addLineToPoint:CGPointMake(20, 22)];
    //[aPath closePath];
    
    arrowLayer = [CAShapeLayer layer];
    arrowLayer.path = aPath.CGPath;
    arrowLayer.fillColor = [UIColor whiteColor].CGColor;
    arrowLayer.strokeColor = [UIColor redColor].CGColor;
    arrowLayer.lineWidth = 2;
    [imgTootlTip.layer addSublayer:arrowLayer];
    imgTootlTip.hidden = true;
    
    
    aPath = [UIBezierPath bezierPath];
    
    // Set the starting point of the shape.
    [aPath moveToPoint:CGPointMake(0.0, 23.0)];
    [aPath addLineToPoint:CGPointMake(20, 23)];
    
    CAShapeLayer *border = [CAShapeLayer layer];
    border.path = aPath.CGPath;
    border.fillColor = [UIColor whiteColor].CGColor;
    border.strokeColor = [UIColor whiteColor].CGColor;
    border.lineWidth = 5;
    [imgTootlTip.layer addSublayer:border];

    [txtView  setTextContainerInset:UIEdgeInsetsMake(0, 5, 0, 5)];
    heightForImagePannel.constant = 0;
    _arrMedias = [NSMutableArray new];
   
    float eachQuestion = self.view.frame.size.width / 5;
    widthForQstnAction.constant = -(eachQuestion);
    vwFifthQuestion.hidden = true;
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe.direction = UISwipeGestureRecognizerDirectionUp;
    [vwBottomPanel addGestureRecognizer:swipe];
    
    swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [vwBottomPanel addGestureRecognizer:swipe];
    vwBottomPanel.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    singleTap.cancelsTouchesInView = NO;
    singleTap.delegate = self;
    [scrollView addGestureRecognizer:singleTap];
    

    [btnAddMedia.layer setBorderColor:[UIColor colorWithRed:0.79 green:0.80 blue:0.81 alpha:1.0].CGColor];
    [btnAddMedia.layer setBorderWidth:1.f];
    btnAddMedia.layer.cornerRadius = 5.f;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    lblDate.text = [self getCurrentDateIsTime:NO];
    lblTime.text = [self getCurrentDateIsTime:YES];
    lblNameAndDetails.text = [User sharedManager].name;
    
    questnImgOne.alpha = 0.2;
    questnImgTwo.alpha = 0.2;
    questnImgThree.alpha = 0.2;
    questnImgFour.alpha = 0.2;
    questnImgFive.alpha = 0.2;
    [txtView becomeFirstResponder];
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedEmotionLbl:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [lblFeelRateAndEmotion setUserInteractionEnabled:YES];
    [lblFeelRateAndEmotion addGestureRecognizer:gesture];
    
    UITapGestureRecognizer* _gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedGoalsLabel:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [lblReactionAndGoals setUserInteractionEnabled:YES];
    [lblReactionAndGoals addGestureRecognizer:_gesture];
    
    _gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedNameAndContacts:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [lblNameAndDetails setUserInteractionEnabled:YES];
    [lblNameAndDetails addGestureRecognizer:_gesture];
    
}

-(void)setUpQuestions{
    
    [vwQuestionHolder.layer setBorderWidth:2.f];
    // drop shadow
    [vwQuestionHolder.layer setShadowOpacity:0.5];
    [vwQuestionHolder.layer setShadowRadius:5.0];
    [vwQuestionHolder.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    vwQuestionHolder.layer.cornerRadius = 25.f;
    [vwQuestionHolder.layer setBorderColor:[UIColor colorWithRed:0.53 green:0.75 blue:0.29 alpha:1.0].CGColor];
    [vwQuestionHolder.layer setShadowColor:[UIColor colorWithRed:0.53 green:0.75 blue:0.29 alpha:1.0].CGColor];
    
    [vwBottomPanel.layer setBorderWidth:1.f];
    // drop shadow
    [vwBottomPanel.layer setShadowOpacity:0.5];
    [vwBottomPanel.layer setShadowRadius:5.0];
    [vwBottomPanel.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    [vwBottomPanel.layer setBorderColor:[UIColor clearColor].CGColor];
    [vwBottomPanel.layer setShadowColor:[UIColor grayColor].CGColor];
   
}

#pragma mark - Question Manager

-(IBAction)enableNextQuestion:(UIButton*)btn{
    
    [self resetAllQuestions];
    switch (btn.tag) {
        case eQuestionOne:
             if (!isQuestionsReady) return;
             [self configureQuestonOne];
             [self animateTipWithClikedBtn:btn];
             break;
        case eQuestionTwo:
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) {
                return;
            }
             [self configureQuestonTwo];
             [self animateTipWithClikedBtn:btn];
             break;
        case eQuestionThree:
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) {
                return;
            }
             [self configureQuestonThree];
             [self animateTipWithClikedBtn:btn];
             break;
        case eQuestionFour:
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) {
                return;
            }
             [self configureQuestonFour];
             [self animateTipWithClikedBtn:btn];
             break;
        case eQuestionFive:
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) {
                return;
            }
             [self configureQuestonFive];
             [self animateTipWithClikedBtn:btn];
             break;
        default:
            break;
            
    }
    [self toggleQuestionViewShow:YES];
    [self animateToNextQuestion];
}

-(IBAction)questionClickedFromIB:(UIButton*)btn{
    
    switch (btn.tag) {
        case eQuestionOne:
            if (!isQuestionsReady) return;
            [self showRateSelection];
            break;
        case eQuestionTwo:
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) {
                return;
            }
            [self showEmotionSelection];
            break;
        case eQuestionThree:
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) {
                return;
            }
            [self showDriveSelection];
            break;
        case eQuestionFour:
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) {
                return;
            }
            [self showGoalsAndDreamsSelection];
            break;
        case eQuestionFive:
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) {
                return;
            }
            [self showActionSelection];
            break;
        default:
            break;
            
    }
    [self toggleQuestionViewShow:YES];
}

-(void)configureQuestonOne{
    
    questnImgOne.alpha = 1.f;
    btnQuestion.tag = eQuestionOne;
    lblQuestion.text = @"Rate how you're feeling now?";
    [questnImgOne.layer setShadowColor:[UIColor colorWithRed:0.53 green:0.75 blue:0.29 alpha:1.0].CGColor];
    [questnImgOne.layer setShadowOpacity:0.5];
    [questnImgOne.layer setShadowRadius:3.0];
    [questnImgOne.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    questnImgOne.layer.cornerRadius = 25.f;
    questnImgOne.layer.borderWidth = 2.f;
    questnImgOne.layer.borderColor = [UIColor whiteColor].CGColor;
    [UIView animateWithDuration:0.3 animations:^{
        questnImgOne.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished){
        //[self showRateSelection];
    }];
    arrowLayer.strokeColor = [UIColor colorWithRed:0.53 green:0.75 blue:0.29 alpha:1.0].CGColor;
    [vwQuestionHolder.layer setBorderColor:[UIColor colorWithRed:0.53 green:0.75 blue:0.29 alpha:1.0].CGColor];
    [vwQuestionHolder.layer setShadowColor:[UIColor colorWithRed:0.53 green:0.75 blue:0.29 alpha:1.0].CGColor];
}

-(void)configureQuestonTwo{
    
    btnQuestion.tag = eQuestionTwo;
    lblQuestion.text = @"What's your emotional state?";
    [questnImgTwo.layer setShadowColor:[UIColor colorWithRed:0.96 green:0.51 blue:0.56 alpha:1.0].CGColor];
    [questnImgTwo.layer setShadowOpacity:0.5];
    [questnImgTwo.layer setShadowRadius:3.0];
    [questnImgTwo.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    questnImgTwo.layer.borderColor = [UIColor whiteColor].CGColor;
    
    questnImgTwo.layer.cornerRadius = 25.f;
    questnImgTwo.layer.borderWidth = 2.f;
    [UIView animateWithDuration:0.3 animations:^{
        questnImgTwo.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished){
       // [self showEmotionSelection];
    }];
    
     arrowLayer.strokeColor = [UIColor colorWithRed:0.96 green:0.51 blue:0.56 alpha:1.0].CGColor;
    [vwQuestionHolder.layer setBorderColor:[UIColor colorWithRed:0.96 green:0.51 blue:0.56 alpha:1.0].CGColor];
    [vwQuestionHolder.layer setShadowColor:[UIColor colorWithRed:0.96 green:0.51 blue:0.56 alpha:1.0].CGColor];
    
}

-(void)configureQuestonThree{
    
    btnQuestion.tag = eQuestionThree;
    lblQuestion.text = @"Do you like your reaction to the situation";
    [questnImgThree.layer setShadowColor:[UIColor colorWithRed:0.36 green:0.57 blue:1.00 alpha:1.0].CGColor];
    [questnImgThree.layer setShadowOpacity:0.5];
    [questnImgThree.layer setShadowRadius:3.0];
    [questnImgThree.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    questnImgThree.layer.borderColor = [UIColor whiteColor].CGColor;
    
    questnImgThree.layer.cornerRadius = 25.f;
    questnImgThree.layer.borderWidth = 2.f;
    [UIView animateWithDuration:0.3 animations:^{
        questnImgThree.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished){
        
       // [self showDriveSelection];
    }];
    arrowLayer.strokeColor = [UIColor colorWithRed:0.36 green:0.57 blue:1.00 alpha:1.0].CGColor;
    [vwQuestionHolder.layer setBorderColor:[UIColor colorWithRed:0.36 green:0.57 blue:1.00 alpha:1.0].CGColor];
    [vwQuestionHolder.layer setShadowColor:[UIColor colorWithRed:0.36 green:0.57 blue:1.00 alpha:1.0].CGColor];
}
-(void)configureQuestonFour{
    
    btnQuestion.tag = eQuestionFour;
    lblQuestion.text = @"Which goal is affected by your reaction";
    [questnImgFour.layer setShadowColor:[UIColor colorWithRed:0.96 green:0.76 blue:0.22 alpha:1.0].CGColor];
    [questnImgFour.layer setShadowOpacity:0.5];
    [questnImgFour.layer setShadowRadius:3.0];
    [questnImgFour.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    questnImgFour.layer.cornerRadius = 25.f;
    questnImgFour.layer.borderWidth = 2.f;
    questnImgFour.layer.borderColor = [UIColor whiteColor].CGColor;
    [UIView animateWithDuration:0.3 animations:^{
        questnImgFour.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished){
       // [self showGoalsAndDreamsSelection];
    }];
    arrowLayer.strokeColor = [UIColor colorWithRed:0.96 green:0.76 blue:0.22 alpha:1.0].CGColor;
    [vwQuestionHolder.layer setBorderColor:[UIColor colorWithRed:0.96 green:0.76 blue:0.22 alpha:1.0].CGColor];
    [vwQuestionHolder.layer setShadowColor:[UIColor colorWithRed:0.96 green:0.76 blue:0.22 alpha:1.0].CGColor];

    
}
-(void)configureQuestonFive{
    
    btnQuestion.tag = eQuestionFive;
    lblQuestion.text = @"Which actions affecting your goal?";
    [questnImgFive.layer setShadowColor:[UIColor colorWithRed:0.99 green:0.48 blue:0.13 alpha:1.0].CGColor];
    [questnImgFive.layer setShadowOpacity:0.5];
    [questnImgFive.layer setShadowRadius:3.0];
    [questnImgFive.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    questnImgFive.layer.cornerRadius = 25.f;
    questnImgFive.layer.borderWidth = 2.f;
    questnImgFive.layer.borderColor = [UIColor whiteColor].CGColor;
    [UIView animateWithDuration:0.3 animations:^{
        questnImgFive.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished){
       // [self showActionSelection];
    }];
    arrowLayer.strokeColor = [UIColor colorWithRed:0.99 green:0.48 blue:0.13 alpha:1.0].CGColor;
    [vwQuestionHolder.layer setBorderColor: [UIColor colorWithRed:0.99 green:0.48 blue:0.13 alpha:1.0].CGColor];
    [vwQuestionHolder.layer setShadowColor: [UIColor colorWithRed:0.99 green:0.48 blue:0.13 alpha:1.0].CGColor];
}

-(void)animateTipWithClikedBtn:(UIButton*)btnClicked{
    
    imgTootlTip.hidden = false;
   CGPoint origin = [btnClicked convertPoint:btnClicked.frame.origin toView:self.view];
   origin.x = (origin.x + (btnClicked.frame.size.width / 2)) - imgTootlTip.frame.size.width / 2;
    float xValue =  origin.x;
   [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5
                     animations:^{
                         leftForToolTip.constant = xValue;
                          [self.view layoutIfNeeded];
                     }completion:^(BOOL finished) {
                         
                }];
}

-(void)resetAllQuestions{
    
    imgTootlTip.hidden = true;
    [questnImgOne.layer setShadowOpacity:0];
    [questnImgOne.layer setShadowRadius:0];
    questnImgOne.layer.borderColor = [UIColor clearColor].CGColor;
    questnImgOne.layer.cornerRadius = 1.f;
    questnImgOne.layer.borderWidth = 0.f;
    
    [questnImgTwo.layer setShadowOpacity:0];
    [questnImgTwo.layer setShadowRadius:0];
    questnImgTwo.layer.borderColor = [UIColor clearColor].CGColor;
    questnImgTwo.layer.cornerRadius = 1.f;
    questnImgTwo.layer.borderWidth = 0.f;
    
    [questnImgThree.layer setShadowOpacity:0];
    [questnImgThree.layer setShadowRadius:0];
    questnImgThree.layer.borderColor = [UIColor clearColor].CGColor;
    questnImgThree.layer.cornerRadius = 1.f;
    questnImgThree.layer.borderWidth = 0.f;
    
    [questnImgFour.layer setShadowOpacity:0];
    [questnImgFour.layer setShadowRadius:0];
    questnImgFour.layer.borderColor = [UIColor clearColor].CGColor;
    questnImgFour.layer.cornerRadius = 1.f;
    questnImgFour.layer.borderWidth = 0.f;
    
    [questnImgFive.layer setShadowOpacity:0];
    [questnImgFive.layer setShadowRadius:0];
    questnImgFive.layer.borderColor = [UIColor clearColor].CGColor;
    questnImgFive.layer.cornerRadius = 1.f;
    questnImgFive.layer.borderWidth = 0.f;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        questnImgOne.transform = CGAffineTransformIdentity;
        questnImgTwo.transform = CGAffineTransformIdentity;
        questnImgThree.transform = CGAffineTransformIdentity;
        questnImgFour.transform = CGAffineTransformIdentity;
        questnImgFive.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        
    }];
}

-(void)toggleQuestionViewShow:(BOOL)show{
    NSInteger height = 0;
    if (show){
        imgTootlTip.hidden = false;
        height = questionPanelWhenOpen;
    }
    else{
        imgTootlTip.hidden = true;
        height = questionPanelWhenHide;
    }
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:.4
                     animations:^{
                         heightForQuestionView.constant = height;
                         bottomForScrollView.constant = -(height);
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         
                     }];

}


#pragma mark - UITextView delegate and Custom methods methods


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    textView.autocorrectionType = UITextAutocorrectionTypeYes;
    [self createInputAccessoryView];
    [textView setInputAccessoryView:inputAccView];
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
    return YES;
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
   // [self decorateTags:textView];
    return YES;
}

-(NSMutableArray*)getAllHashTagsFromText:(NSString *)stringWithTags{
    
    NSMutableArray *arrTags = [NSMutableArray new];
    if (stringWithTags.length) {
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
        NSArray *matches = [regex matchesInString:stringWithTags options:0 range:NSMakeRange(0, stringWithTags.length)];
        for (NSTextCheckingResult *match in matches) {
            NSRange wordRange = [match rangeAtIndex:0];
            NSString* word = [stringWithTags substringWithRange:wordRange];
            [arrTags addObject:word];
        }
        
    }
    return arrTags;
}


-(void)decorateTags:(UITextView *)txtView{
    
    NSString *stringWithTags = txtView.text;
    
    
    NSError *error = nil;
    
    //For "Vijay #Apple Dev"
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:stringWithTags options:0 range:NSMakeRange(0, stringWithTags.length)];
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:stringWithTags];
    UIFont *font=[UIFont fontWithName:CommonFont size:15.0f];
    [attString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, stringWithTags.length)];
    
    for (NSTextCheckingResult *match in matches) {
        
        NSRange wordRange = [match rangeAtIndex:0];
        
        //Set Background Color
        UIColor *backgroundColor = [UIColor colorWithRed:0.90 green:0.93 blue:1.00 alpha:1.0];;
        [attString addAttribute:NSBackgroundColorAttributeName value:backgroundColor range:wordRange];
        
        //Set Foreground Color
        UIColor *foregroundColor = [UIColor blackColor];
        [attString addAttribute:NSForegroundColorAttributeName value:foregroundColor range:wordRange];
        
    }
    
    txtView.attributedText = attString;
}

#pragma mark - Awareness Show Medias

-(IBAction)showUploadingMedias:(UIButton*)sender{
    
    [self.view endEditing:YES];
    if (!mediasVC) {
        mediasVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForImotionalAwarenessMedia];
        mediasVC.delegate = self;
        [self addChildViewController:mediasVC];
        //[mediasVC didMoveToParentViewController:self];
    }
    
    UIView *vwPopUP = mediasVC.view;
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:vwPopUP];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    CGRect point = [self.view convertRect:btnAddMedia.frame fromView:btnAddMedia.superview.superview];
    vwPopUP.center = CGPointMake(point.origin.x + btnAddMedia.frame.size.width / 2 , point.origin.y + btnAddMedia.frame.size.height / 2);
    vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, .01, .01);;
    [UIView animateWithDuration:1 delay:0 options:0 animations:^{
        vwPopUP.transform = CGAffineTransformIdentity;
        vwPopUP.center = self.view.center;
    } completion:^(BOOL finished) {
        
        
    }];
    
   
    
    
}
-(void)saveMedias:(BOOL)shouldSave WithCoordinates:(CLLocationCoordinate2D)coordinates locationName:(NSString*)loc mediaCount:(NSInteger)count andCotactsObj:(NSArray*)contactObjs{
    
    CGRect point = [self.view convertRect:btnAddMedia.frame fromView:btnAddMedia.superview.superview];
    [UIView animateWithDuration:1 delay:0 options:0 animations:^{
        
        mediasVC.view.center = CGPointMake(point.origin.x + btnAddMedia.frame.size.width / 2 , point.origin.y + btnAddMedia.frame.size.height / 2);
        mediasVC.view.transform = CGAffineTransformScale(mediasVC.view.transform, 0.01, 0.01);
        
    } completion:^(BOOL finished) {
        [self setUpMedias];
        [mediasVC.view removeFromSuperview];
                if (!shouldSave) {
                    
                    strContactName = nil;
                    strLocationName = nil;
                    strLocationAddress = nil;
                    locationCordinates = CLLocationCoordinate2DMake(0, 0);;
                    [arrContacts removeAllObjects];
                    
                    lblNameAndDetails.text = [User sharedManager].name;
                    [self removeAllContentsInMediaFolder];
                    [mediasVC removeFromParentViewController];
                    [mediasVC didMoveToParentViewController:nil];
                    mediasVC = nil;
                    
                }else{
                    
                    [self toggleQuestionViewShow:NO];
                    
                    locationCordinates = coordinates;
                    strLocationName = loc;
                    strLocationAddress = loc;
                    
                    arrContacts = [NSMutableArray new];
                    NSMutableArray *names = [NSMutableArray new];
                    for (NSDictionary *dict in contactObjs) {
                        if ([[dict objectForKey:@"isSelected"] boolValue]) {
                            if ([dict objectForKey:@"name"]) {
                                [names addObject:[dict objectForKey:@"name"]];
                                [arrContacts addObject:dict];
                            }
                        }
                    }
                    if (names.count) strContactName = [NSMutableString stringWithString:[names componentsJoinedByString:@","]];
                    
                    NSInteger contactOffset = [User sharedManager].name.length;
                    NSInteger contactLength = 0;
                    
                    NSInteger locOffset = [User sharedManager].name.length;
                    NSInteger locLength = 0;
                    
                    NSMutableString *strUserInfo = [NSMutableString new];
                    if (strContactName.length) {
                        contactOffset += 9;
                        NSArray *contcts = [strContactName componentsSeparatedByString:@","];
                        NSString *text;
                        NSString *name;
                        if (contcts.count > 1) {
                            text =  [NSString stringWithFormat:@" is with %@ & %lu Other(s)",contcts[0],contcts.count - 1];
                            name = contcts[0];
                            contactLength = name.length;
                        }else{
                            text =  [NSString stringWithFormat:@" is with %@",strContactName];
                            contactLength = strContactName.length;
                        }
                        [strUserInfo appendString:text];
                        locOffset += text.length;
                    }
                    if (loc.length) {
                        
                        locOffset += 4;
                        locLength = loc.length;
                        
                        NSString *text = [NSString stringWithFormat:@" at %@",loc];
                        [strUserInfo appendString:text];
                    }
                    
                    NSMutableAttributedString *txtCombined = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",[User sharedManager].name,strUserInfo]];
                    
                    [txtCombined addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold_New size:14]
                                        range:NSMakeRange(0, [User sharedManager].name.length)];
                    
                    [txtCombined addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                                        range:NSMakeRange(contactOffset, contactLength)];
                    
                    [txtCombined addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                                        range:NSMakeRange(locOffset, locLength)];
                    
                    [lblNameAndDetails setAttributedText: txtCombined];
                }
        
    }];

}

#pragma mark - Awareness Media Show Layout


-(void)setUpMedias{
    
    [self getAllMediaFiles];
    if (_arrMedias.count) [self setUpImageTemplate];
    else heightForImagePannel.constant = 0 ;
    [collectionView reloadData];
    
    
}

-(void)setUpImageTemplate{
    
    if (_arrMedias.count == 1){
       [self getImageHeight];
    }
    else if (_arrMedias.count == 2)
        heightForImagePannel.constant = self.view.frame.size.width / 2.0;
    else
        heightForImagePannel.constant = self.view.frame.size.width ;
    return;
}



-(void)getAllMediaFiles{
    
    [_arrMedias removeAllObjects];
    NSString *dataPath = [Utility getJournalMediaSaveFolderPath];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    NSError* error = nil;
    // sort by creation date
    NSMutableArray* filesAndProperties = [NSMutableArray arrayWithCapacity:[directoryContent count]];
    for(NSString* file in directoryContent) {
        
        if (![file isEqualToString:@".DS_Store"]) {
            NSString* filePath = [dataPath stringByAppendingPathComponent:file];
            NSDictionary* properties = [[NSFileManager defaultManager]
                                        attributesOfItemAtPath:filePath
                                        error:&error];
            NSDate* modDate = [properties objectForKey:NSFileCreationDate];
            
            [filesAndProperties addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                           file, @"path",
                                           modDate, @"lastModDate",
                                           nil]];
            
        }
    }
    // sort using a block
    // order inverted as we want latest date first
    NSArray* sortedFiles = [filesAndProperties sortedArrayUsingComparator:
                            ^(id path1, id path2)
                            {
                                // compare
                                NSComparisonResult comp = [[path1 objectForKey:@"lastModDate"] compare:
                                                           [path2 objectForKey:@"lastModDate"]];
                                // invert ordering
                                if (comp == NSOrderedDescending) {
                                    comp = NSOrderedAscending;
                                }
                                else if(comp == NSOrderedAscending){
                                    comp = NSOrderedDescending;
                                }
                                return comp;
                            }];
    
    for (NSInteger i = sortedFiles.count - 1; i >= 0; i --) {
        NSDictionary *dict = sortedFiles[i];
        [_arrMedias insertObject:[dict objectForKey:@"path" ] atIndex:0];
    }
    
}


#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section{
    
    NSInteger count = _arrMedias.count;
    if (count > 4) {
        count = 4;
    }
    return count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GalleryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.rightConstraintImage.constant = 0;
    cell.vwOverLay.hidden = true;
    cell.lblCount.hidden = true;
    [[cell btnAudioPlay]setHidden:true];
    [[cell btnVideoPlay]setHidden:true];
    
    if (_arrMedias.count == 2 && indexPath.row == 0) {
        cell.rightConstraintImage.constant = 2;
    }
    if (_arrMedias.count == 3 && indexPath.row == 1) {
        cell.rightConstraintImage.constant = 2;
    }
    if (_arrMedias.count >= 4) {
        if ((indexPath.row == 0) || (indexPath.row == 2)) {
            cell.rightConstraintImage.constant = 2;
        }
        if ((_arrMedias.count > 4) && (indexPath.row == 3)) {
            cell.vwOverLay.hidden = false;
            cell.lblCount.hidden = false;
            cell.lblCount.text = [NSString stringWithFormat:@"%lu+",_arrMedias.count - 4];
        }
    }
    
    if (indexPath.row < _arrMedias.count) {
        if (indexPath.row < _arrMedias.count) {
            id object = _arrMedias[indexPath.row];
            NSString *strFile;
            if ([object isKindOfClass:[NSString class]]) {
                strFile = (NSString*)_arrMedias[indexPath.row];
                cell.imgView.backgroundColor = [UIColor blackColor];
                NSString *fileName = _arrMedias[indexPath.row];
                if ([[fileName pathExtension] isEqualToString:@"jpeg"]) {
                    //This is Image File with .png Extension , Photos.
                    NSString *filePath = [Utility getJournalMediaSaveFolderPath];
                    NSString *imagePath = [[filePath stringByAppendingString:@"/"] stringByAppendingString:fileName];
                    if (imagePath.length) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                            NSData *data = [[NSFileManager defaultManager] contentsAtPath:imagePath];
                            UIImage *image = [UIImage imageWithData:data];
                            dispatch_sync(dispatch_get_main_queue(), ^(void) {
                                [UIView transitionWithView:cell.imgView
                                                  duration:0.4f
                                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                                animations:^{
                                                    cell.imgView.image = image;
                                                } completion:nil];
                            });
                        });
                        
                    }
                }
                else if ([[fileName pathExtension] isEqualToString:@"mp4"]) {
                    //This is Image File with .mp4 Extension , Video Files
                    NSString *filePath = [Utility getJournalMediaSaveFolderPath];
                    NSString *imagePath = [[filePath stringByAppendingString:@"/"] stringByAppendingString:fileName];
                    [[cell btnVideoPlay]setHidden:false];
                    if (imagePath.length){
                        // [cell.indicator startAnimating];
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                            UIImage *thumbnail = [Utility getThumbNailFromVideoURL:imagePath];
                            dispatch_sync(dispatch_get_main_queue(), ^(void) {
                                cell.imgView.image = thumbnail;
                                
                            });
                        });
                    }
                }
                else if ([[fileName pathExtension] isEqualToString:@"aac"]){
                    // Recorded Audio
                    [[cell btnAudioPlay]setHidden:false];
                    cell.imgView.image = [UIImage imageNamed:@"NoImage_Goals_Dreams"];
                }
                
            }
            
        }
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = _collectionView.frame.size.width;
    float height = _collectionView.frame.size.width;
    
    if(_arrMedias.count == 1){
        
        width = _collectionView.frame.size.width;
        height = heightOfSingleImage;
        
    }else if(_arrMedias.count == 2){
        
        width = _collectionView.frame.size.width / 2;
        height = _collectionView.frame.size.width / 2;
    }
    else if (_arrMedias.count == 3) {
        
        if (indexPath.row == 0) {
            width = _collectionView.frame.size.width;
            height = _collectionView.frame.size.width/2;
            
        }
        else if (indexPath.row == 1) {
            
            width = _collectionView.frame.size.width/2;
            height = _collectionView.frame.size.width/2;
        }
        else if (indexPath.row == 2) {
            
            width = _collectionView.frame.size.width/2 ;
            height = _collectionView.frame.size.width/2;
        }
    }
    else if(_arrMedias.count >= 4){
        
        width = _collectionView.frame.size.width / 2;
        height = _collectionView.frame.size.width / 2;
        
    }
  
    return CGSizeMake(width, height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self showUploadingMedias:btnAddMedia];
}


#pragma mark - RATE YOUR FEEL Actions

-(void)showRateSelection{
    
    [self removeAllPopUps];
    if (!vwFeelSelection) {
        vwFeelSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourFeel" owner:self options:nil] objectAtIndex:0];
        vwFeelSelection.translatesAutoresizingMaskIntoConstraints = NO;
        vwFeelSelection.delegate = self;
    }
    
    [self.view addSubview:vwFeelSelection];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwFeelSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwFeelSelection)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwFeelSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwFeelSelection)]];
    
    UIView *vwPopUP = vwFeelSelection;
    [self.view addSubview:vwPopUP];
    CGRect point = [questnImgOne convertRect:questnImgOne.frame toView:self.view];
    vwPopUP.center = CGPointMake(point.origin.x + 10, point.origin.y + 20);
    vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, .01, .01);
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = self.view.center;
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {

    }];
    
    [vwFeelSelection configureFeelRating];
    
}



-(void)feelingsSelectedWithEmotionType:(NSInteger)emotionType{
    
    selectedFeelValue = emotionType;
    [dictSelectedSteps setObject:[NSNumber numberWithInteger:emotionType] forKey:[NSNumber numberWithInteger:eTypeFeel]];
    UIView *vwPopUP = vwFeelSelection;
    CGRect point = [questnImgOne convertRect:questnImgOne.frame toView:self.view];
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
         vwPopUP.center = CGPointMake(point.origin.x + 10 , point.origin.y + 20);
        vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, 0.01, 0.01);
    } completion:^(BOOL finished) {
        
        [self closeFeelPopUpAndconfigureNextView];
        [self showSelectedRating];
    }];
    
}

-(void)closeFeelPopUpAndconfigureNextView{
    
    questnImgTwo.alpha = 1.0;
    [vwFeelSelection removeFromSuperview];
    vwFeelSelection = nil;
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) {
        [self automateToolTipMovementWithTag:eQuestionTwo];
    }
    
}

-(void)showSelectedRating{
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    UIImage *icon = [UIImage imageNamed:[arrDriveImages objectAtIndex:selectedFeelValue - 1]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  lblFeelRateAndEmotion.font.descender + 2), icon.size.width, icon.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSMutableAttributedString *strCurrent = [[NSMutableAttributedString alloc]initWithAttributedString:lblFeelRateAndEmotion.attributedText] ;
    if (strCurrent.length) {
        [strCurrent replaceCharactersInRange:NSMakeRange(0, 1) withAttributedString:attachmentString];
    }else{
        [strCurrent appendAttributedString:attachmentString];
    }
    lblFeelRateAndEmotion.attributedText = strCurrent;
    
}

#pragma mark - SELECT YOUR EMOTIONS Actions

-(void)showEmotionSelection{
    
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]) {
        return;
    }
    
    [self removeAllPopUps];
    if (!vwEmotionSelection) {
        vwEmotionSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourEmotion" owner:self options:nil] objectAtIndex:0];
        vwEmotionSelection.translatesAutoresizingMaskIntoConstraints = NO;
        vwEmotionSelection.delegate = self;
        NSInteger feelValue = 0;
        switch (selectedFeelValue) {
            case 1:
                feelValue = -2;
                break;
            case 2:
                feelValue = -1;
                break;
            case 3:
                feelValue = 0;
                break;
            case 4:
                feelValue = 1;
                break;
            case 5:
                feelValue = 2;
                break;
                
            default:
                break;
        }
        vwEmotionSelection.emotionaValue = feelValue;
    }
    vwEmotionSelection.selectedEmotionID = selectedEmotionValue;
    UIView *vwPopUP = vwEmotionSelection;
    [self.view addSubview:vwPopUP];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    CGRect point = [questnImgTwo convertRect:questnImgTwo.frame toView:self.view];
    vwPopUP.center = CGPointMake(point.origin.x + 10 , point.origin.y + 20);
    vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, .01, .01);
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = self.view.center;
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
         [vwEmotionSelection showSelectionPopUp];
    }];
    
}

-(void)emotionSelectedWithEmotionTitle:(NSString*)emotionTitle emotionID:(NSInteger)emotionID;{
    
    [self selectYourEmotionPopUpShouldEnableNextMenu:YES];
    selectedEmotionValue = emotionID;
    selectedEmotionTitle = emotionTitle;
    [dictSelectedSteps setObject:[NSNumber numberWithInteger:emotionID] forKey:[NSNumber numberWithInteger:eTypeEmotion]];

}
-(void)selectYourEmotionPopUpShouldEnableNextMenu:(BOOL)ShouldEnableNextMenu{
    
    UIView *vwPopUP = vwEmotionSelection;
    CGRect point = [questnImgTwo convertRect:questnImgTwo.frame toView:self.view];
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = CGPointMake(point.origin.x + 10 , point.origin.y + 20);
        vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, 0.01, 0.01);
    } completion:^(BOOL finished) {
       
        [vwEmotionSelection removeFromSuperview];
        vwEmotionSelection = nil;
        
        if (ShouldEnableNextMenu) {
            [self showSelectedEmotion];
            questnImgThree.alpha = 1.0;
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) {
                [self automateToolTipMovementWithTag:eQuestionThree];
            }
            
        }
        
    }];
}

-(void)showSelectedEmotion{
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    UIImage *icon = [UIImage imageNamed:[arrDriveImages objectAtIndex:selectedFeelValue - 1]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  lblFeelRateAndEmotion.font.descender + 2), icon.size.width, icon.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSMutableAttributedString *strValues = [[NSMutableAttributedString alloc]init] ;
    [strValues appendAttributedString:attachmentString];
    NSMutableAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" FEELING %@",selectedEmotionTitle]];
    [strValues appendAttributedString:myText];
    [strValues addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                       range:NSMakeRange(10, selectedEmotionTitle.length)];
    [strValues addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold_New size:16]
                        range:NSMakeRange(10, selectedEmotionTitle.length)];
    lblFeelRateAndEmotion.attributedText = strValues;
    
}

#pragma mark - SELECT YOUR DRIVE Actions

-(void)showDriveSelection{
    
    
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeEmotion]]) {
        return;
    }
     [self removeAllPopUps];
    
    if (!vwDriveSelection) {
        vwDriveSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourDrive" owner:self options:nil] objectAtIndex:0];
        vwDriveSelection.translatesAutoresizingMaskIntoConstraints = NO;
        vwDriveSelection.delegate = self;
    }
    
    [self.view addSubview:vwDriveSelection];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwDriveSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwDriveSelection)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwDriveSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwDriveSelection)]];
    
    UIView *vwPopUP = vwDriveSelection;
    CGRect point = [questnImgThree convertRect:questnImgThree.frame toView:self.view];
    vwPopUP.center = CGPointMake(point.origin.x + 10, point.origin.y + 20);
    vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, .01, .01);
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = self.view.center;
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
       
    }];
    
     [vwDriveSelection configureDriveRating];
    
}

-(void)selectYourDrivePopUpCloseAppplied{
    
    [vwDriveSelection removeFromSuperview];
    vwDriveSelection.delegate = nil;
    vwDriveSelection = nil;
}

-(void)driveSelectedWithEmotionType:(NSInteger)emotionType{
    
    selectedDriveValue = emotionType;
    [dictSelectedSteps setObject:[NSNumber numberWithInteger:emotionType] forKey:[NSNumber numberWithInteger:eTypeDrive]];
    UIView *vwPopUP = vwDriveSelection;
    CGRect point = [questnImgThree convertRect:questnImgThree.frame toView:self.view];
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = CGPointMake(point.origin.x + 10, point.origin.y + 20);
        vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, 0.01, 0.01);
    } completion:^(BOOL finished) {
        
        [self showSelectedDriveValue];
        [self closeDrivePopUpAndconfigureNextView];
        
    }];
   
}

-(void)closeDrivePopUpAndconfigureNextView{
    
    questnImgFour.alpha = 1.0;
    [vwDriveSelection removeFromSuperview];
    vwDriveSelection = nil;
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) {
         [self automateToolTipMovementWithTag:eQuestionFour];
    }
   
}

-(void)showSelectedDriveValue{
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    UIImage *icon = [UIImage imageNamed:[arrDriveImages objectAtIndex:selectedDriveValue - 1]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  lblReactionAndGoals.font.descender + 2), icon.size.width, icon.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSMutableAttributedString *strCurrent = [[NSMutableAttributedString alloc]initWithAttributedString:lblReactionAndGoals.attributedText] ;
    if (strCurrent.length) {
        [strCurrent replaceCharactersInRange:NSMakeRange(0, 1) withAttributedString:attachmentString];
    }else{
        [strCurrent appendAttributedString:attachmentString];
    }
    lblReactionAndGoals.attributedText = strCurrent;
  
    
}

#pragma mark - Goals Selection

-(void)showGoalsAndDreamsSelection{
    
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeDrive]]) {
        return;
    }
    [self removeAllPopUps];
    if (!vwGoalsSelection) {
        vwGoalsSelection = [[[NSBundle mainBundle] loadNibNamed:@"SelectYourGoalsAndDreams" owner:self options:nil] objectAtIndex:0];
        vwGoalsSelection.translatesAutoresizingMaskIntoConstraints = NO;
        vwGoalsSelection.delegate = self;
    }
    
    vwGoalsSelection.selectedGoalID = selectedGoalsValue;
    [self.view addSubview:vwGoalsSelection];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwGoalsSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwGoalsSelection)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwGoalsSelection]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwGoalsSelection)]];
    
    UIView *vwPopUP = vwGoalsSelection;
    CGRect point = [questnImgFour convertRect:questnImgFour.frame toView:self.view];
    vwPopUP.center = CGPointMake(point.origin.x + 10 , point.origin.y + 20);
    vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, .01, .01);
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = self.view.center;
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
        [vwGoalsSelection showSelectionPopUp];
    }];
    
}

-(void)goalsAndDreamsSelectedWithTitle:(NSString*)title goalId:(NSInteger)goalsId{
    
    btnPost.hidden = FALSE;
    if (title.length) {
        selectedGoalsTitle = title;
        selectedGoalsValue = goalsId;
        [dictSelectedSteps setObject:[NSNumber numberWithInteger:goalsId] forKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]];
        [dictSelectedSteps removeObjectForKey:[NSNumber numberWithInt:eTypeAction]];
    }
    [self closeGoalsAndDreamsPopUpShouldEnableNextMenu:YES];

}

-(void)skipButtonApplied{
    
    selectedActions = nil;
    btnPost.hidden = FALSE;
    selectedGoalsTitle = nil;
    [dictSelectedSteps removeObjectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]];
    [self closeGoalsAndDreamsPopUpShouldEnableNextMenu:NO];
    
    
    
}


-(void)closeGoalsAndDreamsPopUpShouldEnableNextMenu:(BOOL)shouldEnableNextMenu{
    
    UIView *vwPopUP = vwGoalsSelection;
    CGRect point = [questnImgFour convertRect:questnImgFour.frame toView:self.view];
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = CGPointMake(point.origin.x + 10 , point.origin.y + 20);
        vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, 0.01, 0.01);
    } completion:^(BOOL finished) {
        [vwGoalsSelection removeFromSuperview];
        vwGoalsSelection = nil;
        [self displaySelectedGoalsDetails];
        if (shouldEnableNextMenu) {
            
            questnImgFive.alpha = 1.0;
            [self showActionStep];
            if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeAction]]) {
                 [self automateToolTipMovementWithTag:eQuestionFive];
            }
          
            
        }else{
            
            [self toggleQuestionViewShow:NO];
            [self removeActionStep];
            
        }
        
    }];
}

-(void)displaySelectedGoalsDetails{
    
    if (selectedGoalsTitle.length) {
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        UIImage *icon = [UIImage imageNamed:[arrDriveImages objectAtIndex:selectedDriveValue - 1]];
        attachment.image = icon;
        attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  lblReactionAndGoals.font.descender + 2), icon.size.width, icon.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *strValues = [[NSMutableAttributedString alloc]init] ;
        [strValues appendAttributedString:attachmentString];
        
        NSMutableAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" GOAL %@",selectedGoalsTitle]];
        [strValues appendAttributedString:myText];
        [strValues addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                          range:NSMakeRange(7, selectedGoalsTitle.length)];
        [strValues addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold_New size:16]
                          range:NSMakeRange(7, selectedGoalsTitle.length)];
        lblReactionAndGoals.attributedText = strValues;
        
    }else{
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        UIImage *icon = [UIImage imageNamed:[arrDriveImages objectAtIndex:selectedDriveValue - 1]];
        attachment.image = icon;
        attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  lblReactionAndGoals.font.descender + 2), icon.size.width, icon.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *strValues = [[NSMutableAttributedString alloc]init] ;
        [strValues appendAttributedString:attachmentString];
        
        NSMutableAttributedString *actionTitle =  [[NSMutableAttributedString alloc] initWithString:@" No goals"];
        [strValues appendAttributedString:actionTitle];
        [strValues addAttribute:NSFontAttributeName value:[UIFont italicSystemFontOfSize:15]
                          range:NSMakeRange(1, strValues.length - 1)];
        lblReactionAndGoals.attributedText = strValues;
    }
    
    
}

-(void)showActionStep{
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:.4
                     animations:^{
                         widthForQstnAction.constant = 0;
                         vwFifthQuestion.hidden = false;
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         
                     }];
}

-(void)removeActionStep{
    
    [self resetAllQuestions];
    float eachQuestion = self.view.frame.size.width / 5;
    questnImgFive.alpha = 0.2;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:.4
                     animations:^{
                         widthForQstnAction.constant = -(eachQuestion);
                         vwFifthQuestion.hidden = true;
                         [self.view layoutIfNeeded]; // Called on parent view
                     }completion:^(BOOL finished) {
                         
                     }];
}

-(void)hideGoalsAndActionsFromDisplay{
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    UIImage *icon = [UIImage imageNamed:[arrDriveImages objectAtIndex:selectedDriveValue - 1]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  lblReactionAndGoals.font.descender + 2), icon.size.width, icon.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSMutableAttributedString *strValues = [[NSMutableAttributedString alloc]init] ;
    [strValues appendAttributedString:attachmentString];
    lblReactionAndGoals.attributedText = strValues;
}

#pragma mark - Action popup and methods

-(void)showActionSelection{
    
    if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeGoalsAndDreams]]) {
        return;
    }
    [self removeAllPopUps];
    if (!vwActions) {
        vwActions = [[[NSBundle mainBundle] loadNibNamed:@"SelectActions" owner:self options:nil] objectAtIndex:0];
        vwActions.translatesAutoresizingMaskIntoConstraints = NO;
        vwActions.delegate = self;
    }
    
    vwActions.goalID = selectedGoalsValue;
    vwActions.selectedActions = selectedActions;
    [self.view addSubview:vwActions];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwActions]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwActions)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwActions]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwActions)]];
    
    UIView *vwPopUP = vwActions;
    CGRect point = [questnImgFive convertRect:questnImgFive.frame toView:self.view];
    vwPopUP.center = CGPointMake(point.origin.x + 10 , point.origin.y + 20);
    vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, .01, .01);
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = self.view.center;
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
         [vwActions showSelectionPopUp];
    }];

}

-(void)selectYourActionsPopUpCloseAppplied{
    
   [dictSelectedSteps removeObjectForKey:[NSNumber numberWithInt:eTypeAction]];
   [self closeActionsPopUp];
}

-(void)actionsSelectedWithTitle:(NSString*)title actionIDs:(NSDictionary*)selectedAcitons{
    
    selectedActions = selectedAcitons;
    [self closeActionsPopUp];
   
}
-(void)closeActionsPopUp{
    
    UIView *vwPopUP = vwActions;
    CGRect point = [questnImgFive convertRect:questnImgFive.frame toView:self.view];
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        vwPopUP.center = CGPointMake(point.origin.x + 10 , point.origin.y + 20);
        vwPopUP.transform = CGAffineTransformScale(vwPopUP.transform, 0.01, 0.01);
    } completion:^(BOOL finished) {
        
        [self resetAllQuestions];
        [self toggleQuestionViewShow:NO];
        [self displayActionsIfNeeded];
        [vwPopUP removeFromSuperview];
        vwActions = nil;
        
    }];
}

-(void)displayActionsIfNeeded{
    
    if (selectedActions.count) {
        
        [dictSelectedSteps setObject:selectedActions forKey:[NSNumber numberWithInteger:eTypeAction]];
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        UIImage *icon = [UIImage imageNamed:[arrDriveImages objectAtIndex:selectedDriveValue - 1]];
        attachment.image = icon;
        attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  lblReactionAndGoals.font.descender + 2), icon.size.width, icon.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *strValues = [[NSMutableAttributedString alloc]init] ;
        [strValues appendAttributedString:attachmentString];
        
        NSMutableAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" GOAL %@",selectedGoalsTitle]];
        [strValues appendAttributedString:myText];
        [strValues addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                          range:NSMakeRange(7, selectedGoalsTitle.length)];
        [strValues addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold_New size:16]
                          range:NSMakeRange(7, selectedGoalsTitle.length)];
        
        NSInteger pos = strValues.length;
        
        NSString *strActionCount = [NSString stringWithFormat:@"%lu Action(s)",(unsigned long)selectedActions.count];
        NSMutableAttributedString *actionTitle =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" and %@",strActionCount]];
        [strValues appendAttributedString:actionTitle];
        
        [strValues addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                          range:NSMakeRange(pos + 5, strActionCount.length)];
        [strValues addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold_New size:16]
                          range:NSMakeRange(pos + 5, strActionCount.length)];
        
        lblReactionAndGoals.attributedText = strValues;
      
        
    }
    else{
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        UIImage *icon = [UIImage imageNamed:[arrDriveImages objectAtIndex:selectedDriveValue - 1]];
        attachment.image = icon;
        attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  lblReactionAndGoals.font.descender + 2), icon.size.width, icon.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *strValues = [[NSMutableAttributedString alloc]init] ;
        [strValues appendAttributedString:attachmentString];
        
        NSMutableAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" GOAL %@",selectedGoalsTitle]];
        [strValues appendAttributedString:myText];
        [strValues addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                          range:NSMakeRange(6, selectedGoalsTitle.length)];
        [strValues addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold_New size:16]
                          range:NSMakeRange(6, selectedGoalsTitle.length)];
        
        NSInteger currentLength = strValues.length;
        NSMutableAttributedString *actionTitle =  [[NSMutableAttributedString alloc] initWithString:@" and No actions"];
        [strValues appendAttributedString:actionTitle];
        [strValues addAttribute:NSFontAttributeName value:[UIFont italicSystemFontOfSize:15]
                          range:NSMakeRange(currentLength, strValues.length - currentLength)];
        
        lblReactionAndGoals.attributedText = strValues;
        
    }
}

#pragma mark - SelectedConacts List PopUp

-(void)showSelectedContactsList{
    
    if (!selectedContacts) {
        selectedContacts = [[[NSBundle mainBundle] loadNibNamed:@"SelectedContacts" owner:self options:nil] objectAtIndex:0];
        selectedContacts.translatesAutoresizingMaskIntoConstraints = NO;
        selectedContacts.delegate = self;
    }
    
    [self.view addSubview:selectedContacts];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[selectedContacts]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(selectedContacts)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[selectedContacts]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(selectedContacts)]];
    [selectedContacts showContactNameWith:arrContacts];
    
}

-(void)selectContactsListPopUpCloseAppplied{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        selectedContacts.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [selectedContacts removeFromSuperview];
        selectedContacts = nil;
        
    }];
    
}
-(void)showEditedContacts:(NSMutableArray*)contactObjs{
    
    arrContacts = [NSMutableArray new];
    NSMutableArray *names = [NSMutableArray new];
    for (NSDictionary *dict in contactObjs) {
        if ([[dict objectForKey:@"isSelected"] boolValue]) {
            if ([dict objectForKey:@"name"]) {
                [names addObject:[dict objectForKey:@"name"]];
                [arrContacts addObject:dict];
            }
        }
    }
    
    if (names.count) strContactName = [NSMutableString stringWithString:[names componentsJoinedByString:@","]];
    else strContactName = nil;
    NSInteger contactOffset = [User sharedManager].name.length;
    NSInteger contactLength = 0;
    
    NSInteger locOffset = [User sharedManager].name.length;
    NSInteger locLength = 0;
    
    NSMutableString *strUserInfo = [NSMutableString new];
    if (strContactName.length) {
        contactOffset += 9;
        NSArray *contcts = [strContactName componentsSeparatedByString:@","];
        NSString *text;
        NSString *name;
        if (contcts.count > 1) {
            text =  [NSString stringWithFormat:@" is with %@ & %lu Other(s)",contcts[0],contcts.count - 1];
            name = contcts[0];
            contactLength = name.length;
        }else{
            text =  [NSString stringWithFormat:@" is with %@",strContactName];
            contactLength = strContactName.length;
        }
        [strUserInfo appendString:text];
        locOffset += text.length;
    }else{
        [strUserInfo setString:@""];
    }
    if (strLocationName.length) {
        
        locOffset += 4;
        locLength = strLocationName.length;
        
        NSString *text = [NSString stringWithFormat:@" at %@",strLocationName];
        [strUserInfo appendString:text];
    }
    
    NSMutableAttributedString *txtCombined = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",[User sharedManager].name,strUserInfo]];
    
    [txtCombined addAttribute:NSFontAttributeName value:[UIFont fontWithName:CommonFontBold_New size:14]
                        range:NSMakeRange(0, [User sharedManager].name.length)];
    
    [txtCombined addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                        range:NSMakeRange(contactOffset, contactLength)];
    
    [txtCombined addAttribute:NSForegroundColorAttributeName value:[UIColor getThemeColor]
                        range:NSMakeRange(locOffset, locLength)];
    
    [lblNameAndDetails setAttributedText: txtCombined];
    [mediasVC resetContactsIfEditedFromPopUp:arrContacts];

    
}


#pragma mark - Date Methods

-(IBAction)showDatePicker:(id)sender{
    
    [self.view endEditing:YES];
    
    //datePicker.maximumDate = [NSDate date];
    datePicker.date = [NSDate date];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"d MMM,yyyy h:mm a"];
    strDate = [dateformater stringFromDate:datePicker.date];
    [UIView animateWithDuration:.4 animations:^{
        vwPickerOverLay.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
}
-(IBAction)hidePicker:(id)sender{
    
    [UIView animateWithDuration:.4 animations:^{
        vwPickerOverLay.alpha = 0.0;
    } completion:^(BOOL finished) {
        
    }];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"d MMM,yyyy h:mm a"];
    strDate = [dateformater stringFromDate:datePicker.date];
    [dateformater setDateFormat:@"h:mm a"];
    lblTime.text = [dateformater stringFromDate:datePicker.date];
    [dateformater setDateFormat:@"d MMM"];
    
    NSString *_date =  [[dateformater stringFromDate:datePicker.date] uppercaseString];
    lblDate.text = [NSString stringWithFormat:@"%@\n%@",[[_date componentsSeparatedByString:@" "] objectAtIndex:0],[[_date componentsSeparatedByString:@" "] objectAtIndex:1]];
    
}


#pragma mark -Generic Actions


-(void)uploadMediasToServerWithPath:(NSString*)filename OnSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSString *dataPath = [Utility getJournalMediaSaveFolderPath];
    NSString *filePath = [dataPath stringByAppendingPathComponent:filename];
    [APIMapper uploadMediasAtPath:filePath orAsObject:nil type:@"journal" Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        failure(error);
    }];
    
   

}

- (void)tappedEmotionLbl:(UITapGestureRecognizer *)tapGesture {
    
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:lblFeelRateAndEmotion.attributedText];
    
    // Configure layoutManager and textStorage
    [layoutManager addTextContainer:textContainer];
    [textStorage addLayoutManager:layoutManager];
    
    // Configure textContainer
    textContainer.lineFragmentPadding = 0.0;
    textContainer.lineBreakMode = lblFeelRateAndEmotion.lineBreakMode;
    textContainer.maximumNumberOfLines = lblFeelRateAndEmotion.numberOfLines;
    
    
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x,
                                                         locationOfTouchInLabel.y);
    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer
                                                            inTextContainer:textContainer
                                   fractionOfDistanceBetweenInsertionPoints:nil];
    
    if (indexOfCharacter <= 0) {
        [self showRateSelection];
    }else{
        [self showEmotionSelection];
    }

}

- (void)tappedGoalsLabel:(UITapGestureRecognizer *)tapGesture {
    
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:lblReactionAndGoals.attributedText];
    
    // Configure layoutManager and textStorage
    [layoutManager addTextContainer:textContainer];
    [textStorage addLayoutManager:layoutManager];
    
    // Configure textContainer
    textContainer.lineFragmentPadding = 0.0;
    textContainer.lineBreakMode = lblReactionAndGoals.lineBreakMode;
    textContainer.maximumNumberOfLines = lblReactionAndGoals.numberOfLines;
    
    
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x,
                                                         locationOfTouchInLabel.y);
    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer
                                                       inTextContainer:textContainer
                              fractionOfDistanceBetweenInsertionPoints:nil];
    
    NSRange range = NSMakeRange(0, 0);
    if (selectedGoalsTitle.length) {
        NSString *goalTxt = [NSString stringWithFormat:@" GOAL %@",selectedGoalsTitle];
        range = [lblReactionAndGoals.attributedText.string rangeOfString:goalTxt];
    }
    if (indexOfCharacter <= 0) {
        [self showDriveSelection];
    }else if (NSLocationInRange(indexOfCharacter, range)){
        [self showGoalsAndDreamsSelection];
    }else if (!selectedGoalsTitle){
        [self showGoalsAndDreamsSelection];
    }else{
        [self showActionSelection];
    }
    
}

- (void)tappedNameAndContacts:(UITapGestureRecognizer *)tapGesture {
    
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:lblNameAndDetails.attributedText];
    
    // Configure layoutManager and textStorage
    [layoutManager addTextContainer:textContainer];
    [textStorage addLayoutManager:layoutManager];
    
    // Configure textContainer
    textContainer.lineFragmentPadding = 0.0;
    textContainer.lineBreakMode = lblNameAndDetails.lineBreakMode;
    textContainer.maximumNumberOfLines = lblNameAndDetails.numberOfLines;
    
    
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x,
                                                         locationOfTouchInLabel.y);
    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer
                                                       inTextContainer:textContainer
                              fractionOfDistanceBetweenInsertionPoints:nil];
    
    NSRange range = NSMakeRange(0, 0);
    if (lblNameAndDetails.text.length) {
        range = [lblNameAndDetails.attributedText.string rangeOfString:lblNameAndDetails.text];
    }
    if (strContactName.length) {
        if (NSLocationInRange(indexOfCharacter, range)){
            [self showSelectedContactsList];
        }

    }

    
}



-(IBAction)submitJournals:(id)sender{
    
    if (strDescription.length <= 0) {
        [ALToastView toastInView:self.view withText:@"Enter description"];
        [txtView becomeFirstResponder];
        return;
    }
    
    [btnPost setEnabled:false];
    [self showLoadingScreen];

    NSMutableArray *arrFileNames = [NSMutableArray new];
    NSMutableArray *arrThumb = [NSMutableArray new];
    
    if (_arrMedias.count) {
        
        typedef void (^UploadMediaBlock)(NSUInteger);
        
        __block __weak UploadMediaBlock uploadMedia = nil;
       
        
        UploadMediaBlock addMedia = ^(NSUInteger index) {
            
            if (index < _arrMedias.count)
            {
                UploadMediaBlock strongUploadMedia = uploadMedia;
                
                [self uploadMediasToServerWithPath:_arrMedias[index] OnSuccess:^(id responseObject) {
                    
                    if ([responseObject objectForKey:@"media_name"]) {
                        [arrFileNames addObject:[responseObject objectForKey:@"media_name"]];
                    }
                    if ([responseObject objectForKey:@"media_thumb"]) {
                        [arrThumb addObject:[responseObject objectForKey:@"media_thumb"]];
                    }else{
                        [arrThumb addObject:[NSNull null]];
                    }
                    strongUploadMedia(index+1);
                    
                } failure:^(NSError *error) {
                    
                    NSLog(@"Failed");
                    return ;
                }];
                
                
            }else{
                
                [self resumeUploadParametersWithUploadedImageNames:arrFileNames thumbImg:arrThumb];
                NSLog(@"done");
            }
            
        };
        uploadMedia = addMedia;
        addMedia(0);
        
    }else{
        
         [self resumeUploadParametersWithUploadedImageNames:arrFileNames thumbImg:arrThumb];
    }
    
   

}

-(void)resumeUploadParametersWithUploadedImageNames:(NSMutableArray*)arrFileNames thumbImg:(NSMutableArray*)arrThumbs{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"d MMM,yyyy h:mm a"];
    NSDate *dateFromString = [dateFormatter dateFromString:strDate];
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    NSInteger feelValue = 0;
    switch (selectedFeelValue) {
        case 1:
            feelValue = -2;
            break;
        case 2:
            feelValue = -1;
            break;
        case 3:
            feelValue = 0;
            break;
        case 4:
            feelValue = 1;
            break;
        case 5:
            feelValue = 2;
            break;
            
        default:
            break;
    }
    
    NSInteger driveValue = 0;
    switch (selectedDriveValue) {
        case 1:
            driveValue = -2;
            break;
        case 2:
            driveValue = -1;
            break;
        case 3:
            driveValue = 0;
            break;
        case 4:
            driveValue = 1;
            break;
        case 5:
            driveValue = 2;
            break;
            
        default:
            break;
    }
    
    [params setObject:[NSNumber numberWithInteger:feelValue] forKey:@"emotion_value"];
    [params setObject:[NSNumber numberWithInteger:selectedEmotionValue] forKey:@"emotion_id"];
    // [params setObject:[NSNumber numberWithInteger:selectedEventValue] forKey:@"event_id"];
    [params setObject:[NSNumber numberWithInteger:driveValue] forKey:@"drive_value"];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    [params setObject:[NSNumber numberWithDouble:[dateFromString timeIntervalSince1970] ] forKey:@"journal_date"];
    if (selectedGoalsValue > 0) [params setObject:[NSNumber numberWithInteger:selectedGoalsValue] forKey:@"goal_id"];
    if ([selectedActions allKeys].count > 0) [params setObject:[NSNumber numberWithInteger:[[selectedActions allKeys] count]] forKey:@"action_count"];
    if ([selectedActions allKeys].count > 0){
        NSString * result = [[selectedActions allKeys] componentsJoinedByString:@","];
        [params setObject:result forKey:@"goalaction_id"];
    }
    
    [APIMapper createAJournelWith:_arrMedias description:strDescription latitude:locationCordinates.latitude longitude:locationCordinates.longitude locName:strLocationAddress address:strLocationAddress contactName:strContactName hashTags:[self getAllHashTagsFromText:strDescription] emotionAwarenssValues:params mediaNames:arrFileNames thumbNames:arrThumbs OnSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [btnPost setEnabled:true];
        [self hideLoadingScreen];
        
        if ([[responseObject objectForKey:@"code"]integerValue] == kUnauthorizedCode){
            
            if (NULL_TO_NIL([responseObject objectForKey:@"text"])) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"MENTAL STRENGTH"
                                                                               message:[responseObject objectForKey:@"text"]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                                                          [delegate clearUserSessions];
                                                                          [self removeAllContentsInMediaFolder];
                                                                      }];
                [alert addAction:firstAction];
                [[self navigationController] presentViewController:alert animated:YES completion:nil];
                
            }
            
        }
        else if ([[responseObject objectForKey:@"code"]integerValue] == kRequestFail){
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"MENTAL STRENGTH"
                                                                           message:[responseObject objectForKey:@"text"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      
                                                                      
                                                                  }];
            [alert addAction:firstAction];
            [[self navigationController] presentViewController:alert animated:YES completion:nil];
            
        }else{
            
            [self showMyJournals];
            [self removeAllContentsInMediaFolder];
        }
        
        
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [btnPost setEnabled:true];
        [self hideLoadingScreen];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"EMOTIONAL AWARENESS"
                                                                       message:[error localizedDescription]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                              }];
        [alert addAction:firstAction];
        [[self navigationController] presentViewController:alert animated:YES completion:nil];
        
    }];
}

-(void)removeAllPopUps{

    [vwFeelSelection removeFromSuperview];
    vwFeelSelection = nil;
    
    [vwEmotionSelection removeFromSuperview];
    vwEmotionSelection = nil;
    
    [vwDriveSelection removeFromSuperview];
    vwDriveSelection = nil;
    
    [vwGoalsSelection removeFromSuperview];
    vwGoalsSelection = nil;
    
    [vwActions removeFromSuperview];
    vwActions = nil;
 
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Saving...";
    hud.removeFromSuperViewOnHide = YES;
}

-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void)automateToolTipMovementWithTag:(NSInteger)respectedTag{
    
    if ([vwBottomPanel viewWithTag:0]) {
        UIView *vwInner = [vwBottomPanel viewWithTag:0];
        if ([vwInner viewWithTag:0]) {
            UIView *vwQustn = [vwInner viewWithTag:respectedTag];
            for (UIButton *btn in [vwQustn subviews]) {
                if ([btn isKindOfClass:[UIButton class]] && btn.tag == respectedTag) {
                    [self enableNextQuestion:btn];
                }
            }
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UIScrollView class]])
        return YES;
    return NO;
}

-(void)handleTap:(UITapGestureRecognizer*)gesture{
    
    [txtView becomeFirstResponder];
    
}

-(void)getImageHeight{
    
    if (_arrMedias.count) {
        id object = _arrMedias[0];
        if ([object isKindOfClass:[NSString class]]) {
            
            NSString *fileName = _arrMedias[0];
            if ([[fileName pathExtension] isEqualToString:@"jpeg"]) {
                //This is Image File with .png Extension , Photos.
                NSString *filePath = [Utility getJournalMediaSaveFolderPath];
                NSString *imagePath = [[filePath stringByAppendingString:@"/"] stringByAppendingString:fileName];
                if (imagePath.length) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                        NSData *data = [[NSFileManager defaultManager] contentsAtPath:imagePath];
                        UIImage *image = [UIImage imageWithData:data];
                        dispatch_sync(dispatch_get_main_queue(), ^(void) {
                            
                            float width = image.size.width;
                            float height = image.size.height;
                            if ((width && height) > 0) {
                                float ratio = width / height;
                                heightForImagePannel.constant = (collectionView.frame.size.width) / ratio;
                                heightOfSingleImage = (collectionView.frame.size.width) / ratio;
                                [collectionView reloadData];
                            }
                            
                        });
                    });
                    
                }
            }
            else if ([[fileName pathExtension] isEqualToString:@"mp4"]) {
                //This is Image File with .mp4 Extension , Video Files
                NSString *filePath = [Utility getJournalMediaSaveFolderPath];
                NSString *imagePath = [[filePath stringByAppendingString:@"/"] stringByAppendingString:fileName];
                //[[cell btnVideoPlay]setHidden:false];
                if (imagePath.length){
                    // [cell.indicator startAnimating];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                        UIImage *thumbnail = [Utility getThumbNailFromVideoURL:imagePath];
                        dispatch_sync(dispatch_get_main_queue(), ^(void) {
                           
                            float width =  thumbnail.size.width;
                            float height =  thumbnail.size.height;
                            float padding = 0;
                            if ((width && height) > 0) {
                                float ratio = width / height;
                                heightForImagePannel.constant = (collectionView.frame.size.width - padding) / ratio;
                                heightOfSingleImage = (collectionView.frame.size.width) / ratio;
                                 [collectionView reloadData];
                            }
                            
                            
                        });
                    });
                }
            }
            else if ([[fileName pathExtension] isEqualToString:@"aac"]){
                // Recorded Audio
                //[[cell btnAudioPlay]setHidden:false];
                UIImage *noImage = [UIImage imageNamed:@"NoImage_Goals_Dreams"];
                float width =  noImage.size.width;
                float height =  noImage.size.height;
                float padding = 0;
                if ((width && height) > 0) {
                    float ratio = width / height;
                    heightForImagePannel.constant = (collectionView.frame.size.width - padding) / ratio;
                    heightOfSingleImage = (collectionView.frame.size.width) / ratio;
                     [collectionView reloadData];
                }
            }
            
        }

    }
    
}

-(void)handleSwipe:(UISwipeGestureRecognizer*)gesture{
    
    if (!isQuestionsReady) return;
    if (gesture.direction == UISwipeGestureRecognizerDirectionUp)
        [self toggleQuestionViewShow:YES];
    else
        [self toggleQuestionViewShow:NO];
}

-(void)createInputAccessoryView{
    
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 45.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 1];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 85, 5.0f, 80.0f, 35.0f)];
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor getThemeColor]];
    btnDone.layer.cornerRadius = 5.f;
    btnDone.layer.borderWidth = 1.f;
    btnDone.layer.borderColor = [UIColor whiteColor].CGColor;
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    btnDone.titleLabel.font = [UIFont fontWithName:CommonFont_New size:14];
    [inputAccView addSubview:btnDone];
}

-(void)doneTyping{
    
    [self.view endEditing:YES];
    
    strDescription = txtView.text;
    NSString *trimmedString = [strDescription stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if (trimmedString.length) isQuestionsReady = true;
    if (isQuestionsReady){
        if (![dictSelectedSteps objectForKey:[NSNumber numberWithInteger:eTypeFeel]]){
            [self toggleQuestionViewShow:YES];
           [self enableNextQuestion:btnFirstQuestion];
        }
    }

}

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame

    
    
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    bottomForScrollView.constant = - (keyboardBounds.size.height) + bottomPadding;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    
}

-(void)animateToNextQuestion{
    
    [UIView transitionWithView:lblQuestion
                      duration:0.25f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                    } completion:nil];
}

-(NSString*)getCurrentDateIsTime:(BOOL)isTime{
    
    NSDate * today = [NSDate date];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"d MMM,yyyy h:mm a"];
    strDate = [dateformater stringFromDate:today];
    [dateformater setDateFormat:@"d MMM"];
    if (isTime) {
        [dateformater setDateFormat:@"h:mm a"];
        return [dateformater stringFromDate:today];
    }else{
        NSString *_date =  [[dateformater stringFromDate:today] uppercaseString];
        return [NSString stringWithFormat:@"%@\n%@",[[_date componentsSeparatedByString:@" "] objectAtIndex:0],[[_date componentsSeparatedByString:@" "] objectAtIndex:1]];
        
    }
    return nil;
}

-(void)removeAllContentsInMediaFolder{
    
    NSString *dataPath = [Utility getJournalMediaSaveFolderPath];
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager removeItemAtPath:dataPath error:&error];
    if (success){
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
}
-(void)showMyJournals{
    
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    [navigationArray removeLastObject];  // You can pass your index here
    self.navigationController.viewControllers = navigationArray;
    AppDelegate *appdelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appdelegate showJournalListView];
}

-(IBAction)goBack:(id)sender{
    
    if (strDescription.length > 0 || _arrMedias.count > 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"MENTAL STRENGTH"
                                                                       message:@"Do you really want to quit this page?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"YES"
                                                              style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                                  
                                                                  [self removeAllContentsInMediaFolder];
                                                                  [[NSNotificationCenter defaultCenter] removeObserver:self];
                                                                  [[self navigationController] popViewControllerAnimated:YES];
                                                              }];
        [alert addAction:firstAction];
        
        UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"NO"
                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                   
                                                                   
                                                               }];
        [alert addAction:secondAction];
        [[self navigationController] presentViewController:alert animated:YES completion:nil];
        
    }else{
       
        [self removeAllContentsInMediaFolder];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [[self navigationController] popViewControllerAnimated:YES];
    }
    
    
    
    
   
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
