//
//  GalleryCollectionViewCell.h
//  PurposeColor
//
//  Created by Purpose Code on 23/01/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryCollectionViewCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIImageView *imgView;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *rightConstraintImage;

@property (nonatomic,weak) IBOutlet UIButton *btnAudioPlay;
@property (nonatomic,weak) IBOutlet UIButton *btnVideoPlay;

@property (nonatomic,weak) IBOutlet UIView *vwOverLay;
@property (nonatomic,weak) IBOutlet UILabel *lblCount;

@end
