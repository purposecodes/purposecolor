//
//  GEMListingViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 09/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eFollow = 0,
    eFollowPending = 1,
    eFollowed = 2,
    
} eFollowStatus;


static NSString *CollectionViewCellIdentifier = @"GemsListCell";
#define OneK                    1000
#define kPadding                10
#define kDefaultNumberOfCells   1
#define kSuccessCode            200
#define kUnAuthorized           403

#import "HashTagGemListingViewController.h"
#import "GemsListTableViewCell.h"
#import "CommentComposeViewController.h"
#import "GEMDetailViewController.h"
#import "MyGEMListingViewController.h"
#import "GEMSListingHeaderView.h"
#import "Constants.h"
#import "ChatUserListingViewController.h"
#import "ProfilePageViewController.h"
#import "LikedAndCommentedUserListings.h"
#import "shareMedias.h"
#import "CreateActionInfoViewController.H"
#import "MenuViewController.h"
#import "FTPopOverMenu.h"
#import "ReportAbuseViewController.h"
#import "MTDURLPreview.h"
#import <CoreText/CoreText.h>
#import "HashTagList+CoreDataClass.h"

@interface HashTagGemListingViewController ()<GemsListTableViewCellDelegate,CommentActionDelegate,MediaListingPageDelegate,shareMediasDelegate,SWRevealViewControllerDelegate,UIGestureRecognizerDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UIView *vwPaginationPopUp;
    IBOutlet NSLayoutConstraint *paginationBottomConstraint;
    UIRefreshControl *refreshControl;
    IBOutlet UIButton *btnSlideMenu;
    IBOutlet UIView *vwOverLay;
    IBOutlet UILabel *lblTitle;
    
    NSMutableArray *arrGems;
    NSInteger totalPages;
    NSInteger currentPage;
    NSMutableDictionary *dictFollowers;
    eFollowStatus followStatus;
    NSString *strNoDataText;
    
    BOOL isPageRefresing;
    BOOL isDataAvailable;
    NSInteger totalGems;
    
    shareMedias *shareMediaView;
    BOOL isCachedData;
}

@end

@implementation HashTagGemListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self getAllProductsByPagination:NO withPageNumber:currentPage];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



-(void)setUp{
    
    lblTitle.text = _strTagName;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 300;
    
    dictFollowers = [NSMutableDictionary new];
    
    arrGems = [NSMutableArray new];
    currentPage = 1;
    totalPages = 0;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    tableView.alwaysBounceVertical = YES;
    tableView.hidden = true;
    isDataAvailable = false;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedFollowReqChangeFromProfile:)
                                                 name:@"FollowReqFromProfile"
                                               object:nil];
    
}

-(void)receivedFollowReqChangeFromProfile:(NSNotification *) notification
{
    if (notification.userInfo && [notification.userInfo objectForKey:@"userID"]) {
        NSString *userID = [notification.userInfo objectForKey:@"userID"];
        NSNumber *status =  [notification.userInfo objectForKey:@"status"];
        [dictFollowers setObject:status forKey:userID];
        [tableView reloadData];
    }
    
    
}

-(void)getAllProductsByPagination:(BOOL)isPagination withPageNumber:(NSInteger)pageNumber{
    
    if (!isPagination) {
        [self showLoadingScreen];
    }
    
    
    [APIMapper getAllTaggedGemsByPageNo:pageNumber keyWord:_strTagName isPublic:YES success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        if (pageNumber == 1) [self undUpdateReachCount];
        tableView.hidden = false;
        isPageRefresing = NO;
        [refreshControl endRefreshing];
        [self getGemsFromResponds:responseObject];
        [tableView reloadData];
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        [self saveRecordsToDB:responseObject pageNumber:pageNumber];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        if (error && error.localizedDescription){
            [ALToastView toastInView:self.view withText:NETWORK_ERROR_MESSAGE];
            strNoDataText = error.localizedDescription;
            
        }
        tableView.hidden = false;
        isDataAvailable = false;
        isPageRefresing = NO;
        [refreshControl endRefreshing];
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        [tableView reloadData];
        if (pageNumber == 1) [self loadAllCashedRecords];
        
    }];
    
}

-(void)refreshData{
    
    isCachedData = false;
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    [arrGems removeAllObjects];
    [dictFollowers removeAllObjects];
    [self showLoadingScreen];
    currentPage = 1;
    isPageRefresing = YES;
    [self getAllProductsByPagination:YES withPageNumber:currentPage];
    
    
}

-(void)getGemsFromResponds:(NSDictionary*)responseObject{
    
    isDataAvailable = false;
    if ( NULL_TO_NIL([responseObject objectForKey:@"resultarray"])) {
        NSArray *results = [responseObject objectForKey:@"resultarray"];
        for (NSDictionary *dict in results)
            [arrGems addObject:dict];
    }else{
        strNoDataText = [responseObject objectForKey:@"text"];
    }
    if (arrGems.count) isDataAvailable = true;
    
    if (NULL_TO_NIL([responseObject objectForKey:@"pageCount"]))
        totalPages =  [[responseObject objectForKey:@"pageCount"]integerValue];
    
    if (NULL_TO_NIL([responseObject objectForKey:@"currentPage"]))
        currentPage =  [[responseObject objectForKey:@"currentPage"]integerValue];
    
    if (NULL_TO_NIL([responseObject objectForKey:@"totalCount"]))
        totalGems =  [[responseObject objectForKey:@"totalCount"]integerValue];
    
    if (arrGems.count) isDataAvailable = true;
    
    
}

#pragma mark - DB Handling

-(void)saveRecordsToDB:(NSDictionary*)responseObject pageNumber:(NSInteger)pageNumber{
    
    if (isDataAvailable && arrGems.count) {
        if (pageNumber == 1) [self clearTable]; // First call, clear all entries.
        if (responseObject) {
            NSError * err;
            NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:responseObject options:0 error:&err];
            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            if (myString) {
                NSManagedObjectContext* wContext = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
                HashTagList * _community = [[HashTagList alloc] initWithContext: wContext ];
                _community.responds = myString;
                _community.pagenumber = pageNumber;
                _community.type = _strTagName;
                if (![wContext save:nil]) {
                }
            }
        }
    }
    
}

-(void)clearTable{
    
    NSManagedObjectContext* context = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"HashTagList" inManagedObjectContext:context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type == %@", _strTagName];
    [fetch setPredicate:predicate];
    NSArray * result = [context executeFetchRequest:fetch error:nil];
    for (id basket in result)
        [context deleteObject:basket];
}

-(void)loadAllCashedRecords{
    
    isCachedData = true;
    NSError *error = nil;
    NSManagedObjectContext* context = ((AppDelegate*)UIApplication.sharedApplication.delegate).persistentContainer.viewContext;
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"HashTagList" inManagedObjectContext:context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type == %@", _strTagName];
    [fetch setPredicate:predicate];
    [arrGems removeAllObjects];
    NSArray *results = [context executeFetchRequest:fetch error:&error];
    NSError * err;
    for (HashTagList *_community in results) {
        NSString *myString = _community.responds;
        NSData *data =[myString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * responseObject;
        if(data!=nil){
            responseObject = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            if ( NULL_TO_NIL([responseObject objectForKey:@"resultarray"])) {
                NSArray *results = [responseObject objectForKey:@"resultarray"];
                for (NSDictionary *dict in results)
                    [arrGems addObject:dict];
            }
        }
    }
    if (arrGems.count) {
        isDataAvailable = true;
        [tableView reloadData];
    }
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kDefaultNumberOfCells;
    return arrGems.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) {
        UITableViewCell *cell = [Utility getNoDataCustomCellWith:aTableView withTitle:strNoDataText];
        return cell;
    }
    NSString *CellIdentifier = @"GemsListTableViewCell";
    GemsListTableViewCell *cell = (GemsListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.row < arrGems.count) {
        
        NSDictionary *details = arrGems[indexPath.row];
        
        BOOL shouldShowUnfollowCell = [self checkShouldShowUnfollowCellWithDetails:details];
        if (shouldShowUnfollowCell) {
            cell = [self getUnfollowedTemplatCell];
        }else{
            if ([details objectForKey:@"gem_type"] && [[details objectForKey:@"gem_type"] isEqualToString:@"moment"]) {
                cell = [self dequeMomentCellWithIndex:indexPath.row section:indexPath.section andDetails:details];
                [self configureMomentDetailsWith:details cell:cell indexPath:indexPath];
            }
            else{
                cell = [self dequeRequiredCellWithIndex:indexPath.row section:indexPath.section andDetails:details];
            }
        }
        [self configureTextVariables:details cell:cell indexPath:indexPath];
        [self setupPreviewVariables:cell tag:indexPath.row];
        
        NSString *string;
        BOOL isPreview = false;
        if (NULL_TO_NIL([details objectForKey:@"preview_url"])){
            string = [details objectForKey:@"preview_url"];
            isPreview = true;
            
        }else if (NULL_TO_NIL([details objectForKey:@"gem_details"])){
            string = [details objectForKey:@"gem_details"];
        }
        if (string.length) {
            NSError *error = nil;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                       error:&error];
            NSArray *matches = [detector matchesInString:string
                                                 options:0
                                                   range:NSMakeRange(0, [string length])];

            if (matches.count > 0) {
                cell.vwURLPreview.hidden = false;
                cell.previewTopCosntraint.constant = 10;
                if (!isPreview) {
                    cell.previewPanelHeightConstraint.constant = 105;
                    cell.previewImageHeight.constant = 0;
                    cell.previewImageTop.constant = 0;
                }else{
                    cell.previewPanelHeightConstraint.constant = 305;
                    cell.previewImageHeight.constant = 200;
                    cell.previewImageTop.constant = 5;
                }
                if ([[CashedURL sharedManager].dictCashedURLS objectForKey:string]) {
                    MTDURLPreview *preview = [[CashedURL sharedManager].dictCashedURLS objectForKey:string];
                    [cell.previewIndicator stopAnimating];
                    cell.lblPreviewTitle.text = preview.title;
                    cell.lblPreviewDescription.text = preview.content;
                    cell.lblPreviewTitle.text = preview.title;
                    cell.lblPreviewDomain.text = preview.domain;
                    [cell.imgPreview setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
                    if (preview.imageURL) {
                        [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                           placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                   
                                                  }];
                    }
                }else{
                    
                    NSTextCheckingResult *match = [matches firstObject];
                    [cell.previewIndicator startAnimating];
                    [cell.imgPreview setImage:[UIImage imageNamed:@""]];
                    [MTDURLPreview loadPreviewWithURL:[match URL] completion:^(MTDURLPreview *preview, NSError *error) {
                        if (!error) {
                            if (preview) [[CashedURL sharedManager].dictCashedURLS setObject:preview forKey:string];
                        }
                        [cell.previewIndicator stopAnimating];
                        cell.lblPreviewTitle.text = preview.title;
                        cell.lblPreviewDescription.text = preview.content;
                        cell.lblPreviewTitle.text = preview.title;
                        cell.lblPreviewDomain.text = preview.domain;
                        [cell.imgPreview setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
                        if (preview.imageURL) {
                            [cell.imgPreview sd_setImageWithURL:preview.imageURL
                                               placeholderImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]
                                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                          
                                                      }];
                        }
                        
                    }];
                }
                
            }

        }
        
    }
    cell.imgProfile.tag = indexPath.row;
    cell.imgProfile.userInteractionEnabled = true;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserProfilePage:)];
    [cell.imgProfile addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];

    return cell;
    
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self showDetailPageWithIndex:indexPath.row];
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [self undUpdateReachCount];
    /**! Pagination call !**/
    if (isCachedData) return;
    NSInteger endScrolling = scrollView.contentOffset.y+ scrollView.frame.size.height;
    NSInteger height = scrollView.contentSize.height;
    if (endScrolling >= height){
        
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            NSInteger nextPage = currentPage ;
            nextPage += 1;
            if (nextPage  <= totalPages) {
                isPageRefresing = YES;
                [self showPaginationPopUp];
                [self getAllProductsByPagination:YES withPageNumber:nextPage];
            }
            
        }
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([[touch view] isKindOfClass:[KILabel class]]) {
        return NO;
    }
    return YES;
}



#pragma mark - Customise Cells Method


-(void)showDetailPageWithIndex:(NSInteger)index{
    
    if (index < arrGems.count) {
        NSMutableDictionary *gemDetails = [NSMutableDictionary dictionaryWithDictionary:arrGems[index]] ;
        GEMDetailViewController *gemDetailVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForGEMDetailPage];
        if (NULL_TO_NIL([gemDetails objectForKey:@"user_id"])) {
            NSString *followerID = [gemDetails objectForKey:@"user_id"];
            if (NULL_TO_NIL([dictFollowers objectForKey:followerID])){
                NSInteger  status = (int) [[dictFollowers objectForKey:followerID] integerValue];
                [gemDetails setObject:[NSNumber numberWithInteger:status] forKey:@"follow_status"];
            }
        }
        gemDetailVC.gemDetails = gemDetails;
        gemDetailVC.delegate = self;
        gemDetailVC.clickedIndex = index;
        gemDetailVC.canSave = YES;
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.navGeneral pushViewController:gemDetailVC animated:YES];
    }
}


-(void)previewClickedWithGesture:(UIButton*)btn{
    
    if (btn.tag < arrGems.count) {
        
        NSDictionary *details = arrGems[btn.tag];
        NSString *string;
        if (NULL_TO_NIL([details objectForKey:@"preview_url"])){
            string = [details objectForKey:@"preview_url"];
            
        }else if (NULL_TO_NIL([details objectForKey:@"gem_details"])){
            string = [details objectForKey:@"gem_details"];
        }
        if (string.length){
            NSError *error = nil;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                                       error:&error];
            NSArray *matches = [detector matchesInString:string
                                                 options:0
                                                   range:NSMakeRange(0, [string length])];
            if (matches.count > 0) {
                NSTextCheckingResult *match = [matches firstObject];
                if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                    [[UIApplication sharedApplication] openURL:[match URL] options:@{}
                       completionHandler:^(BOOL success) {
                       }];
                }
                
            }

        }
        
    }
}

-(void)setupPreviewVariables:(GemsListTableViewCell*)cell tag:(NSInteger)tag{
    
    cell.lblPreviewDescription.text = @"";
    cell.lblPreviewTitle.text = @"";
    cell.lblPreviewDomain.text = @"";
    cell.vwURLPreview.hidden = true;
    cell.btnShowPreviewURL.tag = tag;
    [cell.btnShowPreviewURL addTarget:self action:@selector(previewClickedWithGesture:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)resetCellVariables:(GemsListTableViewCell*)cell{
    
    cell.vwBg.layer.borderColor = [UIColor colorWithRed:193/255.f green:196/255.f blue:199/255.f alpha:0.5].CGColor;
    cell.vwBg.layer.borderWidth = 1.0;
    
    cell.imgProfile.layer.cornerRadius = 25.f;
    cell.imgProfile.clipsToBounds = YES;
    [cell.imgGemMedia setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
    [cell.activityIndicator stopAnimating];
    
    
    cell.btnDelete.hidden = true;
    cell.btnFollow.hidden = true;
    // cell.btnHide.hidden = true;
    
}

-(GemsListTableViewCell*)dequeMomentCellWithIndex:(NSInteger)index section:(NSInteger)section andDetails:(NSDictionary*)details{
    
    // User Goal Cell
    static NSString *CellIdentifier = @"GemsListMomentsCell";
    GemsListTableViewCell *cell = (GemsListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.delegate = self;
    [self resetCellVariables:cell];
    [cell setUpIndexPathWithRow:index section:section];
    return cell;
    
}
-(GemsListTableViewCell*)dequeRequiredCellWithIndex:(NSInteger)index section:(NSInteger)section andDetails:(NSDictionary*)details{
    
    NSString *CellIdentifier = @"GemsListTableViewCell";
    GemsListTableViewCell *cell = (GemsListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.delegate = self;
    [self resetCellVariables:cell];
    [cell setUpIndexPathWithRow:index section:section];
    return cell;
    
}

-(void)configureFollowButtonWithDetails:(NSDictionary*)details cell:(GemsListTableViewCell*)cell{
    
    cell.btnFollow.layer.borderWidth = 1.f;
    cell.btnFollow.layer.borderColor = [UIColor getThemeColor].CGColor;
    cell.btnFollow.layer.cornerRadius = 5.f;
    
    if (NULL_TO_NIL([details objectForKey:@"follow_status"]))
        followStatus =  [[details objectForKey:@"follow_status"] intValue];
    
    if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
        NSString *followerID = [details objectForKey:@"user_id"];
        if (NULL_TO_NIL([dictFollowers objectForKey:followerID])){
            int follow = (int)[[dictFollowers objectForKey:followerID] integerValue];
            followStatus = follow;
        }
    }
    
    switch (followStatus) {
        case eFollowed:
            [cell.btnFollow setEnabled:true];
            [cell.btnFollow setTitle:@"Unfollow" forState:UIControlStateNormal];
            [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
            [cell.btnFollow setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
            break;
            
        case eFollow:
            [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
            [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
            [cell.btnFollow setEnabled:true];
            [cell.btnFollow setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
    
    float rightConstant = 80;
    if (followStatus == eFollow) rightConstant = 10;
    
    BOOL canFollow = true;
    if (NULL_TO_NIL([details objectForKey:@"can_follow"]))
        canFollow = [[details objectForKey:@"can_follow"] boolValue];
    if ([[User sharedManager].userId isEqualToString:[details objectForKey:@"user_id"]]) {
        canFollow = false;
        rightConstant = 10;
    }
    
    NSInteger isVerified = 0;
    cell.imgVerified.hidden = true;
    if (NULL_TO_NIL([details objectForKey:@"verify_status"])){
        isVerified = [[details objectForKey:@"verify_status"] integerValue];
        if (isVerified == 1) {
            rightConstant += 25;
            cell.imgVerified.hidden = false;
        }
    }
    
    
    cell.trailingForName.constant = -(rightConstant);
    cell.btnFollow.hidden = (canFollow) ? false : true;
}

/*

-(void)configureFollowButtonWithDetails:(NSDictionary*)details cell:(GemsListTableViewCell*)cell{
    
    
    float rightConstant = 80;
    BOOL canFollow = true;
    if (NULL_TO_NIL([details objectForKey:@"can_follow"]))
        canFollow = [[details objectForKey:@"can_follow"] boolValue];
    if ([[User sharedManager].userId isEqualToString:[details objectForKey:@"user_id"]]) {
        canFollow = false;
        rightConstant = 10;
    }
    BOOL isVerified = false;
    cell.imgVerified.hidden = true;
    if (NULL_TO_NIL([details objectForKey:@"verify_status"])){
        isVerified = [[details objectForKey:@"verify_status"] boolValue];
        if (isVerified) {
            rightConstant += 25;
            cell.imgVerified.hidden = false;
        }
    }
    cell.trailingForName.constant = -(rightConstant);
    cell.btnFollow.hidden = (canFollow) ? false : true;
    // cell.btnHide.hidden = [[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId] ? false : true;
    
    cell.btnFollow.layer.borderWidth = 1.f;
    cell.btnFollow.layer.borderColor = [UIColor getThemeColor].CGColor;
    cell.btnFollow.layer.cornerRadius = 5.f;
    
    if (NULL_TO_NIL([details objectForKey:@"follow_status"]))
        followStatus =  [[details objectForKey:@"follow_status"] intValue];
    
    if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
        NSString *followerID = [details objectForKey:@"user_id"];
        if (NULL_TO_NIL([dictFollowers objectForKey:followerID])){
            int follow = (int)[[dictFollowers objectForKey:followerID] integerValue];
            followStatus = follow;
        }
    }
    
    switch (followStatus) {
        case eFollowed:
            [cell.btnFollow setEnabled:true];
            [cell.btnFollow setTitle:@"UnFollow" forState:UIControlStateNormal];
            [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
            [cell.btnFollow setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
            break;
            
        case eFollowPending:
            [cell.btnFollow setEnabled:false];
            [cell.btnFollow setTitle:@"Follow" forState:UIControlStateDisabled];
            [cell.btnFollow setBackgroundColor:[UIColor lightGrayColor]];
            [cell.btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cell.btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
            break;
            
        case eFollow:
            [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
            [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
            [cell.btnFollow setEnabled:true];
            [cell.btnFollow setTitleColor:[UIColor getThemeColor] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
}*/

-(void)configureTextVariables:(NSDictionary*)details cell:(GemsListTableViewCell*)cell indexPath:(NSIndexPath*)indexPath{
    
    cell.titleTopConstraint.constant = 0;
    cell.descriptionTopConstraint.constant = 0;
    cell.previewTopCosntraint.constant = 0;
    cell.topForShareTxt.constant = 0;
    cell.previewPanelHeightConstraint.constant = 0;
    
    cell.vwURLPreview.hidden = true;
    cell.lblName.text = [details objectForKey:@"firstname"];
    cell.lblTime.text = [Utility getDaysBetweenTwoDatesWith:[[details objectForKey:@"gem_datetime"] doubleValue]];
    cell.lblTitle.text = @"";
    cell.lblDescription.text = @"";
    
    NSInteger isVerified = 0;
    cell.imgVerified.hidden = true;
    if (NULL_TO_NIL([details objectForKey:@"verify_status"])){
        isVerified = [[details objectForKey:@"verify_status"] integerValue];
        if (isVerified == 1) {
            cell.imgVerified.hidden = false;
        }
    }
    
    cell.lblShareText.text = @"";
    if (NULL_TO_NIL([details objectForKey:@"share_msg"])){
        cell.topForShareTxt.constant = 10;
        cell.lblShareText.text = [NSString stringWithFormat:@"\'%@\'",[details objectForKey:@"share_msg"]];
    }
    
    if (NULL_TO_NIL([details objectForKey:@"gem_title"])){
        cell.titleTopConstraint.constant = 10;
        cell.lblTitle.text = [details objectForKey:@"gem_title"];
    }
    cell.vwShareHolder.hidden = true;
    
    if ([[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
        cell.vwShareHolder.hidden = false;
    }
    if ([[details objectForKey:@"gem_type"] isEqualToString:@"action"]) {
        [cell.btnBanner setTitle:@"ACTION" forState:UIControlStateNormal];;
    }
    else if ([[details objectForKey:@"gem_type"] isEqualToString:@"goal"]) {
        [cell.btnBanner setTitle:@"GOAL" forState:UIControlStateNormal];;
    }
    else if ([[details objectForKey:@"gem_type"] isEqualToString:@"moment"]) {
        [cell.btnBanner setTitle:@"MOMENT" forState:UIControlStateNormal];;
    }
    else if ([[details objectForKey:@"gem_type"] isEqualToString:@"community"]) {
        [cell.btnBanner setTitle:@"COMMUNITY" forState:UIControlStateNormal];;
    }
    [cell.btnViews setTitle:[NSString stringWithFormat:@"%ld View(s)",[[details objectForKey:@"view_count"] integerValue]] forState:UIControlStateNormal];;
    cell.contraintForLocTop.constant = 0;
    cell.contraintForContTop.constant = 0;
    cell.lblLocation.text = @"";
    cell.lblContact.text = @"";
    if (NULL_TO_NIL([details objectForKey:@"location_name"])) {
        cell.contraintForLocTop.constant = 10;
        NSMutableAttributedString *myString = [NSMutableAttributedString new];
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        UIImage *icon = [UIImage imageNamed:@"Loc_Small"];
        attachment.image = icon;
        attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblLocation.font.descender + 2), icon.size.width, icon.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        [myString appendAttributedString:attachmentString];
        NSAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",[details objectForKey:@"location_name"]]];
        [myString appendAttributedString:myText];
        cell.lblLocation.attributedText = myString;
    }
    
    if (NULL_TO_NIL([details objectForKey:@"contact_name"])) {
        cell.contraintForContTop.constant = 10;
        NSMutableAttributedString *myString = [NSMutableAttributedString new];
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        UIImage *icon = [UIImage imageNamed:@"contact_icon"];
        attachment.image = icon;
        attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblContact.font.descender + 2), icon.size.width, icon.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        [myString appendAttributedString:attachmentString];
        NSAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",[details objectForKey:@"contact_name"]]];
        [myString appendAttributedString:myText];
        cell.lblContact.attributedText = myString;
        
    }
    
    if (NULL_TO_NIL([details objectForKey:@"gem_details"])){
        float lblHeight = [Utility getSizeOfLabelWithText:[details objectForKey:@"gem_details"] width:self.view.frame.size.width - 30 font:[UIFont fontWithName:CommonFont size:15]];
        NSString *des = [details objectForKey:@"gem_details"];
        cell.descriptionTopConstraint.constant = 10;
        cell.lblDescription.text = des;
        if (lblHeight > 112) {
            [self addReadMoreStringToUILabel:cell.lblDescription];
        }
        cell.lblDescription.hashtagLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
            // Open URLs
            [self hashTagClickedWithTag:string];
            
        };

    }
        
    [cell.btnLike setImage:[UIImage imageNamed:@"Like_Buton"] forState:UIControlStateNormal];
    if ([[details objectForKey:@"like_status"] boolValue])
        [cell.btnLike setImage:[UIImage imageNamed:@"Like_Active"] forState:UIControlStateNormal];
    
    NSInteger count = [[details objectForKey:@"likecount"] integerValue];
    if (count >= OneK) cell.lblLikeCnt.text = [NSString stringWithFormat:@"%@",[self getCountInTermsOfThousand:count]];
    else cell.lblLikeCnt.text = [NSString stringWithFormat:@"%d",[[details objectForKey:@"likecount"] integerValue]];
    
    count =  [[details objectForKey:@"comment_count"] integerValue];
    if (count >= OneK) cell.lblCmntCount.text = [NSString stringWithFormat:@"%@",[self getCountInTermsOfThousand:count]];
    else cell.lblCmntCount.text = [NSString stringWithFormat:@"%d",[[details objectForKey:@"comment_count"] integerValue]];
    
    cell.btnMore.hidden = false;
    if ([[details objectForKey:@"type"] isEqualToString:@"purposecolor"]) cell.btnMore.hidden = true;
    
    cell.bnExpandGallery.hidden = TRUE;
    cell.lblMediaCount.hidden = TRUE;
    cell.vwGridContainer.hidden = true;
    NSArray *gallery;
    BOOL isGrid = false;
    if (NULL_TO_NIL([details objectForKey:@"gem_media"])){
        gallery =  [details objectForKey:@"gem_media"];
        isGrid = [self shouldShowImageGrid:gallery cell:cell];
        
    }
    
    if (!isGrid) [self addMaskIfNeededOnView:cell.imgGemMedia details:details];
    if (NULL_TO_NIL ([details objectForKey:@"profileimg"]) && [[details objectForKey:@"profileimg"] length]){
        [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[details objectForKey:@"profileimg"]]
                           placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  }];
    }
    cell.imgGemMedia.hidden = false;
    cell.imgTransparentVideo.hidden = true;
    if ([[details objectForKey:@"display_type"] isEqualToString:@"video"])cell.imgTransparentVideo.hidden = false;
    if ([[details objectForKey:@"display_image"] isEqualToString:@"No"]) cell.imgGemMedia.hidden = true;
    cell.imgGemMedia.image = nil;
    float imageHeight = 0;
    cell.imageTopCosntraint.constant = 0;
    [cell.activityIndicator stopAnimating];
    if (NULL_TO_NIL([details objectForKey:@"display_image"])){
        cell.imageTopCosntraint.constant = 10;
         if ([details objectForKey:@"gem_type"] && [[details objectForKey:@"gem_type"] isEqualToString:@"moment"])  cell.imageTopCosntraint.constant = 0;
        NSString *url = [details objectForKey:@"display_image"];
        float width = [[details objectForKey:@"image_width"] floatValue];
        float height = [[details objectForKey:@"image_height"] floatValue];
        float padding = 10;
        if ((width && height) > 0) {
            float ratio = width / height;
            imageHeight = (tableView.frame.size.width - padding) / ratio;
        }
        if (url.length) {
            [cell.activityIndicator startAnimating];
            [cell.imgGemMedia sd_setImageWithURL:[NSURL URLWithString:url]
                                placeholderImage:[UIImage imageNamed:@""]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           
                                           [cell.activityIndicator stopAnimating];
                                       }];
        }
    }
    cell.constraintForHeight.constant = isGrid ? self.view.frame.size.width - 10 : imageHeight;

    
    
}



- (void)addReadMoreStringToUILabel:(UILabel*)label
{
    NSString *readMoreText = @" ...Continue Reading";
    NSInteger lengthForVisibleString =  [self fitString:label.text intoLabel:label];
    if (lengthForVisibleString > 0) {
        NSMutableString *mutableString = [[NSMutableString alloc] initWithString:label.text];
        NSString *trimmedString = [mutableString stringByReplacingCharactersInRange:NSMakeRange(lengthForVisibleString, (label.text.length - lengthForVisibleString)) withString:@""];
        NSInteger readMoreLength = readMoreText.length;
        NSString *trimmedForReadMore = [trimmedString stringByReplacingCharactersInRange:NSMakeRange((trimmedString.length - readMoreLength), readMoreLength) withString:@""];
        NSMutableAttributedString *answerAttributed = [[NSMutableAttributedString alloc] initWithString:trimmedForReadMore attributes:@{
                                                                                                                                        NSFontAttributeName : label.font
                                                                                                                                        }];
        
        NSMutableAttributedString *readMoreAttributed = [[NSMutableAttributedString alloc] initWithString:readMoreText attributes:@{
                                                                                                                                    NSFontAttributeName : [UIFont fontWithName:CommonFont size:15],
                                                                                                                                    NSForegroundColorAttributeName : [UIColor lightGrayColor]
                                                                                                                                    }];
        
        [answerAttributed appendAttributedString:readMoreAttributed];
        label.attributedText = answerAttributed;
    }
}

- (NSUInteger)fitString:(NSString *)string intoLabel:(UILabel *)label
{
    UIFont *font           = label.font;
    NSLineBreakMode mode   = label.lineBreakMode;
    
    CGFloat labelWidth     = tableView.frame.size.width - 30;
    CGFloat labelHeight    = 112;
    CGSize  sizeConstraint = CGSizeMake(labelWidth, CGFLOAT_MAX);
    
    
    NSDictionary *attributes = @{ NSFontAttributeName : font };
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    CGRect boundingRect = [attributedText boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    {
        if (boundingRect.size.height > labelHeight)
        {
            NSUInteger index = 0;
            NSUInteger prev;
            NSMutableCharacterSet* testCharSet = [[NSMutableCharacterSet alloc] init];
            [testCharSet formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
            [testCharSet formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
            [testCharSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
            [testCharSet formUnionWithCharacterSet:[NSCharacterSet newlineCharacterSet]];
            
            do
            {
                prev = index;
                if (mode == NSLineBreakByCharWrapping)
                    index++;
                else
                    index = [string rangeOfCharacterFromSet:testCharSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
            }
            
            while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height <= labelHeight);
            
            
            return prev;
        }
    }
    
    
    return [string length];
}
-(GemsListTableViewCell*)getUnfollowedTemplatCell{
    
    NSString *CellIdentifier = @"GemsListUnfollowedCell";
    GemsListTableViewCell *cell = (GemsListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Follow the profile to view the content"];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    [attributeString addAttribute:NSForegroundColorAttributeName
                            value:[UIColor getThemeColor]
                            range:NSMakeRange(0, [attributeString length])];
    cell.lblFollowStaticTxt.attributedText = attributeString;
    cell.delegate = self;
    [self resetCellVariables:cell];
    return cell;
}

-(BOOL)checkShouldShowUnfollowCellWithDetails:(NSDictionary*)details{
    return false;
    eFollowStatus _followStatus = eFollow;
    if ([[User sharedManager].userId isEqualToString:[details objectForKey:@"user_id"]]) {
        return false;
    }else{
        
        if (NULL_TO_NIL([details objectForKey:@"follow_status"]))
            _followStatus =  [[details objectForKey:@"follow_status"] intValue];
        
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            NSString *followerID = [details objectForKey:@"user_id"];
            if (NULL_TO_NIL([dictFollowers objectForKey:followerID])){
                int follow = (int)[[dictFollowers objectForKey:followerID] integerValue];
                _followStatus = follow;
            }
        }
        
        if (_followStatus < eFollowed && ![[details objectForKey:@"verify_status"] boolValue]) {
            return true;
            //  Not following so use Follow cells
            
        }
    }
    
    
    return false;
}


#pragma mark - Custom Cell Delegate For More Action Method

-(void)moreButtonClickedWithIndex:(NSInteger)index view:(UIView*)sender{
    
    if (index < arrGems.count) {
        NSDictionary *details = arrGems[index];
        BOOL isOthers = [[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId] ? false : true;
        UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Choose an action" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction* hide;
        if (isOthers) {
            hide = [UIAlertAction actionWithTitle:@"Report Abuse" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                
                [alert dismissViewControllerAnimated:YES completion:nil];
                [self reportAbuseWithIndex:index];
                
            }];
            
        }else{
            hide = [UIAlertAction actionWithTitle:@"Hide" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                
                [alert dismissViewControllerAnimated:YES completion:nil];
                [self hideAGemWithIndex:index];
                
            }];
        }
        
        UIAlertAction* save = [UIAlertAction actionWithTitle:@"Save Post" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   [self saveSelectedPostWithIndex:index button:nil];
                                   
                               }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        if (isOthers)  [alert addAction:save];
        [alert addAction:hide];
        [alert addAction:cancel];
        [self.navigationController presentViewController:alert animated:YES completion:nil];
        
        
    }
    
    
    
}

#pragma mark - Custom Cell Delegate For Repoer Abuse Gem Method

-(void)reportAbuseWithIndex:(NSInteger)index{
    
    if (index < arrGems.count) {
        
        NSDictionary *gemDetails = arrGems[index];
        NSString *gemID;
        NSString *gemType;
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
            gemID = [gemDetails objectForKey:@"gem_id"];
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
            gemType = [gemDetails objectForKey:@"gem_type"];
        
        ReportAbuseViewController *reportAbuseVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForReportAbuse];
        reportAbuseVC.gemDetails = gemDetails;
        [[self navigationController]pushViewController:reportAbuseVC animated:YES];
    }
    
}

#pragma mark - Custom Cell Delegate For Hide A Gem Method

-(void)hideAGemWithIndex:(NSInteger)index{
    
    if (index < arrGems.count) {
        
        NSDictionary *gemDetails = arrGems[index];
        NSString *gemID;
        NSString *gemType;
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
            gemID = [gemDetails objectForKey:@"gem_id"];
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
            gemType = [gemDetails objectForKey:@"gem_type"];
        
        [self updateGEMWithVisibilityStatus:index];
        
        if (gemID && gemType) {
            [APIMapper hideAGEMWith:[User sharedManager].userId gemID:gemID gemType:gemType success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
            }];
        }
        
    }
    
}

-(void)updateGEMWithVisibilityStatus:(NSInteger)index{
    
    if (index < arrGems.count)
        [arrGems removeObjectAtIndex:index];
    totalGems -= 1;
    [tableView reloadData];
}

-(void)updateGemViewCountWithIndex:(NSInteger)index andCount:(NSInteger)count;{
    
    if (index < arrGems.count) {
        NSDictionary *gemsInfo = arrGems[index];
        NSMutableDictionary *gemDetails = [NSMutableDictionary dictionaryWithDictionary:gemsInfo];
        [gemDetails setValue:[NSNumber numberWithInteger:count] forKey:@"view_count"];
        [arrGems replaceObjectAtIndex:index withObject:gemDetails];
    }
    [tableView reloadData];
    
}

#pragma mark - Custom Cell Delegate For Follow Method

-(void)followButtonClickedWith:(NSInteger)index{
    
    if (index < arrGems.count) {
        NSDictionary *details = arrGems[index];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            NSString *followerID = [details objectForKey:@"user_id"];
            [self showLoadingScreen];
            [APIMapper sendFollowRequestWithUserID:[User sharedManager].userId followerID:followerID success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [self hideLoadingScreen];
                if ([[responseObject objectForKey:@"code"] integerValue] == 401) {
                    UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Follow" message:[responseObject objectForKey:@"text"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* copy = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                        
                        [alert dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alert addAction:copy];
                    [self.navigationController presentViewController:alert animated:YES completion:nil];
                    [self updateGemsWithFollowStatusWithIndex:index latestFollowStatus:false followstatus:0];
                }else{
                    [self updateGemsWithFollowStatusWithIndex:index latestFollowStatus:true followstatus:[[responseObject objectForKey:@"follow_status"]integerValue]];
                }
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                [self hideLoadingScreen];
            }];
        }
    }
    
}

/*
-(void)updateGemsWithFollowStatusWithIndex:(NSInteger)index latestFollowStatus:(BOOL)latestFollowStatus{
    
    if (index < arrGems.count) {
        NSMutableDictionary *gemsInfo = [NSMutableDictionary dictionaryWithDictionary:arrGems[index]];
        if (NULL_TO_NIL([gemsInfo objectForKey:@"user_id"])) {
            NSString *followerID = [gemsInfo objectForKey:@"user_id"];
            if ([gemsInfo objectForKey:@"follow_status"]) {
                if (!latestFollowStatus) {
                    [gemsInfo setValue:[NSNumber numberWithBool:false] forKey:@"can_follow"];
                    [arrGems replaceObjectAtIndex:index withObject:gemsInfo];
                }else{
                    int status = (int)[[gemsInfo objectForKey:@"follow_status"] integerValue];
                    if (NULL_TO_NIL([dictFollowers objectForKey:followerID]))
                        status = (int) [[dictFollowers objectForKey:followerID] integerValue];
                    status += 1;
                    if (status + 1 > eFollowed)
                        status = eFollow;
                    [dictFollowers setObject:[NSNumber numberWithInteger:status] forKey:followerID];
                }
                
            }
        }
        [tableView reloadData];
    }
}
*/

-(void)updateGemsWithFollowStatusWithIndex:(NSInteger)index latestFollowStatus:(BOOL)latestFollowStatus followstatus:(NSInteger)status{
    
    if (index < arrGems.count) {
        NSMutableDictionary *gemsInfo = [NSMutableDictionary dictionaryWithDictionary:arrGems[index]];
        if (NULL_TO_NIL([gemsInfo objectForKey:@"user_id"])) {
            NSString *followerID = [gemsInfo objectForKey:@"user_id"];
            if ([gemsInfo objectForKey:@"follow_status"]) {
                if (!latestFollowStatus) {
                    [gemsInfo setValue:[NSNumber numberWithBool:false] forKey:@"can_follow"];
                    [arrGems replaceObjectAtIndex:index withObject:gemsInfo];
                }else{
                    [dictFollowers setObject:[NSNumber numberWithInteger:status] forKey:followerID];
                }
                
            }
        }
        [tableView reloadData];
    }
}


#pragma mark - Custom Cell Delegate For Like Method

-(void)likeGemsClicked:(NSInteger)index{
    
    /*! Like Button Action!*/
    
    if (index < arrGems.count) {
        
        NSDictionary *gemDetails = arrGems[index];
        NSString *gemID;
        NSString *gemType;
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
            gemID = [gemDetails objectForKey:@"gem_id"];
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
            gemType = [gemDetails objectForKey:@"gem_type"];
        
        [self updateGEMWithLikeStatus:index];
        
        if (gemID && gemType) {
            [APIMapper likeAGEMWith:[User sharedManager].userId gemID:gemID gemType:gemType success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
            }];
        }
        
        
    }
    
}

-(void)updateGEMWithLikeStatus:(NSInteger)index{
    
    if (index < arrGems.count) {
        NSDictionary *gemsInfo = arrGems[index];
        NSString *gemID;
        NSInteger count = 0;
        BOOL isAlreadyLiked = 0;
        
        if (NULL_TO_NIL([gemsInfo objectForKey:@"gem_id"]))
            gemID = [gemsInfo objectForKey:@"gem_id"];
        if (NULL_TO_NIL([gemsInfo objectForKey:@"likecount"]))
            count = [[gemsInfo objectForKey:@"likecount"] integerValue];
        if ([[gemsInfo objectForKey:@"like_status"] boolValue])
            isAlreadyLiked = [[gemsInfo objectForKey:@"like_status"] boolValue];
        NSMutableDictionary *gemDetails = [NSMutableDictionary dictionaryWithDictionary:gemsInfo];
        if (isAlreadyLiked) {
            count --;
            if (count < 0)count = 0;
            [gemDetails setValue:[NSNumber numberWithBool:0] forKey:@"like_status"];
        }
        else{
            count ++;
            [gemDetails setValue:[NSNumber numberWithBool:1] forKey:@"like_status"];
        }
        [gemDetails setValue:[NSNumber numberWithInteger:count] forKey:@"likecount"];
        [arrGems replaceObjectAtIndex:index withObject:gemDetails];
        
    }
    
    [tableView reloadData];
}


#pragma mark - Custom Cell Delegate For Favourite Method


-(void)favouriteButtonApplied:(NSInteger)index{
    
    /*! Favourite Button Action!*/
    
    if (index < arrGems.count) {
        
        NSDictionary *gemDetails = arrGems[index];
        NSString *gemID;
        NSString *gemType;
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
            gemID = [gemDetails objectForKey:@"gem_id"];
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
            gemType = [gemDetails objectForKey:@"gem_type"];
        
        [self updateGEMWithFavouriteStatus:index];
        
        if (gemID && gemType) {
            [APIMapper addGemToFavouritesWith:[User sharedManager].userId gemID:gemID gemType:gemType success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
            }];
        }
        
        
    }
    
}

-(void)updateGEMWithFavouriteStatus:(NSInteger)index{
    
    if (index < arrGems.count) {
        
        NSDictionary *gemsInfo = arrGems[index];
        NSString *gemID;
        BOOL isAlreadyLiked = 0;
        
        if (NULL_TO_NIL([gemsInfo objectForKey:@"gem_id"]))
            gemID = [gemsInfo objectForKey:@"gem_id"];
        if ([[gemsInfo objectForKey:@"favourite_status"] boolValue])
            isAlreadyLiked = [[gemsInfo objectForKey:@"favourite_status"] boolValue];
        NSMutableDictionary *gemDetails = [NSMutableDictionary dictionaryWithDictionary:gemsInfo];
        [gemDetails setValue:[NSNumber numberWithBool:1] forKey:@"favourite_status"];
        [arrGems replaceObjectAtIndex:index withObject:gemDetails];
        
    }
    
    [tableView reloadData];
}

#pragma mark - Custom Cell Delegate For Share Method

-(void)saveSelectedPostWithIndex:(NSInteger)index button:(UIButton*)button{
    
    if (index < arrGems.count) {
        
        NSDictionary *gemDetails = arrGems[index];
        NSString *gemID;
        NSString *gemType;
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_id"]))
            gemID = [gemDetails objectForKey:@"gem_id"];
        
        if (NULL_TO_NIL([gemDetails objectForKey:@"gem_type"]))
            gemType = [gemDetails objectForKey:@"gem_type"];
        
        CreateActionInfoViewController *detailPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCreateActionMedias];
        detailPage.actionType = eActionTypeSave;
        if ([gemDetails objectForKey:@"gem_type"]) {
            detailPage.strTitle =[[NSString stringWithFormat:@"SAVE AS %@",[gemDetails objectForKey:@"gem_type"]] uppercaseString] ;
            if ([[gemDetails objectForKey:@"gem_type"] isEqualToString:@"community"]) {
                detailPage.strTitle = @"SAVE POST";
            }
        }
        [detailPage getMediaDetailsForGemsToBeEditedWithGEMID:gemID GEMType:gemType];
        [[self navigationController]pushViewController:detailPage animated:YES];
    }
}

#pragma mark - Custom Cell Delegate For Social Share

-(void)shareGEMsToSocialMedia:(NSInteger)index{
    
    if (index < arrGems.count) {
        
        NSMutableString *strShareMsg = [NSMutableString new];
        NSDictionary *details = arrGems[index];
        if (NULL_TO_NIL([details objectForKey:@"share_url"])){
            [strShareMsg appendString:[details objectForKey:@"share_url"]];
        }
        NSMutableArray *sharingItems = [NSMutableArray new];
        if (strShareMsg) {
            NSURL *shareURL = [NSURL URLWithString:strShareMsg];
            [sharingItems addObject:shareURL];
        }
        UIActivityViewController* activityVC = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
        NSArray *excludeActivities = @[UIActivityTypePostToWeibo,UIActivityTypePrint,UIActivityTypeSaveToCameraRoll,UIActivityTypeAssignToContact,UIActivityTypeAirDrop];
        activityVC.excludedActivityTypes = excludeActivities;
        AppDelegate *deleagte = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [deleagte.window.rootViewController presentViewController:activityVC animated:TRUE completion:nil];
    }
    
}


#pragma mark - Custom Cell Delegate For Get Liked and Commented Users

-(void)showAllLikedUsers:(NSInteger)index{
    
    if (index < arrGems.count) {
        NSDictionary *gemDetails = arrGems[index];
        if ([[gemDetails objectForKey:@"likecount"]integerValue ] > 0) {
            LikedAndCommentedUserListings *userListings =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForLikedAndCommentedUsers];
             [userListings loadUserListingsForType:@"gem" selectedID:[gemDetails objectForKey:@"gem_id"]gemType:[gemDetails objectForKey:@"gem_type"]];
            [[self navigationController]pushViewController:userListings animated:YES];
        }
        
    }
    
}


-(void)showAllCommentedUsers:(NSInteger)index{
    
    
    if (index < arrGems.count) {
        NSDictionary *gemDetails = arrGems[index];
       [self commentComposeViewClickedBy:index];
    }
    
}




#pragma mark - Custom Cell Delegate For Generic Method


-(void)moreGalleryPageClicked:(NSInteger)index {
    
    
}

#pragma mark - Share Medias & Deleagtes


- (void)shareGEMToPublicWith:(NSString *)text items:(NSArray*)items
{
    if (items.count <= 0)return;
    if (!shareMediaView) {
        shareMediaView = [[[NSBundle mainBundle] loadNibNamed:@"shareMedias" owner:self options:nil] objectAtIndex:0];
        shareMediaView.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = shareMediaView;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [shareMediaView setUpWithShareItems:items text:text];
    }];
    
    
}
-(void)closeShareMediasView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        shareMediaView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [shareMediaView removeFromSuperview];
        shareMediaView = nil;
    }];
}
#pragma mark - Comment Showing and its Delegate

-(void)commentComposeViewClickedBy:(NSInteger)index{
    
   CommentComposeViewController *composeComment =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForCommentCompose];
    composeComment.delegate = self;
    if (index < arrGems.count) {
        NSDictionary *gemDetails = arrGems[index];
        composeComment.dictGemDetails = gemDetails;
        composeComment.selectedIndex = index;
    }
    composeComment.isFromCommunityGem = true;
    [self.navigationController pushViewController:composeComment animated:YES];
}

-(void)commentPostedSuccessfullyWithGemID:(NSString*)gemID commentCount:(NSInteger)count index:(NSInteger)index isAddComment:(BOOL)isAddComment{
    
    if (index < arrGems.count) {
        
        NSDictionary *gemsInfo = arrGems[index];
        NSMutableDictionary *gemDetails = [NSMutableDictionary dictionaryWithDictionary:gemsInfo];
        if (isAddComment) {
            [gemDetails setValue:[NSNumber numberWithInteger:count] forKey:@"comment_count"];
            [arrGems replaceObjectAtIndex:index withObject:gemDetails];
        }
        else{
            if (NULL_TO_NIL([gemDetails objectForKey:@"comment_count"])) {
                NSInteger count = [[gemDetails objectForKey:@"comment_count"] integerValue];
                count --;
                if (count  < 0)
                    count = 0;
                [gemDetails setValue:[NSNumber numberWithInteger:count] forKey:@"comment_count"];
                [arrGems replaceObjectAtIndex:index withObject:gemDetails];
            }
            
        }
    }
    
    [tableView reloadData];
    
}

#pragma mark - Media Listing Page Delegates Like / Share / Comment


/*!
 *This method is invoked when user Clicks "FAVOURITE" Button
 */
-(void)favouriteButtonAppliedFromMediaPage:(NSInteger)index{
    
    [self favouriteButtonApplied:index];
    
}


/*!
 *This method is invoked when user Clicks "LIKE" Button
 */
-(void)likeAppliedFromMediaPage:(NSInteger)index{
    
    [self likeGemsClicked:index];
    
}

/*!
 *This method is invoked when user Clicks "COMMENT" Button
 */
-(void)commentAppliedFromMediaPage:(NSInteger)index{
    
    [self commentComposeViewClickedBy:index];
}

/*!
 *This method is invoked when user Clicks "SHARE" Button
 */
-(void)shareAppliedFromMediaPage:(NSInteger)index{
    
    [self shareGEMsToSocialMedia:index];
}

#pragma mark - Image Grid and Mask Implementation

-(BOOL)shouldShowImageGrid:(NSArray*)grid cell:(GemsListTableViewCell*)cell{
    
    cell.vwGridContainer.hidden = false;
    cell.vwGridCountOverLay.hidden = true;
    if (grid.count <= 1) {
        cell.vwGridContainer.hidden = true;
        return false;
    }
    float containerWidth = self.view.frame.size.width - 10;
    float containerHeight = self.view.frame.size.width - 10;
    float padding = 2;
    
    cell.gridImage1Width.constant = 0;
    cell.gridImage2Width.constant = 0;
    cell.gridImage3Width.constant = 0;
    cell.gridImage4Width.constant = 0;
    
    if (grid.count == 2) {
        
        cell.gridImage1Top.constant = 0;
        cell.gridImage1Left.constant = 0;
        cell.gridImage1Width.constant = (containerWidth / 2) - padding;
        cell.gridImage1Height.constant = containerHeight;
        
        cell.gridImage2Top.constant = 0;
        cell.gridImage2Left.constant = containerWidth / 2;
        cell.gridImage2Width.constant = containerWidth / 2;
        cell.gridImage2Height.constant = containerHeight;
        
        
    }
    else if (grid.count == 3) {
        
        cell.gridImage1Top.constant = 0;
        cell.gridImage1Left.constant = 0;
        cell.gridImage1Width.constant = containerWidth;
        cell.gridImage1Height.constant = (containerHeight / 2) - padding;
        
        cell.gridImage2Top.constant = 0;
        cell.gridImage2Left.constant = containerWidth / 2;
        cell.gridImage2Width.constant = 0;
        cell.gridImage2Height.constant = (containerHeight / 2) - padding;
        
        cell.gridImage3Top.constant = padding;
        cell.gridImage3Left.constant = 0;
        cell.gridImage3Width.constant = (containerWidth / 2) - padding;
        cell.gridImage3Height.constant = containerHeight / 2;
        
        cell.gridImage4Top.constant = padding;
        cell.gridImage4Left.constant = padding;
        cell.gridImage4Width.constant = containerWidth / 2;
        cell.gridImage4Height.constant = containerHeight / 2;
        
        NSInteger index = 1;
        for (NSDictionary *dict in grid) {
            if ([cell.vwGridContainer viewWithTag:index]){
                UIImageView *imgVw = [cell.vwGridContainer viewWithTag:index];
                [self mediaKey:dict view:imgVw];
                
            }
            index ++;
            if (index == 2){
                index = 3;
            }
        }
        return true; // Tag problem, since image view with tag 2 needs to be neglected.
    }
    else if (grid.count >= 4) {
        
        cell.gridImage1Top.constant = 0;
        cell.gridImage1Left.constant = 0;
        cell.gridImage1Width.constant = (containerWidth / 2) - padding;
        cell.gridImage1Height.constant = (containerHeight / 2) - padding;
        
        cell.gridImage2Top.constant = 0;
        cell.gridImage2Left.constant = containerWidth / 2;
        cell.gridImage2Width.constant = containerWidth / 2;
        cell.gridImage2Height.constant = (containerHeight / 2) - padding;
        
        cell.gridImage3Top.constant = padding;
        cell.gridImage3Left.constant = 0;
        cell.gridImage3Width.constant = (containerWidth / 2) - padding ;
        cell.gridImage3Height.constant = containerHeight / 2;
        
        cell.gridImage4Top.constant = padding;
        cell.gridImage4Left.constant = padding;
        cell.gridImage4Width.constant = containerWidth / 2;
        cell.gridImage4Height.constant = containerHeight / 2;
        
        if (grid.count > 4){
            cell.vwGridCountOverLay.hidden = false;
            cell.lblMediaCount.hidden = false;
            cell.lblMediaCount.text = [NSString stringWithFormat:@"+%02lu",(unsigned long)(grid.count) - 4];
        }
    }
    
    NSInteger index = 1;
    for (NSDictionary *dict in grid) {
        if ([cell.vwGridContainer viewWithTag:index]){
            if ([cell.vwGridContainer viewWithTag:index]){
                UIImageView *imgVw = [cell.vwGridContainer viewWithTag:index];
                [self mediaKey:dict view:imgVw];
                
            }
            
        }
        index ++;
    }
    return true;
}

-(void)mediaKey:(NSDictionary*)dict view:(UIImageView*)imgView{
    
    NSString *imgName = @"";
    NSString *media_type  = [dict objectForKey:@"media_type"];
    NSString *property = [dict objectForKey:@"gem_media"];
    if ([media_type isEqualToString:@"video"]) {
        property = [dict objectForKey:@"video_thumb"];
    }
    else if ([media_type isEqualToString:@"audio"]) {
        property = [dict objectForKey:@"gem_media"];
        imgName = @"NoImage";
    }
    [self addMaskIfNeededOnView:imgView details:dict];
    [self setMediaIconIfNeededOnView:imgView andType:media_type];
    [self showLoadingIndicatorOn:imgView];
    [imgView sd_setImageWithURL:[NSURL URLWithString:property]
               placeholderImage:[UIImage imageNamed:imgName]
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                          [self hideLoadingIndicator:imgView];
                      }];
    
}

-(void)setMediaIconIfNeededOnView:(UIImageView*)imgView andType:(NSString*)type{
    UIImageView *imgIcon;
    if ([imgView viewWithTag:100]) {
        imgIcon = [imgView viewWithTag:100];
    }else{
        UIImageView *mediaIcon = [UIImageView new];
        [imgView addSubview:mediaIcon];
        mediaIcon.tag = 100;
        mediaIcon.translatesAutoresizingMaskIntoConstraints = NO;
        [mediaIcon setImage:[UIImage imageNamed:@"NoImage"]];
        [imgView addConstraint:[NSLayoutConstraint constraintWithItem:mediaIcon
                                                            attribute:NSLayoutAttributeCenterX
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:imgView
                                                            attribute:NSLayoutAttributeCenterX
                                                           multiplier:1.0
                                                             constant:0]];
        [imgView addConstraint:[NSLayoutConstraint constraintWithItem:mediaIcon
                                                            attribute:NSLayoutAttributeCenterY
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:imgView
                                                            attribute:NSLayoutAttributeCenterY
                                                           multiplier:1.0
                                                             constant:0]];
        
        
        [mediaIcon addConstraint:[NSLayoutConstraint constraintWithItem:mediaIcon
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeWidth
                                                             multiplier:1.0
                                                               constant:50]];
        
        [mediaIcon addConstraint:[NSLayoutConstraint constraintWithItem:mediaIcon
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1.0
                                                               constant:50]];
        imgIcon = mediaIcon;
    }
    
    if ([type isEqualToString:@"image"]) {
        imgIcon.hidden = true;
    }
    else if ([type isEqualToString:@"audio"]) {
        imgIcon.hidden = false;
        [imgIcon setImage:[UIImage imageNamed:@"Audio_Play_Button"]];
    }
    else if ([type isEqualToString:@"video"]) {
        imgIcon.hidden = false;
        [imgIcon setImage:[UIImage imageNamed:@"Video_Perview"]];
    }
    
}


-(void)addMaskIfNeededOnView:(UIImageView*)imgView details:(NSDictionary*)details{
    
    NSInteger safeSearchValue = [[details objectForKey:@"safe_search_status"] integerValue];
    UIImageView *vwMask;
    if ([imgView viewWithTag:101]) {
        vwMask = [imgView viewWithTag:101];
        vwMask.hidden = true;
    }
    
    if (safeSearchValue == 1) {
        if (!vwMask) {
            UIImageView *mask = [UIImageView new];
            [imgView addSubview:mask];
            mask.tag = 101;
            mask.translatesAutoresizingMaskIntoConstraints = NO;
            [mask setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
            [imgView addConstraint:[NSLayoutConstraint constraintWithItem:mask
                                                                attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:imgView
                                                                attribute:NSLayoutAttributeLeft
                                                               multiplier:1.0
                                                                 constant:0]];
            [imgView addConstraint:[NSLayoutConstraint constraintWithItem:mask
                                                                attribute:NSLayoutAttributeRight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:imgView
                                                                attribute:NSLayoutAttributeRight
                                                               multiplier:1.0
                                                                 constant:0]];
            [imgView addConstraint:[NSLayoutConstraint constraintWithItem:mask
                                                                attribute:NSLayoutAttributeBottom
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:imgView
                                                                attribute:NSLayoutAttributeBottom
                                                               multiplier:1.0
                                                                 constant:0]];
            [imgView addConstraint:[NSLayoutConstraint constraintWithItem:mask
                                                                attribute:NSLayoutAttributeTop
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:imgView
                                                                attribute:NSLayoutAttributeTop
                                                               multiplier:1.0
                                                                 constant:0]];
            vwMask = mask;
        }
        vwMask.hidden = false;
    }
    else{
        vwMask.hidden = true;
        
    }
}

-(void)showLoadingIndicatorOn:(UIImageView*)imgView{
    
    UIActivityIndicatorView *activityIndicator;
    if ([imgView viewWithTag:102]) {
        activityIndicator = [imgView viewWithTag:102];
    }else{
        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        [imgView addSubview:activityIndicator];
        [imgView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator
                                                            attribute:NSLayoutAttributeCenterX
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:imgView
                                                            attribute:NSLayoutAttributeCenterX
                                                           multiplier:1.0
                                                             constant:0]];
        [imgView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator
                                                            attribute:NSLayoutAttributeCenterY
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:imgView
                                                            attribute:NSLayoutAttributeCenterY
                                                           multiplier:1.0
                                                             constant:0]];
    }
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.tag = 102;
    
}

-(void)hideLoadingIndicator:(UIImageView*)imgView{
    
    UIActivityIndicatorView *activityIndicator;
    if ([imgView viewWithTag:102]) {
        activityIndicator = [imgView viewWithTag:102];
        [activityIndicator stopAnimating];
    }
}



#pragma mark - Generic Methods

-(void)undUpdateReachCount{
    
    NSArray *paths = [tableView indexPathsForVisibleRows];
    
    //  For getting the cells themselves
    
    for (NSIndexPath *path in paths) {
        NSString *gemID;
        NSString *gemType;
        if (path.section > 0 && path.row < arrGems.count) {
            NSDictionary *_gemDetails = arrGems[path.row];
            if ([[_gemDetails objectForKey:@"view_status"] integerValue] <= 0){
                
                if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_id"]))
                    gemID = [_gemDetails objectForKey:@"gem_id"];
                
                if (NULL_TO_NIL([_gemDetails objectForKey:@"gem_type"]))
                    gemType = [_gemDetails objectForKey:@"gem_type"];
                
                if (gemType && gemID) {
                    
                    [APIMapper updatePostDetailPageWithGemID:gemID gemType:gemType onSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                        
                        if (responseObject && [[responseObject objectForKey:@"code"] integerValue] == 200) {
                            if ([self respondsToSelector:@selector(updateGemViewCountWithIndex:andCount:)]) {
                                [self updateGemViewCountWithIndex:path.row andCount:[[responseObject objectForKey:@"count"] integerValue]];
                            }
                        }
                        
                    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                        
                    }];
                }
            }
            
        }
    }
}



-(void)configureMomentDetailsWith:(NSDictionary*)details cell:(GemsListTableViewCell*)cell indexPath:(NSIndexPath*)indexPath{
    
    NSInteger emotionValue = [[details objectForKey:@"emotion_value"] integerValue] + 3;
    NSInteger driveValue = [[details objectForKey:@"drive_value"] integerValue] + 3;
    
    NSMutableAttributedString *myString = [NSMutableAttributedString new];
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    UIImage *icon = [UIImage imageNamed:[NSString stringWithFormat:@"%ld_Star_Small",(long)emotionValue]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblFeel.font.descender + 2), icon.size.width, icon.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    [myString appendAttributedString:attachmentString];
    NSMutableAttributedString *strEmotion = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" Feeling %@",[details objectForKey:@"emotion_title"]]];
    [strEmotion addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:CommonFontBold_New size:14]
                       range:NSMakeRange(8, strEmotion.length - 8)];
    [strEmotion addAttribute:NSForegroundColorAttributeName
                       value:[UIColor blackColor]
                       range:NSMakeRange(8, strEmotion.length - 8)];
    [myString appendAttributedString:strEmotion];
    cell.lblFeel.attributedText = myString;
    
    attachment = [[NSTextAttachment alloc] init];
    myString = [NSMutableAttributedString new];
    icon = [UIImage imageNamed:[NSString stringWithFormat:@"%ld_Star_Small",(long)driveValue]];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  cell.lblDrive.font.descender + 2), icon.size.width, icon.size.height);
    attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    [myString appendAttributedString:attachmentString];
    
    if ([details objectForKey:@"goal_title"]) {
        NSMutableAttributedString *goalTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" Goal %@",[details objectForKey:@"goal_title"]]];
        [goalTitle addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:CommonFontBold_New size:14]
                          range:NSMakeRange(6, goalTitle.length - 6)];
        [goalTitle addAttribute:NSForegroundColorAttributeName
                          value:[UIColor blackColor]
                          range:NSMakeRange(6, goalTitle.length - 6)];
        [myString appendAttributedString:goalTitle];
    }
    if (NULL_TO_NIL([details objectForKey:@"action_title"])) {
        NSString *Actions = [details objectForKey:@"action_title"];
        NSArray *count = [Actions componentsSeparatedByString:@","];
        NSMutableAttributedString *goalTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" & %lu Action(s)",(unsigned long)count.count]];
        
        [goalTitle addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:CommonFontBold_New size:14]
                          range:NSMakeRange(3, goalTitle.length - 3)];
        [goalTitle addAttribute:NSForegroundColorAttributeName
                          value:[UIColor blackColor]
                          range:NSMakeRange(3, goalTitle.length - 3)];
        
        [myString appendAttributedString:goalTitle];
    }
    
    cell.lblDrive.attributedText = myString;
    
}


-(IBAction)hashTagClickedWithTag:(NSString*)tag{
    
    HashTagGemListingViewController *hashListing =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForHashTagListings];
    hashListing.strTagName = tag;
    [[self navigationController]pushViewController:hashListing animated:YES];
    
}


-(IBAction)showUserProfilePage:(UITapGestureRecognizer*)gesture{
    
    NSInteger tag = gesture.view.tag;
    if (tag < arrGems.count) {
        NSDictionary *details = arrGems[tag];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
            [[self navigationController]pushViewController:profilePage animated:YES];
            profilePage.canEdit = false;
            if ([[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
                profilePage.canEdit = true;
            }
            [profilePage loadUserProfileWithUserID:[details objectForKey:@"user_id"]showBackButton:YES];
            
        }
    }
}


-(NSString*)getCountInTermsOfThousand:(NSInteger)_count{
    NSString *countText;
    NSInteger count = _count / OneK;
    NSInteger reminder = _count % OneK;
    countText = [NSString stringWithFormat:@"%ldK",(long)count];
    if (reminder > 0) {
        countText = [NSString stringWithFormat:@"%ldK+",(long)count];
    }
    return countText;
}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)showPaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)hidePaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = -40;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}




-(IBAction)goBack:(id)sender{
    
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    tableView = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}








/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
