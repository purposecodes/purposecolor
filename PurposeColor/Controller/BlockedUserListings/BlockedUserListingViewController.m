//
//  MenuViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//


#define kTagForTitle            1
#define kEmptyHeaderAndFooter   001
#define kSuccessCode            200


#import "BlockedUserListingViewController.h"
#import "Constants.h"
#import "ProfilePageViewController.h"
#import "BlockedUserListTableViewCell.h"

@interface BlockedUserListingViewController (){
    
    IBOutlet UITableView *tableView;
    NSMutableArray *arrList;
    BOOL isDataAvailable;
    NSString *strNoDataText;
}

@end

@implementation BlockedUserListingViewController{
    
    
    
}

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self setUp];
    [self loadUserList];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 100;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}

-(void)loadUserList{
    
    [self showLoadingScreen];
    
    [APIMapper loadBlockedUserListOnsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self parseResponds:responseObject];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        if (error && error.localizedDescription) [ALToastView toastInView:self.view withText:error.localizedDescription];
        [self hideLoadingScreen];
        
    }];

        
}


-(void)parseResponds:(NSDictionary*)details{
    
    if ([[details objectForKey:@"code"] integerValue] == kSuccessCode) {
        arrList = [NSMutableArray arrayWithArray:[details objectForKey:@"resultarray"]];
        if (arrList.count) isDataAvailable = true;
    }else{
        isDataAvailable = false;
        strNoDataText = @"No Records found";
        if ([details objectForKey:@"text"]) strNoDataText = [details objectForKey:@"text"];
    }
    [tableView reloadData];
  
}

-(void)refreshData{
    
     [arrList removeAllObjects];
     [self loadUserList];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) {
        return 1;
    }
    return arrList.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) {
        UITableViewCell *cell = [Utility getNoDataCustomCellWith:aTableView withTitle:strNoDataText];
        return cell;
    }
    static NSString *CellIdentifier = @"BlockedUserListTableViewCell";
    BlockedUserListTableViewCell *cell = (BlockedUserListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   
    [self configureCellWithCell:cell indexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Show profile page when clciked
    
    NSInteger tag = indexPath.row;
    if (tag < arrList.count) {
        NSDictionary *details = arrList[tag];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            if (!app.navGeneral) {
                app.navGeneral = [[UINavigationController alloc] initWithRootViewController:profilePage];
                app.navGeneral.navigationBarHidden = true;
                [UIView transitionWithView:app.window
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{  app.window.rootViewController = app.navGeneral; }
                                completion:nil];
            }else{
                [app.navGeneral pushViewController:profilePage animated:YES];
            }
            profilePage.canEdit = false;
            if ([[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
                profilePage.canEdit = true;
            }
            [profilePage loadUserProfileWithUserID:[details objectForKey:@"user_id"] showBackButton:YES];
            
        }
    }

    
}

-(void)configureCellWithCell:(BlockedUserListTableViewCell*)cell indexPath:(NSIndexPath*)indexPath{
    
    if (indexPath.row < arrList.count) {
        NSDictionary *details = arrList[[indexPath row]];
        
        cell.btnUnFolow.tag = indexPath.row;
        NSInteger verified = 0;
        cell.imgVerified.hidden = true;
        if (NULL_TO_NIL([details objectForKey:@"verify_status"])){
            verified = [[details objectForKey:@"verify_status"] integerValue];
            if (verified == 1) {
                cell.imgVerified.hidden = false;
            }
        }
        cell.lblName.text = [details objectForKey:@"firstname"];
        if (NULL_TO_NIL ([details objectForKey:@"profileimg"]) && [[details objectForKey:@"profileimg"] length]){
            [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[details objectForKey:@"profileimg"]]
                               placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      }];
        }
       
    }
        

}

-(IBAction)unBlockApplied:(UIButton*)sender{
    
    // Unblock a blocked user
    
    NSInteger index = sender.tag;
    if (index < arrList.count) {
        
        NSDictionary *gemDetails = arrList[index];
        [self showLoadingScreen];
        [APIMapper blockUserWith:[gemDetails objectForKey:@"user_id"] type:@"unblock" Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject objectForKey:@"text"]) [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
            [self hideLoadingScreen];
            [self refreshData];
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            if (error && error.localizedDescription) {
                [ALToastView toastInView:self.view withText:error.localizedDescription];
            }
            [self hideLoadingScreen];
        }];
    }
    
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(IBAction)goBack:(id)sender{
    
  [[self navigationController] popViewControllerAnimated:YES];
       
}


@end
