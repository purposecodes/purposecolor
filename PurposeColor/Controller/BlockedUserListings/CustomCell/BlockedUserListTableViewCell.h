//
//  UserListTableViewCell.h
//  PurposeColor
//
//  Created by Purpose Code on 14/06/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockedUserListTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet  UIImageView *imgVerified;
@property (nonatomic,weak) IBOutlet  UIImageView *imgProfile;
@property (nonatomic,weak) IBOutlet  UILabel *lblName;
@property (nonatomic,weak) IBOutlet  UIButton *btnUnFolow;
@property (nonatomic, weak)IBOutlet UIView *vwBg;

@end
