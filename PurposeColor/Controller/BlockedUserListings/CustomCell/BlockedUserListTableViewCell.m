//
//  UserListTableViewCell.m
//  PurposeColor
//
//  Created by Purpose Code on 14/06/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import "BlockedUserListTableViewCell.h"

@implementation BlockedUserListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _imgProfile.layer.borderColor = [UIColor colorWithRed:0.83 green:0.83 blue:0.83 alpha:1.0].CGColor;
    _imgProfile.layer.borderWidth = 1.f;
    _imgProfile.layer.cornerRadius = 25.f;
    
    _btnUnFolow.layer.borderWidth = 1.f;
    _btnUnFolow.layer.borderColor = [UIColor getThemeColor].CGColor;
    _btnUnFolow.layer.cornerRadius = 3.f;
    
    [_vwBg.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwBg.layer setBorderWidth:1.f];
    // drop shadow
    [_vwBg.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwBg.layer setShadowOpacity:0.3];
    [_vwBg.layer setShadowRadius:2.0];
    [_vwBg.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
