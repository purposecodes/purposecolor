//
//  MenuViewController.h
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ProfilePageViewController : UIViewController


@property (nonatomic,assign) BOOL canEdit;
@property (nonatomic,assign) BOOL shouldOpenInterestPage;

-(void)loadUserProfileWithUserID:(NSString*)userID showBackButton:(BOOL)showBckButton;
-(void)refreshData;
-(IBAction)showGoalCategories;

@end
