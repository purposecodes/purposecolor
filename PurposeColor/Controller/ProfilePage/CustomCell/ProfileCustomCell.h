//
//  UserListTableViewCell.h
//  PurposeColor
//
//  Created by Purpose Code on 14/06/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCustomCell : UITableViewCell

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *trailingToBtn;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *trailingToSuprVw;

@property (nonatomic,weak) IBOutlet  UIButton *btnVerify;

@end
