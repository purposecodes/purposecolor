//
//  MenuViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

typedef enum{
    
    eFieldName = 0,
    eFieldMemberShip = 1,
    eFieldContatcts = 2,
    eFieldEmail = 3,
    eFieldDOB = 4,
    eFieldStatus = 5,
    eFieldChat = 6,
    eFieldInterests = 7,
    eFieldFollowers = 8,
    
}EField;

typedef enum{
    
    eFollow = 0,
    eFollowPending = 1,
    eFollowed = 2,
    
} eFollowStat;

typedef enum{
    
    eToBeVerified = 0,
    eVerified = 1,
    ePending = 2,
    
} eVerification;


#define kTagForTitle            1
#define kCellHeight             50
#define kCellHeightForStatus    100
#define kHeightForHeader        200
#define kEmptyHeaderAndFooter   001
#define kSuccessCode            200


#import "ProfilePageViewController.h"
#import "Constants.h"
#import "MenuViewController.h"
#import "ChangePasswordPopUp.h"
#import "PhotoBrowser.h"
#import "ChatUserListingViewController.h"
#import "FollowersListingViewController.h"
#import "ProfileCustomCell.h"
#import "VerifyUserPopUp.h"
#import "ChatComposeViewController.h"
#import "GoalCategoryViewController.h"

@interface ProfilePageViewController () <UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,SWRevealViewControllerDelegate,ChangePasswordPopUpDelegate,PhotoBrowserDelegate,VerifyUserProfileDelegate,GoalCategoryDelegte>{
    
    UIRefreshControl *refreshControl;
    BOOL isDataAvailable;
    BOOL isPageRefresing;
    BOOL canChat;
    
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnSubmit;
    IBOutlet UIButton *btnFollow;
    IBOutlet UIButton *btnMore;
    IBOutlet UIButton *btnDoneDate;
    IBOutlet UIView *vwOverLay;
    IBOutlet UIView *vwBG;
    
    NSString *strEmail;
    NSString *strName;
    NSString *profileURL;
    double  regDate;
    NSString *strStatusMsg;
    NSInteger verifiedStatus;
    NSInteger indexForTextFieldNavigation;
    UIView *inputAccView;
    BOOL canFollow;
    BOOL _isEditing;
    NSInteger followCount;
    NSInteger followingCount;
    NSString *strNoDataText;
    NSString *strDOB;
    eVerification verifictionStatus;
    NSMutableDictionary *userInfo;
    
    IBOutlet UIView *vwPickerOverLay;
    IBOutlet UIDatePicker *datePicker;
    
    UIImage *imgProfilePic;
    ChangePasswordPopUp *changePwdPopUp;
    PhotoBrowser *photoBrowser;
    NSString *strUserID;
}

@end

@implementation ProfilePageViewController{
    
}



-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self setUp];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    
    btnFollow.layer.cornerRadius = 5.f;
    btnFollow.layer.borderWidth = 1.f;
    btnFollow.layer.borderColor = [UIColor whiteColor].CGColor;
    btnFollow.hidden = true;
    
    btnSubmit.layer.cornerRadius = 5.f;
    btnSubmit.layer.borderWidth = 1.f;
    btnSubmit.layer.borderColor = [UIColor whiteColor].CGColor;
    btnSubmit.hidden = true;
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIColor *topColor = [UIColor colorWithRed:0.02 green:0.47 blue:0.78 alpha:1.0];
    UIColor * bottomColor = [UIColor colorWithRed:0.03 green:0.53 blue:0.88 alpha:1.0];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    gradient.colors = [NSArray arrayWithObjects:(id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    gradient.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:0.7], nil];
    [vwBG.layer addSublayer:gradient];
    [vwBG.layer insertSublayer:gradient atIndex:0];
    
    [btnDoneDate setBackgroundColor:[UIColor getThemeColor]];
    btnDoneDate.layer.cornerRadius = 5.f;
    btnDoneDate.layer.borderWidth = 1.f;
    btnDoneDate.layer.borderColor = [UIColor whiteColor].CGColor;
    vwPickerOverLay.alpha = 0;
    
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 50;
    
}

-(void)refreshData{
    
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    isPageRefresing = YES;
    [self loadUserProfileWithUserID:strUserID showBackButton:YES];
    
    
}

-(void)loadUserProfileWithUserID:(NSString*)userID showBackButton:(BOOL)showBckButton{
    
    strUserID = userID;
    [self showLoadingScreen];
    [APIMapper getUserProfileWithUserID:userID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        isDataAvailable = TRUE;
        [refreshControl endRefreshing];
         isPageRefresing = NO;
        [self parseResponds:responseObject];
        if (self.shouldOpenInterestPage) {
            [self showGoalCategories];
            self.shouldOpenInterestPage = false;
        }
            
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
         [self hideLoadingScreen];
        strNoDataText = error.localizedDescription;
        isDataAvailable = FALSE;
        [refreshControl endRefreshing];
         isPageRefresing = NO;
        [tableView reloadData];
        
    }];
    
}

-(void)parseResponds:(NSDictionary*)details{
    
    userInfo = [NSMutableDictionary dictionaryWithDictionary:details];
    if ([[details objectForKey:@"code"] integerValue] == kSuccessCode) {
        
        if (NULL_TO_NIL([details objectForKey:@"email"])) {
            strEmail = [details objectForKey:@"email"];
        }
        if (NULL_TO_NIL([details objectForKey:@"firstname"])) {
            strName = [details objectForKey:@"firstname"];
        }
        if (NULL_TO_NIL([details objectForKey:@"regdate"])) {
            regDate = [[details objectForKey:@"regdate"] doubleValue];
        }
        if (NULL_TO_NIL([details objectForKey:@"status"])) {
            strStatusMsg = [details objectForKey:@"status"];
        }
        if (NULL_TO_NIL([details objectForKey:@"dob"])) {
            strDOB = [details objectForKey:@"dob"];
        }
        if (NULL_TO_NIL([details objectForKey:@"profileurl"])) {
            profileURL = [details objectForKey:@"profileurl"];
        }
        if (NULL_TO_NIL([details objectForKey:@"can_follow"])) {
            canFollow = [[details objectForKey:@"can_follow"] boolValue];
        }
        if ([details objectForKey:@"verify_status"]) {
            verifictionStatus = [[details objectForKey:@"verify_status"] integerValue];
        }
        if (NULL_TO_NIL([details objectForKey:@"follow_count"])) {
            followCount = [[details objectForKey:@"follow_count"] integerValue];
        }
        if (NULL_TO_NIL([details objectForKey:@"following_count"])) {
            followingCount = [[details objectForKey:@"following_count"] integerValue];
        }
        if ([details objectForKey:@"can_chat"]) {
            NSInteger chatStatus = [[details objectForKey:@"can_chat"] integerValue];
            if (chatStatus == 2) {
                canChat = true;
            }
        }
        if (canFollow && !_canEdit) [self updateFollowButtonWithStatus:[[details objectForKey:@"follow_status"] integerValue] shouldRefresh:NO];
        if (_canEdit) canChat = false;
        
        
    }
    [tableView reloadData];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) {
        return 1;
    }
    return 9;
    
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (!isDataAvailable) {
        static NSString *MyIdentifier = @"MyIdentifier";
        UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
        cell.textLabel.text = strNoDataText;
        cell.textLabel.font = [UIFont fontWithName:CommonFont_New size:17];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor whiteColor];;
        cell.textLabel.numberOfLines = 0;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    if (indexPath.row == eFieldName) {
        cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:@"BasicInfo"];
       
    }
    else if (indexPath.row == eFieldMemberShip) {
        cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:@"BasicInfo"];
        
        if ([cell.contentView viewWithTag:3]) {
            UIView *vwVeirfied = [cell.contentView viewWithTag:3];
            vwVeirfied.hidden = true;
        }

    }
    else if (indexPath.row == eFieldContatcts) {
       ProfileCustomCell *_cell = (ProfileCustomCell *)[aTableView dequeueReusableCellWithIdentifier:@"ContactInfo"];
        _cell.trailingToBtn.priority = 999;
        _cell.trailingToSuprVw.priority = 998;
        if (verifictionStatus == eVerified) {
            _cell.btnVerify.hidden = true;
            _cell.trailingToBtn.priority = 998;
            _cell.trailingToSuprVw.priority = 999;
        }
        cell = _cell;
    }
    
    else if (indexPath.row == eFieldEmail) {
        cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:@"BasicInfo"];
        
        if ([cell.contentView viewWithTag:2]) {
            UIButton *btnPwdChange = (UIButton*)[cell.contentView viewWithTag:2];
            btnPwdChange.hidden = true;
            if (_canEdit) {
                btnPwdChange.hidden = false;
            }
        }
        if ([cell.contentView viewWithTag:3]) {
            UIView *vwVeirfied = [cell.contentView viewWithTag:3];
            vwVeirfied.hidden = true;
            
        }
        
    }
    else if (indexPath.row == eFieldDOB) {
        cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:@"DOB"];
        
    }
    else if (indexPath.row == eFieldStatus) {
        cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:@"StatusInfo"];
    }
    else if (indexPath.row == eFieldChat) {
        cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:@"Chat"];
        
    }
    else if (indexPath.row == eFieldInterests) {
        cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:@"Interests"];
    }
    
    else if (indexPath.row == eFieldFollowers) {
        cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:@"Followers"];
    }
    
    [self configureCellWithCell:cell indexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];

   
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == eFieldName) {
        return kCellHeight;
    }
    else if (indexPath.row == eFieldEmail) {
        if (_canEdit) {
            return 50;
        }
        return 0;
    }
    else if (indexPath.row == eFieldDOB) {
        if (_canEdit) return 50;
        return 0;
    }
    else if (indexPath.row == eFieldContatcts) {
        if (_canEdit) {
            return 120;
        }
        return 0;
    }
    else if (indexPath.row == eFieldChat) {
        if (!canChat) {
            return 0;
        }
        return 80;
    }
    else if (indexPath.row == eFieldMemberShip) {
        return 40;
    }
    else if (indexPath.row == eFieldFollowers) {
        return 110;
    }
    else if (indexPath.row == eFieldInterests) {
        if (_canEdit) {
            return 60;
        }
        return 0;
    }
    else if (indexPath.row == eFieldStatus) {
        if (_isEditing) {
            return kCellHeightForStatus;
        }
        return UITableViewAutomaticDimension;
    }
    return kCellHeight;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor clearColor];
    //User Profile Pic
    
    
    UIImageView *imgDisplay = [UIImageView new];
    [vwHeader addSubview:imgDisplay];
    imgDisplay.translatesAutoresizingMaskIntoConstraints = NO;
    imgDisplay.clipsToBounds = YES;
    imgDisplay.layer.cornerRadius = 65.f;
    imgDisplay.layer.borderWidth = 7.f;
    imgDisplay.backgroundColor = [UIColor blackColor];
    imgDisplay.layer.borderColor = [UIColor whiteColor].CGColor;
    imgDisplay.userInteractionEnabled = true;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserPhotoGallery)];
    [imgDisplay addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
    imgDisplay.userInteractionEnabled = true;
    imgDisplay.contentMode = UIViewContentModeScaleAspectFill;
    [imgDisplay addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                           attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeWidth
                                                          multiplier:1.0
                                                            constant:130.0]];
    [imgDisplay addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeHeight
                                                          multiplier:1.0
                                                            constant:130.0]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:-30]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0.0]];
    
       
    if (profileURL.length) {
        
        if (imgProfilePic) {
            [imgDisplay setImage:imgProfilePic];
        }else{
            [imgDisplay sd_setImageWithURL:[NSURL URLWithString:profileURL]
                          placeholderImage:[UIImage imageNamed:@"UserProfilePic.png"]
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     
                                 }];
        }
        
    }
    
    UIButton* btnCamera = [UIButton new];
    btnCamera.backgroundColor = [UIColor clearColor];
    btnCamera.translatesAutoresizingMaskIntoConstraints = NO;
    [btnCamera setImage:[UIImage imageNamed:@"Camera_Button.png"] forState:UIControlStateNormal];
    [btnCamera addTarget:self action:@selector(showCamera:) forControlEvents:UIControlEventTouchUpInside];
    [vwHeader addSubview:btnCamera];
    
    [btnCamera addConstraint:[NSLayoutConstraint constraintWithItem:btnCamera
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.0
                                                          constant:50]];
    [btnCamera addConstraint:[NSLayoutConstraint constraintWithItem:btnCamera
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0
                                                           constant:50]];
    
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnCamera
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:imgDisplay
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:-10]];
    
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnCamera
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0]];
    if (!_isEditing) btnCamera.hidden = true;
    return vwHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}

- (CGFloat)tableView:(UITableView *)_tableView heightForFooterInSection:(NSInteger)section{
    
    return kEmptyHeaderAndFooter;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == eFieldInterests) {
        
    }
}

-(void)configureCellWithCell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath{
    
    switch (indexPath.row) {
        case eFieldName:
            if ([[[cell contentView]viewWithTag:1] isKindOfClass:[UITextField class]]) {
                UITextField *txtField = [[cell contentView]viewWithTag:1];
                txtField.font = [UIFont fontWithName:CommonFont_New size:25];
                txtField.textAlignment = NSTextAlignmentCenter;
                txtField.text = strName;
                [txtField setBorderStyle:UITextBorderStyleNone];
                txtField.layer.borderColor = [UIColor clearColor].CGColor;
                txtField.backgroundColor = [UIColor clearColor];
                txtField.delegate = self;
                txtField.userInteractionEnabled = false;
                txtField.hidden = true;
                [txtField setTintColor:[UIColor whiteColor]];

                if (verifictionStatus != eVerified) txtField.hidden = false;
                
                if (_isEditing) {
                    txtField.layer.borderWidth = 1.f;
                    txtField.layer.borderColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.5].CGColor;
                    txtField.userInteractionEnabled = true;
                    txtField.hidden = false;
                }
                
                if ([cell.contentView viewWithTag:3]) {
                    UIView *vwVeirfied = [cell.contentView viewWithTag:3];
                    vwVeirfied.hidden = false;
                    
                    if (_isEditing || (verifictionStatus != eVerified))  vwVeirfied.hidden = true;
                    if ([vwVeirfied viewWithTag:4]) {
                        UILabel *name = (UILabel*)[vwVeirfied viewWithTag:4];
                        name.text = strName;
                    }
                    
                }
            }
             break;
        case eFieldEmail:
            if ([[[cell contentView]viewWithTag:1] isKindOfClass:[UITextField class]]) {
                UITextField *txtField = [[cell contentView]viewWithTag:1];
                txtField.font = [UIFont fontWithName:CommonFont size:18];
                txtField.textAlignment = NSTextAlignmentCenter;
                txtField.text = @"";
                if (_canEdit) txtField.text = strEmail;
                txtField.hidden = false;
                txtField.delegate = nil;
                txtField.userInteractionEnabled = false;
                [txtField setBorderStyle:UITextBorderStyleNone];
                [txtField setTintColor:[UIColor whiteColor]];
            }
            break;
        case eFieldDOB:
            if ([[[cell contentView]viewWithTag:eFieldDOB] isKindOfClass:[UITextField class]]) {
                UITextField *txtField = [[cell contentView]viewWithTag:eFieldDOB];
                txtField.font = [UIFont fontWithName:CommonFont_New size:17];
                txtField.textAlignment = NSTextAlignmentCenter;
                if (!strDOB.length) txtField.text = @"Date Of Birth";
                else txtField.text = strDOB;
                [txtField setBorderStyle:UITextBorderStyleNone];
                txtField.layer.borderColor = [UIColor clearColor].CGColor;
                txtField.backgroundColor = [UIColor clearColor];
                txtField.delegate = self;
                txtField.userInteractionEnabled = false;
                txtField.tag = eFieldDOB;
                [txtField setTintColor:[UIColor whiteColor]];
                if (_isEditing) {
                    txtField.layer.borderWidth = 1.f;
                    txtField.layer.borderColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.5].CGColor;
                    txtField.userInteractionEnabled = true;
                }
            }
            break;
        case eFieldMemberShip:
            if ([[[cell contentView]viewWithTag:1] isKindOfClass:[UITextField class]]){
                UITextField *txtField = [[cell contentView]viewWithTag:1];
                txtField.text = [NSString stringWithFormat:@"Member since %@",[self getDaysBetweenTwoDatesWith:regDate]];
                txtField.font = [UIFont fontWithName:CommonFont size:15];
                txtField.textAlignment = NSTextAlignmentCenter;
                txtField.delegate = nil;
                txtField.hidden = false;
                txtField.userInteractionEnabled = false;
                [txtField setBorderStyle:UITextBorderStyleNone];
                [txtField setTintColor:[UIColor whiteColor]];
            }
            break;
        case eFieldStatus:
            if ([[[cell contentView]viewWithTag:2] isKindOfClass:[UILabel class]]){
                UILabel *lblMsg = [[cell contentView]viewWithTag:2];
                lblMsg.text = strStatusMsg;
                lblMsg.hidden = false;
                if (_isEditing) lblMsg.hidden = true;
            }
            if ([[[cell contentView]viewWithTag:1] isKindOfClass:[UITextView class]]){
                UITextView *txtField = [[cell contentView]viewWithTag:1];
                txtField.text = strStatusMsg;
                txtField.hidden = true;
                if (_isEditing) {
                    txtField.font = [UIFont fontWithName:CommonFont size:17];
                    txtField.textAlignment = NSTextAlignmentCenter;
                    txtField.layer.borderWidth = 0.f;
                    txtField.hidden = false;
                    txtField.backgroundColor = [UIColor clearColor];
                    txtField.layer.borderWidth = 1.f;
                    txtField.layer.borderColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.5].CGColor;
                    [txtField setTintColor:[UIColor whiteColor]];
                }
            }
          break;
        case eFieldInterests:
            if ([[[cell contentView]viewWithTag:1] isKindOfClass:[UIButton class]]){
                UIButton *btn = [[cell contentView]viewWithTag:1];
                btn.layer.borderWidth = 1.f;
                btn.layer.cornerRadius = 5.f;
                btn.layer.borderColor = [UIColor whiteColor].CGColor;
            }
           
            break;
        case eFieldFollowers:
            if ([[cell contentView]viewWithTag:1]) {
                UIView *_vwBG = [[cell contentView]viewWithTag:1];
                _vwBG.layer.cornerRadius = 5.f;
                _vwBG.layer.borderWidth = 1.f;
                _vwBG.layer.borderColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.3].CGColor;
                
                if ([_vwBG viewWithTag:2]) {
                    UILabel *lblSettings = [_vwBG viewWithTag:2];
                    lblSettings.text = @"FOLLOWERS";
                }
                if ([_vwBG viewWithTag:3]) {
                    UILabel *lblSettings = [_vwBG viewWithTag:3];
                    lblSettings.text = [NSString stringWithFormat:@"%ld",(long)followCount];
                    
                }
            }
            if ([[cell contentView]viewWithTag:4]) {
                UIView *_vwBG = [[cell contentView]viewWithTag:4];
                _vwBG.layer.cornerRadius = 5.f;
                _vwBG.layer.borderWidth = 1.f;
                _vwBG.layer.borderColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.3].CGColor;
                
                if ([_vwBG viewWithTag:5]) {
                    UILabel *lblSettings = [_vwBG viewWithTag:5];
                    lblSettings.text = @"FOLLOWING";
                }
                if ([_vwBG viewWithTag:6]) {
                    UILabel *lblSettings = [_vwBG viewWithTag:6];
                    lblSettings.text = [NSString stringWithFormat:@"%ld",(long)followingCount];
                    
                }
            }
            break;

        default:
            break;
    }
    
}

#pragma mark - UITextfield delegate methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        if ([[[cell contentView]viewWithTag:1] isKindOfClass:[UITextField class]]){
            UITextField *txtField = [[cell contentView]viewWithTag:1];
        [self getTextFromField:txtField];
    }
}
    
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField.tag == eFieldDOB) {
        [self showBDayPicker];
        return NO;
    }
    textField.autocorrectionType = UITextAutocorrectionTypeYes;
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    NSIndexPath *indexPath;
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        indexForTextFieldNavigation = indexPath.row;
    }
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    [tableView setContentOffset:contentOffset animated:YES];
    
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    
}

#pragma mark - UITextView delegate methods


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    if (!_isEditing) return false;
    textView.autocorrectionType = UITextAutocorrectionTypeYes;
    [self createInputAccessoryView];
    [textView setInputAccessoryView:inputAccView];
    CGPoint pointInTable = [textView.superview.superview convertPoint:textView.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    
    CGPoint pointInTable = [textView.superview.superview convertPoint:textView.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    if ([textView.superview.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
    if ([textView.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textView convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        UITableViewCell *cell = (UITableViewCell*)textView.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        if ([[[cell contentView]viewWithTag:1] isKindOfClass:[UITextView class]]){
            UITextView *txtField = [[cell contentView]viewWithTag:1];
            [self getTextFromField:txtField];
        }
    }
    
    return YES;
}


-(void)getTextFromField:(id)obj{
    if ([obj isKindOfClass:[UITextView class]]) {
        UITextView *txtField = (UITextView*)obj;
        NSString *string = txtField.text;
        strStatusMsg = string;
        
    }else{
        NSString *string;
        UITextField *txtField = (UITextField*)obj;
        if (txtField.tag == eFieldDOB) strDOB = string;
        else strName = txtField.text;
       
    }
    
}

-(void)createInputAccessoryView{
    
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 45.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 1];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 90, 5.0f, 85.0f, 35.0f)];
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor getThemeColor]];
     [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.layer.cornerRadius = 5.f;
    btnDone.layer.borderWidth = 1.f;
    btnDone.layer.borderColor = [UIColor whiteColor].CGColor;
    btnDone.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    [inputAccView addSubview:btnDone];
}


-(void)doneTyping{
    [self.view endEditing:YES];
}

-(IBAction)changeFolloStatus:(UISwitch*)mySwitch{
    
    if ([mySwitch isOn]) {
        canFollow = true;
    } else {
        canFollow = false;
    }
}

-(IBAction)followUser:(id)sender{
    
    NSString *strCurrentFollow = btnFollow.currentTitle;
    if ([strCurrentFollow isEqualToString:@"Unfollow"]) {
        
        UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Unfollow" message:@"Do you really want to unfollow this user?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* block = [UIAlertAction actionWithTitle:@"Unfollow" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
            [alert dismissViewControllerAnimated:YES completion:nil];
            [self changeFollowStatus];
            
        }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:block];
        [alert addAction:cancel];
        [self.navigationController presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        [self changeFollowStatus];
    }
    
}

-(void)changeFollowStatus{
    
    [self showLoadingScreen];
    [APIMapper sendFollowRequestWithUserID:[User sharedManager].userId followerID:strUserID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        if ([[responseObject objectForKey:@"code"] integerValue] == 401) {
            UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Follow" message:[responseObject objectForKey:@"text"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* copy = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:copy];
            [self.navigationController presentViewController:alert animated:YES completion:nil];
            [self updateFollowButtonWithStatus:[[responseObject objectForKey:@"follow_status"] integerValue] shouldRefresh:YES];
            
        }else{
            
            [self updateFollowButtonWithStatus:[[responseObject objectForKey:@"follow_status"] integerValue] shouldRefresh:YES];
        }
        
        NSDictionary* userInfo = @{@"userID": strUserID,@"status":[NSNumber numberWithInteger:[[responseObject objectForKey:@"follow_status"] integerValue]]};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"FollowReqFromProfile" object:self userInfo:userInfo];
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self hideLoadingScreen];
    }];

}

-(void)updateFollowButtonWithStatus:(NSInteger)followStatus shouldRefresh:(BOOL)shouldRefresh{
    
    btnFollow.hidden = true;
    [btnFollow setEnabled:false];
    if (canFollow) {
            btnFollow.hidden = true;
            switch (followStatus) {
                case eFollowPending:
                    [btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
                    [btnFollow setBackgroundColor:[UIColor lightGrayColor]];
                    [btnFollow setTitleColor:[UIColor whiteColor ] forState:UIControlStateNormal];
                    btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
                    btnFollow.hidden = false;
                    [btnFollow setEnabled:false];
                    break;
                    
                case eFollow:
                    [btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
                    [btnFollow setBackgroundColor:[UIColor clearColor]];
                    [btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    btnFollow.layer.borderColor = [UIColor whiteColor].CGColor;
                    btnFollow.hidden = false;
                    [btnFollow setEnabled:true];
                    break;
                    
                case eFollowed:
                    btnFollow.hidden = false;
                    [btnFollow setEnabled:true];
                    [btnFollow setTitle:@"Unfollow" forState:UIControlStateNormal];
                    [btnFollow setBackgroundColor:[UIColor clearColor]];
                    [btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    break;
                    
                default:
                    break;
            }
    }
    
    if (shouldRefresh) [self refreshData];
    
}

-(IBAction)showCamera:(id)sender{
    
    UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Photo" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
   UIAlertAction* camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
       
       [self openCamera];
       
    }];
    [alert addAction:camera];
    UIAlertAction* gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        [self showGallery];
        
    }];
    
    [alert addAction:gallery];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                             {
                                 
                             }];
    
    [alert addAction:cancel];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
  
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = [Utility fixrotation:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    
    float width = image.size.width;
    float height = image.size.height;
    float ratio = width / height;
    height = (self.view.frame.size.width) / ratio;
    if (image) {
        UIImage *newImage = [self imageWithImage:image convertToSize:CGSizeMake(self.view.frame.size.width, height)];
        if (!newImage) imgProfilePic = image;
        else imgProfilePic = newImage;
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        
        
    }];
    
    [tableView reloadData];
}

-(void)showGallery{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}
-(void)openCamera{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

-(IBAction)showFollowersList:(UIButton*)sender{
    
    FollowersListingViewController *pushImgaeDetail =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForFollowList];
    pushImgaeDetail.userID = strUserID;
    pushImgaeDetail.type = sender.tag;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:pushImgaeDetail animated:YES];
    
    
}

-(IBAction)chatWithUser:(UIButton*)sender{
    
    ChatComposeViewController *chatCompose =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatComposer];
    chatCompose.chatUserInfo = userInfo;
    [[self navigationController]pushViewController:chatCompose animated:YES];
}

#pragma mark - Change Password Methods and Delegates


-(IBAction)showChangePwdPopUp:(id)sender{
    
    if (!changePwdPopUp) {
        
        changePwdPopUp = [ChangePasswordPopUp new];
        [self.view addSubview:changePwdPopUp];
        changePwdPopUp.delegate = self;
        changePwdPopUp.backgroundColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:0.7];;
        changePwdPopUp.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[changePwdPopUp]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(changePwdPopUp)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[changePwdPopUp]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(changePwdPopUp)]];
        
        changePwdPopUp.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            changePwdPopUp.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
        
    }
    
    [self.view endEditing:YES];
    [changePwdPopUp setUp];
}



-(void)closeForgotPwdPopUpAfterADelay:(float)delay{
    
    [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        changePwdPopUp.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [changePwdPopUp removeFromSuperview];
        changePwdPopUp = nil;
    }];
}

-(IBAction)editProfile:(id)sender{
   
    _isEditing = true;
    btnSubmit.hidden = false;
    [tableView reloadData];
}



-(IBAction)submitDetails:(UIButton*)sender{
    
    [self.view endEditing:YES];
    
    if (strName.length > 0 && strStatusMsg.length > 0) {
        [self showLoadingScreen];
        if (imgProfilePic) {
            [self uploadMediasToServerOnSuccess:^(id responseObject) {
                if ([responseObject objectForKey:@"media_name"]) {
                    [self resumeUserUpdateProfileWithUploadedImageName:[responseObject objectForKey:@"media_name"]];
                }else{
                    [self hideLoadingScreen];
                    if ([responseObject objectForKey:@"text"]) {
                        [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
                    }
                }
                
            } failure:^(NSError *error) {
                
                [self hideLoadingScreen];
                [ALToastView toastInView:self.view withText:error.localizedDescription];
                
            }];
        }else{
            
             [self resumeUserUpdateProfileWithUploadedImageName:nil];
        }
    }else{
        [ALToastView toastInView:self.view withText:@"Please fill the Fields"];
    }

   
    
}

-(void)resumeUserUpdateProfileWithUploadedImageName:(NSString*)imageName{
    
    [APIMapper updateUserProfileWithUserID:[User sharedManager].userId firstName:strName statusMsg:strStatusMsg uploadedImage:imageName canFollow:canFollow birthDay:strDOB success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideLoadingScreen];
        if ([[responseObject objectForKey:@"code"]integerValue] == kSuccessCode) {
            [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
            _isEditing = false;
            [tableView reloadData];
            [self updateUserInfoWithDetails:responseObject];
           // [self goBack:nil];
            btnSubmit.hidden = true;
        }else
            if ([[responseObject objectForKey:@"code"]integerValue] == kUnauthorizedCode){
                if (NULL_TO_NIL([responseObject objectForKey:@"text"])) {
                        [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
                        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                        [delegate clearUserSessions];
                        
                    }
                    
                }else{
                   [ALToastView toastInView:self.view withText:[responseObject objectForKey:@"text"]];
                }
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        [self hideLoadingScreen];
        
    }];
}

-(void)updateUserInfoWithDetails:(NSDictionary*)userDetails{
    
    if (NULL_TO_NIL([userDetails objectForKey:@"firstname"])) {
        [User sharedManager].name  = [userDetails objectForKey:@"firstname"];
    }
    
    if (NULL_TO_NIL([userDetails objectForKey:@"profile_url"])) {
        [User sharedManager].profileurl  = [userDetails objectForKey:@"profile_url"];
        profileURL = [userDetails objectForKey:@"profile_url"];
    }
    
    /*!............ Saving user to NSUserDefaults.............!*/
    
    [Utility saveUserObject:[User sharedManager] key:@"USER"];
    
}


-(IBAction)goBack:(id)sender{
   
  [[self navigationController] popViewControllerAnimated:YES];
    
}

-(IBAction)closeSlider:(id)sender{
    
}

-(NSString*)getDaysBetweenTwoDatesWith:(double)timeInSeconds{
    
    NSDate * today = [NSDate date];
    NSDate * refDate = [NSDate dateWithTimeIntervalSince1970:timeInSeconds];
    NSDate *fromDate;
    NSDate *toDate;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:refDate];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:today];
        NSString *msgDate;
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"d MMM yyyy"];
    msgDate = [dateformater stringFromDate:refDate];
    return msgDate;
    
}

-(IBAction)verifyUserProfile:(id)sender{
    
    UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Verify Porfile" message:@"Do you want your profile get verified?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* block = [UIAlertAction actionWithTitle:@"Verify" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self verifyProfile];
        
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:block];
    [alert addAction:cancel];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

-(void)verifyProfile{
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"VerifyUserPopUp"
                                                          owner:nil
                                                        options:nil];
    
    VerifyUserPopUp *vwPopUP = [arrayOfViews objectAtIndex:0];
    vwPopUP.delegate = self;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.4 delay:.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
}

-(IBAction)showChatUser:(id)sender{
    
    ChatUserListingViewController *chatUser =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForChatUserListings];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.navGeneral pushViewController:chatUser animated:YES];
    
}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

#pragma mark - Goal Category selction

-(IBAction)showGoalCategories{
    
    GoalCategoryViewController *goalCategory =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:HomeDetailsStoryBoard Identifier:StoryBoardIdentifierForGoalCategory];
    goalCategory.isMultiSelectionAllowed = YES;
    goalCategory.delegate = self;
    goalCategory.savedIDS = [userInfo objectForKey:@"interests"];
    [self.navigationController pushViewController:goalCategory animated:YES];
}


-(void)savedCategoryIDs:(NSArray*)strIDS{
    
    NSString *ids = [strIDS componentsJoinedByString:@","];
    [userInfo setObject:ids forKey:@"interests"];
    
}

#pragma mark - Date Picker

-(void)showBDayPicker{
    
    [self.view endEditing:YES];
    datePicker.maximumDate = [NSDate date];
    datePicker.date = [NSDate date];
    [UIView animateWithDuration:.4 animations:^{
        vwPickerOverLay.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
}

-(IBAction)hidePicker:(id)sender{
    
    [UIView animateWithDuration:.4 animations:^{
        vwPickerOverLay.alpha = 0.0;
    } completion:^(BOOL finished) {
        
    }];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc]init];
    [dateformater setDateFormat:@"yyyy-MM-dd"];
    strDOB = [dateformater stringFromDate:datePicker.date];
    [tableView reloadData];
  
    
}


#pragma mark - Photo Browser & Deleagtes

-(void)showUserPhotoGallery{
    
    NSArray *images = [NSArray arrayWithObject:[NSURL URLWithString:profileURL]];
    [self presentGalleryWithImages:images];
}


- (void)presentGalleryWithImages:(NSArray*)images
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}


-(void)uploadMediasToServerOnSuccess:(void (^)(id responseObject))success failure:(void (^)( NSError *error))failure{
    
    
    [APIMapper uploadMediasAtPath:nil orAsObject:imgProfilePic type:@"profile" Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        failure(error);
    }];
    
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}




@end
