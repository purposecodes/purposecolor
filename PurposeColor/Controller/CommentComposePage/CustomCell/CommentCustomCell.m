//
//  CommentCustomCell.m
//  PurposeColor
//
//  Created by Purpose Code on 11/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CommentCustomCell.h"

@implementation CommentCustomCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.imgProfilePic.layer.borderWidth = 1.f;
    self.imgProfilePic.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgProfilePic.layer.cornerRadius = 20.f;
    
    self.imgRepliedUser.layer.borderWidth = 1.f;
    self.imgRepliedUser.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgRepliedUser.layer.cornerRadius = 15.f;
    
    self.vwBg.layer.borderWidth = 1.f;
    self.vwBg.layer.borderColor = [UIColor clearColor].CGColor;
    self.vwBg.layer.cornerRadius = 10.f;
    
    _btnLike.dataset = @[
                    [[EMEmojiableOption alloc] initWithImage:@"Like_Blue" withName:@"Like" withColor:[UIColor getThemeColor]],
                       [[EMEmojiableOption alloc] initWithImage:@"Haha" withName:@"Haha" withColor:[UIColor colorWithRed:0.94 green:0.73 blue:0.25 alpha:1.0]],
                       [[EMEmojiableOption alloc] initWithImage:@"Wow" withName:@"Wow" withColor:[UIColor colorWithRed:0.94 green:0.73 blue:0.25 alpha:1.0]],
                       [[EMEmojiableOption alloc] initWithImage:@"Sad" withName:@"Sad" withColor:[UIColor colorWithRed:0.94 green:0.73 blue:0.25 alpha:1.0]],
                       [[EMEmojiableOption alloc] initWithImage:@"Angry" withName:@"Angry" withColor:[UIColor colorWithRed:0.90 green:0.46 blue:0.23 alpha:1.0] ],
                       ];
    
    _lblComment.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        // Open URLs
        [self attemptOpenURL:[NSURL URLWithString:string]];
    };
    _lblComment.hashtagLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        // Open URLs
    };
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)deleteCommentClicked:(UIButton*)btn{
    
    if ([self.delegate respondsToSelector:@selector(deleteCommentClicked:)]) {
        [self.delegate deleteCommentClicked:_index];
    }
    
}
-(IBAction)replyCommentClicked:(UIButton*)btn{
    
    if ([self.delegate respondsToSelector:@selector(replyToCommentClicked:)]) {
        [self.delegate replyToCommentClicked:_index];
    }
    
}

-(IBAction)showLikedUsersAtCommentIndex:(UIButton*)btn{
    
    if ([self.delegate respondsToSelector:@selector(likedUserListClicked:)]) {
        [self.delegate likedUserListClicked:_index];
    }
    
}
-(IBAction)showUserProfileIndex:(UIButton*)btn{
    
    if ([self.delegate respondsToSelector:@selector(profileBtnClicked:)]) {
        [self.delegate profileBtnClicked:_index];
    }
    
}

- (void)attemptOpenURL:(NSURL *)url
{
    
    
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (!safariCompatible) {
        
        NSString *urlString = url.absoluteString;
        urlString = [NSString stringWithFormat:@"http://%@",url.absoluteString];
        url = [NSURL URLWithString:urlString];
        
    }
    safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem"
                                                                       message:@"The selected link cannot be opened."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dismiss"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:cancel];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
    }
}


@end
