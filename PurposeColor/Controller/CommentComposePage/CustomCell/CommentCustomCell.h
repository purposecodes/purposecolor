//
//  CommentCustomCell.h
//  PurposeColor
//
//  Created by Purpose Code on 11/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//
@protocol CommentCellDelegate <NSObject>


@optional


/*!
 *This method is invoked when user Clicks "COMMENT" Button
 */
-(void)deleteCommentClicked:(NSInteger)index ;
-(void)replyToCommentClicked:(NSInteger)index ;
-(void)likedUserListClicked:(NSInteger)index ;
-(void)profileBtnClicked:(NSInteger)index ;

@end


#import <UIKit/UIKit.h>
#import "EMEmojiableBtn.h"
#import "KILabel.h"

@interface CommentCustomCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *imgProfilePic;
@property (nonatomic, weak) IBOutlet UIView *vwReply;
@property (nonatomic, weak) IBOutlet UIView *vwBg;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet KILabel *lblComment;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UIImageView *imgVerified;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (nonatomic, weak) IBOutlet UIButton *btnProfile;
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *trailingForName;
@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *heightForReply;

@property (nonatomic, weak) IBOutlet UIButton *btnRply;
@property (nonatomic, weak) IBOutlet EMEmojiableBtn *btnLike;
@property (nonatomic, weak) IBOutlet UIButton *btnLikeCount;

@property (nonatomic, weak) IBOutlet UIImageView *imgRepliedUser;
@property (nonatomic, weak) IBOutlet UILabel *lblRplyUserName;
@property (nonatomic, weak) IBOutlet KILabel *lblReply;
@property (nonatomic, weak) IBOutlet UILabel *lblRplyCount;


@property (nonatomic,weak)  id<CommentCellDelegate>delegate;

@end
