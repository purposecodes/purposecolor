//
//  CommentComposeViewController.m
//  PurposeColor
//
//  Created by Purpose Code on 11/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kHeightForHeader        65
#define kMinimumCellCount       1
#define kSuccessCode            200
#define kDefaultCellHeight      150
#define kHeightForFooter        0.001
#define kSuccessCode            200

#import "CommentComposeViewController.h"
#include "CommentCustomCell.h"
#import  "Constants.h"
#import "HPGrowingTextView.h"
#import "ReplyComposeViewController.h"
#import "EMEmojiableBtn.h"
#import "LikedAndCommentedUserListings.h"
#import "ProfilePageViewController.h"
#import "ReportAbuseViewController.h"

@interface CommentComposeViewController ()<HPGrowingTextViewDelegate,CommentCellDelegate,ReplyActionDelegate,EMEmojiableBtnDelegate,UIGestureRecognizerDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UIButton *btnCancelEdit;
    NSMutableArray *arrComments;
    UIView *containerView;
    HPGrowingTextView *textView;
    BOOL isDataAvailable;
    UIButton *btnDone;
    NSString *strNoDataText;
    NSInteger totalPages;
    NSInteger currentPage;
    BOOL isUnderPagination;
    NSString *commentID;
    NSInteger indexOfComment;
    
    IBOutlet NSLayoutConstraint *bottomForContainer;
    IBOutlet NSLayoutConstraint *heightForContainer;
}

@end

@implementation CommentComposeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self getAllCommentsWithPageNum:currentPage];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)setUp{
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
    [tapGesture setNumberOfTapsRequired:1];
    tapGesture.delegate = self;
    [tableView addGestureRecognizer:tapGesture];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    [tableView addGestureRecognizer:lpgr];
    tapGesture.delegate = self;
    
    isUnderPagination = false;
    currentPage = 1;
    totalPages = 0;
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 100;
    arrComments = [NSMutableArray new];
    self.automaticallyAdjustsScrollViewInsets = NO;
    tableView.hidden = true;
    
    btnCancelEdit.hidden = true;
    btnCancelEdit.layer.cornerRadius = 5.f;
    btnCancelEdit.layer.borderColor = [UIColor getThemeColor].CGColor;
    btnCancelEdit.layer.borderWidth = 1.f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification 
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                            name:UIKeyboardWillChangeFrameNotification object:nil];

    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setUpGrowingTextView];
}

-(void)getAllCommentsWithPageNum:(NSInteger)pageNum {
    
    if (_dictGemDetails) {
        NSString *gemID;
        NSString *gemType;
        NSInteger gemShare = 1;
        if (!_isFromCommunityGem)
            gemShare = 0;
        if (NULL_TO_NIL([_dictGemDetails objectForKey:@"gem_id"]))
            gemID = [_dictGemDetails objectForKey:@"gem_id"];
        if (NULL_TO_NIL([_dictGemDetails objectForKey:@"gem_type"]))
            gemType = [_dictGemDetails objectForKey:@"gem_type"];
        if (gemType&&gemID) {
            isUnderPagination = true;
            [self showLoadingScreenWithTitle:@"Loading.."];
            [APIMapper getAllCommentsForAGemWithGemID:gemID gemType:gemType shareComment:gemShare pageNumber:pageNum success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                isUnderPagination = false;
                [self parseResponds:responseObject];
                [self hideLoadingScreen];
                
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
                 isUnderPagination = false;
                [self hideLoadingScreen];
                if (error && error.localizedDescription){
                    [ALToastView toastInView:self.view withText:NETWORK_ERROR_MESSAGE];
                    strNoDataText = error.localizedDescription;
                    
                }
                tableView.hidden = false;
                [tableView reloadData];
                
            }];
        }

    }
    
}

-(void)parseResponds:(NSDictionary*)responds{
    
    if ([[responds objectForKey:@"code"] integerValue] == kSuccessCode) {
        
        if (NULL_TO_NIL([responds objectForKey:@"resultarray"])) {
            [arrComments addObjectsFromArray:[NSMutableArray arrayWithArray:[responds objectForKey:@"resultarray"]]];
        }
        if (NULL_TO_NIL([responds objectForKey:@"currentPage"])) {
            currentPage = [[responds objectForKey:@"currentPage"] integerValue];
        }
        if (NULL_TO_NIL([responds objectForKey:@"pageCount"])) {
            totalPages = [[responds objectForKey:@"pageCount"] integerValue];
        }
        
    }
    
    else if ([[responds objectForKey:@"code"]integerValue] == kUnauthorizedCode){
        
        if (NULL_TO_NIL([responds objectForKey:@"text"])) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Comment"
                                                                message:[responds objectForKey:@"text"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate clearUserSessions];
            
        }
        
    }else{
        
        if ([responds objectForKey:@"text"]) {
            strNoDataText = [responds objectForKey:@"text"];
        }
        
    }
    
    if (arrComments.count) isDataAvailable = true;
    tableView.hidden = false;
    [tableView reloadData];
}

#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
     if (!isDataAvailable) return kMinimumCellCount;
    return arrComments.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        static NSString *CellIdentifier = @"NoComments";
        UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        return cell;
    }
    static NSString *CellIdentifier = @"CommentCustomCell";
    CommentCustomCell *cell = (CommentCustomCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell.imgProfilePic setImage:[UIImage imageNamed:@"NoImage_Goals_Dreams"]];
    cell.index = indexPath.row;
    cell.vwBg.tag = indexPath.row;
    cell.btnRply.tag = indexPath.row;
    cell.btnLike.tag = indexPath.row;
    cell.btnLike.delegate = self;
    cell.btnProfile.tag = indexPath.row;
    cell.btnLike.vwBtnSuperView = self.view;
    [cell.btnLike privateInit];
    
    cell.delegate = self;
    [self setUpCommentWith:cell index:indexPath.row];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;{
    
   [self.view endEditing:YES];    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    // remove bottom extra 20px space.
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    // remove bottom extra 20px space.
    return CGFLOAT_MIN;
}


-(void)setUpCommentWith:(CommentCustomCell*)cell index:(NSInteger)row{
    
    if (row < arrComments.count) {
        NSDictionary *comment = arrComments[row];
        if (NULL_TO_NIL([comment objectForKey:@"comment_datetime"])) {
            double serverTime = [[comment objectForKey:@"comment_datetime"] doubleValue];
            cell.lblDate.text = [Utility getCommentedTimeWith:serverTime];
        }
        
        if (NULL_TO_NIL([comment objectForKey:@"firstname"])) cell.lblName.text = [comment objectForKey:@"firstname"];
        if (NULL_TO_NIL([comment objectForKey:@"comment_txt"])){
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineHeightMultiple = 1.2f;
            NSDictionary *attributes = @{NSParagraphStyleAttributeName:paragraphStyle,
                                         };
            //NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[comment objectForKey:@"comment_txt"] attributes:attributes];
            //cell.lblComment.attributedText = attributedText;
            cell.lblComment.text = [comment objectForKey:@"comment_txt"];
            
        }
        cell.heightForReply.constant = 0;
        if (NULL_TO_NIL([comment objectForKey:@"reply"])) {
            NSArray *replies = [comment objectForKey:@"reply"];
            if (replies.count) {
                cell.heightForReply.constant = 65;
                NSDictionary *reply = replies[0];
                cell.lblRplyCount.text = [NSString stringWithFormat:@"View all replies"];
                cell.lblRplyUserName.text = [reply objectForKey:@"firstname"];
                cell.lblReply.text = [reply objectForKey:@"reply_txt"];
                [cell.imgRepliedUser sd_setImageWithURL:[NSURL URLWithString:[reply objectForKey:@"profileurl"]]
                                      placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             }];
            }
        }
        if (NULL_TO_NIL([comment objectForKey:@"profileurl"])) {
            NSString *videoThumb = [comment objectForKey:@"profileurl"];
            if (videoThumb.length) {
                [cell.imgProfilePic sd_setImageWithURL:[NSURL URLWithString:videoThumb]
                                    placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           }];
            }
        }
        
        NSInteger likeStatus = [[comment objectForKey:@"like_status"] integerValue];
        if (likeStatus < 0) {
            [cell.btnLike setTitleColor:[UIColor colorWithRed:0.56 green:0.58 blue:0.62 alpha:1.0] forState:UIControlStateNormal];
            [cell.btnLike setTitle:@"Like" forState:UIControlStateNormal];
        }else{
            EMEmojiableOption *option = [cell.btnLike.dataset objectAtIndex:likeStatus];
            [cell.btnLike setTitle:option.name forState:UIControlStateNormal];
            [cell.btnLike setTitleColor:option.color forState:UIControlStateNormal];
        }
        cell.btnLikeCount.hidden = true;
        if ([[comment objectForKey:@"total_like"] integerValue] > 0) {
             cell.btnLikeCount.hidden = false;
             [cell.btnLikeCount setTitle:[NSString stringWithFormat:@" %d",[[comment objectForKey:@"total_like"] integerValue]] forState:UIControlStateNormal];
        }
        
        cell.btnDelete.hidden = true;
        if ([[comment objectForKey:@"user_id"] integerValue] == [[User sharedManager].userId integerValue]) {
            cell.btnDelete.hidden = false;
        }
        
        cell.imgVerified.hidden = true;
        if ([[comment objectForKey:@"verify_status"] integerValue] == 1){
            cell.imgVerified.hidden = false;
        }
        
        cell.lblComment.allOtherClicks = ^(KILabel *label, NSString *string, NSRange range) {
            [self.view endEditing:YES];
        };
        
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (isUnderPagination) return;
    
    /**! Pagination call !**/
    
    NSInteger endScrolling = scrollView.contentOffset.y+ scrollView.frame.size.height;
    NSInteger height = scrollView.contentSize.height;
    if (endScrolling >= height){
        NSInteger nextPage = currentPage ;
        nextPage += 1;
        if (nextPage  <= totalPages) {
            [self getAllCommentsWithPageNum:nextPage];
        }
    }
    
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:p];
    
    if (indexPath == nil){
        
    }
    else {
        CommentCustomCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        CGPoint pointInCell = [cell convertPoint:p fromView:tableView];
        if (!CGRectContainsPoint(cell.vwReply.frame, pointInCell)) {
            if (indexPath.row < arrComments.count) {
                NSDictionary *gemDetails = arrComments[indexPath.row];
                BOOL isOthers = [[gemDetails objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId] ? false : true;
                UIAlertController * alert=  [UIAlertController alertControllerWithTitle:@"Comment" message:@"Choose an action" preferredStyle:UIAlertControllerStyleActionSheet];
                UIAlertAction* hide;
                UIAlertAction* edit;
                UIAlertAction* copy;
                UIAlertAction* delete;
                if (isOthers) {
                    hide = [UIAlertAction actionWithTitle:@"Report Abuse" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                            [alert dismissViewControllerAnimated:YES completion:nil];
                             [self reportAbuseWithIndex:indexPath.row];
                            
                        }];
                    [alert addAction:hide];
                       
                }else{
                    
                   
                    
                    edit = [UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                        [alert dismissViewControllerAnimated:YES completion:nil];
                        [self editComment:indexPath.row];
                        
                    }];
                    [alert addAction:edit];
                    
                    delete = [UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
                        [alert dismissViewControllerAnimated:YES completion:nil];
                        [self deleteCommentClicked:indexPath.row];
                        
                    }];
                    [alert addAction:delete];
                }
                copy = [UIAlertAction actionWithTitle:@"Copy" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    pasteboard.string = [gemDetails objectForKey:@"comment_txt"];
                    [ALToastView toastInView:self.view withText:@"Comment has been copied to clipboard"];
                    
                }];
                
                UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                         {
                                         }];
                
                [alert addAction:copy];
                
                [alert addAction:cancel];
                AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
                
            }
        }
    }
    
    
    
    
   
}


#pragma mark - Growing Text View


- (void)setUpGrowingTextView {
    
    containerView = [[UIView alloc] init];
    [self.view addSubview:containerView];
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
     [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[containerView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(containerView)]];
    heightForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                      attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil
                                                      attribute:NSLayoutAttributeHeight
                                                     multiplier:1.0
                                                       constant:50];
    [containerView addConstraint:heightForContainer];
    
    if (@available(iOS 11, *)){
        
        bottomForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0];
        
    }else{
        
        bottomForContainer = [NSLayoutConstraint constraintWithItem:containerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0];
        
    }
   
    [self.view addConstraint:bottomForContainer];
    containerView.backgroundColor = [UIColor whiteColor];
    UIView *vwBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , 0.5)];
    vwBorder.backgroundColor = [UIColor colorWithRed:0.92 green:0.92 blue:0.93 alpha:1.0];
    [containerView addSubview:vwBorder];
    
    textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(5, 10, self.view.frame.size.width - 50, 40)];
    textView.isScrollable = NO;
    textView.contentInset = UIEdgeInsetsMake(5, 5, 0, 5);
    textView.layer.borderWidth = 1.f;
    textView.layer.borderColor = [UIColor clearColor].CGColor;
    textView.minNumberOfLines = 1;
    textView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
    textView.returnKeyType = UIReturnKeyGo; //just as an example
    textView.font = [UIFont fontWithName:CommonFont_New size:15];
    textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
    textView.placeholder = @"Write a comment..";
    textView.internalTextView.autocorrectionType = UITextAutocorrectionTypeYes;
    textView.textColor = [UIColor getTitleBlackColor];
    //textView.keyboardType=UIKeyboardTypeASCIICapable;

    // textView.text = @"test\n\ntest";
    // textView.animateHeightChange = NO; //turns off animation
    
    
   // textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:textView];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    doneBtn.frame = CGRectMake(self.view.frame.size.width - 45, 5, 40, 40);
    [doneBtn setImage:[UIImage imageNamed:@"Comment_Send"] forState:UIControlStateNormal];
   // [doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn.titleLabel.font = [UIFont fontWithName:CommonFontBold size:16];
    [doneBtn addTarget:self action:@selector(postComment) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:doneBtn];
    btnDone = doneBtn;
    [btnDone setEnabled:FALSE];
}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    bottomForContainer.constant =  - (keyboardBounds.size.height - bottomPadding) ;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    
    // commit animations
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    bottomForContainer.constant = 0;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    // commit animations
    [UIView commitAnimations];
}

-(void) keyboardWillChangeFrame:(NSNotification *)note{
    
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    bottomForContainer.constant =  - (keyboardBounds.size.height - bottomPadding) ;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    
    // commit animations
    [UIView commitAnimations];
}


- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    heightForContainer.constant = r.size.height;
}

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView{
    
     NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (trimmedString.length > 0) [btnDone setEnabled:TRUE];
    else [btnDone setEnabled:FALSE];
    
}

- (void)growingTextViewDidEndEditing:(HPGrowingTextView *)growingTextView{
    
}

#pragma mark - Like  Comment


- (void)EMEmojiableBtn:( EMEmojiableBtn* _Nonnull)button selectedOption:(NSUInteger)emojiCode{
    
    if (button.tag < arrComments.count) {
        
        EMEmojiableOption *option = [button.dataset objectAtIndex:emojiCode];
        [button setTitle:option.name forState:UIControlStateNormal];
        [button setTitleColor:option.color forState:UIControlStateNormal];
        NSMutableDictionary *comment = [NSMutableDictionary dictionaryWithDictionary:arrComments[button.tag]];
        NSInteger count = [[comment objectForKey:@"total_like"] integerValue];
        NSInteger likeStatus = [[comment objectForKey:@"like_status"] integerValue];
        [comment setObject:[NSNumber numberWithInteger:emojiCode] forKey:@"like_status"];
        if (likeStatus >= 0) {
            
        }else{
          count += 1;
        }
        if (count < 0) {
            count = 0;
        }
        [comment setObject:[NSNumber numberWithInteger:count] forKey:@"total_like"];
        [arrComments replaceObjectAtIndex:button.tag withObject:comment];
        [tableView reloadData];
        [self likeACommentWithIndex:button.tag andEmoji:emojiCode];
    }
    
   
    
}
- (void)EMEmojiableBtnCanceledAction:(EMEmojiableBtn* _Nonnull)button{
    
}
- (void)EMEmojiableBtnSingleTap:(EMEmojiableBtn* _Nonnull)button{
    
    if (button.tag < arrComments.count) {
        NSMutableDictionary *comment = [NSMutableDictionary dictionaryWithDictionary:arrComments[button.tag]];
        NSInteger status = 0;
        NSInteger count = [[comment objectForKey:@"total_like"] integerValue];
        if ([[comment objectForKey:@"like_status"] integerValue] >= 0) {
            status = -1;
            count -= 1;
        }else{
             count += 1;
        }
        if (count < 0) {
            count = 0;
        }
        [comment setObject:[NSNumber numberWithInteger:count] forKey:@"total_like"];
        [comment setObject:[NSNumber numberWithInteger:status] forKey:@"like_status"];
        [arrComments replaceObjectAtIndex:button.tag withObject:comment];
        [tableView reloadData];
        [self likeACommentWithIndex:button.tag andEmoji:status];
        
    }
    
    
}


-(void)likeACommentWithIndex:(NSInteger)index andEmoji:(NSInteger)emoji{
    
    if (index < arrComments.count) {
        NSDictionary *comment = arrComments[index];
        [APIMapper likeReplyOrCommentWithType:@"comment" gemID:[comment objectForKey:@"gem_id"] selectedID:[comment objectForKey:@"comment_id"] emojiCode:emoji gemType: [_dictGemDetails objectForKey:@"gem_type"] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
        }];
    }
}


#pragma mark - Reply Compose

-(void)replyToCommentClicked:(NSInteger)index{
    
    if (index < arrComments.count) {
        NSDictionary *comment = arrComments[index];
        ReplyComposeViewController *replyPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForReplyComment];
        replyPage.delegate = self;
        replyPage.index = index;
        replyPage.dictCommentInfo = comment;
        if (NULL_TO_NIL([_dictGemDetails objectForKey:@"gem_type"]))
            replyPage.strGemType = [_dictGemDetails objectForKey:@"gem_type"];
        [self.navigationController pushViewController:replyPage animated:YES];
        
    }
}

-(void)updateCommentPageWithIndex:(NSInteger)row reply:(NSDictionary*)reply replyCount:(NSInteger)count isEdited:(BOOL)isEdited{
    
    if (row < arrComments.count) {
        NSMutableDictionary *comment = [NSMutableDictionary dictionaryWithDictionary:arrComments[row]];
        if (reply) {
            if ([comment objectForKey:@"reply"]) {
                NSMutableArray *replies = [NSMutableArray arrayWithArray:[comment objectForKey:@"reply"]];
                if (isEdited) {
                    [replies replaceObjectAtIndex:0 withObject:reply];
                }else{
                   [replies insertObject:reply atIndex:0];
                }
                
                [comment setObject:replies forKey:@"reply"];
            }else{
                NSMutableArray *replies = [NSMutableArray arrayWithObject:reply];
                [comment setObject:replies forKey:@"reply"];
            }
            [comment setObject:[NSNumber numberWithInteger:count] forKey:@"reply_count"];
          
        }else{
            if ([comment objectForKey:@"reply"]) [comment removeObjectForKey:@"reply"];
        }
        [arrComments replaceObjectAtIndex:row withObject:comment];
        
        [tableView reloadData];
        if (row < arrComments.count) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            [tableView scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionTop
                                     animated:NO];
        }
    }
}

-(void)profileBtnClicked:(NSInteger)index{
    
    if (index < arrComments.count) {
        NSMutableDictionary *details = arrComments[index];
        if (NULL_TO_NIL([details objectForKey:@"user_id"])) {
            ProfilePageViewController *profilePage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForProfilePage];
            
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            if (!app.navGeneral) {
                app.navGeneral = [[UINavigationController alloc] initWithRootViewController:profilePage];
                app.navGeneral.navigationBarHidden = true;
                [UIView transitionWithView:app.window
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{  app.window.rootViewController = app.navGeneral; }
                                completion:nil];
            }else{
                [app.navGeneral pushViewController:profilePage animated:YES];
            }
            profilePage.canEdit = false;
            if ([[details objectForKey:@"user_id"] isEqualToString:[User sharedManager].userId]) {
                profilePage.canEdit = true;
            }
            [profilePage loadUserProfileWithUserID:[details objectForKey:@"user_id"]showBackButton:YES];
            
        }
        
    }
   
}

#pragma mark - Post Comment


-(void)postComment
{
    [textView resignFirstResponder];
    [self.view endEditing:YES];
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (trimmedString.length > 0) {
        
        if (_dictGemDetails) {
            NSString *gemID;
            NSString *gemType;
            NSInteger gemShare = 1;
            if (!_isFromCommunityGem)
                gemShare = 0;
            if (NULL_TO_NIL([_dictGemDetails objectForKey:@"gem_id"]))
                gemID = [_dictGemDetails objectForKey:@"gem_id"];
            if (NULL_TO_NIL([_dictGemDetails objectForKey:@"gem_type"]))
                gemType = [_dictGemDetails objectForKey:@"gem_type"];
            if (gemID && gemType) {
                 [self showLoadingScreenWithTitle:@"Posting.."];
                NSString *comment = textView.internalTextView.text;
                [APIMapper postCommentWithUserID:[User sharedManager].userId gemID:gemID gemType:gemType comment:comment shareComment:gemShare commentID:commentID success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if ([[responseObject objectForKey:@"code"] integerValue] == kSuccessCode) {
                        if (NULL_TO_NIL([responseObject objectForKey:@"comment_count"])) {
                            NSInteger count = [[responseObject objectForKey:@"comment_count"] integerValue];
                            if (!commentID) {
                                [self updateCommentCountWithGemID:gemID commentCount:count isAddComment:YES];
                                if ([responseObject objectForKey:@"comment"]) {
                                    [arrComments addObject:[responseObject objectForKey:@"comment"]];
                                    tableView.hidden = false;
                                }
                                [tableView reloadData];
                                [self tableScrollToBottom];
                            }else{
                                if (indexOfComment < arrComments.count) [arrComments replaceObjectAtIndex:indexOfComment withObject:[responseObject objectForKey:@"comment"]];
                            }
                            if (arrComments.count > 0)isDataAvailable = true;
                            [tableView reloadData];
                           
                        }
                        btnCancelEdit.hidden = true;
                    }
                    else if ([[responseObject objectForKey:@"code"]integerValue] == kUnauthorizedCode){
                        
                        if (NULL_TO_NIL([responseObject objectForKey:@"text"])) {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Comment"
                                                                                message:[responseObject objectForKey:@"text"]
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                            [alertView show];
                            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                            [delegate clearUserSessions];
                            
                        }
                        
                    }
                    [self hideLoadingScreen];
                    textView.text = @"";
                } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                    
                    [self showMessage:@"Failed to add comment!"];
                    [self hideLoadingScreen];
                    
                }];
                
            }
        }
    }
}

-(void)tableScrollToBottom{
    
    if (arrComments.count - 1 > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:arrComments.count - 1 inSection:0];
        [tableView scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionBottom
                                 animated:YES];
    }
    
    
    
  
   
    
}

-(void)deleteCommentClicked:(NSInteger)index {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete"
                                  message:@"Delete the selected Comment?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"DELETE"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             if (index < arrComments.count) {
                                 
                                 NSDictionary *comment = arrComments[index];
                                 if (NULL_TO_NIL([comment objectForKey:@"comment_id"])) {
                                     NSString *commentID = [comment objectForKey:@"comment_id"];
                                     if (commentID) {
                                         [self showLoadingScreenWithTitle:@"Deleting.."];
                                         [APIMapper removeCommentWithCommentID:commentID success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             if ([[responseObject objectForKey:@"code"] integerValue] == kSuccessCode)
                                                 [self updateCommentCountWithGemID:nil commentCount:0 isAddComment:NO];
                                             
                                             else if ([[responseObject objectForKey:@"code"]integerValue] == kUnauthorizedCode){
                                                 
                                                 if (NULL_TO_NIL([responseObject objectForKey:@"text"])) {
                                                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Comment"
                                                                                                         message:[responseObject objectForKey:@"text"]
                                                                                                        delegate:nil
                                                                                               cancelButtonTitle:@"OK"
                                                                                               otherButtonTitles:nil];
                                                     [alertView show];
                                                     AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                                     [delegate clearUserSessions];
                                                     
                                                 }
                                             }
                                             
                                                 
                                             isDataAvailable = true;
                                             if (index < arrComments.count) {
                                                 [arrComments removeObjectAtIndex:index];
                                                 if (arrComments.count <= 0) isDataAvailable = false;
                                                // [tableView reloadData];
                                                 NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [tableView numberOfSections])];
                                                 [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
                                             }
                                             [self hideLoadingScreen];
                                             
                                             
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                             
                                             [self showMessage:@"Failed to delete comment!"];
                                             [self hideLoadingScreen];
                                         }];
                                     }
                                 }
                             }
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


-(void)editComment:(NSInteger)index {
    
    btnCancelEdit.hidden = false;
    if (index < arrComments.count) {
        
        NSDictionary *comment = arrComments[index];
        if (NULL_TO_NIL([comment objectForKey:@"comment_id"])) {
            NSString *_commentID = [comment objectForKey:@"comment_id"];
            if (_commentID) {
                commentID = _commentID;
                textView.text = [comment objectForKey:@"comment_txt"];
                indexOfComment = index;
                [textView becomeFirstResponder];
            }
        }
    }
    
}

-(IBAction)cancelEdit:(id)sender{
    
    btnCancelEdit.hidden = true;
    commentID = nil;
    textView.text = @"";
    [self.view endEditing:YES];
    
}



-(void)showMessage:(NSString*)message{
    
    [[[UIAlertView alloc] initWithTitle:@"Comment" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)reportAbuseWithIndex:(NSInteger)index{
    
    if (index < arrComments.count) {
        
        NSDictionary *gemDetails = arrComments[index];
        ReportAbuseViewController *reportAbuseVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:GEMDetailsStoryBoard Identifier:StoryBoardIdentifierForReportAbuse];
        reportAbuseVC.gemDetails = gemDetails;
        [self.navigationController pushViewController:reportAbuseVC animated:YES];
    }
    
}

-(void)closeKeyboard{
    
    [self.view endEditing:YES];
}

-(IBAction)closePopUp{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.navigationController popViewControllerAnimated:YES];
    
 
}

-(void)updateCommentCountWithGemID:(NSString*)gemID commentCount:(NSInteger)count isAddComment:(BOOL)isAddComment{
    
    if ([self.delegate respondsToSelector:@selector(commentPostedSuccessfullyWithGemID:commentCount:index:isAddComment:)]) {
        [self.delegate commentPostedSuccessfullyWithGemID:gemID commentCount:count index:_selectedIndex isAddComment:isAddComment];
    }
}


-(void)likedUserListClicked:(NSInteger)index{
    if (index < arrComments.count) {
        NSDictionary *comment = arrComments[index];
        NSInteger count = [[comment objectForKey:@"total_like"] integerValue];
        if (count <= 0) {
            return;
        }
        
        LikedAndCommentedUserListings *userListings =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ChatDetailsStoryBoard Identifier:StoryBoardIdentifierForLikedAndCommentedUsers];
        [userListings loadUserListingsForType:@"comment" selectedID:[comment objectForKey:@"comment_id"]gemType:nil];
        [self.navigationController pushViewController:userListings animated:YES];

    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    if ([[touch view] isKindOfClass:[KILabel class]]) {
        return NO;
    }
    
    return YES;
}


-(void)dealloc{
    
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
