//
//  NoDataTableViewCell.m
//  PurposeColor
//
//  Created by Purpose Code on 30/07/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import "NoDataTableViewCell.h"

@implementation NoDataTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _vwBg.layer.borderWidth = 1.f;
    _vwBg.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    _vwBg.layer.cornerRadius = 5.f;
    _vwBg.clipsToBounds = YES;
    
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
