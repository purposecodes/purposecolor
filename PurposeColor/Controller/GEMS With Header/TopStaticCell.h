//
//  ArcCollectionViewCell.h
//  PurposeColor
//
//  Created by Purpose Code on 19/04/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KILabel.h"

@interface TopStaticCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIView *vwHeader;

@property (nonatomic,weak) IBOutlet UIView *vwJournal;
@property (nonatomic,weak) IBOutlet UIView *vwGoals;
@property (nonatomic,weak) IBOutlet UIView *vwInspired;
@property (nonatomic,weak) IBOutlet UIView *vwBorder;

@property (nonatomic,weak) IBOutlet UILabel *lblPvtOne;
@property (nonatomic,weak) IBOutlet UILabel *lblPvtTwo;
@property (nonatomic,weak) IBOutlet UILabel *lblPvtThree;

@property (nonatomic,weak) IBOutlet UIView *selfHelpNetwork;
@property (nonatomic,weak) IBOutlet UIImageView *imgUser;

@property (nonatomic,weak) IBOutlet UIView *bgForBday;
@property (nonatomic,weak) IBOutlet UIView *bgForBdayCount;
@property (nonatomic,weak) IBOutlet UILabel *lblBdayCount;

@property (nonatomic,weak) IBOutlet KILabel *lblUserInfo;
@property (nonatomic,weak) IBOutlet UIView *vwInfoBg;

-(void)animateClick;

@end
