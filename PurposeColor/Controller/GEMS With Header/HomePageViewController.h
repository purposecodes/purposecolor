//
//  GEMSListingsViewController.h
//  PurposeColor
//
//  Created by Purpose Code on 28/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePageViewController : UIViewController

-(void)refreshData:(BOOL)shouldDisableCach;
-(void)getBdayCount;
-(void)showSurveyPopUpWithInfo:(NSDictionary*)responds;
@end
