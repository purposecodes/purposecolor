//
//  ArcCollectionViewCell.m
//  PurposeColor
//
//  Created by Purpose Code on 19/04/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import "TopStaticCell.h"

@interface TopStaticCell(){
    
    IBOutlet UIView *vwPostToCommunity;
}

@end

@implementation TopStaticCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    _vwJournal.layer.borderWidth = 1.f;
    _vwJournal.layer.cornerRadius = 25.f;
    _vwJournal.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    _vwGoals.layer.borderWidth = 1.f;
    _vwGoals.layer.cornerRadius = 25.f;
    _vwGoals.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    _vwInspired.layer.borderWidth = 1.f;
    _vwInspired.layer.cornerRadius = 25.f;
    _vwInspired.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    _imgUser.layer.borderWidth = 1.f;
    _imgUser.layer.cornerRadius = 20.f;
    _imgUser.layer.borderColor = [UIColor clearColor].CGColor;
    
    [_imgUser sd_setImageWithURL:[NSURL URLWithString:[User sharedManager].profileurl]
                       placeholderImage:[UIImage imageNamed:@"UserProfilePic"]
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              }];
    
    
    _selfHelpNetwork.layer.borderWidth = 1.f;
    _selfHelpNetwork.layer.cornerRadius = 20.f;
    _selfHelpNetwork.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    _vwInfoBg.layer.borderWidth = 1.f;
    _vwInfoBg.layer.cornerRadius = 5.f;
    _vwInfoBg.layer.borderColor = [UIColor colorWithRed:0.69 green:0.91 blue:0.99 alpha:1.0].CGColor;
    
    
    
    [_vwHeader.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwHeader.layer setBorderWidth:1.f];
    // drop shadow
    [_vwHeader.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwHeader.layer setShadowOpacity:0.3];
    [_vwHeader.layer setShadowRadius:2.0];
    [_vwHeader.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder.strokeColor = [UIColor colorWithRed:0.15 green:0.23 blue:0.48 alpha:1.0].CGColor;
    yourViewBorder.fillColor = nil;
    yourViewBorder.lineDashPattern = @[@2, @2];
    yourViewBorder.frame = _vwBorder.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:_vwBorder.bounds].CGPath;
    [_vwBorder.layer addSublayer:yourViewBorder];
    
    NSMutableAttributedString *myString = [NSMutableAttributedString new];
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    UIImage *icon = [UIImage imageNamed:@"Lock"];
    attachment.image = icon;
    attachment.bounds = CGRectMake(0, (-(icon.size.height / 2) -  _lblPvtOne.font.descender + 2), icon.size.width, icon.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    [myString appendAttributedString:attachmentString];
    NSAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" Only Me"]];
    [myString appendAttributedString:myText];
    _lblPvtOne.attributedText = myString;
    _lblPvtTwo.attributedText = myString;
    _lblPvtThree.attributedText = myString;
    
    _bgForBdayCount.layer.borderWidth = 1.f;
    _bgForBdayCount.layer.borderColor = [UIColor clearColor].CGColor;
    _bgForBdayCount.layer.cornerRadius = 10.5;
    
    _lblUserInfo.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        // Open URLs
        [self attemptOpenURL:[NSURL URLWithString:string]];
    };
    
    
}

- (void)attemptOpenURL:(NSURL *)url
{
    
    
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (!safariCompatible) {
        NSString *urlString = url.absoluteString;
        urlString = [NSString stringWithFormat:@"http://%@",url.absoluteString];
        url = [NSURL URLWithString:urlString];
    }
    safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:url options:@{}
                                     completionHandler:^(BOOL success) {
                                         
                                     }];
        }
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem"
                                                                       message:@"The selected link cannot be opened."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dismiss"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:cancel];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.window.rootViewController presentViewController:alert animated:YES completion:nil];
    }
}


-(void)animateClick{
    
    return;
    
    CATransition *animation=[CATransition animation];
    [animation setDelegate:nil];
    [animation setDuration:.3];
    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    [animation setType:@"rippleEffect"];
    
    [animation setFillMode:kCAFillModeRemoved];
    animation.endProgress=1;
    [animation setRemovedOnCompletion:YES];
    [vwPostToCommunity.layer addAnimation:animation forKey:nil];

}



@end
