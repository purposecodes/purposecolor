//
//  NoDataTableViewCell.h
//  PurposeColor
//
//  Created by Purpose Code on 30/07/18.
//  Copyright © 2018 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoDataTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIView *vwBg;


@end
