//
//  EMEmojiableOption.h
//  Pods
//
//  Created by Erekle on 4/2/16.
//
//

#import <Foundation/Foundation.h>

@interface EMEmojiableOption : NSObject
@property (strong,nonatomic) NSString *imageName;
@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) UIColor *color;

-(instancetype)initWithImage:(NSString*)imageName withName:(NSString*)name withColor:(UIColor*)color;
@end
